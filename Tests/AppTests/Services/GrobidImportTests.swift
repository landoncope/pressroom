//
//  GrobidImportTests.swift
//  AppTests
//
//  Created by Dan Browne on 31/05/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import MPFoundation
import XCTest

// swiftlint:disable private_unit_test function_body_length

final class GrobidImportTests: XCTestCase {

    // MARK: Private variables

    private static var testBundle: Bundle!
    private static var testImportedManuscriptFileURL: URL!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        testBundle = Bundle(for: self)
        testImportedManuscriptFileURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(UUID().uuidString),
                                            isDirectory: false).appendingPathComponent("untitled.manuscript")
    }

    override class func tearDown() {
        do {
            try FileManager.default.removeItem(at: testImportedManuscriptFileURL.deletingLastPathComponent())
        } catch {
            XCTFail("Temporary working directory for Tests failed to be removed!")
        }
        testBundle = nil
        testImportedManuscriptFileURL = nil
        super.tearDown()
    }

    func testGrobidXMLImportServiceCompleteResponse() throws {
        let packageController = try testPackageController()
        let url = type(of: self).testBundle.url(forResource: "example_response",
                                                withExtension: MPImportServiceContentGrobidXML.grobidXMLFileExtension)

        let importedObjects = try packageController.contentImportService.import(from: url!)
        guard let importedAbstract = (importedObjects.filter { $0 is MPSection } as? [MPSection])?.first else {
            XCTFail("Abstract section does not exist in 'importedObjects'!")
            return
        }
        guard let importedContributors = (importedObjects.filter { $0 is MPContributor } as? [MPContributor])?
            .sorted(by: { $0.priority < $1.priority }) else {
                XCTFail("Contributors do not exist in 'importedObjects'!")
                return
        }

        let validEmailAddresses: [String?] = ["iassinovski@multitel.be",
                                              "ester.ciancamerla@enea.it",
                                              nil,
                                              "luisa.lavalle@enea.it",
                                              "tatiana.patriarca@enea.italberto.tofani@enea.it",
                                              nil]

        // Initial sanity checks
        XCTAssertNotNil(importedObjects)
        XCTAssertEqual(importedObjects.count, 7)
        XCTAssertEqual(importedContributors.count, 6)

        // Title
        // swiftlint:disable:next line_length
        XCTAssertEqual(packageController.manuscriptsController.primaryManuscript.title, "Heterogeneous and distributed simulation models for Critical Infrastructures Interdependencies analysis and risk mitigation *")

        // Abstract
        XCTAssertEqual(importedAbstract.title, "Abstract")
        guard let innerHTML = (importedAbstract.elements as? [MPParagraphElement])?.first?.innerHTML else {
            XCTFail("Abstract section's MPParagraphElement doesn't exist (or doesn't contain 'innerHTML')!")
            return
        }
        // swiftlint:disable:next line_length
        XCTAssert(innerHTML.contains("The paper presents and discusses the implementation and first results of complex simulation models used for risk assessment and mitigation in interconnected Critical Infrastructures (CIs). It can be considered as continuation of paper published last year [1]. This work is relevant because damage estimation under various reference scenarios of CIs interdependencies, adverse events and SCADA procedures represents an important part of risk assessment. And simulation is essentially the only way for such an estimation due to complexity and interdependencies of CIs and to probabilistic nature of adverse events like cyber attacks or others."))

        // Affiliation details
        guard let affiliationOne = importedContributors[0].affiliations?.first else {
            XCTFail("'affiliationOne' should exist!")
            return
        }
        guard let affiliationTwo = importedContributors[1].affiliations?.first else {
            XCTFail("'affiliationTwo' should exist!")
            return
        }

        XCTAssertEqual(affiliationOne.institution, "Multitel Research Center")
        XCTAssertEqual(affiliationOne.city, "Mons")
        XCTAssertEqual(affiliationOne.country, "Belgium")
        XCTAssertEqual(affiliationTwo.department, "ENEA -Lungotevere Thaon di Revel")
        XCTAssertEqual(affiliationTwo.addressLine1, "76")
        XCTAssertEqual(affiliationTwo.city, "ROMA")
        XCTAssertEqual(affiliationTwo.country, "Italia")
        XCTAssertEqual(affiliationTwo.postCode, "00196")

        // Contributors
        for (index, contributor) in importedContributors.enumerated() {
            XCTAssertEqual(contributor.priority, index)
            XCTAssertEqual(contributor.affiliations?.count, 1)

            // Check that the contributor's affiliation is correct
            if index == 0 {
                XCTAssertEqual(contributor.affiliations?.first, affiliationOne)
            } else {
                XCTAssertEqual(contributor.affiliations?.first, affiliationTwo)
            }

            // Check email address
            XCTAssert(contributor.email == validEmailAddresses[index])
        }
    }

    /// Tests an incomplete Grobid XML response where the <abstract> node is empty (will be ignored during import)
    /// and the first author does not contain a <persName> node (will also be ignored during import).
    func testGrobidXMLImportServiceIncompleteResponse() throws {
        let packageController = try testPackageController()
        let url = type(of: self).testBundle.url(forResource: "incomplete_response",
                                                withExtension: MPImportServiceContentGrobidXML.grobidXMLFileExtension)

        let importedObjects = try packageController.contentImportService.import(from: url!)
        let importedAbstract = (importedObjects.filter { $0 is MPSection } as? [MPSection])?.first
        XCTAssertNil(importedAbstract,
                     "Abstract section for an empty <abstract> node should not exist in 'importedObjects'!")

        guard let importedContributors = (importedObjects.filter { $0 is MPContributor } as? [MPContributor])?
            .sorted(by: { $0.priority < $1.priority }) else {
                XCTFail("Contributors do not exist in 'importedObjects'!")
                return
        }

        // Initial sanity checks
        XCTAssertNotNil(importedObjects)
        XCTAssertEqual(importedObjects.count, 5)
        XCTAssertEqual(importedContributors.count, 5)

        // Title
        // swiftlint:disable:next line_length
        XCTAssertEqual(packageController.manuscriptsController.primaryManuscript.title, "Heterogeneous and distributed simulation models for Critical Infrastructures Interdependencies analysis and risk mitigation *")

        // Affiliation details
        guard let affiliationOne = importedContributors[0].affiliations?.first else {
            XCTFail("'affiliationOne' should exist!")
            return
        }

        XCTAssertEqual(affiliationOne.department, "ENEA -Lungotevere Thaon di Revel")
        XCTAssertEqual(affiliationOne.addressLine1, "76")
        XCTAssertEqual(affiliationOne.city, "ROMA")
        XCTAssertEqual(affiliationOne.country, "Italia")
        XCTAssertEqual(affiliationOne.postCode, "00196")

        // Contributors
        for (index, contributor) in importedContributors.enumerated() {
            XCTAssertEqual(contributor.priority, index + 1)
            XCTAssertEqual(contributor.affiliations?.count, 1)

            // Check that the contributor's affiliation is correct
            XCTAssertEqual(contributor.affiliations?.first, affiliationOne)
        }
    }

    /// Tests an incomplete Grobid XML response where the <title> and <abstract> nodes are present,
    /// but there are no <author> nodes present in the response.
    func testGrobidXMLImportServiceNoAuthorsResponse() throws {
        let packageController = try testPackageController()
        let url = type(of: self).testBundle.url(forResource: "no_authors_response",
                                                withExtension: MPImportServiceContentGrobidXML.grobidXMLFileExtension)

        let importedObjects = try packageController.contentImportService.import(from: url!)
        guard let importedAbstract = (importedObjects.filter { $0 is MPSection } as? [MPSection])?.first else {
            XCTFail("Abstract section does not exist in 'importedObjects'!")
            return
        }
        guard let importedContributors = (importedObjects.filter { $0 is MPContributor } as? [MPContributor])?
            .sorted(by: { $0.priority < $1.priority }) else {
                XCTFail("Contributors do not exist in 'importedObjects'!")
                return
        }

        // Initial sanity checks
        XCTAssertNotNil(importedObjects)
        XCTAssertEqual(importedObjects.count, 1)
        XCTAssertEqual(importedContributors.count, 0)

        // Title
        // swiftlint:disable:next line_length
        XCTAssertEqual(packageController.manuscriptsController.primaryManuscript.title, "Heterogeneous and distributed simulation models for Critical Infrastructures Interdependencies analysis and risk mitigation *")

        // Abstract
        XCTAssertEqual(importedAbstract.title, "Abstract")
        guard let innerHTML = (importedAbstract.elements as? [MPParagraphElement])?.first?.innerHTML else {
            XCTFail("Abstract section's MPParagraphElement doesn't exist (or doesn't contain 'innerHTML')!")
            return
        }
        // swiftlint:disable:next line_length
        XCTAssert(innerHTML.contains("The paper presents and discusses the implementation and first results of complex simulation models used for risk assessment and mitigation in interconnected Critical Infrastructures (CIs). It can be considered as continuation of paper published last year [1]. This work is relevant because damage estimation under various reference scenarios of CIs interdependencies, adverse events and SCADA procedures represents an important part of risk assessment. And simulation is essentially the only way for such an estimation due to complexity and interdependencies of CIs and to probabilistic nature of adverse events like cyber attacks or others."))
    }

    /// Initialises a package controller for testing purposes.
    ///
    /// - Returns: A `MPManuscriptsPackageController` initialised using `GrobidImportTests.testPackageRootDirectory`.
    /// - Throws: An `Error` if there is a problem initializing the package controller.
    private func testPackageController() throws -> MPManuscriptsPackageController {
        let rootURLHandler: MPDatabasePackageControllerRootURLHandler = { nil }
        let pkgDelegate = MPDatabasePackageControllerBlockBasedDelegate(packageController: nil,
                                                                        rootURLHandler: rootURLHandler) { _ in }

        let packageController = try MPManuscriptsPackageController(fileURL: type(of: self).testImportedManuscriptFileURL,
                                                                   readOnly: false,
                                                                   delegate: pkgDelegate)
        packageController.ensureInitialStateInitialized()
        packageController.ensureManuscriptListenerReady()

        return packageController
    }

    // MARK: - All tests

    static let allTests = [
        ("testGrobidXMLImportServiceCompleteResponse", testGrobidXMLImportServiceCompleteResponse),
        ("testGrobidXMLImportServiceIncompleteResponse", testGrobidXMLImportServiceIncompleteResponse)
    ]
}
