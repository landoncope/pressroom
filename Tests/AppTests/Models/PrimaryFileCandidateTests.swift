//
//  PrimaryFileCandidateTests.swift
//  AppTests
//
//  Created by Dan Browne on 21/09/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import MPFoundation
import XCTest

final class PrimaryFileCandidateTests: XCTestCase {

    // MARK: - Private variables

    private static var testBundle: Bundle!
    private static var workingDirectoryFileURL: URL!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // Init testBundle
        testBundle = Bundle(for: self)

        workingDirectoryFileURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(UUID().uuidString),
                                      isDirectory: true)
        do {
            try FileManager.default.createDirectory(at: workingDirectoryFileURL,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
        } catch {
            XCTFail("Temporary working directory for Tests failed to be created!")
        }
    }

    override class func tearDown() {
        do {
            try FileManager.default.removeItem(at: workingDirectoryFileURL)
        } catch {
            XCTFail("Temporary working directory for Tests failed to be removed!")
        }
        testBundle = nil
        workingDirectoryFileURL = nil
        super.tearDown()
    }

    // MARK: - Test cases

    func testEmptyContainerError() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "empty",
                                                           withExtension: "zip")
        let debuggingInfo = PrimaryFileCandidate.PrimaryFileCandidateDebuggingInfo.emptyDocumentContainerAttached
        try verifyPrimaryFileCandidateError(forContainer: url!,
                                            expectedDebuggingInfo: debuggingInfo)
    }

    func testMultipleIndexFilesError() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "index_tex_and_index_md",
                                                           withExtension: "zip")
        let debuggingInfo = PrimaryFileCandidate.PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound
        try verifyPrimaryFileCandidateError(forContainer: url!,
                                            expectedDebuggingInfo: debuggingInfo)
    }

    func testMixedCandidateFileFormatsError() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "mixed_candidate_file_formats",
                                                           withExtension: "zip")
        let debuggingInfo = PrimaryFileCandidate.PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound
        try verifyPrimaryFileCandidateError(forContainer: url!,
                                            expectedDebuggingInfo: debuggingInfo)
    }

    func testMultipleMarkdownCandidatesError() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "multiple_md_candidates",
                                                           withExtension: "zip")
        let debuggingInfo = PrimaryFileCandidate.PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound
        try verifyPrimaryFileCandidateError(forContainer: url!,
                                            expectedDebuggingInfo: debuggingInfo)
    }

    func testPrimaryFileHeaderError() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "primary_file_header",
                                                           withExtension: "zip")
        let debuggingInfo = PrimaryFileCandidate.PrimaryFileCandidateDebuggingInfo.invalidPrimaryFile
        try verifyPrimaryFileCandidateError(forContainer: url!,
                                            primaryFileName: "not_the_real_file.docx",
                                            expectedDebuggingInfo: debuggingInfo)
    }

    func testPrimaryFileHeaderSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "primary_file_header",
                                                           withExtension: "zip")
        let name = "test.md"
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              primaryFileName: name,
                                              expectedSelectionCriterion: .primaryFileHeaderProvided(fileName: name))
    }

    func testMultipleTeXCandidatesSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "multiple_tex_candidates",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .texCandidate(fileName: "test1.tex"))
    }

    func testSelectLargestFileFallbackSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "select_largest_file",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .largestFileFallback(fileName: "test.pdf"))
    }

    func testSingleDocumentSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "single_document",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .singleFileContainer(fileName: "test.html"))
    }

    func testSingleIndexMarkdownSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "single_index_md",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .indexFile(fileName: "index.md"))
    }

    func testSingleIndexTeXSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "single_index_tex",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .indexFile(fileName: "index.tex"))
    }

    func testSingleMarkdownCandidateSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "single_md_candidate",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .markdownCandidate(fileName: "test.md"))
    }

    func testSingleTeXCandidateSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "single_tex_candidate",
                                                           withExtension: "zip")
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .texCandidate(fileName: "test.tex"))
    }

    func testSingleTeXCandidateWithNoIncludeStatementsSuccess() throws {
        let url = PrimaryFileCandidateTests.testBundle.url(forResource: "single_tex_candidate_no_include_statements",
                                                           withExtension: "zip")
        let name = "article_2.tex"
        try verifyPrimaryFileCandidateSuccess(forContainer: url!,
                                              expectedSelectionCriterion: .texCandidate(fileName: name))
    }

    // MARK: - Private methods

    private func decompressZIP(at zipFileURL: URL) throws -> URL {
        // swiftlint:disable:next line_length
        let decompressedFileURL = PrimaryFileCandidateTests.workingDirectoryFileURL.appendingPathComponent("\(zipFileURL.lastPathComponent)_decompressed")
        try FileManager.default.createDirectory(at: decompressedFileURL,
                                                withIntermediateDirectories: true,
                                                attributes: nil)
        try MPDecompressor.decompressZIP(at: zipFileURL, to: decompressedFileURL)

        return decompressedFileURL
    }

    private func verifyPrimaryFileCandidateError<T>(forContainer containerFileURL: URL,
                                                    primaryFileName: String? = nil,
                                                    expectedDebuggingInfo: T) throws where T: DebuggingInfo, T.RawValue == String {

        let decompressedFileURL = try decompressZIP(at: containerFileURL)
        var candidate: PrimaryFileCandidate!

        do {
            candidate = try PrimaryFileCandidate(for: decompressedFileURL, primaryFileName: primaryFileName)
        } catch {
            guard let error = error as? PressroomError else {
                XCTFail("Received error was not of type 'PressroomError'")
                return
            }
            XCTAssertEqual(error.typeIdentifier, "PrimaryFileCandidateError")
            XCTAssertEqual(error.identifier, expectedDebuggingInfo.rawValue)
        }
        if candidate != nil {
            XCTFail("DocumentTransformationsController.Error was expected, got an initialized PrimaryFileCandidate instead!")
        }
    }

    private func verifyPrimaryFileCandidateSuccess(forContainer containerFileURL: URL,
                                                   primaryFileName: String? = nil,
                                                   expectedSelectionCriterion: PrimaryFileCandidate.SelectionCriterion) throws {

        let decompressedFileURL = try decompressZIP(at: containerFileURL)

        do {
            let candidate = try PrimaryFileCandidate(for: decompressedFileURL, primaryFileName: primaryFileName)
            if let criterion = candidate.selectionCriterion {
                XCTAssertEqual(criterion,
                               expectedSelectionCriterion,
                               "'\(criterion)' was expected to be equal to '\(expectedSelectionCriterion)'!")
            } else {
                XCTFail("'candidate.selectionCriterion' was expected to be equal to '\(expectedSelectionCriterion)', not 'nil'!")
            }
        } catch {
            XCTFail("No errors determining the primary file candidate were expected!")
        }
    }

    // MARK: - All tests

    static let allTests = [
        ("testEmptyContainerError", testEmptyContainerError),
        ("testMultipleIndexFilesError", testMultipleIndexFilesError),
        ("testMixedCandidateFileFormatsError", testMixedCandidateFileFormatsError),
        ("testMultipleMarkdownCandidatesError", testMultipleMarkdownCandidatesError),
        ("testPrimaryFileHeaderError", testPrimaryFileHeaderError),
        ("testPrimaryFileHeaderSuccess", testPrimaryFileHeaderSuccess),
        ("testMultipleTeXCandidatesSuccess", testMultipleTeXCandidatesSuccess),
        ("testSelectLargestFileFallbackSuccess", testSelectLargestFileFallbackSuccess),
        ("testSingleDocumentSuccess", testSingleDocumentSuccess),
        ("testSingleIndexMarkdownSuccess", testSingleIndexMarkdownSuccess),
        ("testSingleIndexTeXSuccess", testSingleIndexTeXSuccess),
        ("testSingleMarkdownCandidateSuccess", testSingleMarkdownCandidateSuccess),
        ("testSingleTeXCandidateSuccess", testSingleTeXCandidateSuccess),
        ("testSingleTeXCandidateWithNoIncludeStatementsSuccess", testSingleTeXCandidateWithNoIncludeStatementsSuccess)
    ]
}
