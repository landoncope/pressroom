//
//  ServiceSecretTests.swift
//  AppTests
//
//  Created by Dan Browne on 30/01/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import XCTest

final class ServiceSecretTests: XCTestCase {

    private static let notBase64EncodedSecret = "zzzzzzz"
    private static let validUsernameAndPasswordSecret = "dXNlcm5hbWU6cGFzc3dvcmQ="
    private static let validUsernameAndPasswordAndKeySecret = "dXNlcm5hbWU6cGFzc3dvcmQ6a2V5"

    func testNotBase64Secret() {
        XCTAssertThrowsError(try ServiceSecret(encoded: type(of: self).notBase64EncodedSecret, secretType: .onePart),
                             "A non base64-encoded secret should result in an error.") { error in
                                verifyError(error, expectedError: .notBase64Encoded)
        }
    }

    func testInvalidFormat() {
        XCTAssertThrowsError(try ServiceSecret(encoded: type(of: self).validUsernameAndPasswordSecret, secretType: .onePart),
                             "A secret with an invalid expected format should result in an error.") { error in
                                verifyError(error, expectedError: .invalidFormat)
        }
        XCTAssertThrowsError(try ServiceSecret(encoded: type(of: self).validUsernameAndPasswordSecret,
                                               secretType: .twoParts,
                                               separator: .semicolon),
                             "A secret with an invalid expected format should result in an error.") { error in
                                verifyError(error, expectedError: .invalidFormat)
        }
    }

    func testCredentialNotPresent() throws {
        let secret = try ServiceSecret(encoded: type(of: self).validUsernameAndPasswordSecret, secretType: .twoParts)
        XCTAssertThrowsError(try secret.decode(.key),
                             "A secret with an expected credential that is not present should result in an error.") { error in
                                verifyError(error, expectedError: .credentialNotPresent)
        }
    }

    func testCredentialDecoding() throws {
        let secret = try ServiceSecret(encoded: type(of: self).validUsernameAndPasswordAndKeySecret, secretType: .threeParts)

        XCTAssertEqual(try secret.decode(.username), "username")
        XCTAssertEqual(try secret.decode(.password), "password")
        XCTAssertEqual(try secret.decode(.key), "key")
    }

    // MARK: - Private methods

    private func verifyError(_ error: Error, expectedError: ServiceSecret.DecodingError) {
        guard let decodingError = error as? ServiceSecret.DecodingError,
            decodingError == expectedError else {
                XCTFail("The error should be '\(expectedError)', got '\(error)' instead.")
                return
        }
    }

    // MARK: - All tests

    static let allTests = [
        ("testNotBase64Secret", testNotBase64Secret),
        ("testInvalidFormat", testInvalidFormat),
        ("testCredentialNotPresent", testCredentialNotPresent),
        ("testCredentialDecoding", testCredentialDecoding)
    ]
}
