//
//  EndpointMockable.swift
//  AppTests
//
//  Created by Dan Browne on 22/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

/// An enum describing possible custom error states of an `EndpointMockable` test suite.
enum EndpointMockableError: Error {
    /// The file `URL` for a given test resource could not be determined
    /// (probably due to a typo, or that it hasn't been added to the 'AppTests' target correctly).
    case noFileURL(resource: String, fileExtension: String)
}

/// An enum describing the possible result verification conditions of an `EndpointMockable` unit test.
enum EndpointMockableResultVerification {
    /// The unit test should verify the 'success' case occurs.
    case success
    /// The unit test should verify the 'failure' (without throwing an error) case occurs.
    case failureNoThrow
    /// The unit test should verify the 'failure' (due to a thrown error) case occurs.
    case failureThrowsError
}

protocol EndpointMockable {

    /// The `App` instance that mock `Request`s will be executed against.
    static var app: Application! { get }
    /// A mock API-key value (i.e. that which would normally be returned for the 'MPPressroomServerAPIKey' env var).
    static var mockAPIKey: String { get }

    /// Mocks a `Request` for unit testing purposes.
    ///
    /// - Parameters:
    ///   - method: The REST method to use.
    ///   - url: A string representation of a URL to direct the request at.
    ///   - headers: A `String` dictionary of HTTP header key/value pairs.
    ///   - contentType: A `MediaType` representing the `Content-Type` header value.
    ///   - body: Body data (e.g. some JSON or plaintext data).
    ///   - app: The `App` instance that the `Request` will be executed against.
    /// - Returns: A `Request` based on the passed-in configuration options.
    // swiftlint:disable:next function_parameter_count
    func mockRequest(method: HTTPMethod,
                     url: URLRepresentable,
                     headers: HTTPHeaders,
                     contentType: MediaType?,
                     body: LosslessHTTPBodyRepresentable,
                     using app: Application?) -> Request

    /// Verify that a received `Response` has succeeded with some basic expected conditions.
    ///
    /// - Parameters:
    ///   - response: The received `Response` to verify.
    ///   - expectedContentType: The expected value of the `Content-Type` header of the `Response`.
    ///   - expectedResponseHeaders: The expected headers to be included in the `Response`
    ///                              (not including the default headers).
    ///   - expectedBodyData: The expected body data to be included in the `Response`.
    /// - Returns: The `Response` body as `Future<Data>`. This can be used for further tests at the callsite.
    func verifyResponseSuccess(_ response: Response,
                               expectedContentType: MediaType,
                               expectedResponseHeaders: HTTPHeaders?,
                               expectedBodyData: Data?) -> Future<Data>
}

protocol BodyDataMockable {

    /// The `Bundle` representing the unit test target, containing relevant test fixtures.
    static var testBundle: Bundle! { get }

    /// Mocks `Request` body data for a given test resource.
    ///
    /// - Parameters:
    ///   - resource: The test resource file name.
    ///   - fileExtension: The test resource file extension.
    /// - Returns: `Data` representation of the test resource.
    /// - Throws: An `Error` if the body data encoding fails for some reason.
    func httpBodyData(for resource: String, fileExtension: String) throws -> Data
}

// MARK: Default implementations

extension EndpointMockable {
    func mockRequest(method: HTTPMethod = .GET,
                     url: URLRepresentable = URL.root,
                     headers: HTTPHeaders = .init(),
                     contentType: MediaType? = nil,
                     body: LosslessHTTPBodyRepresentable = HTTPBody(),
                     using app: Application? = Self.app) -> Request {
        var httpReq = HTTPRequest(method: method, url: url, headers: headers, body: body)
        // Customize the 'Content-Type' of the Request (if set)
        if let contentType = contentType {
            httpReq.contentType = contentType
        }

        return Request(http: httpReq, using: app!)
    }

    func verifyResponseSuccess(_ response: Response,
                               expectedContentType: MediaType,
                               expectedResponseHeaders: HTTPHeaders? = nil,
                               expectedBodyData: Data? = nil) -> Future<Data> {
        XCTAssertEqual(response.http.status, HTTPResponseStatus.ok)
        guard let contentType = response.http.headers.firstValue(name: .contentType) else {
            XCTFail("'Content-Type' header should not be missing!")
            return response.eventLoop.newSucceededFuture(result: Data())
        }
        XCTAssertEqual(contentType, expectedContentType.serialize())

        // Additionally, if `expectedResponseHeaders` have been provided,
        // check each one is found with its respective expected value in the `Response` headers
        if let expectedResponseHeaders = expectedResponseHeaders {
            for (expectedHeader, expectedHeaderValue) in expectedResponseHeaders {
                XCTAssertEqual(response.http.headers[expectedHeader.lowercased()].first, expectedHeaderValue)
            }
        }

        // Consume the body data (needed in cases where `Response` is a chunked stream,
        // where there is otherwise no static `Data`)
        let dataFuture = response.http.body.consumeData(on: response)

        return dataFuture.map { bodyData -> Data in
            // If `expectedBodyData` is provided, actually check for equality
            // Otherwise, sanity check that some data is attached to the `Response`
            if let expectedBodyData = expectedBodyData {
                XCTAssertEqual(bodyData, expectedBodyData)
            } else {
                XCTAssert(bodyData.count > 0)
            }

            return bodyData
        }
    }
}

extension BodyDataMockable {
    func httpBodyData(for resource: String, fileExtension: String) throws -> Data {
        guard let fileURL = Self.testBundle.url(forResource: resource, withExtension: fileExtension) else {
            throw EndpointMockableError.noFileURL(resource: resource, fileExtension: fileExtension)
        }
        let encodedDocument = ArbitrarilyFormattedDocument(file: File(data: try Data(contentsOf: fileURL),
                                                                      filename: fileURL.lastPathComponent))

        return try FormDataEncoder().encode(encodedDocument,
                                            boundary: "--------------------------728980769844419903575308")
    }
}
