//
//  Bundle+Swizzling.swift
//  AppTests
//
//  Created by Dan Browne on 08/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import MPFoundation

private let swizzling: (AnyClass, Selector, Selector) -> Void = { forClass, originalSelector, swizzledSelector in
    guard
        let originalMethod = class_getClassMethod(forClass, originalSelector),
        let swizzledMethod = class_getClassMethod(forClass, swizzledSelector) else {
            return
    }
    method_exchangeImplementations(originalMethod, swizzledMethod)
}

extension Bundle {

    /// Swizzle the method implementation for `Bundle.main` to a custom
    /// implementation for PressroomServer unit tests.
    static func swizzleMainBundle() {
        let originalSelector = #selector(getter: main)
        let swizzledSelector = #selector(mainBundle_swizzled)
        swizzling(Bundle.self, originalSelector, swizzledSelector)
    }

    @objc private static func mainBundle_swizzled() -> Bundle {
        return Bundle.init(for: BibliographyTransformationsControllerTests.self)
    }

    /// Swizzle the method implementation for `Bundle.app()` to a custom
    /// implementation for PressroomServer unit tests.
    static func swizzleAppBundle() {
        let originalSelector = #selector(app)
        let swizzledSelector = #selector(appBundle_swizzled)
        swizzling(Bundle.self, originalSelector, swizzledSelector)
    }

    @objc private static func appBundle_swizzled() -> Bundle {
        return Bundle.init(for: BibliographyTransformationsControllerTests.self)
    }

    /// Swizzle the method implementation for `Bundle.pressRoom()` to a custom
    /// implementation for PressroomServer unit tests.
    static func swizzlePressRoomBundle() {
        let originalSelector = #selector(pressRoom)
        let swizzledSelector = #selector(pressRoomBundle_swizzled)
        swizzling(Bundle.self, originalSelector, swizzledSelector)
    }

    @objc private static func pressRoomBundle_swizzled() -> Bundle? {
        // Return `nil` so that callsite in `MPXSLT` favors
        // the MPFoundation.framework bundle instead in unit tests
        return nil
    }
}
