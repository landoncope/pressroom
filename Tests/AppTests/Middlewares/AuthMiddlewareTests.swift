//
//  AuthMiddlewareTests.swift
//  AppTests
//
//  Created by Dan Browne on 04/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Vapor
import XCTest

final class AuthMiddlewareTests: XCTestCase {

    private static var app: Application!

    // MARK: Test suite setup

    override class func setUp() {
        super.setUp()

        // Initialize an `Application` instance to use for the AuthMiddleware test methods
        var services = Services.default()
        var middlewareConfig = MiddlewareConfig()
        middlewareConfig.use(AuthMiddleware.self)
        services.register(AuthMiddleware.self)
        services.register(middlewareConfig)
        // swiftlint:disable:next force_try
        app = try! Application(services: services)
    }

    override class func tearDown() {
        app = nil
        super.tearDown()
    }

    // MARK: Bearer token auth path test cases

    func testNoAuthorizationHeaderRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.noAuthenticationMethod.rawValue)
    }

    func testInvalidFormattingAuthorizationHeaderRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Blort"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        // swiftlint:disable:next line_length
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.invalidAuthorizationHeaderFormat.rawValue)
    }

    func testInvalidIncompleteAuthorizationHeaderRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        // swiftlint:disable:next line_length
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.invalidAuthorizationHeaderFormat.rawValue)
    }

    func testMalformedJWTRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer a.b"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        // swiftlint:disable:next line_length
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.invalidAuthorizationHeaderFormat.rawValue)
    }

    func testInvalidJWTSignatureRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer \(jwtWithInvalidSignature)"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.AuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.reason, "Invalid JWT signature")
    }

    func testNonBase64JWTSignatureRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer \(jwtWithNonBase64Signature)"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.AuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.reason, "JWT signature is not valid base64-url")
    }

    func testInvalidJWTPayloadStructureRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer \(jwtWithMissingAppIdInPayload)"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.jwtDecodingError.rawValue)
    }

    func testExpiredPayloadRespondsUnauthorized() throws {
        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer \(jwtWithExpiredPayload)"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.AuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.reason, "JWT has expired (i.e. its 'expiry' date is in the past)")
    }

    func testValidJWTRespondsAuthorized() throws {

        let request = try mockRequest(method: .POST,
                                      headers: ["Authorization": "Bearer \(validJWT)"],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.AuthorizationErrorResponse.self).wait()

        // No routes configured in the unit tests, so a valid JWT will result in a 404 Not Found
        XCTAssertEqual(response.http.status, .notFound)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.reason, "Not Found")
    }

    // MARK: - API Key auth path test cases

    func testNoAPIKeyHeaderRespondsUnauthorized() throws {

        let request = try mockRequest(method: .POST, using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.noAuthenticationMethod.rawValue)
    }

    func testInvalidAPIKeyRespondsUnauthorized() throws {
        let request = try mockRequest(method: .POST,
                                      headers: ["Pressroom-API-Key": invalidAPIKey],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.DetailedAuthorizationErrorResponse.self).wait()

        XCTAssertEqual(response.http.status, .unauthorized)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.errorObject.identifier, AuthMiddleware.AuthMiddlewareDebuggingInfo.invalidAPIKeyHeader.rawValue)
    }

    func testValidAPIKeyRespondsAuthorized() throws {
        let request = try mockRequest(method: .POST,
                                      headers: ["Pressroom-API-Key": validAPIKey],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.AuthorizationErrorResponse.self).wait()

        // No routes configured in the unit tests, so a valid API key will result in a 404 Not Found
        XCTAssertEqual(response.http.status, .notFound)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.reason, "Not Found")
    }

    func testValidAPIKeyAndJWTRespondsAuthorized() throws {
        let request = try mockRequest(method: .POST,
                                      headers: ["Pressroom-API-Key": validAPIKey,
                                                "Authorization": validJWT],
                                      using: AuthMiddlewareTests.app)
        let response = try AuthMiddlewareTests.app.make(Responder.self).respond(to: request).wait()
        let errorResponse = try response.content.decode(AuthMiddleware.AuthorizationErrorResponse.self).wait()

        // No routes configured in the unit tests, so a valid API key and JWT will result in a 404 Not Found
        XCTAssertEqual(response.http.status, .notFound)
        XCTAssertEqual(errorResponse.error, true)
        XCTAssertEqual(errorResponse.reason, "Not Found")
    }

    // MARK: - All tests

    static let allTests = [
        ("testNoAuthorizationHeaderRespondsUnauthorized", testNoAuthorizationHeaderRespondsUnauthorized),
        ("testInvalidFormattingAuthorizationHeaderRespondsUnauthorized", testInvalidFormattingAuthorizationHeaderRespondsUnauthorized),
        ("testInvalidIncompleteAuthorizationHeaderRespondsUnauthorized", testInvalidIncompleteAuthorizationHeaderRespondsUnauthorized),
        ("testMalformedJWTRespondsUnauthorized", testMalformedJWTRespondsUnauthorized),
        ("testInvalidJWTSignatureRespondsUnauthorized", testInvalidJWTSignatureRespondsUnauthorized),
        ("testNonBase64JWTSignatureRespondsUnauthorized", testNonBase64JWTSignatureRespondsUnauthorized),
        ("testInvalidJWTPayloadStructureRespondsUnauthorized", testInvalidJWTPayloadStructureRespondsUnauthorized),
        ("testExpiredPayloadRespondsUnauthorized", testExpiredPayloadRespondsUnauthorized),
        ("testValidJWTRespondsAuthorized", testValidJWTRespondsAuthorized),
        ("testNoAPIKeyHeaderRespondsUnauthorized", testNoAPIKeyHeaderRespondsUnauthorized),
        ("testInvalidAPIKeyRespondsUnauthorized", testInvalidAPIKeyRespondsUnauthorized),
        ("testValidAPIKeyRespondsAuthorized", testValidAPIKeyRespondsAuthorized),
        ("testValidAPIKeyAndJWTRespondsAuthorized", testValidAPIKeyAndJWTRespondsAuthorized)
    ]

    // MARK: - Private variables and methods

    private func mockRequest(method: HTTPMethod = .GET,
                             url: URLRepresentable = URL.root,
                             headers: HTTPHeaders = .init(),
                             body: LosslessHTTPBodyRepresentable = HTTPBody(),
                             using app: Application) throws -> Request {
        let httpReq = HTTPRequest(method: method, url: url, headers: headers, body: body)
        return Request(http: httpReq, using: app)
    }

    // swiftlint:disable:next line_length
    private let validJWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjoiOGQ1NzQwYzAtY2M4OS00OGFmLWE3ZjQtMDRmNjAwMDI0ZjU3IiwidXNlcklkIjoiZHJkZXJwQGxvbC5jb20iLCJhcHBJZCI6ImNvbS5tYW51c2NyaXB0cy5wcmVzc3Jvb21zZXJ2ZXIiLCJleHBpcnkiOjE4NDk3ODQxMTV9.DKp6p47hvIP9nov94pt5x1FDhPwv3XlhIs3k8oN45yU"

    // swiftlint:disable:next line_length
    private let jwtWithInvalidSignature = "zzzzzzzzzzJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjoiOGQ1NzQwYzAtY2M4OS00OGFmLWE3ZjQtMDRmNjAwMDI0ZjU3IiwidXNlcklkIjoiZHJkZXJwQGxvbC5jb20iLCJhcHBJZCI6ImNvbS5tYW51c2NyaXB0cy5wcmVzc3Jvb21zZXJ2ZXIiLCJleHBpcnkiOjE4NDk3ODQxMTV9.6CXniCd7XxC7m8eu5nXJEYfH_bj931HIqWuUklH0aYI"

    // swiftlint:disable:next line_length
    private let jwtWithNonBase64Signature = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjoiOGQ1NzQwYzAtY2M4OS00OGFmLWE3ZjQtMDRmNjAwMDI0ZjU3IiwidXNlcklkIjoiZHJkZXJwQGxvbC5jb20iLCJhcHBJZCI6ImNvbS5tYW51c2NyaXB0cy5wcmVzc3Jvb21zZXJ2ZXIiLCJleHBpcnkiOjE4NDk3ODQxMTV9.z"

    // swiftlint:disable:next line_length
    private let jwtWithMissingAppIdInPayload = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjoiOGQ1NzQwYzAtY2M4OS00OGFmLWE3ZjQtMDRmNjAwMDI0ZjU3IiwidXNlcklkIjoiZHJkZXJwQGxvbC5jb20iLCJleHBpcnkiOjE4NDk3ODQxMTV9.N6FmcWMruj-Bwk0MNNJZHAr3uBYPLCvbTn3ta3GBTNU"

    // swiftlint:disable:next line_length
    private let jwtWithExpiredPayload = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjoiOGQ1NzQwYzAtY2M4OS00OGFmLWE3ZjQtMDRmNjAwMDI0ZjU3IiwidXNlcklkIjoiZHJkZXJwQGxvbC5jb20iLCJhcHBJZCI6ImNvbS5tYW51c2NyaXB0cy5wcmVzc3Jvb21zZXJ2ZXIiLCJleHBpcnkiOjE1MzQyOTExNDB9.0rYCDFyCNxpWyo38xmGj23hb-9J-ktEjUM5u13Ibw4k"

    private let validAPIKey = "PLACEHOLDER-API-KEY"

    private let invalidAPIKey = "BLORT"
}
