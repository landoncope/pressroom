//
//  DocumentDepositsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 31/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

final class DocumentDepositsControllerTests: XCTestCase, EndpointMockable, BodyDataMockable {

    static var app: Application!
    static var testBundle: Bundle!
    static var mockAPIKey = "PLACEHOLDER-API-KEY"
    static var documentDepositsController: DocumentDepositsController!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        documentDepositsController = DocumentDepositsController()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)

        // Ensure initial state of `app` is configured
        // swiftlint:disable:next force_try
        try! boot(app)

        // Init testBundle
        testBundle = Bundle(for: self)
    }

    override class func tearDown() {
        app = nil
        testBundle = nil
        documentDepositsController = nil

        super.tearDown()
    }

    // MARK: - Test cases

    func testEndToEndInvalidContentType() {
        endToEndTest(resourceName: "example",
                     resourceExtension: "rtf",
                     expectedContentType: .json,
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndMissingDepositNotificationCallbackURL() {
        endToEndTest(resourceName: "example_project_bundle",
                     depositSubmissionIdentifier: "98765",
                     depositSubmissionJournalName: "Awesome Journal",
                     depositNotificationCallbackURL: nil,
                     jatsSubmissionIdentifier: "123",
                     jatsSubmissionDOI: "10.5555/123",
                     expectedContentType: .json,
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndMissingDepositSubmissionIdentifier() {
        endToEndTest(resourceName: "example_project_bundle",
                     depositSubmissionIdentifier: nil,
                     depositSubmissionJournalName: "Awesome Journal",
                     depositNotificationCallbackURL: "https://somefakehost.com/web-hook",
                     jatsSubmissionIdentifier: "123",
                     jatsSubmissionDOI: "10.5555/123",
                     expectedContentType: .json,
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndMissingDepositSubmissionJournalName() {
        endToEndTest(resourceName: "example_project_bundle",
                     depositSubmissionIdentifier: "98765",
                     depositSubmissionJournalName: nil,
                     depositNotificationCallbackURL: "https://somefakehost.com/web-hook",
                     jatsSubmissionIdentifier: "123",
                     jatsSubmissionDOI: "10.5555/123",
                     expectedContentType: .json,
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndMissingJATSSubmissionDOI() {
        endToEndTest(resourceName: "example_project_bundle",
                     depositSubmissionIdentifier: "98765",
                     depositSubmissionJournalName: "Awesome Journal",
                     depositNotificationCallbackURL: "https://somefakehost.com/web-hook",
                     jatsSubmissionIdentifier: "123",
                     jatsSubmissionDOI: nil,
                     expectedContentType: .json,
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndSuccess() {
        endToEndTest(resourceName: "example_project_bundle",
                     depositSubmissionIdentifier: "98765",
                     depositSubmissionJournalName: "Awesome Journal",
                     depositNotificationCallbackURL: "https://somefakehost.com/web-hook",
                     jatsSubmissionIdentifier: "123",
                     jatsSubmissionDOI: "10.5555/123",
                     expectedContentType: .json)
    }

    // MARK: - Private methods

    // swiftlint:disable:next function_body_length
    private func endToEndTest(resourceName: String,
                              resourceExtension: String = "manuproj",
                              depositSubmissionIdentifier: String? = nil,
                              depositSubmissionJournalName: String? = nil,
                              depositNotificationCallbackURL: String? = nil,
                              jatsSubmissionIdentifier: String? = nil,
                              jatsSubmissionDOI: String? = nil,
                              expectedContentType: MediaType,
                              expectedResponseHeaders: HTTPHeaders? = nil,
                              resultVerification: EndpointMockableResultVerification = .success,
                              completionTimeout: Double? = 30.0) {

        let expectation = XCTestExpectation(description: "endToEndTestExpectation")

        // The `mockAPIKey` is set as a request header in all cases
        var requestHeaders: HTTPHeaders = [Request.pressroomHeaders.apiKey.stringValue: type(of: self).mockAPIKey]

        // Add optional parameters to request headers
        if let depositSubmissionIdentifier = depositSubmissionIdentifier {
            requestHeaders.add(name: Request.pressroomHeaders.depositSubmissionIdentifier, value: depositSubmissionIdentifier)
        }
        if let depositSubmissionJournalName = depositSubmissionJournalName {
            requestHeaders.add(name: Request.pressroomHeaders.depositSubmissionJournalName, value: depositSubmissionJournalName)
        }
        if let depositNotificationCallbackURL = depositNotificationCallbackURL {
            requestHeaders.add(name: Request.pressroomHeaders.depositNotificationCallbackURL, value: depositNotificationCallbackURL)
        }
        if let jatsSubmissionIdentifier = jatsSubmissionIdentifier {
            requestHeaders.add(name: Request.pressroomHeaders.jatsSubmissionIdentifier, value: jatsSubmissionIdentifier)
        }
        if let jatsSubmissionDOI = jatsSubmissionDOI {
            requestHeaders.add(name: Request.pressroomHeaders.jatsSubmissionDOI, value: jatsSubmissionDOI)
        }

        // Init mock request
        guard let bodyData = try? httpBodyData(for: resourceName, fileExtension: resourceExtension) else {
            XCTFail("Test Request body data failed to be initialized.")
            return
        }
        let request = mockRequest(method: .POST,
                                  headers: requestHeaders,
                                  contentType: MediaType(type: "multipart",
                                                         subType: "form-data",
                                                         parameters: ["boundary": "--------------------------728980769844419903575308"]),
                                  body: bodyData)

        // Compile manuscript and verify the response when the `Future` returns successfully
        // OR handle an error depending on the `resultVerification` value
        switch resultVerification {
        case .success:
            XCTAssertNoThrow(try Self.documentDepositsController.depositManuscript(request)) { responseFuture in
                responseFuture.whenSuccess {[unowned self] response in
                    let bodyDataFuture = self.verifyResponseSuccess(response,
                                                                    expectedContentType: expectedContentType,
                                                                    expectedResponseHeaders: expectedResponseHeaders,
                                                                    expectedBodyData: Data())
                    bodyDataFuture.whenSuccess({ _ in
                        // Fulfill the 'success' case expectation
                        expectation.fulfill()
                    })
                }

                responseFuture.whenFailure { error in
                    XCTFail("No document transformation error expected - '\(error)' thrown instead!")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureNoThrow:
            XCTAssertNoThrow(try Self.documentDepositsController.depositManuscript(request)) { responseFuture in
                responseFuture.whenSuccess { _ in
                    XCTFail("Failure (i.e. an error) expected, but Future<Response> was fulfilled successfully!")
                    expectation.fulfill()
                }

                responseFuture.whenFailure { error in
                    guard let pressroomError = error as? PressroomError else {
                        XCTFail("Received error was not of type 'PressroomError'")
                        // Fulfill the 'failure' case expectation
                        expectation.fulfill()
                        return
                    }
                    XCTAssertNotEqual(pressroomError.status, HTTPResponseStatus.ok)
                    XCTAssertEqual(pressroomError.typeIdentifier, "DocumentDepositsControllerError")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureThrowsError:
            XCTAssertThrowsError(try Self.documentDepositsController.depositManuscript(request)) { _ in
                // Fulfill the 'failure' (throwing an error) case expectation
                expectation.fulfill()
            }
        }

        // Wait for the async document compilation to complete
        wait(for: [expectation], timeout: completionTimeout!)
    }

    // MARK: - All tests

    static let allTests = [
        ("testEndToEndInvalidContentType", testEndToEndInvalidContentType),
        ("testEndToEndMissingDepositNotificationCallbackURL", testEndToEndMissingDepositNotificationCallbackURL),
        ("testEndToEndMissingDepositSubmissionIdentifier", testEndToEndMissingDepositSubmissionIdentifier),
        ("testEndToEndMissingDepositSubmissionJournalName", testEndToEndMissingDepositSubmissionJournalName),
        ("testEndToEndMissingJATSSubmissionDOI", testEndToEndMissingJATSSubmissionDOI),
        ("testEndToEndSuccess", testEndToEndSuccess)
    ]
}
