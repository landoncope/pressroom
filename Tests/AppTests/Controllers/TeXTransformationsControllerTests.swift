//
//  TeXTransformationsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 22/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

final class TeXTransformationsControllerTests: XCTestCase, EndpointMockable, BodyDataMockable {

    // MARK: - Static variables

    static var app: Application!
    static var testBundle: Bundle!
    static let mockAPIKey = "PLACEHOLDER-API-KEY"
    static var teXTransformationsController: TeXTransformationsController!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        teXTransformationsController = TeXTransformationsController()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)

        // Init testBundle
        testBundle = Bundle(for: self)
    }

    override class func tearDown() {
        app = nil
        teXTransformationsController = nil
        super.tearDown()
    }

    // MARK: - Test cases

    func testEndToEndContainerWithSingleTeXFile() {
        endToEndTest(targetLaTeXMLOutputFormat: "html",
                     resourceName: "latexml_single_tex_file",
                     resultVerification: .success)
    }

    func testEndToEndContainerWithMultipleTeXFiles() {
        endToEndTest(targetLaTeXMLOutputFormat: "html",
                     resourceName: "latexml_multiple_tex_files",
                     resultVerification: .success)
    }

    func testEndToEndContainerIncludingAuxiliaryFiles() {
        endToEndTest(targetLaTeXMLOutputFormat: "html",
                     resourceName: "latexml_multiple_tex_files_inc_auxiliary_files",
                     resultVerification: .success)
    }

    func testEndToEndContainerWithNonTeXFiles() {
        endToEndTest(targetLaTeXMLOutputFormat: "html",
                     resourceName: "latexml_multiple_tex_files_inc_non_tex_files",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndEmptyContainer() {
        endToEndTest(targetLaTeXMLOutputFormat: "html",
                     resourceName: "empty",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndMissingLaTeXMLOutputFormat() {
        endToEndTest(targetLaTeXMLOutputFormat: nil,
                     resourceName: "latexml_single_tex_file",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndInvalidLaTeXMLOutputFormat() {
        endToEndTest(targetLaTeXMLOutputFormat: "blort",
                     resourceName: "latexml_single_tex_file",
                     resultVerification: .failureNoThrow)
    }

    // MARK: - Private methods

    // swiftlint:disable:next function_body_length
    private func endToEndTest(targetLaTeXMLOutputFormat: String?,
                              resourceName: String,
                              resultVerification: EndpointMockableResultVerification,
                              completionTimeout: Double? = 30.0) {

        let expectationDescription = "endToEndTest_\(targetLaTeXMLOutputFormat ?? "nil_targetLaTeXMLOutputFormat")"
        let expectation = XCTestExpectation(description: expectationDescription)
        // The `mockAPIKey` is set as a request header in all cases
        var requestHeaders: HTTPHeaders = [Request.pressroomHeaders.apiKey.stringValue: type(of: self).mockAPIKey]

        if let targetLaTeXMLOutputFormat = targetLaTeXMLOutputFormat {
            requestHeaders.add(name: Request.pressroomHeaders.targetLaTeXMLOutputFormat,
                               value: targetLaTeXMLOutputFormat)
        }

        // Init mock request
        guard let bodyData = try? httpBodyData(for: resourceName, fileExtension: "zip") else {
            XCTFail("Test Request body data failed to be initialized.")
            return
        }
        let request = mockRequest(method: .POST,
                                  headers: requestHeaders,
                                  contentType: MediaType(type: "multipart",
                                                         subType: "form-data",
                                                         parameters: ["boundary": "--------------------------728980769844419903575308"]),
                                  body: bodyData)

        // Compile TeX document and verify the response when the `Future` returns successfully
        // OR handle an error depending on the `resultVerification` value
        switch resultVerification {
        case .success:
            XCTAssertNoThrow(try type(of: self).teXTransformationsController.compileManuscript(request)) { responseFuture in
                responseFuture.whenSuccess {[unowned self] response in
                    let bodyDataFuture = self.verifyResponseSuccess(response, expectedContentType: .zip)
                    bodyDataFuture.whenSuccess({ _ in
                        // Fulfill the 'success' case expectation
                        expectation.fulfill()
                    })
                }

                responseFuture.whenFailure { error in
                    XCTFail("No TeX document transformation error expected - '\(error)' thrown instead!")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureNoThrow:
            XCTAssertNoThrow(try type(of: self).teXTransformationsController.compileManuscript(request)) { responseFuture in
                responseFuture.whenSuccess { _ in
                    XCTFail("Failure (i.e. an error) expected, but Future<Response> was fulfilled successfully!")
                    expectation.fulfill()
                }

                responseFuture.whenFailure { error in
                    guard let pressroomError = error as? PressroomError else {
                        XCTFail("Received error was not of type 'PressroomError'")
                        // Fulfill the 'failure' case expectation
                        expectation.fulfill()
                        return
                    }
                    XCTAssertNotEqual(pressroomError.status, HTTPResponseStatus.ok)
                    XCTAssertEqual(pressroomError.typeIdentifier, "TeXTransformationsControllerError")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureThrowsError:
            XCTAssertThrowsError(try type(of: self).teXTransformationsController.compileManuscript(request)) { _ in
                // Fulfill the 'failure' (throwing an error) case expectation
                expectation.fulfill()
            }
        }

        // Wait for the async TeX compilation to complete
        wait(for: [expectation], timeout: completionTimeout!)
    }

    // MARK: - All tests

    static let allTests = [
        ("testEndToEndContainerWithSingleTeXFile", testEndToEndContainerWithSingleTeXFile),
        ("testEndToEndContainerWithMultipleTeXFiles", testEndToEndContainerWithMultipleTeXFiles),
        ("testEndToEndContainerIncludingAuxiliaryFiles", testEndToEndContainerIncludingAuxiliaryFiles),
        ("testEndToEndContainerWithNonTeXFiles", testEndToEndContainerWithNonTeXFiles),
        ("testEndToEndEmptyContainer", testEndToEndEmptyContainer),
        ("testEndToEndMissingLaTeXMLOutputFormat", testEndToEndMissingLaTeXMLOutputFormat),
        ("testEndToEndInvalidLaTeXMLOutputFormat", testEndToEndInvalidLaTeXMLOutputFormat)
    ]
}
