//
//  SachsTransformationsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 20/02/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import MPFoundation
import Vapor
import XCTest

// swiftlint:disable file_length type_body_length function_body_length cyclomatic_complexity

final class SachsTransformationsControllerTests: XCTestCase, EndpointMockable, BodyDataMockable {

    // MARK: - Static variables

    static var app: Application!
    static var testBundle: Bundle!
    static let mockAPIKey = "PLACEHOLDER-API-KEY"
    static var sachsTransformationsController: SachsTransformationsController!
    static var workingDirectoryFileURL: URL!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        sachsTransformationsController = SachsTransformationsController()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)

        // Init testBundle
        testBundle = Bundle(for: self)

        workingDirectoryFileURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(UUID().uuidString),
                                      isDirectory: true)
        do {
            try FileManager.default.createDirectory(at: workingDirectoryFileURL,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
        } catch {
            XCTFail("Temporary working directory for Tests failed to be created!")
        }
    }

    override class func tearDown() {
        do {
            try FileManager.default.removeItem(at: workingDirectoryFileURL)
        } catch {
            XCTFail("Temporary working directory for Tests failed to be removed!")
        }
        testBundle = nil
        workingDirectoryFileURL = nil
        app = nil
        sachsTransformationsController = nil
        super.tearDown()
    }

    // MARK: - Request parameters test cases

    func testEndToEndMissingJATSOutputFormat() {
        endToEndTest(targetJATSOutputFormat: nil,
                     documentProcessingLevel: "full_text",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndInvalidJATSOutputFormat() {
        endToEndTest(targetJATSOutputFormat: "blort",
                     documentProcessingLevel: "full_text",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndMissingDocumentProcessingLevel() {
        endToEndTest(targetJATSOutputFormat: "jats",
                     documentProcessingLevel: nil,
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndInvalidDocumentProcessingLevel() {
        endToEndTest(targetJATSOutputFormat: "jats",
                     documentProcessingLevel: "blort",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumJATSMissingSubmissionIdentifier() {
        endToEndTest(targetJATSOutputFormat: "literatum-jats",
                     documentProcessingLevel: "full-text",
                     targetJATSVersion: "1.2d1",
                     submissionIdentifier: nil,
                     submissionDOI: "10.5555/MPProject:01b6563b-c67a-4322-84f0-d20211f43032",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumJATSMissingSubmissionDOI() {
        endToEndTest(targetJATSOutputFormat: "literatum-jats",
                     documentProcessingLevel: "full-text",
                     targetJATSVersion: "1.2d1",
                     submissionIdentifier: "MPProject:01b6563b-c67a-4322-84f0-d20211f43032",
                     submissionDOI: nil,
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumDOMissingSubmissionIdentifier() {
        endToEndTest(targetJATSOutputFormat: "literatum-do",
                     documentProcessingLevel: "full-text",
                     submissionIdentifier: nil,
                     submissionDOI: "10.5555/123",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumDOMissingSubmissionDOI() {
        endToEndTest(targetJATSOutputFormat: "literatum-do",
                     documentProcessingLevel: "full-text",
                     submissionIdentifier: "123",
                     submissionDOI: nil,
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumDOMissingDigitalObjectType() {
        endToEndTest(targetJATSOutputFormat: "literatum-do",
                     digitalObjectType: nil,
                     documentProcessingLevel: "full-text",
                     submissionIdentifier: "123",
                     submissionDOI: "10.5555/123",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumSubmissionMissingSubmissionIdentifier() {
        endToEndTest(targetJATSOutputFormat: "literatum-submission",
                     documentProcessingLevel: "full-text",
                     groupDOI: "10.5555/GROUP_ID",
                     seriesCode: "SERIES_CODE",
                     submissionIdentifier: nil,
                     submissionDOI: "10.5555/SUBMISSION_ID",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumSubmissionMissingSubmissionDOI() {
        endToEndTest(targetJATSOutputFormat: "literatum-submission",
                     documentProcessingLevel: "full-text",
                     groupDOI: "10.5555/GROUP_ID",
                     seriesCode: "SERIES_CODE",
                     submissionIdentifier: "SUBMISSION_ID",
                     submissionDOI: nil,
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumSubmissionMissingGroupIdentifier() {
        endToEndTest(targetJATSOutputFormat: "literatum-submission",
                     documentProcessingLevel: "full-text",
                     groupDOI: nil,
                     seriesCode: "SERIES_CODE",
                     submissionIdentifier: "123",
                     submissionDOI: "10.5555/123",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndLiteratumSubmissionMissingSeriesCode() {
        endToEndTest(targetJATSOutputFormat: "literatum-submission",
                     documentProcessingLevel: "full-text",
                     groupDOI: "10.5555/GROUP_ID",
                     seriesCode: nil,
                     submissionIdentifier: "SUBMISSION_ID",
                     submissionDOI: "10.5555/SUBMISSION_ID",
                     resourceName: "example_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndInvalidContainer() {
        endToEndTest(targetJATSOutputFormat: "jats",
                     documentProcessingLevel: "full_text",
                     resourceName: "invalid_project_bundle",
                     resultVerification: .failureNoThrow)
    }

    // MARK: - Document transformation test cases

    func testEndToEndLiteratumDOExport() {
        endToEndTest(targetJATSOutputFormat: "literatum-do",
                     digitalObjectType: "science",
                     documentProcessingLevel: "full_text",
                     submissionIdentifier: "123",
                     submissionDOI: "10.5555/123",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndJATSExportFullText() {
        endToEndTest(targetJATSOutputFormat: "jats",
                     documentProcessingLevel: "full_text",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndJATSExportFrontMatterOnly() {
        endToEndTest(targetJATSOutputFormat: "jats",
                     documentProcessingLevel: "front_matter_only",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndLiteratumJATSExport() {
        endToEndTest(targetJATSOutputFormat: "literatum-jats",
                     documentProcessingLevel: "full_text",
                     targetJATSVersion: "1.2d1",
                     submissionIdentifier: "MPProject:01b6563b-c67a-4322-84f0-d20211f43032",
                     submissionDOI: "10.5555/MPProject:01b6563b-c67a-4322-84f0-d20211f43032",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndWileyMLBundleExport() {
        endToEndTest(targetJATSOutputFormat: "wileyml-bundle",
                     documentProcessingLevel: "full_text",
                     groupDOI: "10.5555/GROUP_ID",
                     seriesCode: "SERIES_CODE",
                     submissionIdentifier: "SUBMISSION_ID",
                     submissionDOI: "10.5555/SUBMISSION_ID",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndJATSBundleExport() {
        endToEndTest(targetJATSOutputFormat: "jats-bundle",
                     documentProcessingLevel: "full_text",
                     groupDOI: "10.5555/GROUP_ID",
                     seriesCode: "SERIES_CODE",
                     submissionIdentifier: "SUBMISSION_ID",
                     submissionDOI: "10.5555/SUBMISSION_ID",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndHTMLExport() {
        endToEndTest(targetJATSOutputFormat: "html",
                     documentProcessingLevel: "full_text",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    func testEndToEndPDFExport() {
        endToEndTest(targetJATSOutputFormat: "pdf",
                     documentProcessingLevel: "full_text",
                     resourceName: "example_project_bundle",
                     resultVerification: .success)
    }

    // MARK: - Private methods

    private func endToEndTest(targetJATSOutputFormat: String?,
                              digitalObjectType: String? = nil,
                              documentProcessingLevel: String?,
                              groupDOI: String? = nil,
                              targetJATSVersion: String? = nil,
                              seriesCode: String? = nil,
                              submissionIdentifier: String? = nil,
                              submissionDOI: String? = nil,
                              resourceName: String,
                              resultVerification: EndpointMockableResultVerification,
                              completionTimeout: Double? = 30.0) {

        let expectationDescription = "endToEndTest_\(targetJATSOutputFormat ?? "nil_targetLaTeXMLOutputFormat")"
        let expectation = XCTestExpectation(description: expectationDescription)

        // The `mockAPIKey` is set as a request header in all cases
        var requestHeaders: HTTPHeaders = [Request.pressroomHeaders.apiKey.stringValue: type(of: self).mockAPIKey]

        // Optional headers
        if let targetJATSOutputFormat = targetJATSOutputFormat {
            requestHeaders.add(name: Request.pressroomHeaders.targetJATSOutputFormat, value: targetJATSOutputFormat)
        }
        if let digitalObjectType = digitalObjectType {
            requestHeaders.add(name: Request.pressroomHeaders.digitalObjectType, value: digitalObjectType)
        }
        if let documentProcessingLevel = documentProcessingLevel {
            requestHeaders.add(name: Request.pressroomHeaders.jatsDocumentProcessingLevel, value: documentProcessingLevel)
        }
        if let groupDOI = groupDOI {
            requestHeaders.add(name: Request.pressroomHeaders.jatsGroupDOI, value: groupDOI)
        }
        if let targetJATSVersion = targetJATSVersion {
            requestHeaders.add(name: Request.pressroomHeaders.targetJATSVersion, value: targetJATSVersion)
        }
        if let seriesCode = seriesCode {
            requestHeaders.add(name: Request.pressroomHeaders.jatsSeriesCode, value: seriesCode)
        }
        if let submissionIdentifier = submissionIdentifier {
            requestHeaders.add(name: Request.pressroomHeaders.jatsSubmissionIdentifier, value: submissionIdentifier)
        }
        if let submissionDOI = submissionDOI {
            requestHeaders.add(name: Request.pressroomHeaders.jatsSubmissionDOI, value: submissionDOI)
        }

        // Init mock request
        guard let bodyData = try? httpBodyData(for: resourceName, fileExtension: "manuproj") else {
            XCTFail("Test Request body data failed to be initialized.")
            return
        }
        let request = mockRequest(method: .POST,
                                  headers: requestHeaders,
                                  contentType: MediaType(type: "multipart",
                                                         subType: "form-data",
                                                         parameters: ["boundary": "--------------------------728980769844419903575308"]),
                                  body: bodyData)

        // Compile a JATS/HTML/DO document and verify the response when the `Future` returns successfully
        // OR handle an error depending on the `resultVerification` value
        switch resultVerification {
        case .success:
            XCTAssertNoThrow(try Self.sachsTransformationsController.compileManuscript(request)) { responseFuture in
                responseFuture.whenSuccess {[unowned self] response in

                    var outputFormat: SachsTransformationService.OutputFormat?
                    if targetJATSOutputFormat != nil {
                        outputFormat = SachsTransformationService.OutputFormat(rawValue: targetJATSOutputFormat!)
                    } else {
                        outputFormat = nil
                    }

                    var expectedContentType: MediaType = .zip
                    if outputFormat == .pdf {
                        expectedContentType = .pdf
                    }

                    let bodyDataFuture = self.verifyResponseSuccess(response, expectedContentType: expectedContentType)
                    bodyDataFuture.whenSuccess({ [unowned self] bodyData in

                        guard let processingLevel = DocumentProcessingLevel(rawValue: documentProcessingLevel ?? "full_text") else {
                            XCTFail("A valid 'DocumentProcessingLevel' should have been initialized.")
                            expectation.fulfill()
                            return
                        }

                        self.inspectSachsResult(data: bodyData,
                                                documentProcessingLevel: processingLevel,
                                                expectedOutputFormat: outputFormat,
                                                expectedJATSVersion: targetJATSVersion,
                                                expectedGroupDOI: groupDOI,
                                                expectedSubmissionIdentifier: submissionIdentifier,
                                                expectedSubmissionDOI: submissionDOI)

                        // Fulfill the 'success' case expectation
                        expectation.fulfill()
                    })
                }

                responseFuture.whenFailure { error in
                    XCTFail("No JATS XML document transformation error expected - '\(error)' thrown instead!")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureNoThrow:
            XCTAssertNoThrow(try Self.sachsTransformationsController.compileManuscript(request)) { responseFuture in
                responseFuture.whenSuccess { _ in
                    XCTFail("Failure (i.e. an error) expected, but Future<Response> was fulfilled successfully!")
                    expectation.fulfill()
                }

                responseFuture.whenFailure { error in
                    guard let pressroomError = error as? PressroomError else {
                        XCTFail("Received error was not of type 'PressroomError'")
                        // Fulfill the 'failure' case expectation
                        expectation.fulfill()
                        return
                    }
                    XCTAssertNotEqual(pressroomError.status, HTTPResponseStatus.ok)
                    XCTAssertEqual(pressroomError.typeIdentifier, "SachsTransformationsControllerError")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureThrowsError:
            XCTAssertThrowsError(try Self.sachsTransformationsController.compileManuscript(request)) { _ in
                // Fulfill the 'failure' (throwing an error) case expectation
                expectation.fulfill()
            }
        }

        // Wait for the async JATS XML compilation to complete
        wait(for: [expectation], timeout: completionTimeout!)
    }

    // swiftlint:disable:next function_parameter_count
    private func inspectSachsResult(data: Data,
                                    documentProcessingLevel: DocumentProcessingLevel,
                                    expectedOutputFormat: SachsTransformationService.OutputFormat?,
                                    expectedJATSVersion: String?,
                                    expectedGroupDOI: String?,
                                    expectedSubmissionIdentifier: String?,
                                    expectedSubmissionDOI: String?) {

        let expectedOutputFormat = expectedOutputFormat != nil ? expectedOutputFormat! : SachsTransformationService.OutputFormat.jats

        do {
            let fileExtension = expectedOutputFormat != .pdf ? "zip" : "pdf"
            let sachsResultFileURL = type(of: self).workingDirectoryFileURL.appendingPathComponent("sachs_result.\(fileExtension)")
            try data.write(to: sachsResultFileURL)
            let decompressedDocumentDirectoryFileURL = expectedOutputFormat != .pdf ?
                try self.decompressZIP(at: sachsResultFileURL) : sachsResultFileURL

            // Decompressed document filenames check (output format dependent)
            // (Dictionary of file paths keyed against if they are expected to be a directory or not)
            var decompressedDocumentFileStructure: [String: Bool]!
            switch expectedOutputFormat {
            case .literatumDO:
                decompressedDocumentFileStructure = ["manifest.xml": false,
                                                     "\(expectedSubmissionIdentifier!)/meta": true,
                                                     "\(expectedSubmissionIdentifier!)/meta/\(expectedSubmissionIdentifier!).xml": false]
            case .jats, .literatumJATS:
                if expectedSubmissionIdentifier != nil {
                    decompressedDocumentFileStructure = ["\(expectedSubmissionIdentifier!).xml": false]
                } else {
                    decompressedDocumentFileStructure = ["manuscript.xml": false]
                }
            case .gatewayBundle:
                XCTFail("IMPLEMENT THIS CASE HANDLING")
            case .html:
                decompressedDocumentFileStructure = ["index.html": false]
            case .pdf:
                decompressedDocumentFileStructure = [:]
            case .jatsBundle,
                 .wileyMLBundle:
                guard let groupIdentifierSuffix = expectedGroupDOI?.split(separator: "/").last else {
                    XCTFail("'expectedGroupDOI' should be in the format of '10.5555/123'.")
                    return
                }
                let expectedGroupIdentifier = String(groupIdentifierSuffix)
                let bundledDocumentsParentDirectory = "\(expectedGroupIdentifier)/\(expectedSubmissionIdentifier!)"
                decompressedDocumentFileStructure = [expectedGroupIdentifier: true,
                                                     "\(bundledDocumentsParentDirectory)/\(expectedSubmissionIdentifier!).pdf": false,
                                                     "\(bundledDocumentsParentDirectory)/\(expectedSubmissionIdentifier!).xml": false,
                                                     "manifest.xml": false]
            }

            var decompressedDocumentFileURLs = [URL]()
            for (expectedFileName, expectedIsDir) in decompressedDocumentFileStructure {
                decompressedDocumentFileURLs.append(decompressedDocumentDirectoryFileURL.appendingPathComponent(expectedFileName))
                var isDir: ObjCBool = false
                XCTAssert(FileManager.default.fileExists(atPath: decompressedDocumentFileURLs.last!.path,
                                                         isDirectory: &isDir) && isDir.boolValue == expectedIsDir)
            }

            var documentXML: XMLDocument!
            if expectedOutputFormat == .jats
                || expectedOutputFormat == .literatumDO
                || expectedOutputFormat == .literatumJATS
                || expectedOutputFormat == .wileyMLBundle
                || expectedOutputFormat == .jatsBundle {
                var xmlFileURL: URL!
                if expectedOutputFormat == .literatumDO
                    || expectedOutputFormat == .wileyMLBundle
                    || expectedOutputFormat == .jatsBundle {
                    xmlFileURL = decompressedDocumentFileURLs.first(where: { $0.lastPathComponent == "manifest.xml" })!
                } else {
                    xmlFileURL = decompressedDocumentFileURLs.first!
                }
                documentXML = try XMLDocument(contentsOf: xmlFileURL)
                guard documentXML.dtd != nil else {
                    XCTFail("No XML DTD definition found in '\(xmlFileURL.lastPathComponent)'.")
                    return
                }
            }

            switch expectedOutputFormat {
            case .gatewayBundle:
                XCTFail("IMPLEMENT THIS CASE HANDLING")
            case .literatumDO:
                validateLiteratumDOManifest(documentXML)
            case .jatsBundle:
                validateLiteratumDOManifest(documentXML)
                let mainXMLFileURL = decompressedDocumentFileURLs
                    .first(where: { $0.lastPathComponent == "\(expectedSubmissionIdentifier!).xml" })!
                validateJATSXMLStructure(try XMLDocument(contentsOf: mainXMLFileURL), documentProcessingLevel: documentProcessingLevel)
            case .wileyMLBundle:
                validateLiteratumDOManifest(documentXML)
                let mainXMLFileURL = decompressedDocumentFileURLs
                    .first(where: { $0.lastPathComponent == "\(expectedSubmissionIdentifier!).xml" })!
                validateWileyMLStructure(try XMLDocument(contentsOf: mainXMLFileURL), documentProcessingLevel: documentProcessingLevel)
            case .literatumJATS:

                validateJATSXMLStructure(documentXML, documentProcessingLevel: documentProcessingLevel)

                // JATS version should always be 1.2d1
                validateJATSVersionOnePointTwoDeeOne(documentXML.dtd!)

                // Also inspect the JATS XML for submission ID and DOI
                let metadata = try documentXML.nodes(forXPath: "//article/front/article-meta").first
                guard let observedIdentifier = try metadata?.nodes(forXPath: ".//article-id[@pub-id-type='publisher-id']").first else {
                    XCTFail("No '<article-id pub-id-type=\"publisher-id\">' node present in the JATS XML.")
                    return
                }
                guard let observedDOI = try metadata?.nodes(forXPath: ".//article-id[@pub-id-type='doi']").first else {
                    XCTFail("No '<article-id pub-id-type=\"doi\">' node present in the JATS XML.")
                    return
                }

                XCTAssertEqual(observedIdentifier.stringValue, expectedSubmissionIdentifier)
                XCTAssertEqual(observedDOI.stringValue, expectedSubmissionDOI)
            case .jats:

                validateJATSXMLStructure(documentXML, documentProcessingLevel: documentProcessingLevel)

                if expectedJATSVersion == "1.2d1" {
                    validateJATSVersionOnePointTwoDeeOne(documentXML.dtd!)
                } else if expectedJATSVersion == "1.1" {
                    validateJATSVersionOnePointOne(documentXML.dtd!)
                } else if expectedJATSVersion == "1.2" || expectedJATSVersion == nil {
                    validateJATSVersionOnePointTwo(documentXML.dtd!)
                }
            case .html:
                try validateHTML(at: decompressedDocumentFileURLs.first!)
            case .pdf:
                try validatePDF(at: sachsResultFileURL)
            }
        } catch {
            XCTFail("No JATS XML document transformation error expected - '\(error)' thrown instead!")
        }
    }

    private func validateLiteratumDOManifest(_ manifestXML: XMLDocument) {
        XCTAssertEqual(manifestXML.dtd!.name, "submission")
        XCTAssertEqual(manifestXML.dtd!.publicID, """
                                                  -//Atypon//DTD Literatum Content Submission Manifest DTD v4.2 \
                                                  20140519//EN
                                                  """)
        XCTAssertEqual(manifestXML.dtd!.systemID, "manifest.4.2.dtd")
    }

    private func validateJATSXMLStructure(_ jatsXML: XMLDocument,
                                          documentProcessingLevel: DocumentProcessingLevel) {
        // Front-matter should always be present
        guard let frontXML = try? jatsXML.nodes(forXPath: "//article/front") else {
            XCTFail("<front></front> node expected in exported JATS XML.")
            return
        }
        XCTAssertEqual(frontXML.count, 1)

        let bodyXML = try? jatsXML.nodes(forXPath: "//article/body")
        let backXML = try? jatsXML.nodes(forXPath: "//article/back")

        // Validate <article> content based on `documentProcessingLevel`
        switch documentProcessingLevel {
        case .fullText:
            XCTAssertNotNil(bodyXML)
            XCTAssertNotNil(backXML)
            XCTAssertEqual(bodyXML?.count, 1)
            XCTAssertEqual(backXML?.count, 1)
        case .frontMatterOnly:
            XCTAssertNotNil(bodyXML)
            XCTAssertNotNil(backXML)
            XCTAssertEqual(bodyXML?.count, 0)
            XCTAssertEqual(backXML?.count, 0)
        }
    }

    private func validateWileyMLStructure(_ wileyML: XMLDocument,
                                          documentProcessingLevel: DocumentProcessingLevel) {
        // Front-matter should always be present
        guard let headerXML = try? wileyML.nodes(forXPath: "//component/header") else {
            XCTFail("<header></header> node expected in exported WileyML.")
            return
        }
        XCTAssertEqual(headerXML.count, 1)

        let bodyXML = try? wileyML.nodes(forXPath: "//component/body")

        // Validate <article> content based on `documentProcessingLevel`
        switch documentProcessingLevel {
        case .fullText:
            XCTAssertNotNil(bodyXML)
            XCTAssertEqual(bodyXML?.count, 1)
        case .frontMatterOnly:
            XCTAssertNotNil(bodyXML)
            XCTAssertEqual(bodyXML?.count, 0)
        }
    }

    private func validateJATSVersionOnePointOne(_ dtd: XMLDTD) {
        XCTAssertEqual(dtd.name, "article")
        XCTAssertEqual(dtd.publicID, """
                                     -//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD \
                                     with OASIS Tables with MathML3 v1.1 20151215//EN
                                     """)
        XCTAssertEqual(dtd.systemID, "http://jats.nlm.nih.gov/archiving/1.1/JATS-archive-oasis-article1-mathml3.dtd")
    }

    private func validateJATSVersionOnePointTwo(_ dtd: XMLDTD) {
        XCTAssertEqual(dtd.name, "article")
        XCTAssertEqual(dtd.publicID, """
                                     -//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD with \
                                     OASIS Tables with MathML3 v1.2 20190208//EN
                                     """)
        XCTAssertEqual(dtd.systemID, "http://jats.nlm.nih.gov/archiving/1.2/JATS-archive-oasis-article1-mathml3.dtd")
    }

    private func validateJATSVersionOnePointTwoDeeOne(_ dtd: XMLDTD) {
        XCTAssertEqual(dtd.name, "article")
        XCTAssertEqual(dtd.publicID, """
                                     -//NLM//DTD JATS (Z39.96) Journal Archiving and Interchange DTD with \
                                     OASIS Tables with MathML3 v1.2d1 20170631//EN
                                     """)
        XCTAssertEqual(dtd.systemID, "http://jats.nlm.nih.gov/archiving/1.2d1/JATS-archive-oasis-article1-mathml3.dtd")
    }

    private func validateHTML(at fileURL: URL) throws {
        try validateBinaryFile(at: fileURL, expectedUTI: "public.html", expectedFileSize: 873)
    }

    private func validatePDF(at fileURL: URL) throws {
        try validateBinaryFile(at: fileURL, expectedUTI: "com.adobe.pdf")
    }

    private func validateBinaryFile(at fileURL: URL,
                                    expectedUTI: String,
                                    expectedFileSize: Int? = nil) throws {
        let values = try fileURL.resourceValues(forKeys: [URLResourceKey.fileSizeKey,
                                                          URLResourceKey.typeIdentifierKey])
        guard let fileSize = values.fileSize,
            let uti = values.typeIdentifier else {
                XCTFail("File size or UTI attributes could not be determined for the decompressed 'index.html' file.")
                return
        }

        if let expectedFileSize = expectedFileSize {
            XCTAssertEqual(expectedFileSize, fileSize)
        } else {
            XCTAssertGreaterThan(fileSize, 0)
        }
        XCTAssertEqual(uti, expectedUTI)
    }

    private func decompressZIP(at zipFileURL: URL) throws -> URL {
        let decompressedFileURL = type(of: self).workingDirectoryFileURL.appendingPathComponent("decompressed")
        try FileManager.default.createDirectory(at: decompressedFileURL,
                                                withIntermediateDirectories: true,
                                                attributes: nil)
        try MPDecompressor.decompressZIP(at: zipFileURL, to: decompressedFileURL)

        return decompressedFileURL
    }

    // MARK: - All tests

    static let allTests = [
        ("testEndToEndMissingJATSOutputFormat", testEndToEndMissingJATSOutputFormat),
        ("testEndToEndInvalidJATSOutputFormat", testEndToEndInvalidJATSOutputFormat),
        ("testEndToEndMissingDocumentProcessingLevel", testEndToEndMissingDocumentProcessingLevel),
        ("testEndToEndInvalidDocumentProcessingLevel", testEndToEndInvalidDocumentProcessingLevel),
        ("testEndToEndLiteratumJATSMissingSubmissionIdentifier", testEndToEndLiteratumJATSMissingSubmissionIdentifier),
        ("testEndToEndLiteratumJATSMissingSubmissionDOI", testEndToEndLiteratumJATSMissingSubmissionDOI),
        ("testEndToEndLiteratumDOMissingSubmissionIdentifier", testEndToEndLiteratumDOMissingSubmissionIdentifier),
        ("testEndToEndLiteratumDOMissingSubmissionDOI", testEndToEndLiteratumDOMissingSubmissionDOI),
        ("testEndToEndLiteratumDOMissingDigitalObjectType", testEndToEndLiteratumDOMissingDigitalObjectType),
        ("testEndToEndInvalidContainer", testEndToEndInvalidContainer),
        ("testEndToEndLiteratumDOExport", testEndToEndLiteratumDOExport),
        ("testEndToEndJATSExportFullText", testEndToEndJATSExportFullText),
        ("testEndToEndJATSExportFrontMatterOnly", testEndToEndJATSExportFrontMatterOnly),
        ("testEndToEndLiteratumJATSExport", testEndToEndLiteratumJATSExport),
        ("testEndToEndWileyMLBundleExport", testEndToEndWileyMLBundleExport),
        ("testEndToEndJATSBundleExport", testEndToEndJATSBundleExport),
        ("testEndToEndHTMLExport", testEndToEndHTMLExport)
    ]
}
