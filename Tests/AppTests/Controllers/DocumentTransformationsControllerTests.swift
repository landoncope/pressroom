//
//  DocumentTransformationsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 29/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

final class DocumentTransformationsControllerTests: XCTestCase, EndpointMockable, BodyDataMockable {

    // MARK: - Static variables

    static var app: Application!
    static var testBundle: Bundle!
    static let mockAPIKey = "PLACEHOLDER-API-KEY"
    static var documentTransformationsController: DocumentTransformationsController!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        documentTransformationsController = DocumentTransformationsController()
        // Ensure initial state is configured correctly before use
        DocumentTransformationsController.configureManuscriptsDependenciesInitialState()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)

        // Ensure initial state of `app` is configured
        // swiftlint:disable:next force_try
        try! boot(app)

        // Init testBundle
        testBundle = Bundle(for: self)

        // Swizzle `Bundle` methods (to return correct `Bundle` when running from unit tests)
        Bundle.swizzleMainBundle()
        Bundle.swizzleAppBundle()
        Bundle.swizzlePressRoomBundle()
    }

    override class func tearDown() {
        app = nil
        testBundle = nil
        documentTransformationsController = nil

        // Swizzle `Bundle` methods again (to return their implementations to original state)
        Bundle.swizzleMainBundle()
        Bundle.swizzleAppBundle()
        Bundle.swizzlePressRoomBundle()

        super.tearDown()
    }

    // MARK: - Test cases

    func testEndToEndHTML() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.tex").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(targetFileExtension: "html",
                     resourceName: "single_index_tex",
                     expectedContentType: .zip,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndLaTeX() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.md").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(targetFileExtension: "tex",
                     resourceName: "single_index_md",
                     expectedContentType: .zip,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndProjectBundleObjectIDRegeneration() {
        let expectedResponseHeaders: HTTPHeaders = [Response.pressroomHeaders.projectBundleObjectIDsRegenerated.stringValue: "true"]
        endToEndTest(resourceName: "example_project_bundle",
                     resourceExtension: "manuproj",
                     additionalHeaders: [Request.pressroomHeaders.regenerateProjectBundleObjectIDs.stringValue: "true"],
                     expectedContentType: .zip,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndProjectBundleImplicit() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.tex").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(resourceName: "single_index_tex",
                     expectedContentType: .zip,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndProjectBundleExplicit() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.tex").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(targetFileExtension: "manuproj",
                     resourceName: "single_index_tex",
                     expectedContentType: .zip,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndProjectBundleFromRTF() {
        endToEndTest(targetFileExtension: "manuproj",
        resourceName: "example",
        resourceExtension: "rtf",
        expectedContentType: .zip)
    }

    func testEndToEndMarkdown() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.tex").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(targetFileExtension: "md",
                     resourceName: "single_index_tex",
                     expectedContentType: .zip,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndMicrosoftWord() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.tex").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(targetFileExtension: "docx",
                     resourceName: "single_index_tex",
                     expectedContentType: .fileExtension("docx")!,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndPDF() {
        let headerKey = Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue
        let headerValue = PrimaryFileCandidate.SelectionCriterion.indexFile(fileName: "index.tex").headerValue()
        let expectedResponseHeaders: HTTPHeaders = [headerKey: headerValue]
        endToEndTest(targetFileExtension: "pdf",
                     resourceName: "single_index_tex",
                     expectedContentType: .pdf,
                     expectedResponseHeaders: expectedResponseHeaders)
    }

    func testEndToEndPDFFromRTF() {
        endToEndTest(targetFileExtension: "pdf",
                     resourceName: "example",
                     resourceExtension: "rtf",
                     expectedContentType: .pdf)
    }

    func testEndToEndImportFailureWithoutHeaderOverride() {
        endToEndTest(targetFileExtension: "manuproj",
                     resourceName: "pandoc_failing_manuscript",
                     resourceExtension: "zip",
                     additionalHeaders: [Request.pressroomHeaders.enrichDocumentMetadata.stringValue: "true"],
                     expectedContentType: .zip,
                     resultVerification: .failureNoThrow)
    }

    func testEndToEndImportSuccessWithHeaderOverride() {
        endToEndTest(targetFileExtension: "manuproj",
                     resourceName: "pandoc_failing_manuscript",
                     resourceExtension: "zip",
                     additionalHeaders: [Request.pressroomHeaders.enrichDocumentMetadata.stringValue: "true",
                                         Request.pressroomHeaders.continueOnErrors.stringValue: "true"],
                     expectedContentType: .zip)
    }

    // MARK: - Private methods

    // swiftlint:disable:next function_body_length
    private func endToEndTest(targetFileExtension: String? = nil,
                              resourceName: String,
                              resourceExtension: String = "zip",
                              additionalHeaders: HTTPHeaders? = nil,
                              expectedContentType: MediaType,
                              expectedResponseHeaders: HTTPHeaders? = nil,
                              resultVerification: EndpointMockableResultVerification = .success,
                              completionTimeout: Double? = 30.0) {

        let expectationDescription = "endToEndTest_\(targetFileExtension ?? "nil_targetFileExtension")"
        let expectation = XCTestExpectation(description: expectationDescription)

        // The `mockAPIKey` is set as a request header in all cases
        var requestHeaders: HTTPHeaders = [Request.pressroomHeaders.apiKey.stringValue: type(of: self).mockAPIKey]

        // Add 'Pressroom-Target-File-Extension' header if `targetFileExtension` is set
        if let targetFileExtension = targetFileExtension {
            requestHeaders.add(name: Request.pressroomHeaders.targetFileExtension, value: targetFileExtension)
        }

        // Add any `additionalHeaders` that are present
        if let additionalHeaders = additionalHeaders {
            for (header, value) in additionalHeaders {
                requestHeaders.add(name: header, value: value)
            }
        }

        // Init mock request
        guard let bodyData = try? httpBodyData(for: resourceName, fileExtension: resourceExtension) else {
            XCTFail("Test Request body data failed to be initialized.")
            return
        }
        let request = mockRequest(method: .POST,
                                  headers: requestHeaders,
                                  contentType: MediaType(type: "multipart",
                                                         subType: "form-data",
                                                         parameters: ["boundary": "--------------------------728980769844419903575308"]),
                                  body: bodyData)

        // Compile manuscript and verify the response when the `Future` returns successfully
        // OR handle an error depending on the `resultVerification` value
        switch resultVerification {
        case .success:
            XCTAssertNoThrow(try type(of: self).documentTransformationsController.compileManuscript(request)) { responseFuture in
                responseFuture.whenSuccess {[unowned self] response in
                    let bodyDataFuture = self.verifyResponseSuccess(response,
                                                                    expectedContentType: expectedContentType,
                                                                    expectedResponseHeaders: expectedResponseHeaders)
                    bodyDataFuture.whenSuccess({ _ in
                        // Fulfill the 'success' case expectation
                        expectation.fulfill()
                    })
                }

                responseFuture.whenFailure { error in
                    XCTFail("No document transformation error expected - '\(error)' thrown instead!")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureNoThrow:
            XCTAssertNoThrow(try type(of: self).documentTransformationsController.compileManuscript(request)) { responseFuture in
                responseFuture.whenSuccess { _ in
                    XCTFail("Failure (i.e. an error) expected, but Future<Response> was fulfilled successfully!")
                    expectation.fulfill()
                }

                responseFuture.whenFailure { error in
                    guard let pressroomError = error as? PressroomError else {
                        XCTFail("Received error was not of type 'PressroomError'")
                        // Fulfill the 'failure' case expectation
                        expectation.fulfill()
                        return
                    }
                    XCTAssertNotEqual(pressroomError.status, HTTPResponseStatus.ok)
                    XCTAssertEqual(pressroomError.typeIdentifier, "DocumentTransformationsControllerError")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureThrowsError:
            XCTAssertThrowsError(try type(of: self).documentTransformationsController.compileManuscript(request)) { _ in
                // Fulfill the 'failure' (throwing an error) case expectation
                expectation.fulfill()
            }
        }

        // Wait for the async document compilation to complete
        wait(for: [expectation], timeout: completionTimeout!)
    }

    // MARK: - All tests

    static let allTests = [
        ("testEndToEndHTML", testEndToEndHTML),
        ("testEndToEndLaTeX", testEndToEndLaTeX),
        ("testEndToEndProjectBundleObjectIDRegeneration", testEndToEndProjectBundleObjectIDRegeneration),
        ("testEndToEndProjectBundleImplicit", testEndToEndProjectBundleImplicit),
        ("testEndToEndProjectBundleExplicit", testEndToEndProjectBundleExplicit),
        ("testEndToEndMarkdown", testEndToEndMarkdown),
        ("testEndToEndMicrosoftWord", testEndToEndMicrosoftWord),
        ("testEndToEndPDF", testEndToEndPDF),
        ("testEndToEndPDFFromRTF", testEndToEndPDFFromRTF),
        ("testEndToEndImportFailureWithoutHeaderOverride", testEndToEndImportFailureWithoutHeaderOverride),
        ("testEndToEndImportSuccessWithHeaderOverride", testEndToEndImportSuccessWithHeaderOverride)
    ]
}
