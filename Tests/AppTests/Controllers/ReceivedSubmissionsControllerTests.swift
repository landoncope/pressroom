//
//  ReceivedSubmissionsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 27/04/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

// swiftlint:disable file_length type_body_length

final class ReceivedSubmissionsControllerTests: XCTestCase, EndpointMockable, BodyDataMockable {

    static var app: Application!
    static var testBundle: Bundle!
    static var mockAPIKey = "PLACEHOLDER-API-KEY"
    static var receivedSubmissionsController: ReceivedSubmissionsController!

    private let validScholarOneEEOSubmissionJSONData = """
    {
        "depositoryCode": "S1",
        "importEntryId": 1,
        "metadata": {
            "doi": "10.5555/123",
            "manuscriptId": "QUA-2019-0146",
            "manuscriptTitle": "Difference between symmetric division deg index and geometric--arithmetic index",
            "journalTitle": "International Journal of Quantum Chemistry",
            "journalAbbreviation": "QUA",
            "digitalISSN": "1097-461X",
            "submissionStatus": "Submitted",
            "keywords": [
                "symmetric division deg index",
                "geometric--arithmetic index",
                "tree",
                "unicyclic graph",
                "bicyclic graph"
            ],
            "authors": [
                {
                    "salutation": "Dr.",
                    "firstName": "Yingui",
                    "lastName": "Pan",
                    "email": "panygui@163.com",
                    "isCorresponding": true,
                    "affiliations": [
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematics ",
                            "personTitle": "",
                            "room": "",
                            "phone": "18810965681",
                            "fax": "",
                            "address": [
                                "Sanyi street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematics ",
                            "phone": "18810965681",
                            "address": [
                                "Sanyi street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematics ",
                            "personTitle": "",
                            "room": "",
                            "phone": "",
                            "fax": "",
                            "address": [
                                "Sanyi street",
                                "",
                                ""
                            ]
                        }
                    ]
                },
                {
                    "salutation": "Dr.",
                    "firstName": "Jianping",
                    "lastName": "Li",
                    "email": "lijianping65@nudt.edu.cn",
                    "isCorresponding": false,
                    "affiliations": [
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematic",
                            "personTitle": "",
                            "room": "",
                            "phone": "13975899065",
                            "fax": "",
                            "address": [
                                "Sanyi Street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematic",
                            "personTitle": "",
                            "room": "",
                            "phone": "13975899065",
                            "fax": "",
                            "address": [
                                "Sanyi Street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematic",
                            "personTitle": "",
                            "room": "",
                            "phone": "13975899065",
                            "fax": "",
                            "address": [
                                "Sanyi Street",
                                "",
                                ""
                            ]
                        }
                    ]
                }
            ]
        },
        "attachments": [
            {
                "name": "example.docx",
                "format": "docx",
                "designation": "Main Document",
                "url": "https://file-examples.com/wp-content/uploads/2017/02/file-sample_100kB.docx",
                "urlValiditySeconds": ""
            },
            {
                "name": "article.xml",
                "format": "xml",
                "designation": "Article JATS",
                "url": "https://jats.nlm.nih.gov/publishing/tag-library/1.2/FullArticleSamples/bmj_sample.xml",
                "urlValiditySeconds": ""
            },
            {
                "name": "abstract-image.pdf",
                "format": "pdf",
                "designation": "Graphical Abstract Text and Image",
                "url": "https://aayyad:17070/v1/api/eeo/import/download/1?file=abstract-image.pdf",
                "urlValiditySeconds": ""
            },
            {
                "name": "gradient.png",
                "format": "png",
                "designation": "Image",
                "url": "https://www.fnordware.com/superpng/pnggrad8rgb.png",
                "urlValiditySeconds": ""
            },
            {
                "name": "halflife.jpg",
                "format": "jpg",
                "designation": "Image",
                "url": "https://upload.wikimedia.org/wikipedia/commons/8/88/Half-life_example_graph.jpg",
                "urlValiditySeconds": ""
            }
        ]
    }
    """.data(using: .utf8)!

    private let validAuthoreaEEOSubmissionJSONData = """
    {
        "depositoryCode": "authorea",
        "importEntryId": 1,
        "metadata": {
            "doi": "10.5555/123",
            "manuscriptId": "QUA-2019-0146",
            "manuscriptTitle": "Difference between symmetric division deg index and geometric--arithmetic index",
            "journalTitle": "International Journal of Quantum Chemistry",
            "journalAbbreviation": "QUA",
            "digitalISSN": "1097-461X",
            "submissionStatus": "Submitted",
            "keywords": [
                "symmetric division deg index",
                "geometric--arithmetic index",
                "tree",
                "unicyclic graph",
                "bicyclic graph"
            ],
            "authors": [
                {
                    "salutation": "Dr.",
                    "firstName": "Yingui",
                    "lastName": "Pan",
                    "email": "panygui@163.com",
                    "isCorresponding": true,
                    "affiliations": [
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematics ",
                            "personTitle": "",
                            "room": "",
                            "phone": "18810965681",
                            "fax": "",
                            "address": [
                                "Sanyi street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematics ",
                            "phone": "18810965681",
                            "address": [
                                "Sanyi street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematics ",
                            "personTitle": "",
                            "room": "",
                            "phone": "",
                            "fax": "",
                            "address": [
                                "Sanyi street",
                                "",
                                ""
                            ]
                        }
                    ]
                },
                {
                    "salutation": "Dr.",
                    "firstName": "Jianping",
                    "lastName": "Li",
                    "email": "lijianping65@nudt.edu.cn",
                    "isCorresponding": false,
                    "affiliations": [
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematic",
                            "personTitle": "",
                            "room": "",
                            "phone": "13975899065",
                            "fax": "",
                            "address": [
                                "Sanyi Street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematic",
                            "personTitle": "",
                            "room": "",
                            "phone": "13975899065",
                            "fax": "",
                            "address": [
                                "Sanyi Street",
                                "",
                                ""
                            ]
                        },
                        {
                            "institutionId": "58294",
                            "institutionType": "58294",
                            "institutionName": "National University of Defense Technology",
                            "city": "Changsha",
                            "zipCode": "410073",
                            "state": "Hunan",
                            "country": "China",
                            "department": "Mathematic",
                            "personTitle": "",
                            "room": "",
                            "phone": "13975899065",
                            "fax": "",
                            "address": [
                                "Sanyi Street",
                                "",
                                ""
                            ]
                        }
                    ]
                }
            ]
        },
        "attachments": [
            {
                "name": "example.docx",
                "format": "docx",
                "designation": "Main Document",
                "url": "https://file-examples.com/wp-content/uploads/2017/02/file-sample_100kB.docx",
                "urlValiditySeconds": ""
            },
            {
                "name": "article.xml",
                "format": "xml",
                "designation": "Article JATS",
                "url": "https://jats.nlm.nih.gov/publishing/tag-library/1.2/FullArticleSamples/bmj_sample.xml",
                "urlValiditySeconds": ""
            },
            {
                "name": "abstract-image.pdf",
                "format": "pdf",
                "designation": "Graphical Abstract Text and Image",
                "url": "https://aayyad:17070/v1/api/eeo/import/download/1?file=abstract-image.pdf",
                "urlValiditySeconds": ""
            },
            {
                "name": "gradient.png",
                "format": "png",
                "designation": "Image",
                "url": "https://www.fnordware.com/superpng/pnggrad8rgb.png",
                "urlValiditySeconds": ""
            },
            {
                "name": "halflife.jpg",
                "format": "jpg",
                "designation": "Image",
                "url": "https://upload.wikimedia.org/wikipedia/commons/8/88/Half-life_example_graph.jpg",
                "urlValiditySeconds": ""
            }
        ]
    }
    """.data(using: .utf8)!

    private let invalidEEOSubmissionJSONData = """
    {
        "blort": "blart"
    }
    """.data(using: .utf8)!

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        receivedSubmissionsController = ReceivedSubmissionsController()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)

        // Ensure initial state of `app` is configured
        // swiftlint:disable:next force_try
        try! boot(app)

        // Init testBundle
        testBundle = Bundle(for: self)
    }

    override class func tearDown() {
        app = nil
        testBundle = nil
        receivedSubmissionsController = nil

        super.tearDown()
    }

    // MARK: - JSON decoding test cases

    func testValidScholarOneEEOSubmissionJSONDecodingSuccess() {
        XCTAssertNotNil(validScholarOneEEOSubmissionJSONData,
                        "Valid EEO Submission JSON data representation should not be 'nil'!")

        do {
            let decodedSubmission = try JSONDecoder().decode(EEOReceivedSubmission.self,
                                                             from: validScholarOneEEOSubmissionJSONData)
            XCTAssertNotNil(decodedSubmission, "The decoded EEO Received Submission should not be nil!")
            XCTAssertEqual(decodedSubmission.attachments.count, 5)
            XCTAssertEqual(decodedSubmission.metadata.keywords, ["symmetric division deg index",
                                                                  "geometric--arithmetic index",
                                                                  "tree",
                                                                  "unicyclic graph",
                                                                  "bicyclic graph"])
            XCTAssertEqual(decodedSubmission.metadata.authors.count, 2)
            XCTAssertEqual(decodedSubmission.metadata.authors[0].affiliations.count, 3)
            XCTAssertEqual(decodedSubmission.metadata.authors[1].affiliations.count, 3)
        } catch {
            XCTFail("No JSON decoding error expected - '\(error)' thrown instead!")
        }
    }

    func testValidAuthoreaEEOSubmissionJSONDecodingSuccess() {
        XCTAssertNotNil(validAuthoreaEEOSubmissionJSONData,
                        "Valid EEO Submission JSON data representation should not be 'nil'!")

        do {
            let decodedSubmission = try JSONDecoder().decode(EEOReceivedSubmission.self,
                                                             from: validAuthoreaEEOSubmissionJSONData)
            XCTAssertNotNil(decodedSubmission, "The decoded EEO Received Submission should not be nil!")
            XCTAssertEqual(decodedSubmission.attachments.count, 5)
            XCTAssertEqual(decodedSubmission.metadata.keywords, ["symmetric division deg index",
                                                                  "geometric--arithmetic index",
                                                                  "tree",
                                                                  "unicyclic graph",
                                                                  "bicyclic graph"])
            XCTAssertEqual(decodedSubmission.metadata.authors.count, 2)
            XCTAssertEqual(decodedSubmission.metadata.authors[0].affiliations.count, 3)
            XCTAssertEqual(decodedSubmission.metadata.authors[1].affiliations.count, 3)
        } catch {
            XCTFail("No JSON decoding error expected - '\(error)' thrown instead!")
        }
    }

    func testInvalidEEOSubmissionJSONDecodingError() {
        XCTAssertNotNil(invalidEEOSubmissionJSONData, "Invalid EEO Submission JSON  data representation should not be 'nil'!")

        XCTAssertThrowsError(try JSONDecoder().decode(EEOReceivedSubmission.self, from: invalidEEOSubmissionJSONData),
                             "Decoding invalid EEO Submission JSON should result in an error!") { error in
            XCTAssert((error as? DecodingError) != nil, "The error should be a DecodingError, got '\(error) instead!'")
        }
    }

    // MARK: - Transformation test cases

    // MARK: - All tests

    static let allTests = [
        ("testValidScholarOneEEOSubmissionJSONDecodingSuccess", testValidScholarOneEEOSubmissionJSONDecodingSuccess),
        ("testValidAuthoreaEEOSubmissionJSONDecodingSuccess", testValidAuthoreaEEOSubmissionJSONDecodingSuccess),
        ("testInvalidEEOSubmissionJSONDecodingError", testInvalidEEOSubmissionJSONDecodingError)
    ]
}
