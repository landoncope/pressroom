//
//  BibliographyTransformationsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 29/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

// swiftlint:disable type_body_length
// swiftlint:disable type_name
// swiftlint:disable line_length
// swiftlint:disable file_length
final class BibliographyTransformationsControllerTests: XCTestCase, EndpointMockable {

    // MARK: - Static and private variables

    static var app: Application!
    static let mockAPIKey = "PLACEHOLDER-API-KEY"
    static var bibliographyTransformationsController: BibliographyTransformationsController!
    private let validCSLJSON = """
    [
        {
            "id": "2656243/9G79BL7D",
            "type": "article-journal",
            "title": "Some Notes on Gertrude Stein and Deixis",
            "container-title": "Arizona Quarterly: A Journal of American Literature, Culture, and Theory",
            "page": "91-102",
            "volume": "53",
            "issue": "1",
            "URL": "https://muse-jhu-edu.pitt.idm.oclc.org/article/445403/pdf",
            "DOI": "10.1353/arq.1997.0020",
            "author": [
                {
                    "family": "Cook",
                    "given": "Albert"
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        1997
                    ]
                ]
            }
        },
        {
            "id": "2656243/J8NNQ8A9",
            "type": "article-journal",
            "title": "Semantic Satiation and Cognitive Dynamics",
            "container-title": "The Journal of Special Education",
            "page": "35-44",
            "volume": "2",
            "issue": "1",
            "URL": "http://journals.sagepub.com/doi/10.1177/002246696700200103",
            "DOI": "10.1177/002246696700200103",
            "journalAbbreviation": "J Spec Educ",
            "language": "en",
            "author": [
                {
                    "family": "Jakobovits",
                    "given": "Leon A."
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        "1967",
                        10,
                        1
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        2,
                        8
                    ]
                ]
            }
        },
        {
            "id": "2656243/PJZSJ24B",
            "type": "article-journal",
            "title": "Introduction to WordNet: An On-line Lexical Database",
            "container-title": "International Journal of Lexicography",
            "page": "235-244",
            "volume": "3",
            "issue": "4",
            "URL": "http://wordnetcode.princeton.edu/5papers.pdf",
            "DOI": "10.1093/ijl/3.4.235",
            "shortTitle": "Introduction to WordNet",
            "language": "en",
            "author": [
                {
                    "family": "Miller",
                    "given": "George A."
                },
                {
                    "family": "Beckwith",
                    "given": "Richard"
                },
                {
                    "family": "Fellbaum",
                    "given": "Christiane"
                },
                {
                    "family": "Gross",
                    "given": "Derek"
                },
                {
                    "family": "Miller",
                    "given": "Katherine J."
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        1990
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        4,
                        12
                    ]
                ]
            }
        },
        {
            "id": "2656243/YY8DHXB9",
            "type": "article-journal",
            "title": "A COMPREHENSIVE ANALYSIS OF USING WORDNET, PART-OF-SPEECH TAGGING, AND WORD SENSE DISAMBIGUATION IN TEXT CATEGORIZATION",
            "page": "94",
            "URL": "https://www.cmpe.boun.edu.tr/~gungort/theses/A%20Comprehensive%20Analysis%20of%20Using%20Wordnet,%20Part-of-speech%20Tagging,%20and%20Word%20Sense%20Disambiguation%20in%20Text%20Categorization.pdf",
            "language": "en",
            "author": [
                {
                    "family": "Celik",
                    "given": "Kerem"
                }
            ]
        },
        {
            "id": "2656243/EYRWTTXJ",
            "type": "article-journal",
            "title": "Jesse Norman, ed., Edmund Burke: Reflections on the Revolution in France and Other Writings",
            "container-title": "Society",
            "page": "377-382",
            "volume": "54",
            "issue": "4",
            "URL": "http://link.springer.com/article/10.1007/s12115-017-0159-0",
            "DOI": "10.1007/s12115-017-0159-0",
            "shortTitle": "Jesse Norman, ed., Edmund Burke",
            "journalAbbreviation": "Soc",
            "language": "en",
            "author": [
                {
                    "family": "Weiner",
                    "given": "G."
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        2017,
                        8,
                        1
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        4,
                        23
                    ]
                ]
            }
        },
        {
            "id": "2656243/L4GBBQZR",
            "type": "article-journal",
            "title": "Truth and Fiction in Tim O'Brien's 'If I Die in a Combat Zone' and 'The Things They Carried'",
            "container-title": "College Literature",
            "page": "1-18",
            "volume": "29",
            "issue": "2",
            "URL": "http://www.jstor.org/stable/25112634",
            "author": [
                {
                    "family": "Wesley",
                    "given": "Marilyn"
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        2002
                    ]
                ]
            },
            "accessed": {
                "date-parts": [
                    [
                        2018,
                        4,
                        17
                    ]
                ]
            }
        },
        {
            "id": "2656243/V7HV24YA",
            "type": "article-journal",
            "title": "Simulacra and Simulations",
            "page": "22",
            "language": "en",
            "author": [
                {
                    "family": "Baudrillard",
                    "given": "Jean"
                }
            ]
        }
    ]
    """
    private let validBibTeX = """
    @article{Baudrillard, title={Simulacra and Simulations}, author={Baudrillard, Jean}, pages={22}}
    @article{Celik, title={A COMPREHENSIVE ANALYSIS OF USING WORDNET, PART-OF-SPEECH TAGGING, AND WORD SENSE DISAMBIGUATION IN TEXT CATEGORIZATION}, url={https://www.cmpe.boun.edu.tr/~gungort/theses/A%20Comprehensive%20Analysis%20of%20Using%20Wordnet,%20Part-of-speech%20Tagging,%20and%20Word%20Sense%20Disambiguation%20in%20Text%20Categorization.pdf}, author={Celik, Kerem}, pages={94}}
    @article{Cook, title={Some Notes on Gertrude Stein and Deixis}, volume={53}, url={https://muse-jhu-edu.pitt.idm.oclc.org/article/445403/pdf}, DOI={10.1353/arq.1997.0020}, number={1}, author={Cook, Albert}, pages={91–102}}
    @article{Jakobovits, title={Semantic Satiation and Cognitive Dynamics}, volume={2}, url={http://journals.sagepub.com/doi/10.1177/002246696700200103}, DOI={10.1177/002246696700200103}, number={1}, author={Jakobovits, Leon A.}, pages={35–44}}
    @article{Miller_Beckwith_Fellbaum_Gross_Miller, title={Introduction to WordNet: An On-line Lexical Database}, volume={3}, url={http://wordnetcode.princeton.edu/5papers.pdf}, DOI={10.1093/ijl/3.4.235}, number={4}, author={Miller, George A. and Beckwith, Richard and Fellbaum, Christiane and Gross, Derek and Miller, Katherine J.}, pages={235–244}}
    @article{Weiner, title={Jesse Norman, ed., Edmund Burke: Reflections on the Revolution in France and Other Writings}, volume={54}, url={http://link.springer.com/article/10.1007/s12115-017-0159-0}, DOI={10.1007/s12115-017-0159-0}, number={4}, author={Weiner, G.}, pages={377–382}}
    @article{Wesley, title={Truth and Fiction in Tim O’Brien’s “If I Die in a Combat Zone” and “The Things They Carried”}, volume={29}, url={http://www.jstor.org/stable/25112634}, number={2}, author={Wesley, Marilyn}, pages={1–18}}
    """
    private let validEndNote = """
    %0 Journal Article
    %T Simulacra and Simulations
    %A Baudrillard, Jean
    %F Baudrillard
    %9 journal article
    %P 22

    %0 Journal Article
    %T A COMPREHENSIVE ANALYSIS OF USING WORDNET, PART-OF-SPEECH TAGGING, AND WORD SENSE DISAMBIGUATION IN TEXT CATEGORIZATION
    %A Celik, Kerem
    %F Celik
    %9 journal article
    %U https://www.cmpe.boun.edu.tr/~gungort/theses/A%20Comprehensive%20Analysis%20of%20Using%20Wordnet,%20Part-of-speech%20Tagging,%20and%20Word%20Sense%20Disambiguation%20in%20Text%20Categorization.pdf
    %P 94

    %0 Journal Article
    %T Some Notes on Gertrude Stein and Deixis
    %A Cook, Albert
    %V 53
    %N 1
    %F Cook
    %9 journal article
    %U https://muse-jhu-edu.pitt.idm.oclc.org/article/445403/pdf
    %U http://dx.doi.org/10.1353/arq.1997.0020
    %P 91-102

    %0 Journal Article
    %T Semantic Satiation and Cognitive Dynamics
    %A Jakobovits, Leon A.
    %V 2
    %N 1
    %F Jakobovits
    %9 journal article
    %U http://journals.sagepub.com/doi/10.1177/002246696700200103
    %U http://dx.doi.org/10.1177/002246696700200103
    %P 35-44

    %0 Journal Article
    %T Introduction to WordNet: An On-line Lexical Database
    %A Miller, George A.
    %A Beckwith, Richard
    %A Fellbaum, Christiane
    %A Gross, Derek
    %A Miller, Katherine J.
    %V 3
    %N 4
    %F Miller_Beckwith_Fellbaum_Gross_Miller
    %9 journal article
    %U http://wordnetcode.princeton.edu/5papers.pdf
    %U http://dx.doi.org/10.1093/ijl/3.4.235
    %P 235-244

    %0 Journal Article
    %T Jesse Norman, ed., Edmund Burke: Reflections on the Revolution in France and Other Writings
    %A Weiner, G.
    %V 54
    %N 4
    %F Weiner
    %9 journal article
    %U http://link.springer.com/article/10.1007/s12115-017-0159-0
    %U http://dx.doi.org/10.1007/s12115-017-0159-0
    %P 377-382

    %0 Journal Article
    %T Truth and Fiction in Tim O’Brien’s “If I Die in a Combat Zone” and “The Things They Carried”
    %A Wesley, Marilyn
    %V 29
    %N 2
    %F Wesley
    %9 journal article
    %U http://www.jstor.org/stable/25112634
    %P 1-18
    """
    private let validRIS = """
    TY  - JOUR
    AU  - Baudrillard, Jean
    TI  - Simulacra and Simulations
    SP  - 22
    ID  - Baudrillard
    ER  -
    TY  - JOUR
    AU  - Celik, Kerem
    TI  - A COMPREHENSIVE ANALYSIS OF USING WORDNET, PART-OF-SPEECH TAGGING, AND WORD SENSE DISAMBIGUATION IN TEXT CATEGORIZATION
    SP  - 94
    UR  - https://www.cmpe.boun.edu.tr/~gungort/theses/A%20Comprehensive%20Analysis%20of%20Using%20Wordnet,%20Part-of-speech%20Tagging,%20and%20Word%20Sense%20Disambiguation%20in%20Text%20Categorization.pdf
    ID  - Celik
    ER  -
    TY  - JOUR
    AU  - Cook, Albert
    TI  - Some Notes on Gertrude Stein and Deixis
    SP  - 91
    EP  - 102
    VL  - 53
    IS  - 1
    UR  - https://muse-jhu-edu.pitt.idm.oclc.org/article/445403/pdf
    DO  - 10.1353/arq.1997.0020
    ID  - Cook
    ER  -
    TY  - JOUR
    AU  - Jakobovits, Leon A.
    TI  - Semantic Satiation and Cognitive Dynamics
    SP  - 35
    EP  - 44
    VL  - 2
    IS  - 1
    UR  - http://journals.sagepub.com/doi/10.1177/002246696700200103
    DO  - 10.1177/002246696700200103
    ID  - Jakobovits
    ER  -
    TY  - JOUR
    AU  - Miller, George A.
    AU  - Beckwith, Richard
    AU  - Fellbaum, Christiane
    AU  - Gross, Derek
    AU  - Miller, Katherine J.
    TI  - Introduction to WordNet: An On-line Lexical Database
    SP  - 235
    EP  - 244
    VL  - 3
    IS  - 4
    UR  - http://wordnetcode.princeton.edu/5papers.pdf
    DO  - 10.1093/ijl/3.4.235
    ID  - Miller_Beckwith_Fellbaum_Gross_Miller
    ER  -
    TY  - JOUR
    AU  - Weiner, G.
    TI  - Jesse Norman, ed., Edmund Burke: Reflections on the Revolution in France and Other Writings
    SP  - 377
    EP  - 382
    VL  - 54
    IS  - 4
    UR  - http://link.springer.com/article/10.1007/s12115-017-0159-0
    DO  - 10.1007/s12115-017-0159-0
    ID  - Weiner
    ER  -
    TY  - JOUR
    AU  - Wesley, Marilyn
    TI  - Truth and Fiction in Tim O’Brien’s “If I Die in a Combat Zone” and “The Things They Carried”
    SP  - 1
    EP  - 18
    VL  - 29
    IS  - 2
    UR  - http://www.jstor.org/stable/25112634
    ID  - Wesley
    ER  -
    TY  - JOUR
    A1  - Bloggs, Joseph
    TI  - The Best Research Paper in the World
    JF  - Journal of Amazing Papers
    Y1  - 2019
    ER  -
    """

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        bibliographyTransformationsController = BibliographyTransformationsController()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)
    }

    override class func tearDown() {
        app = nil
        bibliographyTransformationsController = nil
        super.tearDown()
    }

    // MARK: - CSL-JSON -> Arbitrary bib format test cases

    func testEndToEndCSLJSONToADS() {
        endToEndTest(targetBibliographyFormat: "ads", resultVerification: .success)
    }

    func testEndToEndCSLJSONToBibTeX() {
        endToEndTest(targetBibliographyFormat: "bib", resultVerification: .success)
    }

    func testEndToEndCSLJSONToEndnote() {
        endToEndTest(targetBibliographyFormat: "end", resultVerification: .success)
    }

    func testEndToEndCSLJSONToISI() {
        endToEndTest(targetBibliographyFormat: "isi", resultVerification: .success)
    }

    func testEndToEndCSLJSONToMODS() {
        endToEndTest(targetBibliographyFormat: "xml", resultVerification: .success)
    }

    func testEndToEndCSLJSONToRIS() {
        endToEndTest(targetBibliographyFormat: "ris", resultVerification: .success)
    }

    func testEndToEndCSLJSONToWordBib() {
        endToEndTest(targetBibliographyFormat: "wordbib", resultVerification: .success)
    }

    // MARK: - Arbitrary bib format -> CSL-JSON test cases

    func testEndToEndBibTeXToCSLJSON() {
        endToEndTest(sourceBibliographyFormat: "bib", resultVerification: .success)
    }

    func testEndToEndEndNoteToCSLJSON() {
        endToEndTest(sourceBibliographyFormat: "end", resultVerification: .success)
    }

    func testEndToEndRISToCSLJSON() {
        endToEndTest(sourceBibliographyFormat: "ris", resultVerification: .success)
    }

    // MARK: - Failure test cases

    func testEndToEndMissingSourceAndTargetBibliographyFormats() {
        endToEndTest(sourceBibliographyFormat: nil, targetBibliographyFormat: nil, resultVerification: .failureNoThrow)
    }

    func testEndToEndInvalidSourceBibliographyFormat() {
        endToEndTest(sourceBibliographyFormat: "blort", resultVerification: .failureNoThrow)
    }

    func testEndToEndInvalidTargetBibliographyFormat() {
        endToEndTest(targetBibliographyFormat: "blort", resultVerification: .failureNoThrow)
    }

    // MARK: - Private methods

    // swiftlint:disable:next function_body_length
    private func endToEndTest(sourceBibliographyFormat: String? = nil,
                              targetBibliographyFormat: String? = nil,
                              resultVerification: EndpointMockableResultVerification,
                              completionTimeout: Double? = 30.0) {

        let expectation = XCTestExpectation(description: "endToEndTest_\(targetBibliographyFormat ?? "nil_targetFileExtension")")

        // The `mockAPIKey` is set as a request header in all cases
        var requestHeaders: HTTPHeaders = [Request.pressroomHeaders.apiKey.stringValue: type(of: self).mockAPIKey]
        // Allows for testing against failure cases where the 'sourceBibliographyFormat'
        // and 'targetBibliographyFormat' headers are both missing
        var httpBody: HTTPBody = validCSLJSON.convertToHTTPBody()
        var requestContentType: MediaType = .json
        var responseContentType: MediaType = .plainText

        // Init headers and HTTPBody depdending on if there is an sourceBibliographyFormat
        // or targetBibliographyFormat
        if let sourceBibliographyFormat = sourceBibliographyFormat,
            let inputFormat = CSLJSONTransformationService.InputFormat(rawValue: sourceBibliographyFormat) {
            requestHeaders.add(name: Request.pressroomHeaders.sourceBibliographyFormat,
                               value: sourceBibliographyFormat)

            switch inputFormat {
            case .bibTeX:
                httpBody = validBibTeX.convertToHTTPBody()
            case .endNote:
                httpBody = validEndNote.convertToHTTPBody()
            case .JSON:
                httpBody = validCSLJSON.convertToHTTPBody()
            case .RIS:
                httpBody = validRIS.convertToHTTPBody()
            }

            requestContentType = .plainText
            responseContentType = .json
        } else if let targetBibliographyFormat = targetBibliographyFormat {
            requestHeaders.add(name: Request.pressroomHeaders.targetBibliographyFormat,
                               value: targetBibliographyFormat)
        }

        // Init mock request
        let request = mockRequest(method: .POST,
                                  headers: requestHeaders,
                                  contentType: requestContentType,
                                  body: httpBody)

        // Compile bibliography and verify the response when the `Future` returns successfully
        // OR handle an error depending on the `resultVerification` value
        switch resultVerification {
        case .success:
            XCTAssertNoThrow(try type(of: self).bibliographyTransformationsController.compileBibliography(request)) { (responseFuture: Future<Response>) in
                responseFuture.whenSuccess({[unowned self] response in
                    let bodyDataFuture = self.verifyResponseSuccess(response, expectedContentType: responseContentType)
                    bodyDataFuture.whenSuccess({ _ in
                        // Fulfill the 'success' case expectation
                        expectation.fulfill()
                    })
                })

                responseFuture.whenFailure({ error in
                    XCTFail("No bibliography document transformation error expected - '\(error)' thrown instead!")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                })
            }
        case .failureNoThrow:
            XCTAssertThrowsError(try type(of: self).bibliographyTransformationsController.compileBibliography(request)) { error in
                guard let pressroomError = error as? PressroomError else {
                    XCTFail("Received error was not of type 'PressroomError'")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                    return
                }
                XCTAssertNotEqual(pressroomError.status, HTTPResponseStatus.ok)
                XCTAssertEqual(pressroomError.typeIdentifier, "BibliographyTransformationsControllerError")
                // Fulfill the 'failure' case expectation
                expectation.fulfill()
            }
        case .failureThrowsError:
            return
        }

        // Wait for the async bibliography compilation to complete
        wait(for: [expectation], timeout: completionTimeout!)
    }

    // MARK: - All tests

    static let allTests = [
        ("testEndToEndCSLJSONToADS", testEndToEndCSLJSONToADS),
        ("testEndToEndCSLJSONToBibTeX", testEndToEndCSLJSONToBibTeX),
        ("testEndToEndCSLJSONToEndnote", testEndToEndCSLJSONToEndnote),
        ("testEndToEndCSLJSONToISI", testEndToEndCSLJSONToISI),
        ("testEndToEndCSLJSONToMODS", testEndToEndCSLJSONToMODS),
        ("testEndToEndCSLJSONToRIS", testEndToEndCSLJSONToRIS),
        ("testEndToEndCSLJSONToWordBib", testEndToEndCSLJSONToWordBib),
        ("testEndToEndBibTeXToCSLJSON", testEndToEndBibTeXToCSLJSON),
        ("testEndToEndEndNoteToCSLJSON", testEndToEndEndNoteToCSLJSON),
        ("testEndToEndRISToCSLJSON", testEndToEndRISToCSLJSON),
        ("testEndToEndMissingSourceAndTargetBibliographyFormats", testEndToEndMissingSourceAndTargetBibliographyFormats),
        ("testEndToEndInvalidSourceBibliographyFormat", testEndToEndInvalidSourceBibliographyFormat),
        ("testEndToEndInvalidTargetBibliographyFormat", testEndToEndInvalidTargetBibliographyFormat)
    ]
}
