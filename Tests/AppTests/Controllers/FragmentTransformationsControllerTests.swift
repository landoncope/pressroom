//
//  FragmentTransformationsControllerTests.swift
//  AppTests
//
//  Created by Dan Browne on 08/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

@testable import App
import Foundation
import Vapor
import XCTest

final class FragmentTransformationsControllerTests: XCTestCase, EndpointMockable {

    static var app: Application!
    static let mockAPIKey = "PLACEHOLDER-API-KEY"
    static var fragmentTransformationsController: FragmentTransformationsController!

    // MathML examples
    private let gausssLaw = """
                            <math xmlns="http://www.w3.org/1998/Math/MathML">
                                <mo>&#x2207;</mo>
                                <mo>&#x22C5;</mo>
                                <mi mathvariant="bold">E</mi>
                                <mo>=</mo>
                                <mfrac>
                                    <mi>&#x3C1;</mi>
                                    <msub>
                                        <mi>&#x3B5;</mi>
                                        <mn>0</mn>
                                    </msub>
                                </mfrac>
                            </math>
                            """

    private let gausssLawOfMagnetism = """
                                    <math xmlns="http://www.w3.org/1998/Math/MathML">
                                        <mo>&#x2207;</mo>
                                        <mo>&#x22C5;</mo>
                                        <mi mathvariant="bold">B</mi>
                                        <mo>=</mo>
                                        <mn>0</mn>
                                    </math>
                                    """

    private let faradysLaw = """
                            <math xmlns="http://www.w3.org/1998/Math/MathML">
                                <mo>&#x2207;</mo>
                                <mo>&#xD7;</mo>
                                <mi mathvariant="bold">E</mi>
                                <mo>=</mo>
                                <mo>-</mo>
                                <mfrac>
                                    <mrow>
                                        <mo>&#x2202;</mo>
                                        <mi mathvariant="bold">B</mi>
                                    </mrow>
                                    <mrow>
                                        <mo>&#x2202;</mo>
                                        <mi>t</mi>
                                    </mrow>
                                </mfrac>
                            </math>
                            """

    private let amperesLaw = """
                            <math xmlns="http://www.w3.org/1998/Math/MathML">
                                <mo>&#x2207;</mo>
                                <mo>&#xD7;</mo>
                                <mi mathvariant="bold">B</mi>
                                <mo>=</mo>
                                <msub>
                                    <mi>&#x3BC;</mi>
                                    <mn>0</mn>
                                </msub>
                                <mfenced>
                                    <mrow>
                                        <mi mathvariant="bold">J</mi>
                                        <mo>+</mo>
                                        <msub>
                                            <mi>&#x3B5;</mi>
                                            <mn>0</mn>
                                        </msub>
                                        <mfrac>
                                            <mrow>
                                                <mo>&#x2202;</mo>
                                                <mi mathvariant="bold">E</mi>
                                            </mrow>
                                            <mrow>
                                                <mo>&#x2202;</mo>
                                                <mi>t</mi>
                                            </mrow>
                                        </mfrac>
                                    </mrow>
                                </mfenced>
                            </math>
                            """

    private let eulersIdentity = """
                                <math xmlns="http://www.w3.org/1998/Math/MathML">
                                    <msup>
                                        <mi>e</mi>
                                        <mrow>
                                            <mi>i</mi>
                                            <mi mathvariant="normal">&#x3C0;</mi>
                                        </mrow>
                                    </msup>
                                    <mo>+</mo>
                                    <mn>1</mn>
                                    <mo>=</mo>
                                    <mn>0</mn>
                                </math>
                                """

    private let emptyMathML = """
                            <math xmlns="http://www.w3.org/1998/Math/MathML"></math>
                            """

    private let invalidMathML = """
                                blort
                                """

    // Expected TeX responses
    private let gausssLawTeX = "\\nabla \\cdot \\mathbf{E}=\\frac{\\rho }{{\\epsilon }_{0}}"
    private let gausssLawOfMagnetismTeX = "\\nabla \\cdot \\mathbf{B}=0"
    private let faradaysLawTeX = "\\nabla  \\times \\mathbf{E}=-\\frac{\\partial \\mathbf{B}}{\\partial t}"
    // swiftlint:disable:next line_length
    private let amperesLawTeX = "\\nabla  \\times \\mathbf{B}={\\mu }_{0}\\left(\\mathbf{J}+{\\epsilon }_{0}\\frac{\\partial \\mathbf{E}}{\\partial t}\\right)"
    private let eulersIdentityTeX = "{e}^{i\\mathrm{\\pi }}+1=0"
    private let emptyMathMLTeX = ""

    // MARK: - Test suite setup and tear-down

    override class func setUp() {
        super.setUp()

        // SUT initialization
        fragmentTransformationsController = FragmentTransformationsController()

        // Initialize an `Application` instance to use for the test methods
        let services = Services.default()
        // swiftlint:disable:next force_try
        app = try! Application(services: services)

        // Swizzle `Bundle.pressRoom()` method (to return correct `Bundle` when running from unit tests)
        Bundle.swizzlePressRoomBundle()
    }

    override class func tearDown() {
        app = nil
        fragmentTransformationsController = nil

        // Swizzle `Bundle.pressRoom()` method again (to return its implementation to original state)
        Bundle.swizzlePressRoomBundle()

        super.tearDown()
    }

    // MARK: - Test cases

    func testGausssLaw() {
        endToEndTest(mathMLString: gausssLaw,
                     expectedTeXResponse: gausssLawTeX,
                     resultVerification: .success)
    }

    func testGausssLawOfMagnetism() {
        endToEndTest(mathMLString: gausssLawOfMagnetism,
                     expectedTeXResponse: gausssLawOfMagnetismTeX,
                     resultVerification: .success)
    }

    func testFaradysLaw() {
        endToEndTest(mathMLString: faradysLaw,
                     expectedTeXResponse: faradaysLawTeX,
                     resultVerification: .success)
    }

    func testAmperesLaw() {
        endToEndTest(mathMLString: amperesLaw,
                     expectedTeXResponse: amperesLawTeX,
                     resultVerification: .success)
    }

    func testEulersIdentity() {
        endToEndTest(mathMLString: eulersIdentity,
                     expectedTeXResponse: eulersIdentityTeX,
                     resultVerification: .success)
    }

    func testEmptyMathML() {
        endToEndTest(mathMLString: emptyMathML,
                     expectedTeXResponse: emptyMathMLTeX,
                     resultVerification: .success)
    }

    func testInvalidMathML() {
        endToEndTest(mathMLString: invalidMathML,
                     resultVerification: .failureNoThrow)
    }

    func testEmptyRequestBody() {
        endToEndTest(mathMLString: "",
                     resultVerification: .failureNoThrow)
    }

    // MARK: - Private methods

    private func endToEndTest(mathMLString: String,
                              expectedTeXResponse: String? = nil,
                              resultVerification: EndpointMockableResultVerification,
                              completionTimeout: Double? = 15.0) {

        let expectation = XCTestExpectation(description: "endToEndTest_MathMLString")

        // Init headers, HTTPBody, request and response 'Content-Type'
        let requestHeaders: HTTPHeaders = ["\(Request.pressroomHeaders.apiKey)": type(of: self).mockAPIKey,
                                           "\(Request.pressroomHeaders.documentFragmentInputFormat)": "mathml",
                                           "\(Request.pressroomHeaders.documentFragmentOutputFormat)": "tex"]
        let httpBody = mathMLString.convertToHTTPBody()
        let requestContentType = MediaType.xml
        let responseContentType = MediaType.plainText

        // Init mock request
        let request = mockRequest(method: .POST,
                                  headers: requestHeaders,
                                  contentType: requestContentType,
                                  body: httpBody)

        // Compile document fragment and verify the response when the `Future` returns successfully
        // OR handle an error depending on the `resultVerification` value
        switch resultVerification {
        case .success:
            XCTAssertNoThrow(try type(of: self).fragmentTransformationsController.compileFragment(request)) { responseFuture in
                responseFuture.whenSuccess {[unowned self] response in
                    let bodyDataFuture = self.verifyResponseSuccess(response,
                                                                    expectedContentType: responseContentType,
                                                                    expectedBodyData: expectedTeXResponse?.data(using: .utf8))
                    bodyDataFuture.whenSuccess({ _ in
                        // Fulfill the 'success' case expectation
                        expectation.fulfill()
                    })
                }

                responseFuture.whenFailure { error in
                    XCTFail("No document fragment transformation error expected - '\(error)' thrown instead!")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureNoThrow:
            XCTAssertNoThrow(try type(of: self).fragmentTransformationsController.compileFragment(request)) { responseFuture in
                responseFuture.whenSuccess { _ in
                    XCTFail("Failure (i.e. an error) expected, but Future<Response> was fulfilled successfully!")
                    expectation.fulfill()
                }

                responseFuture.whenFailure { error in
                    guard let pressroomError = error as? PressroomError else {
                        XCTFail("Received error was not of type 'PressroomError'")
                        // Fulfill the 'failure' case expectation
                        expectation.fulfill()
                        return
                    }
                    XCTAssertNotEqual(pressroomError.status, HTTPResponseStatus.ok)
                    XCTAssertEqual(pressroomError.typeIdentifier, "FragmentTransformationsControllerError")
                    // Fulfill the 'failure' case expectation
                    expectation.fulfill()
                }
            }
        case .failureThrowsError:
            XCTAssertThrowsError(try type(of: self).fragmentTransformationsController.compileFragment(request)) { _ in
                // Fulfill the 'failure' (throwing an error) case expectation
                expectation.fulfill()
            }
        }

        // Wait for the async document fragment compilation to complete
        wait(for: [expectation], timeout: completionTimeout!)
    }

    // MARK: - All tests

    static let allTests = [
        ("testGausssLaw", testGausssLaw),
        ("testGuasssLawOfMagnetism", testGausssLawOfMagnetism),
        ("testFaradysLaw", testFaradysLaw),
        ("testAmperesLaw", testAmperesLaw),
        ("testEulersIdentity", testEulersIdentity),
        ("testEmptyMathML", testEmptyMathML),
        ("testInvalidMathML", testInvalidMathML),
        ("testEmptyRequestBody", testEmptyRequestBody)
    ]
}
