<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.tei-c.org/ns/1.0 /home/grobid/grobid-home/schemas/xsd/Grobid.xsd"
 xmlns:xlink="http://www.w3.org/1999/xlink">
	<teiHeader xml:lang="en">
		<encodingDesc>
			<appInfo>
				<application version="0.5.4" ident="GROBID" when="2019-05-30T11:28+0000">
					<ref target="https://github.com/kermitt2/grobid">GROBID - A machine learning software for extracting information from scholarly documents</ref>
				</application>
			</appInfo>
		</encodingDesc>
		<fileDesc>
			<titleStmt>
				<title level="a" type="main">Heterogeneous and distributed simulation models for Critical Infrastructures Interdependencies analysis and risk mitigation *</title>
			</titleStmt>
			<publicationStmt>
				<publisher/>
				<availability status="unknown"><licence/></availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<analytic>
						<author>
							<persName xmlns="http://www.tei-c.org/ns/1.0"><forename type="first">Serguei</forename><surname>Iassinovski</surname></persName>
							<email>iassinovski@multitel.be</email>
							<affiliation key="aff0">
								<orgName type="institution">Multitel Research Center</orgName>
								<address>
									<settlement>Mons</settlement>
									<country key="BE">Belgium</country>
								</address>
							</affiliation>
						</author>
						<author>
							<persName xmlns="http://www.tei-c.org/ns/1.0"><forename type="first">E</forename><surname>Ciancamerla</surname></persName>
							<email>ester.ciancamerla@enea.it</email>
							<affiliation key="aff1">
								<orgName type="department">ENEA -Lungotevere Thaon di Revel</orgName>
								<address>
									<addrLine>76</addrLine>
									<postCode>00196</postCode>
									<settlement>ROMA</settlement>
									<country key="IT">Italia</country>
								</address>
							</affiliation>
						</author>
						<author>
							<persName xmlns="http://www.tei-c.org/ns/1.0"><forename type="first">B</forename><surname>Fresilli</surname></persName>
							<affiliation key="aff1">
								<orgName type="department">ENEA -Lungotevere Thaon di Revel</orgName>
								<address>
									<addrLine>76</addrLine>
									<postCode>00196</postCode>
									<settlement>ROMA</settlement>
									<country key="IT">Italia</country>
								</address>
							</affiliation>
						</author>
						<author>
							<persName xmlns="http://www.tei-c.org/ns/1.0"><forename type="first">L</forename><surname>Lavalle</surname></persName>
							<email>luisa.lavalle@enea.it</email>
							<affiliation key="aff1">
								<orgName type="department">ENEA -Lungotevere Thaon di Revel</orgName>
								<address>
									<addrLine>76</addrLine>
									<postCode>00196</postCode>
									<settlement>ROMA</settlement>
									<country key="IT">Italia</country>
								</address>
							</affiliation>
						</author>
						<author>
							<persName xmlns="http://www.tei-c.org/ns/1.0"><forename type="first">T</forename><surname>Patriarca</surname></persName>
							<email>tatiana.patriarca@enea.italberto.tofani@enea.it</email>
							<affiliation key="aff1">
								<orgName type="department">ENEA -Lungotevere Thaon di Revel</orgName>
								<address>
									<addrLine>76</addrLine>
									<postCode>00196</postCode>
									<settlement>ROMA</settlement>
									<country key="IT">Italia</country>
								</address>
							</affiliation>
						</author>
						<author>
							<persName xmlns="http://www.tei-c.org/ns/1.0"><forename type="first">A</forename><surname>Tofani</surname></persName>
							<affiliation key="aff1">
								<orgName type="department">ENEA -Lungotevere Thaon di Revel</orgName>
								<address>
									<addrLine>76</addrLine>
									<postCode>00196</postCode>
									<settlement>ROMA</settlement>
									<country key="IT">Italia</country>
								</address>
							</affiliation>
						</author>
						<title level="a" type="main">Heterogeneous and distributed simulation models for Critical Infrastructures Interdependencies analysis and risk mitigation *</title>
					</analytic>
					<monogr>
						<imprint>
							<date/>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<textClass>
				<keywords>
					<term>Industrial and Automation Control System</term>
					<term>Critical Infrastructure</term>
					<term>Cyber Security</term>
					<term>Risk Mitigation</term>
					<term>Quality of Service Indicators</term>
					<term>Discrete-event Simulation</term>
				</keywords>
			</textClass>
			<abstract>
				<p>The paper presents and discusses the implementation and first results of complex simulation models used for risk assessment and mitigation in interconnected Critical Infrastructures (CIs). It can be considered as continuation of paper published last year [1]. This work is relevant because damage estimation under various reference scenarios of CIs interdependencies, adverse events and SCADA procedures represents an important part of risk assessment. And simulation is essentially the only way for such an estimation due to complexity and interdependencies of CIs and to probabilistic nature of adverse events like cyber attacks or others.</p>
			</abstract>
		</profileDesc>
	</teiHeader>
	<text xml:lang="en">
	</text>
</TEI>
