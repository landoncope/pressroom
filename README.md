# Pressroom 📰🏨

Pressroom is a document and bibliography transformation oriented web service for converting documents between docx, markdown, LaTeX and HTML (as well as a number of other rarer formats); as well as bibliographies between ADS, BibTeX, CSL-JSON, EndNote, ISI, MODS, RIS and Wordbib.

## Vapor, wait, what?

Pressroom is a [Vapor](http://vapor.codes/) based microservice, which allows the use of Manuscripts for Mac legacy file format compatible import and export components without introducing bugs during initial launch of [Manuscripts.io](https://github.com/atypon/manuscripts-frontend). Going forwards, parallel refactoring efforts on the Manuscripts for Mac and Pressroom codebases will reduce the scope of the dependencies on the Manuscripts codebase. The ultimate goal is for the dependency graph of Pressroom to contain only Swift code, which will also allow for Linux-based deployments.

## Features

Pressroom is split into five endpoints, each with its own responsibilities.

### /v1/compile/document

This endpoint can be used for transforming documents between HTML, LaTeX, Manuscripts Project Bundle (see below), Markdown, Microsoft Word and PDF. All formats apart from PDF are available as bidirectional transformations, whereas PDF is only available as a transformation output format. The endpoint supports single file transformations (i.e. when providing a .docx file as an input), as well as compressed document containers (i.e. a LaTeX document consisting of several TeX files as well as auxiliary files such as Figures). The HTTP response streams the transformed document data back to allow support for large document transformations.

### /v1/compile/document/fragment

This endpoint can be used for transforming document fragments, which is useful for doing on-the-fly transformations of equations to insert into a larger document. Currently, the only supported transformation is from MathML -> TeX (but this will expand in the future).

### /v1/compile/document/jats

This enpoint can be used for transforming Manuscript Project Bundle files (see below) into JATS XML (or HTML) compressed document containers. Under-the-hood, this endpoint depends on [sachs](https://gitlab.com/mpapp-private/sachs). The HTTP response streams the transformed document data back to allow support for large document transformations.

### /v1/compile/document/latexml

This enpoint can be used for transforming TeX documents into HTML, HTML4, HTML5, XHTML or XML (i.e. LaTeXML) outputs. These transformations use LaTeXML as an intermediate document representation. The endpoint supports compressed document containers (i.e. a TeX document consisting of several TeX files as well as auxiliary files such as Figures). The HTTP response streams the transformed document data back to allow support for large document transformations.

### /v1/compile/bibliography

This endpoint can be used for transforming bibliographic reference items data between ADS, BibTeX, CSL-JSON, EndNote, ISI, MODS, RIS and Wordbib. A matrix of available transformations is shown the table below. Items marked with a '✓' are direct transformations, whereas items marked with a '*' are indirect transformations that would need to use a 'to CSL-JSON' transformation as a first step.

| Input ↓ / Output → | ADS | BibTex | CSL-JSON | EndNote | ISI | MODS | RIS | Wordbib |
|--------------------|-----|--------|----------|---------|-----|------|-----|---------|
| ADS                |     |        |          |         |     |      |     |         |
| BibTex             | *   | *      | ✓        | *       | *   | *    | *   | *       |
| CSL-JSON           | ✓   | ✓      | ✓        | ✓       | ✓   | ✓    | ✓   | ✓       |
| EndNote            | *   | *      | ✓        | *       | *   | *    | *   | *       |
| ISI                |     |        |          |         |     |      |     |         |
| MODS               |     |        |          |         |     |      |     |         |
| RIS                | *   | *      | ✓        | *       | *   | *    | *   | *       |
| WordBib            |     |        |          |         |     |      |     |         |

### /v1/docs

This endpoint can be used for viewing the API documentation for Pressroom, including information on all of its endpoints (including this one). The documentation is served by default in OpenAPI 3.x compatible JSON. Adding a query parameter of 'html' will serve the documentation in an easy to read HTML page (i.e. 'v1/docs?html').

## Getting started

Pressroom can be run locally if you have a Mac and Xcode 11.x. Once you have cloned the repository, follow the steps below to ensure you have Pressroom in a state where all its dependencies are installed and configured correctly:

1.  Install [Homebrew](https://brew.sh/) if you don't already have it on your system.
2.  Install a suitable TeX distribution. [MacTeX](https://tug.org/mactex/downloading.html) is highly recommended, (install using its default installation options).
3.  Install [OpenJDK v8 JRE](https://github.com/AdoptOpenJDK/homebrew-openjdk) if the target machine does not already have it installed. This is favoured over the Oracle JDK due to licensing restrictions, and its installation is not part of the bootstrap script because an administrator password is required when installing via homebrew. The JRE is required in order to run the 'citeproc-java' command-line utility, which is used internally by Pressroom.
3.  In a terminal, go to the repository root directory and run the following command:
```
$ ./bootstrap
```

## Building & running Pressroom

After running the bootstrap script to complete one-time setup (see above), Pressroom can be built from the command-line using the following command:

```
$ xcodebuild -project PressroomServer.xcodeproj -scheme PressroomServer clean build
```

Pressroom can also be built from Xcode by selecting the 'PressroomServer' scheme and then clicking the 'Build and run' play button or using the Cmd+R keyboard shortcut.

**NOTES & KNOWN ISSUES:**
- **Pressroom builds using Xcode 11.0 (or higher)**. If you have multiple versions of Xcode installed, you need to ensure the Xcode 11.x command line tools are being used with the following command in a terminal. Replace <NAME_OF_XCODE_11.X> with the name you've given to your Xcode 11.x app binary:
```
sudo xcode-select -s /Applications/<NAME_OF_XCODE_11.X>.app/Contents/Developer/
```
- When building via Xcode or the command-line you may need to clear the DerviedData folder of Xcode between builds:
```
$ rm -rf ~/Library/Developer/Xcode/DerivedData
```
- New changes to Manuscripts (or any of Manuscripts' sub-dependencies) via updates to Cartfile or Cartfile.resolved mean that you will need to run the following commands in a terminal at the repository root directory before re-building Pressroom:
```
$ rm -rf Carthage/Build/Mac
$ XCODE_XCCONFIG_FILE=$(pwd)/xcconfigs/AFNetworking_Xcode_11_workaround.xcconfig carthage build --cache-builds --platform mac
```
- Pressroom uses [Swiftlint](https://github.com/realm/SwiftLint) to enforce coding style and conventions. The configuration of the linting rules is stored in `.swiftlint.yml` in the root directory of the repository. Swiftlint is run automatically during developer builds of Pressroom (if it is installed on the system doing the builds). The Gitlab CI pipeline also runs Swiftlint against all open merge requests, so community contributions will only be accepted if the linting step passes. Therefore it is recommended that interested parties install Swiftlint locally to get early notice of linting warnings and errors.

## Open source roadmap

Whilst Pressroom itself is now open source, Manuscripts for Mac (which is the main dependency of Pressroom) is still currently closed source. This means that currently it is not possible to build Pressroom from source. The Manuscripts Team is working hard towards making Manuscripts for Mac open source as soon as possible.

## Additional resources

### API Documentation

Pressroom is a self-documenting microservice, OpenAPI 3.x documentation is [available here](https://pressroom.manuscripts.io/v1/docs?html).

### Example requests

Below are a few example requests, in the form of cURL commands. When running a local instance of Pressroom, the server IP address is [http://0.0.0.0:5511](http://0.0.0.0:5511). For authentication purposes the value of the `Pressroom-API-Key` header should be `PLACEHOLDER-API-KEY`. The production instance of Pressroom is available at [https://pressroom.manuscripts.io](https://pressroom.manuscripts.io). Please get in touch with the Manuscripts team for details of how to authenticate against the production server. Please note that all endpoint URLs are currently prefixed by `/v1/`.

#### Transform CSL-JSON into BibTeX

```
curl -X POST \
  http://0.0.0.0:5511/v1/compile/bibliography \
  -H 'Content-Type: application/json' \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Target-Bibliography-Format: bib' \
  -d '[
        {
            "id": "2656243/9G79BL7D",
            "type": "article-journal",
            "title": "Some Notes on Gertrude Stein and Deixis",
            "container-title": "Arizona Quarterly: A Journal of American Literature, Culture, and Theory",
            "page": "91-102",
            "volume": "53",
            "issue": "1",
            "URL": "https://muse-jhu-edu.pitt.idm.oclc.org/article/445403/pdf",
            "DOI": "10.1353/arq.1997.0020",
            "author": [
                {
                    "family": "Cook",
                    "given": "Albert"
                }
            ],
            "issued": {
                "date-parts": [
                    [
                        1997
                    ]
                ]
            }
        }
      ]'
```

#### Transform a Microsoft Word document into a TeX document (compressed container)

```
curl -X POST \
  http://0.0.0.0:5511/v1/document/compile \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Target-File-Extension: tex' \
  -F file=@/Users/admin/Documents/My_Paper.docx -o ~/Documents/My_Paper_TeX.zip
```

#### Transform a Microsoft Word document into a Manuscripts Project Bundle

```
curl -X POST \
  http://0.0.0.0:5511/v1/document/compile \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Enrich-Document-Metadata' \
  -F file=@/Users/admin/Documents/My_Paper.docx -o ~/Documents/My_Paper.manuproj
```

**N.B:** The `Pressroom-Enrich-Document-Metadata` header instructs Pressroom to attempt to enrich the title, abstract, authors and affiliations metadata of the Manuscripts Project Bundle. The `Pressroom-Target-File-Extension` header can be omitted as the default target file format is `manuproj`.

#### Transform a TeX document (compressed container) into a Manuscripts Project Bundle

```
curl -X POST \
  http://0.0.0.0:5511/v1/compile/document \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Target-File-Extension: manuproj' \
  -H 'Pressroom-Primary-File: main.tex' \
  -F file=@/Users/admin/Documents/My_Paper_TeX.zip -o ~/Documents/My_Paper.manuproj
```
**N.B:** A "primary file" can be specified by the `Pressroom-Primary-File` header as an entry point for transforming the TeX document. This should be a file present inside the compressed container. In most cases this will be inferred internally by Pressroom, but it is sometimes useful to specify the file explicitly.

#### Transform a TeX document (compressed container) into HTML

```
curl -X POST \
  http://0.0.0.0:5511/v1/compile/document/latexml \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Target-Latexml-Output-Format: html' \
  -F file=@/Users/admin/Documents/My_Paper_TeX.zip -o ~/Documents/My_Paper_HTML.zip
```

#### Transform a TeX document (single file) into a PDF

```
curl -X POST \
  http://0.0.0.0:5511/v1/compile/document \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Target-File-Extension: pdf' \
  -F file=@/Users/admin/Documents/My_Paper.tex -o ~/Documents/My_Paper.pdf
```

#### Transform a Manuscripts Project Bundle into JATS XML (compressed container)

```
curl -X POST \
  http://0.0.0.0:5511/v1/document/compile/jats \
  -H 'Pressroom-API-Key: PLACEHOLDER-API-KEY' \
  -H 'Pressroom-Target-JATS-Output-Format: literatum-jats' \
  -H 'Pressroom-JATS-Submission-Identifier: SOME_ID' \
  -H 'Pressroom-JATS-Submission-DOI: 10.1000/182' \
  -H 'Pressroom-JATS-Version: 1.2d1' \
  -F file=@/Users/admin/Documents/My_Paper.manuproj -o ~/Documents/My_Paper_JATS.zip
```

### Manuscripts Project Bundle format (aka .manuproj)

The Manuscripts Project Bundle format is a compressed document container which consists of the following directory structure:

- index.manuscript-json
- Data/
    - Any associated auxiliary files (e.g. Figure images)

The "index.manuscript-json" file is a compiled version of the document in [manuscript-json](https://github.com/atypon/manuscripts-json-schema) format, that also references any auxiliary files that the original source document may contain.

The "Data" directory contains those auxiliary files (if present). An example of the required naming scheme for auxiliary files, is as follows:

`MPFigure_D836BDAB-DF31-42D0-99D8-97DF176698D5`

The `MPFigure_` prefix refers to the Figure model object in the [manuscript-json](https://github.com/atypon/manuscripts-json-schema). The suffix should be a unique version 4 UUID whose value matches any corresponding references to the same `MPFigure` object in the `index.manuscript-json` file. **NOTE: Auxiliary files should not include a file extension in their name.**

An example Manuscripts Project Bundle can be found under [`Tests/Resources/example_project_bundle.manuproj`](https://gitlab.com/mpapp-private/pressroom/blob/master/Tests/Resources/example_project_bundle.manuproj).

### License

© 2019 - 2020 Atypon Systems LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Acknowledgements

Pressroom uses these third-party open source libraries:

#### [Vapor](https://github.com/vapor/vapor/blob/master/LICENSE)
#### [JWT](https://github.com/vapor/jwt/blob/master/LICENSE)
#### [StringMetric.swift](https://github.com/autozimu/StringMetric.swift/blob/master/LICENSE.txt)
#### [SwiftyBeaver](https://github.com/SwiftyBeaver/SwiftyBeaver/blob/master/LICENSE)
#### [SwiftyBeaver-Vapor](https://github.com/SwiftyBeaver/SwiftyBeaver-Vapor/blob/master/LICENSE)
#### [Swiftgger](https://github.com/mczachurski/Swiftgger/blob/master/LICENSE.md)
#### [LibreOffice](https://www.libreoffice.org/)