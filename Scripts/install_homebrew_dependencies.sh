#!/bin/bash

set -e

# Ensure 'citeproc-java' command-line utility is installed via Homebrew (used internally for CSL-JSON -> BibTeX transformations)
echo "Installing 'citeproc-java'…"
brew tap michel-kraemer/citeproc-java
brew reinstall citeproc-java

# Ensure 'LaTeXML' command-line utilities are installed via Homebrew (used internally for TeX -> HTML-like transformations)
echo "Installing 'latexml'…"
brew reinstall latexml

# 'libressl' is needed by some SSL-related Vapor components
echo "Installing 'libressl'…"
brew reinstall libressl

# Ensure libreOffice is installed via Homebrew (used internally for MS Word -> PDF transformations)
echo "Installing 'libreoffice'…"
brew cask reinstall libreoffice

# Prince is a sub-dependency of 'sachs'
echo "Installing 'prince'…"
brew cask reinstall --no-quarantine prince

# pandoc and pandoc-citeproc is a sub-dependency of 'sachs'
echo "Installing 'pandoc' and 'pandoc-citeproc'…"
brew reinstall pandoc
brew reinstall pandoc-citeproc