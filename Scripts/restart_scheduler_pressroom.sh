#!/bin/bash

# (Helper) Path to the PressroomServer Restart Scheduler launchd plist file
PRESSROOM_SCHEDULER_PLIST_FILE="com.manuscripts.pressroomserver.restartscheduler.plist"
# (Helper) Path to the PressroomServer launchd plist file
PRESSROOM_PLIST_FILE="com.manuscripts.pressroomserver.plist"

# Unload the current PressroomServer Restart Scheduler launch daemon (if one is already running)
launchctl unload ~/Library/LaunchAgents/$PRESSROOM_SCHEDULER_PLIST_FILE

# Generate a `launchd` configuration plist for PressroomServer Restart Scheduler
# (Schedules a restart of PressroomServer itself at 01:00 local time)
/usr/libexec/PlistBuddy -c "add Label string com.manuscripts.pressroomserver.restartscheduler" $PRESSROOM_SCHEDULER_PLIST_FILE
/usr/libexec/PlistBuddy -c "add ProgramArguments array" $PRESSROOM_SCHEDULER_PLIST_FILE
/usr/libexec/PlistBuddy -c "add ProgramArguments:0 string launchctl unload $HOME/Library/LaunchAgents/$PRESSROOM_PLIST_FILE" $PRESSROOM_SCHEDULER_PLIST_FILE
/usr/libexec/PlistBuddy -c "add ProgramArguments:1 string launchctl load $HOME/Library/LaunchAgents/$PRESSROOM_PLIST_FILE" $PRESSROOM_SCHEDULER_PLIST_FILE
/usr/libexec/PlistBuddy -c "add StartCalendarInterval dict" $PRESSROOM_SCHEDULER_PLIST_FILE
/usr/libexec/PlistBuddy -c "add StartCalendarInterval:Hour integer 1" $PRESSROOM_SCHEDULER_PLIST_FILE
/usr/libexec/PlistBuddy -c "add StartCalendarInterval:Minute integer 0" $PRESSROOM_SCHEDULER_PLIST_FILE

# Move the launch daemon plist file into place
mv $PRESSROOM_SCHEDULER_PLIST_FILE ~/Library/LaunchAgents/$PRESSROOM_SCHEDULER_PLIST_FILE

# Load (and implicitly start) the newly-installed PressroomServer Restart Scheduler launch daemon
launchctl load ~/Library/LaunchAgents/$PRESSROOM_SCHEDULER_PLIST_FILE