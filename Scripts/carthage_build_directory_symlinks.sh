#!/bin/bash
set -e

# (Helper) Path to the 'Scripts' directory, regardless of which location this script was executed from
SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
# 'pressroom' directory is one level up from 'Scripts' directory
PRESSROOM_DIR=$(dirname ${SCRIPTS_DIR})

echo "Adding symlink to '${PRESSROOM_DIR}/Carthage/Build' for each Manuscripts dependency at 'SomeDependency/Carthage/Build' if the 'Carthage' intermediate directory exists..."

for D in "${PRESSROOM_DIR}"/Carthage/Checkouts/Manuscripts/Carthage/Checkouts/*; do
    if [ -d "${D}" ] && [ -d "${D}/Carthage/" ]; then
        ln -sf "${PRESSROOM_DIR}/Carthage/Build/" "${D}/Carthage/"
    fi
done

echo "Symlinks added successfully!"