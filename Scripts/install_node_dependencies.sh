#!/bin/bash

set -e

# mathjax-pandoc-filter is a sub-dependency of 'sachs'
npm install mathjax-pandoc-filter@0.4.0 -g
# Ensure 'sachs' command-line utility is installed globally via NPM (used internally for Manuscripts Project Bundle -> JATS XML or HTML transformations)
npm install @manuscripts/sachs@0.11.0 -g