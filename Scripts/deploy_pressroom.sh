#!/bin/bash

# Check if all required env vars are set for the script
if [[ -z "$CI_COMMIT_TAG_OR_SHORT_SHA" ]] ; then echo "'CI_COMMIT_TAG_OR_SHORT_SHA' not set."; exit 1; fi
if [[ -z "$MPPressroomServerJWTSecret" ]] ; then echo "'MPPressroomServerJWTSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerAPIKey" ]] ; then echo "'MPPressroomServerAPIKey' not set."; exit 1; fi
if [[ -z "$MPPressroomServerSwiftyBeaverAppID" ]] ; then echo "'MPPressroomServerSwiftyBeaverAppID' not set."; exit 1; fi
if [[ -z "$MPPressroomServerSwiftyBeaverAppSecret" ]] ; then echo "'MPPressroomServerSwiftyBeaverAppSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerSwiftyBeaverEncryptionKey" ]] ; then echo "'MPPressroomServerSwiftyBeaverEncryptionKey' not set."; exit 1; fi
if [[ -z "$MPPressroomServerIAMTokenIntrospectionSecret" ]] ; then echo "'MPPressroomServerIAMTokenIntrospectionSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerEdifixSecret" ]] ; then echo "'MPPressroomServerEdifixSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerExtylesArcSecret" ]] ; then echo "'MPPressroomServerExtylesArcSecret' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPUsername" ]] ; then echo "'MPPressroomServerLiteratumSFTPUsername' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPHostName" ]] ; then echo "'MPPressroomServerLiteratumSFTPHostName' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPIdentityFilePath" ]] ; then echo "'MPPressroomServerLiteratumSFTPIdentityFilePath' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumSFTPRemotePathPrefix" ]] ; then echo "'MPPressroomServerLiteratumSFTPRemotePathPrefix' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSUsername" ]] ; then echo "'MPPressroomServerLiteratumFTPSUsername' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSPassword" ]] ; then echo "'MPPressroomServerLiteratumFTPSPassword' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSHostName" ]] ; then echo "'MPPressroomServerLiteratumFTPSHostName' not set."; exit 1; fi
if [[ -z "$MPPressroomServerLiteratumFTPSRemotePathPrefix" ]] ; then echo "'MPPressroomServerLiteratumFTPSRemotePathPrefix' not set."; exit 1; fi
if [[ -z "$MPPressroomServerEEODepositServiceSecret" ]] ; then echo "'MPPressroomServerEEODepositServiceSecret' not set."; exit 1; fi

# (Helper) Path to the 'Scripts' directory, regardless of which location this script was executed from
DEPLOY_PRESSROOM_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
# (Helper) Path to the PressroomServer launchd plist file
PRESSROOM_PLIST_FILE="com.manuscripts.pressroomserver.plist"

# Copy boot_pressroom.sh to another location so it remains fixed for this deployment (overwrites existing)
mkdir -p $HOME/.PressroomServerScripts && cp $DEPLOY_PRESSROOM_SCRIPT_DIR/boot_pressroom.sh $HOME/.PressroomServerScripts

# Unload the current PressroomServer launch daemon (if one is already running)
launchctl unload ~/Library/LaunchAgents/$PRESSROOM_PLIST_FILE

# Generate a `launchd` configuration plist for PressroomServer
/usr/libexec/PlistBuddy -c "add Label string com.manuscripts.pressroomserver" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables dict" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:XCARCHIVE_LOCATION string $HOME/.PressroomServerArchives/PressroomServer-$CI_COMMIT_TAG_OR_SHORT_SHA.xcarchive" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerJWTSecret string $MPPressroomServerJWTSecret" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerAPIKey string $MPPressroomServerAPIKey" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerSwiftyBeaverAppID string $MPPressroomServerSwiftyBeaverAppID" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerSwiftyBeaverAppSecret string $MPPressroomServerSwiftyBeaverAppSecret" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerSwiftyBeaverEncryptionKey string $MPPressroomServerSwiftyBeaverEncryptionKey" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerIAMTokenIntrospectionSecret string $MPPressroomServerIAMTokenIntrospectionSecret" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerEdifixSecret string $MPPressroomServerEdifixSecret" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerExtylesArcSecret string $MPPressroomServerExtylesArcSecret" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumSFTPUsername string $MPPressroomServerLiteratumSFTPUsername" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumSFTPHostName string $MPPressroomServerLiteratumSFTPHostName" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumSFTPIdentityFilePath string $MPPressroomServerLiteratumSFTPIdentityFilePath" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumSFTPRemotePathPrefix string $MPPressroomServerLiteratumSFTPRemotePathPrefix" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumFTPSUsername string $MPPressroomServerLiteratumFTPSUsername" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumFTPSPassword string $MPPressroomServerLiteratumFTPSPassword" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumFTPSHostName string $MPPressroomServerLiteratumFTPSHostName" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerLiteratumFTPSRemotePathPrefix string $MPPressroomServerLiteratumFTPSRemotePathPrefix" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add EnvironmentVariables:MPPressroomServerEEODepositServiceSecret string $MPPressroomServerEEODepositServiceSecret" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add ProgramArguments array" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add ProgramArguments:0 string $HOME/.PressroomServerScripts/boot_pressroom.sh" $PRESSROOM_PLIST_FILE
/usr/libexec/PlistBuddy -c "add KeepAlive bool true" $PRESSROOM_PLIST_FILE

# Move the launch daemon plist file into place
mv $PRESSROOM_PLIST_FILE ~/Library/LaunchAgents/$PRESSROOM_PLIST_FILE

# Load (and implicitly start) the newly-installed PressroomServer launch daemon
launchctl load ~/Library/LaunchAgents/$PRESSROOM_PLIST_FILE

# Also implicitly install the PressroomServer Restart Scheduler daemon (schedules restarts at 01:00 local time)
"$DEPLOY_PRESSROOM_SCRIPT_DIR/restart_scheduler_pressroom.sh"
