//
//  main.swift
//  App
//
//  Created by Dan Browne on 04/06/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import App
import Dispatch
import Foundation

// We need to let GCD own the main queue
// - unlike AppKit/UIKit, the main dispatch queue is not automatically
// initialised for us at the entry point to the Vapor app.

// We kick off the main queue, ensuring it's not immediately blocked
let serverQueue = DispatchQueue(label: "manuscripts.api.server.queue")
serverQueue.async {
    // Run the Vapor app
    //
    // (The serverQueue is immediately blocked, as `run()` executes forever,
    // but this is fine as we want to not be blocking the main queue instead)
    // swiftlint:disable:next force_try
    try! app(.detect()).run()
}

// Allow GCD to take over the main thread, using it for the main queue
//
// (Calling `dispatchMain()` instead of `RunLoop.current.run()` would NOT ensure the main queue runs on the main thread,
// however we need to ensure this mapping is in tact because the Manuscripts app
// makes this assumption in several places).
RunLoop.current.run()
