//
//  RESTAPIClient.swift
//  App
//
//  Created by Dan Browne on 03/09/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

protocol RESTAPIClient {

    /// An enum describing the available endpoint URLs for the REST API service.
    associatedtype Endpoint: RawRepresentable where Endpoint.RawValue: StringProtocol

    /// A base URL for the REST API service.
    static var baseURL: String { get }
}

protocol AuthenticatedRESTAPIClient: RESTAPIClient {

    /// A secret to use for accessing a REST API service that requires authentication.
    var secret: ServiceSecret { get }
}
