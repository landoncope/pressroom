//
//  IAMTokenIntrospectionService.swift
//  App
//
//  Created by Dan Browne on 07/05/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

final class IAMTokenIntrospectionService: RESTAPIClient {

    // MARK: - Public enum definitions

    /// An enum describing the token introspection endpoints
    ///
    /// - introspect: The default token introspection endpoint.
    enum Endpoint: String {
        case introspect = "api/oauth/introspect"

        var defaultHeaders: HTTPHeaders {
            switch self {
            case .introspect:
                let secret: String = Environment.essentialValueForKey(.tokenIntrospectionSecret)
                let authorizationHeader = "Basic \(secret)"
                return [HTTPHeaderName.authorization.stringValue: authorizationHeader]
            }
        }
    }

    static let baseURL = "https://www.atyponconnect.com"

    // MARK: - Private struct definitions and variables

    /// Represents the token payload that should be sent to the token introspection service.
    private struct IAMTokenPayload: Content {
        let token: String
    }

    /// Represents the possible response values from the token introspection service.
    /// (All properties are Optional because the response structure differs depending on if
    /// the token is valid, invalid, or if some other error has occurred).
    private struct IntrospectionResponse: Content {
        let active: Bool?

        let error: String?
        let errorDescription: String?

        let audience: [String]?
        let issuer: String?
        let clientID: String?
        let scope: String?
        let subject: String?
        let tokenType: String?

        let issuedAt: UInt?
        let expiry: UInt?

        // swiftlint:disable:next nesting
        enum CodingKeys: String, CodingKey {
            case active
            case error
            case errorDescription = "error_description"
            case audience   = "aud"
            case issuer     = "iss"
            case clientID   = "client_id"
            case scope
            case subject    = "sub"
            case tokenType  = "token_type"
            case issuedAt   = "iat"
            case expiry     = "exp"
        }
    }

    // MARK: - Public methods

    /// Introspect a received OAuth token (that was attached to a `Request`) to check its validity.
    ///
    /// - Parameters:
    ///   - token: The received token `String`.
    ///   - endpoint: Optionally specify which `Endpoint` to use when introspecting the `token`
    ///               (defaults to `.introspect` if it is not provided).
    ///   - headers: Optional custom `HTTPHeaders` to attach to the `Request` that is sent to the token introspection
    ///              service (the default headers for the `endpoint` will be used if custom headers are not provided).
    ///   - originatingRequest: The `Request` that was initially received by Pressroom
    ///                         (should contain a `Authorization` header with a Bearer OAuth token).
    /// - Returns: A `Future<Void>` which is fulfilled successfully if the token is valid.
    /// - Throws: An `PressroomError` if the token is invalid or the introspection fails for some reason.
    func result(for token: String,
                endpoint: Endpoint? = nil,
                headers: HTTPHeaders? = nil,
                originatingRequest: Request) throws -> Future<Void> {
        // If custom headers have been provided use them, otherwise use the default headers for the specified endpoint
        let endpoint = endpoint != nil ? endpoint! : .introspect
        let headers = headers != nil ? headers! : endpoint.defaultHeaders

        // POST to the token introspection web service
        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(endpoint.rawValue)",
        headers: headers) { introspectionRequest in

            // Init the introspection service `Request` content before the POST is triggered
            let tokenPayload = IAMTokenPayload(token: token)
            try introspectionRequest.content.encode(tokenPayload, as: .urlEncodedForm)

            }.flatMap { apiResponse -> Future<Void> in
                return try apiResponse.content.decode(IntrospectionResponse.self).flatMap { introspectionResponse -> Future<Void> in
                    // Inspect the decoded token properties
                    if let active = introspectionResponse.active {
                        if active {
                            // Valid token
                            return originatingRequest.eventLoop.newSucceededFuture(result: ())
                        } else {
                            // Invalid token
                            let error = PressroomError.error(for: IAMTokenIntrospectionDebuggingInfo.invalidToken,
                                                             sourceLocation: .capture())

                            return originatingRequest.eventLoop.newFailedFuture(error: error)
                        }
                    } else if let introspectionError = introspectionResponse.error {
                        // Introspection error occurred
                        var errorString = introspectionError.count > 0 ? introspectionError : "Error"
                        if let errorDescription = introspectionResponse.errorDescription, errorDescription.count > 0 {
                            errorString += ": \(errorDescription)"
                        }
                        let introspectionError = UnderlyingIntrospectionError(status: apiResponse.http.status,
                                                                              reason: errorString)
                        let error = PressroomError.error(for: IAMTokenIntrospectionDebuggingInfo.introspectionError,
                                                         sourceLocation: .capture(),
                                                         underlyingError: introspectionError)

                        return originatingRequest.eventLoop.newFailedFuture(error: error)
                    } else {
                        // Return an `Error` if the introspection service response is of an unexpected structure
                        let error = PressroomError.error(for: IAMTokenIntrospectionDebuggingInfo.unexpectedResponse,
                                                         sourceLocation: .capture())

                        return originatingRequest.eventLoop.newFailedFuture(error: error)
                    }
                }
        }
    }
}
