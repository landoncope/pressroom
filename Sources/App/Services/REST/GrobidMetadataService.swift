//
//  GrobidMetadataService.swift
//  App
//
//  Created by Dan Browne on 22/02/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

/// Extracts metadata from a provided PDF file via the Grobid web service into Grobid
/// (i.e. Grobid-flavored TEI) XML format.
final class GrobidMetadataService: RESTAPIClient {

    // MARK: - Enum and struct definitions

    /// An enum describing the PDF to Grobid (i.e. Grobid-flavored TEI) XML metadata extraction endpoints
    ///
    /// - processHeaderDocument: Extract the header of the input PDF document, normalize it
    ///                          and convert it into a TEI XML format.
    /// - proccessFulltextDocument: Convert the complete input document into TEI XML format
    ///                             (header, body and bibliographical section).
    /// - processReferences: Extract and convert all the bibliographical references present
    ///                      in the input document into TEI XML format.
    enum Endpoint: String {

        case processHeaderDocument      = "api/processHeaderDocument"
        case proccessFulltextDocument   = "api/processFulltextDocument"
        case processReferences          = "api/processReferences"

        var defaultHeaders: HTTPHeaders {
            switch self {
            case .processHeaderDocument:
                return [Headers.consolidateHeader.stringValue: "1"]
            case .proccessFulltextDocument:
                return  [Headers.consolidateHeader.stringValue: "1",
                         Headers.consolidateCitations.stringValue: "0"]
            case .processReferences:
                return [Headers.consolidateCitations.stringValue: "0"]
            }
        }
    }

    /// An enum describing the `String` document metadata fragment to Grobid
    /// (i.e. Grobid-flavored TEI) XML metadata extraction endpoints.
    ///
    /// - processDate: Parse a raw date string and return the corresponding normalized date in ISO 8601
    ///                embedded in a TEI fragment.
    /// - processHeaderNames: Parse a raw string corresponding to a name or a sequence of names from a header
    ///                       section and return the corresponding normalized authors in TEI format.
    /// - processCitationNames: Parse a raw sequence of names from a bibliographical reference and
    ///                         return the corresponding normalized authors in TEI format.
    /// - processAffiliations: Parse a raw sequence of affiliations/addresses with or without address and
    ///                        return the corresponding normalized affiliations with address in TEI format.
    /// - processCitation: Parse a raw bibliographical reference (in isolation) and return the corresponding
    ///                    normalized bibliographical reference in TEI format.
    enum QueryEndpoint: String {

        // Raw text to TEI XML conversion
        case processDate                = "api/processDate"
        case processHeaderNames         = "api/processHeaderNames"
        case processCitationNames       = "api/processCitationNames"
        case processAffiliations        = "api/processAffiliations"
        case processCitation            = "api/processCitation"

        var defaultHeaders: HTTPHeaders {
            switch self {
            case .processDate,
                 .processHeaderNames,
                 .processCitationNames,
                 .processAffiliations:
                return [:]
            case .processCitation:
                return [Headers.consolidateCitations.stringValue: "0"]
            }
        }
    }

    static let baseURL = "https://grobid.manuscripts.io"

    /// Represents Grobid document content
    /// (of any given format representable directly in UTF8-encoding, or in a compressed container)
    /// that is intended to be attached to a Grobid `Request`, where it will be attached as multi-part form data.
    struct GrobidDocument: Content {
        /// The document content, parsed into a `File` object containing the underlying `Data`,
        /// along with other file metadata.
        let input: File
    }

    /// Represents `String` document metadata fragment(s) that should be treated by the Grobid web service
    /// as one or more dates.
    struct GrobidDate: Content {
        let date: String
    }

    /// Represents `String` document metadata fragment(s) that should be treated by the Grobid web service
    /// as one or more author names.
    struct GrobidNames: Content {
        let names: String
    }

    /// Represents `String` document metadata fragment(s) that should be treated by the Grobid web service
    /// as one or more author affiliations.
    struct GrobidAffiliations: Content {
        let affiliations: String
    }

    /// Represents `String` document metadata fragment(s) that should be treated by
    /// the Grobid web service as one or more citations.
    struct GrobidCitations: Content {
        let citations: String
    }

    // MARK: - Private variables

    /// Optional configuration headers for attempting to complete metadata via additonal calls to CrossRef API,
    /// and returning the coordinates of specific metadata elements in the source PDF.
    private struct Headers {
        static let consolidateHeader    = HTTPHeaderName("consolidateHeader")
        static let consolidateCitations = HTTPHeaderName("consolidateCitations")
        static let teiCoordinates       = HTTPHeaderName("teiCoordinates")
    }

    // MARK: - Public methods

    /// Transform a `String` containing one or more document data fragments (e.g. dates, affiliations, citations etc)
    /// to Grobid XML contained in a `Response`.
    ///
    /// - Parameters:
    ///   - string: The document data fragment(s).
    ///   - endpoint: The `QueryEndpoint` to use to process the document data fragment(s).
    ///   - headers: Optional custom `HTTPHeaders` to attach to the `Request` that is sent to the  Grobid API
    ///              (the default headers for the `endpoint` will be used if custom headers are not provided).
    ///   - originatingRequest: The `Request` that was initially received by Pressroom
    ///                         (should contain a `Pressroom-Enrich-Document-Metadata` header).
    /// - Returns: A `Response` containing a Grobid XML record of the document data fragment(s).
    /// - Throws: An `Error` if the transformation to Grobid XML fails for some reason.
    func metadataResponse(for string: String,
                          endpoint: QueryEndpoint,
                          headers: HTTPHeaders? = nil,
                          originatingRequest: Request) throws -> Future<Response> {

        // If custom headers have been provided use them, otherwise use the default headers for the specified endpoint
        let headers = headers != nil ? headers! : endpoint.defaultHeaders

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(endpoint.rawValue)",
        headers: headers) { grobidRequest in
            // Encode the document data fragment(s) to the relevant Grobid query parameter
            // `Data` format for the given `QueryEndpoint`
            grobidRequest.http.contentType = .urlEncodedForm
            try grobidRequest.encodeGrobidQueryParameter(with: string, for: endpoint)
        }
    }

    /// Extract specific document metadata (or the full-text) from a PDF file,
    /// depending on which Grobid API endpoint is specified.
    ///
    /// - Parameters:
    ///   - pdfFileURL: A file `URL` to a PDF stored on-disk.
    ///   - endpoint: The `Endpoint` to use to extract either specific document metadata,
    ///               or the full-text of the document.
    ///   - headers: Optional custom `HTTPHeaders` to attach to the `Request` that is sent to the  Grobid API
    ///              (the default headers for the `endpoint` will be used if custom headers are not provided).
    ///   - originatingRequest: The `Request` that was initially received by Pressroom
    ///                         (should contain a `Pressroom-Enrich-Document-Metadata` header).
    /// - Returns: A `Response` containing a Grobid XML record of the extract document metadata (or document full-text).
    /// - Throws: An `Error` if the transformation to Grobid XML fails for some reason.
    func metadataResponse(for pdfFileURL: URL,
                          endpoint: Endpoint,
                          headers: HTTPHeaders? = nil,
                          originatingRequest: Request) throws -> Future<Response> {

        // If custom headers have been provided use them, otherwise use the default headers for the specified endpoint
        let headers = headers != nil ? headers! : endpoint.defaultHeaders

        // POST a `Request` to Grobid for metadata extraction,
        // and return a `Future<Response>` which will resolve eventually
        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(endpoint.rawValue)",
        headers: headers) { grobidRequest in
            // Init a encodable representation of the PDF on disk,
            // then encode it into multipart form data on a `Request`
            let pdfToExtract = GrobidDocument(input: File(data: try Data(contentsOf: pdfFileURL),
                                                          filename: "pdf_to_inspect.pdf"))
            try grobidRequest.content.encode(pdfToExtract, as: .formData)
        }
    }
}

// MARK: - Grobid query parameter encoding extension

extension Request {

    /// The `Data` representation of `String` document metadata fragment(s) for use in a Grobid API `Request`,
    /// type-encoded depending on the `QueryEndpoint` specified.
    ///
    /// - Parameter endpoint: The `QueryEndpoint` to use to process the document data fragment(s).
    /// - Returns: The `Data` representation of `String` document metadata fragment(s).
    /// - Throws: An `Error` if the data encoding fails for some reason.

    /// Encodes the relevant query parameter on the receiver depending on the `QueryEndpoint` specified, corresponding
    /// to a `String` of document metadata fragment(s) for use in a Grobid API `Request`.
    ///
    /// - Parameters:
    ///   - string: The document metadata fragment(s) `String` to encode.
    ///   - endpoint: The `QueryEndpoint` that the receiver is being directed to
    ///               (on which the query parameters are being encoded).
    /// - Throws: An `Error` if the query parameter encoding or string serialization fails for some reason.
    func encodeGrobidQueryParameter(with string: String, for endpoint: GrobidMetadataService.QueryEndpoint) throws {
        switch endpoint {
        case .processDate:
            try query.encode(GrobidMetadataService.GrobidDate(date: string))
        case .processHeaderNames:
            try query.encode(GrobidMetadataService.GrobidNames(names: string))
        case .processCitationNames:
            try query.encode(GrobidMetadataService.GrobidNames(names: string))
        case .processAffiliations:
            try query.encode(GrobidMetadataService.GrobidAffiliations(affiliations: string))
        case .processCitation:
            try query.encode(GrobidMetadataService.GrobidCitations(citations: string))
        }
    }
}
