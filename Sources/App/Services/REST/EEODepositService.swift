//
//  EEODepositService.swift
//  App
//
//  Created by Dan Browne on 19/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

// swiftlint:disable nesting

/// Manages depositing documents to the EEO Deposit Service API.
final class EEODepositService: RESTAPIClient {

    // MARK: - Public enum and struct definitions

    /// An enum describing the available EEO Deposit Service API endpoints.
    enum Endpoint: String {
        case authenticate = "v1/api/oauth2/token"
        case deposit = "v1/api/eeo/deposit"

        var defaultHeaders: HTTPHeaders {
            switch self {
            case .authenticate:
                let secret: String = Environment.essentialValueForKey(.eeoDepositServiceSecret)
                let authorizationHeader = "\(OAuth2Token.TokenType.basic.rawValue) \(secret)"
                return [HTTPHeaderName.authorization.stringValue: authorizationHeader]
            case .deposit:
                return [:]
            }
        }
    }

    struct OAuth2Token: Decodable {

        enum TokenScope: String, Codable {
            case eeoDeposit
        }

        enum TokenType: String, Decodable {
            case basic
            case bearer
        }

        let token: String
        let scope: TokenScope
        let type: TokenType
        let expiresAt: Date

        func isValid() -> Bool {
            return expiresAt.timeIntervalSinceNow < 0
        }

        func authHeader() -> HTTPHeaders {
            return [HTTPHeaderName.authorization.stringValue: "\(type.rawValue) \(token)"]
        }
    }

    struct AuthenticationAPIResponse: Decodable {

        enum CodingKeys: String, CodingKey {
            case accessToken = "access_token"
            case scope
            case tokenType = "token_type"
            case expiresIn = "expires_in"

            case error
            case message
            case timestamp
        }

        let accessToken: OAuth2Token?

        let error: String?
        let message: String?
        let timestamp: Date?

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)

            if let expiresIn = try values.decodeIfPresent(Int.self, forKey: .expiresIn),
                let token = try values.decodeIfPresent(String.self, forKey: .accessToken),
                let scope = try values.decodeIfPresent(OAuth2Token.TokenScope.self, forKey: .scope),
                let type = try values.decodeIfPresent(OAuth2Token.TokenType.self, forKey: .tokenType) {
                let expiresAt = Date().addingTimeInterval(TimeInterval(expiresIn))
                accessToken = OAuth2Token(token: token,
                                          scope: scope,
                                          type: type,
                                          expiresAt: expiresAt)
            } else {
                accessToken = nil
            }

            error = try values.decodeIfPresent(String.self, forKey: .error)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            timestamp = try values.decodeIfPresent(Date.self, forKey: .timestamp)
        }
    }

    struct EEOSubmission {
        let frontMatterJATS: File
        let reviewPDF: File
        let identifier: String
        let journalName: String
        let notificationCallbackURL: String
    }

    /// An enum describing the possible error states of `EEODepositService`.
    private enum Error: LocalizedError {
        case authenticationFailure(String)

        var errorDescription: String? {
            switch self {
            case .authenticationFailure(let message):
                return "EEO Deposit Service API authentication failed: \(message)"
            }
        }
    }

    static let baseURL = "https://sd-stag.literatumonline.com"

    // MARK: - Private enum and struct definitions

    private struct AuthenticationRequestPayload: Content {

        enum GrantType: String, Content {
            case clientCredentials = "client_credentials"
        }

        enum CodingKeys: String, CodingKey {
            case grantType = "grant_type"
            case scope
        }

        let grantType: GrantType
        let scope: OAuth2Token.TokenScope
    }

    private struct DepositRequestPayload: Content {

        enum TextBool: String, Content {
            case `true`
            case `false`
        }

        enum CodingKeys: String, CodingKey {
            case frontMatterJATS = "frontMatterJats"
            case submissionFile
            case submissionId
            case journalName
            case notificationCallbackUrl
            case async
        }

        let frontMatterJATS: File
        let submissionFile: File
        let submissionId: String
        let journalName: String
        let notificationCallbackUrl: String
        let async: TextBool
    }

    // MARK: - API authentication

    /// Obtain an access token from the EEO Deposit Service API
    /// (using credentials supplied to Pressroom as an environment variable).
    /// - Parameter originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: An `Future<LogIAuthenticationAPIResponsenResponse>` with a session token,
    ///            or error details if the log in attempt was unsuccessful.
    /// - Throws: An `Error` if the log in attempt fails for some reason.
    func authenticate(originatingRequest: Request) throws -> Future<AuthenticationAPIResponse> {

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(Endpoint.authenticate.rawValue)",
        headers: Endpoint.authenticate.defaultHeaders) { authRequest in
            try authRequest.query.encode(AuthenticationRequestPayload(grantType: .clientCredentials,
                                                                       scope: .eeoDeposit))
        }.flatMap({ response -> Future<AuthenticationAPIResponse> in
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .millisecondsSince1970

            return try response.content.decode(json: AuthenticationAPIResponse.self, using: decoder)
                .flatMap({ authResponse -> Future<AuthenticationAPIResponse> in
                    guard authResponse.accessToken != nil,
                        authResponse.error == nil,
                        authResponse.message == nil,
                        authResponse.timestamp == nil else {
                            throw Error.authenticationFailure("\(authResponse.error!) - \(authResponse.message!)")
                    }
                    return originatingRequest.eventLoop.newSucceededFuture(result: authResponse)
            })
        })
    }

    // MARK: - Document deposit

    /// Deposit a document submission to the EEO Deposit Service API.
    /// - Parameters:
    ///   - submission: An `EEOSubmission`, containing an API access token, document content and metadata.
    ///   - accessToken: An `OAuth2Token` access token, obtained previously by calling `authenticate(originatingRequest: Request)`.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: An `Future<Response>` or error details if the log in attempt was unsuccessful.
    /// - Throws: An `Error` if the log in attempt fails for some reason.
    func deposit(submission: EEOSubmission, accessToken: OAuth2Token, originatingRequest: Request) throws -> Future<Response> {

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(Endpoint.deposit.rawValue)",
        headers: accessToken.authHeader()) { depositRequest in
            try depositRequest.content.encode(DepositRequestPayload(frontMatterJATS: submission.frontMatterJATS,
                                                                    submissionFile: submission.reviewPDF,
                                                                    submissionId: submission.identifier,
                                                                    journalName: submission.journalName,
                                                                    notificationCallbackUrl: submission.notificationCallbackURL,
                                                                    async: .true),
                                              as: .formData)
        }
    }
}
