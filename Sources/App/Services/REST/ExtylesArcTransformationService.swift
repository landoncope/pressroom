//
//  ExtylesArcTransformationService.swift
//  App
//
//  Created by Dan Browne on 02/09/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

// swiftlint:disable identifier_name nesting

/// Transforms the full-text of a MS Word .docx file (or just its metadata) into JATS XML.
final class ExtylesArcTransformationService: AuthenticatedRESTAPIClient {

    // MARK: - Public enum and struct definitions

    /// An enum describing the available eXtyles Arc API endpoints.
    enum Endpoint: String {
        case logIn      = "api/login"
        case logOut     = "api/logout"
        case createJob  = "api/create_job"
        case checkJob   = "api/check_job_status"
        case getJob     = "api/get_results"
    }

    static let baseURL = "https://www.extylesarc.com"

    let secret: ServiceSecret

    enum EditorialStyle: String, Content {
        case noFormat = "(No Format)"
        case ama = "AMA"
        case apa = "APA"
        case ieee = "IEEE"
    }

    struct JobId: Content {
        let jobId: Int

        enum CodingKeys: String, CodingKey {
            case jobId = "job_id"
        }
    }

    struct LogInResponse: Content {
        let token: String
    }

    struct CreateJobResponse: Content {
        let jobId: Int
    }

    struct CheckJobResponse: Content {
        enum Status: String, Content {
            case pending
            case inProgress = "in_progress"
            case finished
            case error
        }

        let status: Status
    }

    // MARK: - Private Public enum and struct definitions

    private struct LogInRequestPayload: Content {
        let username: String
        let password: String
        let apiKey: String

        enum CodingKeys: String, CodingKey {
            case username
            case password
            case apiKey = "api_key"
        }
    }

    private struct AuthenticationAPIResponse: Content {
        let token: String?
        let status: ResponseStatus
        let message: String
    }

    fileprivate struct CreateJobRequestPayload: Content {

        enum ProcessingLevel: String, Content {
            case fullText = "FULLTEXT"
            case frontMatterOnly = "METADATA"
            case referencesOnly = "REFERENCES"
            case metadataAndReferencesOnly = "METAREFS"
            case exportOnly = "EXPORTONLY"
            case fullTextWithImages = "FULLTEXT_WITH_IMAGES"
        }

        let inputFileName: String
        let inputFileType: ProcessingLevel
        let editorialStyle: EditorialStyle
        let file: File

        enum CodingKeys: String, CodingKey {
            case inputFileName = "input_file_name"
            case inputFileType = "input_file_type"
            case editorialStyle = "editorial_style"
            case file
        }
    }

    private struct CreateJobAPIResponse: Content {
        let jobId: Int?
        let status: ResponseStatus
        let message: String
        let numberOfJobsInQueue: Int
    }

    private struct CheckJobAPIResponse: Content {

        enum JobStatus: String, Content {
            case new        = "NEW"
            case running    = "RUNNING"
            case finished   = "FINISHED"
            case error      = "ERROR"

            func responseRepresentation() -> CheckJobResponse.Status {
                switch self {
                case .new:
                    return .pending
                case .running:
                    return .inProgress
                case .finished:
                    return .finished
                case .error:
                    return .error
                }
            }
        }

        let inputFilename: String?
        let outputFilename: String?
        let editorialStyle: EditorialStyle?
        let inputSource: InputSource?
        let expectedTime: Int?
        let actualTime: Int?
        let maximumTime: Int?
        let jobStatus: JobStatus?
        let status: ResponseStatus
        let message: String
    }

    private struct GetJobAPIResponse: Content {
        let jobId: Int?
        let status: ResponseStatus
        let message: String
    }

    private struct APIHeaders {
        static let token = HTTPHeaderName("token")
    }

    private enum InputSource: String, Content {
        case ui = "UI"
        case api = "API"
    }

    private enum ResponseStatus: String, Content {
        case ok
        case error
    }

    /// An enum describing the possible error states of `ExtylesArcTransformationService`.
    private enum Error: LocalizedError {
        case logInFailure(String)
        case createJobFailure(String)
        case checkJobStatusFailure(String)
        case getJobResultsFailure(String)
        case logOutFailure(String)

        var errorDescription: String? {
            switch self {
            case .logInFailure(let message):
                return "API log in failed: \(message)"
            case .createJobFailure(let message):
                return "Creating the transformation job failed: \(message)"
            case .checkJobStatusFailure(let message):
                return "Checking the transformation job status failed: \(message)"
            case .getJobResultsFailure(let message):
                return "Getting the transformation job results failed: \(message)"
            case .logOutFailure(let message):
                return "API Log out failed: \(message)"
            }
        }
    }

    // MARK: - Life cycle

    init(secret: ServiceSecret) {
        self.secret = secret
    }

    // MARK: - API authentication

    /// Log in to the eXtyles Arc web service (using credentials supplied to Pressroom as an environment variable).
    ///
    /// - Parameter originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: An `Future<LogInResponse>` with a session token, or error details if the log in attempt was unsuccessful.
    /// - Throws: An `Error` if the log in attempt fails for some reason.
    func logIn(originatingRequest: Request) throws -> Future<LogInResponse> {

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(Endpoint.logIn.rawValue)") { logInRequest in
            try logInRequest.content.encode(LogInRequestPayload(username: secret.decode(.username),
                                                                password: secret.decode(.password),
                                                                apiKey: secret.decode(.key)))
            }.flatMap({ response -> Future<LogInResponse> in
                return try response.content.decode(AuthenticationAPIResponse.self).flatMap({ authResponse -> Future<LogInResponse> in
                    guard authResponse.token != nil, authResponse.status == .ok else {
                        throw Error.logInFailure(authResponse.message)
                    }

                    return originatingRequest.eventLoop.newSucceededFuture(result: LogInResponse(token: authResponse.token!))
                })
            })
    }

    /// Log out of the eXtyles Arc web service, for a given active session token.
    ///
    /// - Parameters:
    ///   - token: An active session token.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: An `Future<Void>`, if the log out attempt was successful.
    /// - Throws: An `Error` if the log out attempt fails for some reason. (e.g. no active session matching the token).
    func logOut(token: String, originatingRequest: Request) throws -> Future<Void> {

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(Endpoint.logOut.rawValue)") { logOutRequest in
            logOutRequest.http.headers.add(name: APIHeaders.token, value: token)
            }.flatMap({ response -> Future<Void> in
                return try response.content.decode(AuthenticationAPIResponse.self).flatMap({ authResponse -> Future<Void> in
                    guard authResponse.token == nil, authResponse.status == .ok else {
                        throw Error.logOutFailure(authResponse.message)
                    }

                    return originatingRequest.eventLoop.newSucceededFuture(result: ())
                })
            })
    }

    // MARK: - Transformation Job management

    /// Submits a MS Word .docx file for transformation to JATS XML, customized based on provided configuration options.
    ///
    /// - Parameters:
    ///   - documentProcessingLevel: The desired document processing level for the document transformation.
    ///   - editorialStyle: The desired style to use when formatting references included in the resulting JATS XML.
    ///   - file: A `File` representing the MS Word document to transform.
    ///   - token: An active session token.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: A `Future<CreateJobResponse>`, containing the job ID,
    ///            or error details if creating the transformation job was unsuccessful.
    /// - Throws: An `Error` if the job creation attempt fails for some reason.
    func createJob(documentProcessingLevel: DocumentProcessingLevel,
                   editorialStyle: EditorialStyle,
                   file: File,
                   token: String,
                   originatingRequest: Request) throws -> Future<CreateJobResponse> {

        return try originatingRequest.client().post("\(type(of: self).baseURL)/\(Endpoint.createJob.rawValue)") { createJobRequest in
            try createJobRequest.content.encode(CreateJobRequestPayload(inputFileName: file.filename,
                                                                        inputFileType: documentProcessingLevel.payloadRepresentation(),
                                                                        editorialStyle: editorialStyle,
                                                                        file: file), as: .formData)
            createJobRequest.http.headers.add(name: APIHeaders.token, value: token)
            }.flatMap({ response -> Future<CreateJobResponse> in
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase

                // swiftlint:disable:next line_length
                return try response.content.decode(json: CreateJobAPIResponse.self, using: decoder).flatMap({ createJobResponse -> Future<CreateJobResponse> in
                    guard createJobResponse.jobId != nil, createJobResponse.status == .ok else {
                        throw Error.createJobFailure(createJobResponse.message)
                    }

                    return originatingRequest.eventLoop.newSucceededFuture(result: CreateJobResponse(jobId: createJobResponse.jobId!))
                })
            })
    }

    /// Checks the status of a previously-created transformation job.
    ///
    /// - Parameters:
    ///   - jobId: A `JobId`, containing an ID for a previously-created transformation job.
    ///   - token: An active session token.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: A `Future<CheckJobResponse>` containing the details of the job progress,
    ///            or error details if progress request was unsuccessful.
    /// - Throws: An `Error` if the job progress query fails for some reason.
    func checkJob(_ jobId: JobId,
                  token: String,
                  originatingRequest: Request) throws -> Future<CheckJobResponse> {

        return try originatingRequest.client().get("\(type(of: self).baseURL)/\(Endpoint.checkJob.rawValue)") { checkJobRequest in
            try checkJobRequest.query.encode(jobId)
            checkJobRequest.http.headers.add(name: APIHeaders.token, value: token)
            }.flatMap({ response -> Future<CheckJobResponse> in
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase

                // swiftlint:disable:next line_length
                return try response.content.decode(json: CheckJobAPIResponse.self, using: decoder).flatMap({ apiResponse -> Future<CheckJobResponse> in
                    guard apiResponse.status == .ok else {
                        throw Error.checkJobStatusFailure(apiResponse.message)
                    }

                    let checkJobResponse = CheckJobResponse(status: apiResponse.jobStatus!.responseRepresentation())

                    return originatingRequest.eventLoop.newSucceededFuture(result: checkJobResponse)
                })
            })
    }

    /// Returns the compiled JATS XML for a completed transformation job.
    ///
    /// - Parameters:
    ///   - jobId: A `JobId`, containing an ID for a previously-completed transformation job.
    ///   - token: An active session token.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: A `Future<Response>`.
    /// - Throws: An `Error` if the document fetch request fails for some reason.
    func getJob(_ jobId: JobId,
                token: String,
                originatingRequest: Request) throws -> Future<Response> {

        return try originatingRequest.client().get("\(type(of: self).baseURL)/\(Endpoint.getJob.rawValue)") { getJobRequest in
            try getJobRequest.query.encode(jobId)
            getJobRequest.http.headers.add(name: APIHeaders.token, value: token)
            }.flatMap({ response -> Future<Response> in

                // A completed transformation will result in a ZIP file,
                // An incomplete (or invalid based on job_id) transformation will give a JSON response
                if response.http.contentType == .zip {
                    return originatingRequest.eventLoop.newSucceededFuture(result: response)
                } else {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase

                    // swiftlint:disable:next line_length
                    return try response.content.decode(json: GetJobAPIResponse.self, using: decoder).thenThrowing({ errorResponse -> Response in
                        throw Error.getJobResultsFailure(errorResponse.message)
                    })
                }
            })
    }
}

// MARK: - `DocumentProcessingLevel` enum ExtylesArc extension

fileprivate extension DocumentProcessingLevel {

    func payloadRepresentation() -> ExtylesArcTransformationService.CreateJobRequestPayload.ProcessingLevel {
        switch self {
        case .fullText:
            return .fullTextWithImages
        case .frontMatterOnly:
            return .frontMatterOnly
        }
    }
}
