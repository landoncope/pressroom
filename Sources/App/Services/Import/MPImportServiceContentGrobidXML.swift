//
//  MPImportServiceContentGrobidXML.swift
//  App
//
//  Created by Dan Browne on 12/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import MPFoundation

public class MPImportServiceContentGrobidXML: MPImportServiceContent {

    /// A custom file extension for the TEI-flavored XML response from the Grobid web service.
    /// (This is a workaround for XML import service file extension inference).
    static let grobidXMLFileExtension = "grobidxml"

    // MARK: - Type definitions

    enum Error: LocalizedError {
        case abstractParsingFailed
        case affiliationKeyParsingFailed
        case affiliationInitializationFailed
        case contributorInitializationFailed
        case missingAffiliation(key: String)
        case missingManuscriptPackageController

        /// A custom error description to provide more context for the error
        /// (i.e. when using `error.localizedDescription`).
        var errorDescription: String? {
            switch self {
            case .abstractParsingFailed:
                return "The <abstract> node in the Grobid XML could not be represented as a String."
            case .affiliationKeyParsingFailed:
                return "The 'key' for an <affiliation> node could not be represented as a String."
            case .affiliationInitializationFailed:
                return """
                        The import operation failed when importing an <affiliation> node \
                        for an author from the Grobid XML.
                        """
            case .contributorInitializationFailed:
                return "The import operation failed when importing an <author> node from the Grobid XML."
            case .missingAffiliation(key: let key):
                return "A matching <affiliation> node for the key '\(key)' could not be found."
            case .missingManuscriptPackageController:
                return "The 'manuscriptsPackageController' for the Grobid import service is unexpectedly unitialized."
            }
        }
    }

    // MARK: - Public methods and variables

    override public static func name() -> String {
        return "Grobid TEI XML"
    }

    override public static func priority() -> Int {
        return MPImportServiceContentPriority.grobid.rawValue
    }

    override public static func pasteboardPriority() -> Int {
        return MPImportServiceContentPasteboardPriority.grobid.rawValue
    }

    override public static func nameShorthand() -> String {
        return "Grobid XML"
    }

    override public static func utis() -> Set<String> {
        return [MPImportServiceContentUTIGrobidXML]
    }

    override public static func fileExtensions() -> Set<String> {
        return [grobidXMLFileExtension]
    }

    override public static func mimeTypes() -> Set<String> {
        return ["application/tei+xml"]
    }

    override public static func canonicalFileExtension() -> String {
        return grobidXMLFileExtension
    }

    override public static func group() -> UInt {
        return MPImportServiceContentGroup.webService.rawValue
    }

    override public func imported(from url: URL, options: MPContentImportOptions?) throws -> [MPManagedObject] {

        // Initialize a XMLDocument of the Grobid XML response for introspection
        let grobidXML = try XMLDocument(contentsOf: url)
        // Array of imported managed objects
        var importedObjects = [MPManagedObject]()

        // First, check `manuscriptPackageController` is initialized
        guard let manuscriptPackageController = manuscriptPackageController else {
            throw Error.missingManuscriptPackageController
        }

        // Manuscript title
        if let title = try grobidXML.nodes(forXPath: "//titleStmt/title").first?.stringValue {
            manuscriptPackageController.manuscriptsController.primaryManuscript.title = title
        }

        // Manuscript abstract
        if let abstractNode = try grobidXML.nodes(forXPath: "//profileDesc/abstract").first,
            abstractNode.stringValue?.count ?? 0 > 0 {
            let grobidAbstract = try importedAbstract(from: abstractNode,
                                                      packageController: manuscriptPackageController)
            importedObjects.append(grobidAbstract)
        }

        // Manuscript authors and affiliations
        let authorNodes = try grobidXML.nodes(forXPath: "//biblStruct/analytic/author")

        if let grobidContributors = try importedContributors(from: authorNodes,
                                                             packageController: manuscriptPackageController) {
            importedObjects.append(contentsOf: grobidContributors)
        }

        return importedObjects
    }

    // MARK: - Private methods

    /// Imports an `MPSection` representing an abstract, based on an 'abstract' node present in the Grobid XML response.
    ///
    /// - Parameters:
    ///   - abstractNode: An `XMLNode` array representing an 'abstact' node present in the Grobid XML response.
    ///   - packageController: The 'MPManuscriptsPackageController' on which to persist the abstract `MPSection`.
    /// - Returns: An `MPSection` representing the abstract as present in the Grobid XML response.
    /// - Throws: An `Error` if the import operation fails for some reason.
    private func importedAbstract(from abstractNode: XMLNode,
                                  packageController: MPManuscriptsPackageController) throws -> MPSection {

        guard let grobidAbstractStringValue = abstractNode.stringValue else {
            throw Error.abstractParsingFailed
        }
        // Initialize the new abstract section based on the Grobid XML response
        let grobidAbstract = packageController.sectionsController.newSection(withParent: nil)
        grobidAbstract.saveSilently()

        // Abstract paragraph attributes
        let paragraphStyle = packageController.stylesController.defaultParagraphStyle(forXHTMLElement: "p")
        paragraphStyle?.saveSilently()
        let paragraphElement = MPParagraphElement(controller: packageController.elementsController,
                                                  elementType: "p",
                                                  innerHTML: grobidAbstractStringValue)
        paragraphElement.paragraphStyle = paragraphStyle
        paragraphElement.saveSilently()
        let sharedPackageController = MPSharedPackageController.sharedPackageController()
        grobidAbstract.category = sharedPackageController.sectionCategoriesController.sectionCategoryAbstract
        grobidAbstract.elements = [paragraphElement]
        grobidAbstract.title = "Abstract"
        grobidAbstract.saveSilently()

        return grobidAbstract
    }

    /// Imports `MPContributor` instances, based on any 'author' nodes present in the Grobid XML response.
    ///
    /// - Parameters:
    ///   - authorNodes: An `XMLNode` array representing any 'author' nodes present in the Grobid XML response.
    ///   - packageController: The 'MPManuscriptsPackageController' on which to persist `MPContributor` instances.
    /// - Returns: An optional array of `MPContributor` instances of the objects that have been imported to the
    ///            package controller.
    /// - Throws: An `Error` if the import operation fails for some reason.
    private func importedContributors(from authorNodes: [XMLNode],
                                      packageController: MPManuscriptsPackageController) throws -> [MPContributor]? {
        var contributors = [MPContributor]()

        // Import ALL available affiliations from the entire Grobid XML response
        guard let firstAuthor = authorNodes.first else {
            return nil
        }
        let globalAffiliationNodes = try firstAuthor.nodes(forXPath: "//affiliation")
        let importedAffiliationsByKey = try importedAffiliations(from: globalAffiliationNodes,
                                                                 packageController: packageController)

        // Only imports `<author>` nodes that also contain a nested `<persName>` node
        // (This will avoid spurious "Unknown Authors" in cases where Grobid only extracted affiliations)
        for (index, authorNode) in authorNodes.enumerated()
            where try authorNode.nodes(forXPath: ".//persName").count > 0 {
            // Author
            let firstNames = try authorNode.nodes(forXPath: ".//persName/forename[@type='first']").compactMap { $0.stringValue }
            let middleNames = try authorNode.nodes(forXPath: ".//persName/forename[@type='middle']").compactMap { $0.stringValue }
            let prenames = firstNames + middleNames
            let lastNames = try authorNode.nodes(forXPath: ".//persName/surname").compactMap { $0.stringValue }
            let emailAddress = try authorNode.nodes(forXPath: ".//email").first

            guard let contributor = packageController.contributorsController.newObject() as? MPContributor else {
                throw Error.contributorInitializationFailed
            }

            contributor.prename = prenames.count > 0 ? prenames.joined(separator: " ") : nil
            contributor.lastName = lastNames.count > 0 ? lastNames.joined(separator: " ") : nil
            contributor.role = MPContributorRoleAuthor
            contributor.email = emailAddress?.stringValue

            // Set the priority (based on the Grobid XML response,
            // which maintains the author order when extracting metadata)
            contributor.setValue(index, ofProperty: "priority")

            // Author affiliations
            contributor.affiliations = try importedAffiliations(for: authorNode,
                                                                mappedAffiliations: importedAffiliationsByKey)
            contributor.saveSilently()
            contributors.append(contributor)
        }

        return contributors
    }

    /// Imports `MPAffiliation` instances, based on any 'affiliation' nodes present in the Grobid XML response
    /// and uniquely imported depending on their 'key' attribute value.
    ///
    /// - Parameters:
    ///   - globalAffiliationNodes: An `XMLNode` array representing any 'affiliation' nodes present in the
    ///                             Grobid XML response.
    ///   - packageController: The 'MPManuscriptsPackageController' on which to persist `MPContributor` instances.
    /// - Returns: An dictionary of `MPAffiliation` instances of the objects that have been imported to the
    ///            package controller, mapped to their specified 'key' from the original Grobid XML response.
    /// - Throws: An `Error` if the import operation fails for some reason.
    private func importedAffiliations(from globalAffiliationNodes: [XMLNode],
                                      packageController: MPManuscriptsPackageController) throws
        -> [String: MPAffiliation] {
        var affiliationsByKey = [String: MPAffiliation]()

        for affiliationNode in globalAffiliationNodes {
            let keyValue = try affiliationKey(for: affiliationNode)
            if affiliationsByKey[keyValue] == nil {
                // swiftlint:disable:next line_length
                let laboratory = try affiliationNode.nodes(forXPath: ".//orgName[@type='laboratory']").compactMap { $0.stringValue }.joined(separator: " ")
                // swiftlint:disable:next line_length
                let department = try affiliationNode.nodes(forXPath: ".//orgName[@type='department']").compactMap { $0.stringValue }.joined(separator: " ")
                // swiftlint:disable:next line_length
                let institution = try affiliationNode.nodes(forXPath: ".//orgName[@type='institution']").compactMap { $0.stringValue}.joined(separator: " ")
                let streetAddress = try affiliationNode.nodes(forXPath: ".//address/street").first?.stringValue
                let addressLine = try affiliationNode.nodes(forXPath: ".//address/addrLine").first?.stringValue
                let addressLines = try affiliationNode.nodes(forXPath: ".//address/addrLine[@*]").compactMap { $0.stringValue }
                let city = try affiliationNode.nodes(forXPath: ".//address/settlement").first?.stringValue
                let county = try affiliationNode.nodes(forXPath: ".//address/region").first?.stringValue
                let postCode = try affiliationNode.nodes(forXPath: ".//address/postCode").first?.stringValue
                let country = try affiliationNode.nodes(forXPath: ".//address/country[@*]").first?.stringValue

                guard let affiliation = packageController.affiliationsController.newObject() as? MPAffiliation else {
                    throw Error.affiliationInitializationFailed
                }
                affiliation.department = department.count > 0 ? department : (laboratory.count > 0 ? laboratory : nil)
                affiliation.institution = institution.count > 0 ? institution : nil
                // swiftlint:disable:next line_length
                affiliation.addressLine1 = streetAddress != nil ? streetAddress : (addressLine != nil ? addressLine : addressLines[ifExists: 0])
                affiliation.addressLine2 = addressLines[ifExists: 1]
                affiliation.addressLine3 = addressLines[ifExists: 2]
                affiliation.city = city
                affiliation.county = county
                affiliation.postCode = postCode
                affiliation.country = country

                affiliation.saveSilently()
                affiliationsByKey[keyValue] = affiliation
            }
        }

        return affiliationsByKey
    }

    /// The previously-imported `MPAffiliation` objects for a given author node from the Grobid XML response.
    ///
    /// - Parameters:
    ///   - authorNode: A given author node from the Grobid XML response.
    ///   - mappedAffiliations: The `MPAffiliation` instances, mapped by their 'key' attribute value.
    /// - Returns: An array of `MPAffiliation` objects associated with the author node.
    /// - Throws: An `Error` if associated `MPAffiliation` objects cannot be found for some reason.
    private func importedAffiliations(for authorNode: XMLNode,
                                      mappedAffiliations: [String: MPAffiliation]) throws -> [MPAffiliation] {
        var affiliations = [MPAffiliation]()

        for affiliationNode in try authorNode.nodes(forXPath: ".//affiliation") {
            let affKey = try affiliationKey(for: affiliationNode)
            guard let parsedAffiliation = mappedAffiliations[affKey] else {
                throw Error.missingAffiliation(key: affKey)
            }
            affiliations.append(parsedAffiliation)
        }

        return affiliations
    }

    /// The identifier string used as an affiliation's 'key' in a Grobid XML response.
    ///
    /// - Parameter affiliationNode: An `XMLNode` representing an affiliation in a Grobid XML response.
    /// - Returns: The value of the 'key' attribute for the affiliation node.
    /// - Throws: An `Error` if the 'key' attribute cannot be found for some reason.
    private func affiliationKey(for affiliationNode: XMLNode) throws -> String {
        guard affiliationNode.kind == .element,
            let affiliationElement = affiliationNode as? XMLElement,
            let affiliationKeyAttribute = affiliationElement.attributes?.first(where: { attributeNode -> Bool in
                return attributeNode.name == "key"
            }),
            let keyValue = affiliationKeyAttribute.stringValue else {
                throw Error.affiliationKeyParsingFailed
        }
        return keyValue
    }
}
