//
//  LibreOfficeTransformationService.swift
//  App
//
//  Created by Dan Browne on 04/06/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

final class LibreOfficeTransformationService: PDFTransformable {

    /// An enum describing the possible document output formats of `LibreOfficeTransformationService`.
    enum OutputFormat: String {
        case docx
        case pdf
    }

    let continueOnErrors = false

    // MARK: - Public methods

    func transform(_ candidate: PrimaryFileCandidate) throws -> URL {
        return try transform(candidate, outputFormat: .pdf)
    }

    func transform(_ candidate: PrimaryFileCandidate, outputFormat: OutputFormat) throws -> URL {

        let candidateFileURL = try candidate.ensuredFileURL()
        let fileNameBase = candidateFileURL.deletingPathExtension().lastPathComponent

        do {
            // Create the 'transformed_documents' subdirectory if it doesn't already exist
            // swiftlint:disable:next line_length
            let transformedDocumentsDirectoryFileURL = candidateFileURL.deletingLastPathComponent().appendingPathComponent(type(of: self).transformedDocumentsDirectory)
            if FileManager.default.fileExists(atPath: transformedDocumentsDirectoryFileURL.path) == false {
                try FileManager.default.createDirectory(at: transformedDocumentsDirectoryFileURL,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            }

            // Transform the Word document into a finished PDF file
            let compiledPDFFileURL = transformedDocumentsDirectoryFileURL.appendingPathComponent("\(fileNameBase).\(outputFormat.rawValue)")
            _ = try executeCommand("/usr/local/bin/soffice",
                                   arguments: ["--headless",
                                               "--convert-to",
                                               outputFormat.rawValue,
                                               candidateFileURL.path,
                                               "--outdir",
                                               transformedDocumentsDirectoryFileURL.path])

            return compiledPDFFileURL
        } catch {
            // Clean-up the temporary working directory before throwing `Error`
            //
            // (Cannot `defer` here because then the clean-up for the success case would happen before the file
            // could be chunk-streamed in a `Response`, where `transform(...)` has been called up-stream.
            cleanWorkingDirectory(at: candidateFileURL.deletingLastPathComponent())

            // Re-throw error
            throw error
        }
    }
}
