//
//  CSLJSONTransformationService.swift
//  App
//
//  Created by Dan Browne on 24/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

// CSLJSONTransformationService depends on the 'citeproc-java' command-line utility:
// https://michel-kraemer.github.io/citeproc-java/

/// Transforms a CSL-JSON formatted bibliography into another format (or vice versa).
final class CSLJSONTransformationService: CommandLineTransformable {

    /// An enum describing the possible bibliography input formats to the service.
    enum InputFormat: String {
        /// The bibliography input is formatted as BibTeX.
        case bibTeX = "bib"
        /// The bibliography input is formatted as EndNote.
        case endNote = "end"
        /// The bibliography input is formatted as CSL-JSON.
        case JSON = "json"
        /// The bibliography input is formatted as RIS.
        case RIS = "ris"

        /// The file extension corresponding to the `InputFormat`.
        var fileExtension: String {
            switch self {
            case .endNote:
                return "enl"
            case .bibTeX, .JSON, .RIS:
                return rawValue
            }
        }
    }

    /// An enum describing the possible bibliography output formats from the service.
    enum OutputFormat: String {
        /// The bibliography output will be formatted as BibTeX.
        case bibTeX = "bibtex"

        /// The file extension corresponding to the `OutputFormat`.
        var fileExtension: String {
            switch self {
            case .bibTeX:
                return rawValue
            }
        }
    }

    let continueOnErrors = false

    // MARK: - Public methods

    /// Transform some bibliography data (formatted as BibTeX, EndNote, CSL-JSON or RIS) to a CSL-JSON representation.
    ///
    /// - Parameters:
    ///   - bibliographyData: `Data` (formatted as BibTeX, EndNote, CSL-JSON or RIS).
    ///   - inputFormat: The input format of the bibliography data before transformation
    ///                  (it should match the format of the data in `bibliographyData`).
    /// - Returns: `Data` representation of the resulting CSL-JSON output.
    /// - Throws: A `CommandLineTransformationError` if any stage of the
    ///           bibliography transformation fails for some reason.
    func transform(_ bibliographyData: Data, from inputFormat: InputFormat) throws -> Data {
        // Create working directory
        let workingDirectoryFileURL = try createWorkingDirectory()

        // Defer block for clean-up of working directory regardless of how executions exits this method's scope
        defer {
            cleanWorkingDirectory(at: workingDirectoryFileURL)
        }

        var processedBibliographyData: Data
        switch inputFormat {
        case .bibTeX, .endNote, .JSON:
            processedBibliographyData = bibliographyData
        case .RIS:
            processedBibliographyData = try cleanupRISRecord(bibliographyData)
        }

        // Write incoming bibliography data to temporary file
        let tempBibFileURL = workingDirectoryFileURL.appendingPathComponent("temp_bibliography.\(inputFormat.fileExtension)")
        try write(processedBibliographyData, to: tempBibFileURL)

        return try executeCommand("/usr/local/bin/citeproc-java",
                                  arguments: ["json",
                                              "-i",
                                              tempBibFileURL.path]).standardOut
    }

    /// Transform some bibliography data (formatted as CSL-JSON) to another bibliography format.
    ///
    /// - Parameters:
    ///   - bibliographyData: `Data` (formatted as an array of CSL-JSON items).
    ///   - outputFormat: The output format of the bibliography data after transformation.
    /// - Returns: `Data` in the requested bibliography output format.
    /// - Throws: A `CommandLineTransformationError` if any stage of the
    ///           bibliography transformation fails for some reason.
    func transform(_ bibliographyData: Data, to outputFormat: OutputFormat) throws -> Data {

        // Create working directory
        let workingDirectoryFileURL = try createWorkingDirectory()

        // Defer block for clean-up of working directory regardless of how executions exits this method's scope
        defer {
            cleanWorkingDirectory(at: workingDirectoryFileURL)
        }

        // Write incoming bibliography data to temporary file
        let tempBibFileURL = workingDirectoryFileURL.appendingPathComponent("csl-json_bibliography.json")
        try write(bibliographyData, to: tempBibFileURL)

        return try executeCommand("/usr/local/bin/citeproc-java",
                                  arguments: ["bibliography",
                                              "-i",
                                              tempBibFileURL.path,
                                              "-s",
                                              outputFormat.rawValue]).standardOut
    }
}

// MARK: - RIS record processing

fileprivate extension CSLJSONTransformationService {

    /// An enum describing the possible error states of `CSLJSONTransformationService`.
    enum Error: LocalizedError {
        case missingStringRepresentation

        var errorDescription: String? {
            switch self {
            case .missingStringRepresentation:
                return "The provided bibliographic references data does not have a valid string representation."
            }
        }
    }

    /// Cleans up RIS record data
    /// (i.e. maps fields that citeproc-java doesn't support to other fields, if an equivalent exists).
    /// - Parameter risRecordData: `Data` (formatted as a RIS record).
    func cleanupRISRecord(_ risRecordData: Data) throws -> Data {

        guard var risRecord = String(data: risRecordData, encoding: .utf8) else {
            throw Error.missingStringRepresentation
        }

        let citeprocRISFieldsMap = ["A1  -": "AU  -",
                                    "JF  -": "JO  -",
                                    "Y1  -": "DA  -"]

        for (originalField, mappedField) in citeprocRISFieldsMap {
            risRecord = risRecord.replacingOccurrences(of: originalField, with: mappedField)
        }

        return risRecord.convertToData()
    }
}
