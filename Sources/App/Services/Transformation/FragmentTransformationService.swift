//
//  FragmentTransformationService.swift
//  App
//
//  Created by Dan Browne on 05/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import MPFoundation

/// Transforms a document fragment in a given `InputFormat` to a fragment in a given `OutputFormat`.
final class FragmentTransformationService {

    // MARK: Enum definitions

    /// An enum describing the possible document fragment input formats to `FragmentTransformationService`.
    enum InputFormat: String {
        case mathML = "mathml"
    }

    /// An enum describing the possible document fragment output formats to `FragmentTransformationService`.
    enum OutputFormat: String {
        case teX = "tex"
    }

    /// An enum describing the possible error states of `FragmentTransformationService`.
    enum Error: LocalizedError {
        case missingStringRepresentation
        case invalidInputOutputFormatCombination

        var errorDescription: String? {
            switch self {
            case .missingStringRepresentation:
                return "The provided document fragment data does not have a valid string representation."
            case .invalidInputOutputFormatCombination:
                return "The specified values for 'InputFormat' and 'OutputFormat' are an invalid combination."
            }
        }
    }

    // MARK: - Public methods

    /// Transform the `Data` representation of a document fragment, based on provided format parameters.
    ///
    /// - Parameters:
    ///   - fragmentData: The `Data` representation of a document fragment `String`.
    ///   - inputFormat: The input format of `fragmentData`.
    ///   - outputFormat: The intended output format of the compiled document fragment.
    /// - Returns: A `String` representation of the document fragment, in the format specified by `outputFormat`.
    /// - Throws: An `Error` if `fragmentData` could not be represented as a `String`,
    ///           or if the transformation failed for some reason.
    func transform(_ fragmentData: Data,
                   from inputFormat: InputFormat,
                   to outputFormat: OutputFormat) throws -> String {

        guard let fragment = String(data: fragmentData, encoding: .utf8) else {
            throw Error.missingStringRepresentation
        }

        if inputFormat == .mathML && outputFormat == .teX {
            return try MPImportServiceContentMathML.teXString(forMathMLString: fragment)
        }

        throw Error.invalidInputOutputFormatCombination
    }
}
