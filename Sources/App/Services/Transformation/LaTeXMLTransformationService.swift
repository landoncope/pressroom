//
//  LaTeXMLTransformationService.swift
//  App
//
//  Created by Dan Browne on 14/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

// LaTeXMLTransformationService depends on LaTeXML (with MacTeX as an implicit dependency):
// https://dlmf.nist.gov/LaTeXML/

/// Transform a TeX formatted document into another format.
final class LaTeXMLTransformationService: CommandLineTransformable {

    /// An enum describing the possible output formats from the service.
    ///
    /// - html: The file will be formatted as HTML
    /// - html4: The file will be formatted as HTML4
    /// - html5: The file will be formatted as HTML5
    /// - xhtml: The file will be formatted as XHTML
    /// - xml: The file will be formatted as XML (i.e. LaTeXML)
    enum OutputFormat: String {
        case html
        case html4
        case html5
        case xhtml
        case xml
    }

    // MARK: - Private variables

    /// A directory (relative to the temporary working directory)
    /// where the transformed documents are written to, before being compressed.
    private static let transformedDocumentsDirectory = "transformed_documents"

    private let outputFormat: OutputFormat

    let continueOnErrors = false

    // MARK: - Initialization

    /// Transforms TeX documents into HTML-like output formats (via LaTeXML).
    ///
    /// - Parameter outputFormat: The output format for the document after transformation.
    init(outputFormat: OutputFormat) {
        self.outputFormat = outputFormat
    }

    // MARK: - Public methods

    /// Transform a TeX document to a given HMTL output format.
    ///
    /// - Parameters:
    ///   - teXDocument: `PersistableContent<ArbitrarilyFormattedDocument>` representing the received document
    ///                  (i.e. parsed from a received `Request`) and its working directory.
    /// - Returns: The file `URL` of the generated file after transformation, inside the temporary working directory.
    /// - Throws: A `CommandLineTransformationError` if any stage of the TeX file transformation fails for some reason.
    func transform(_ teXDocument: PersistableContent<ArbitrarilyFormattedDocument>) throws -> URL {

        let tempTeXFileURL = teXDocument.workingDirectory.appendingPathComponent(teXDocument.content.file.filename)

        let fileNameBase = (teXDocument.content.file.filename.split(separator: ".").first.map(String.init))!
        do {
            // Create the 'transformed_documents' subdirectory if it doesn't already exist
            // swiftlint:disable:next line_length
            let transformedDocumentsDirectoryFileURL = teXDocument.workingDirectory.appendingPathComponent(LaTeXMLTransformationService.transformedDocumentsDirectory)
            if FileManager.default.fileExists(atPath: transformedDocumentsDirectoryFileURL.path) == false {
                try FileManager.default.createDirectory(at: transformedDocumentsDirectoryFileURL,
                                                        withIntermediateDirectories: true,
                                                        attributes: nil)
            }

            // Transform TeX into LaTeXML (this is an intermediary file, place immediately under working directory)
            let tempLaTeXMLFileURL = teXDocument.workingDirectory.appendingPathComponent("\(fileNameBase).xml")
            _ = try executeCommand("/usr/local/bin/latexml",
                                   arguments: ["--dest=\(tempLaTeXMLFileURL.path)",
                                               tempTeXFileURL.path])

            // Transform LaTeXML into output format
            // (this is the final output file, place relative to 'transformed_documents' directory)
            // swiftlint:disable:next line_length
            let tempFinalOutputFileURL = transformedDocumentsDirectoryFileURL.appendingPathComponent("\(fileNameBase).\(outputFormat.rawValue)")
            _ = try executeCommand("/usr/local/bin/latexmlpost",
                                   arguments: ["--format=\(outputFormat.rawValue)",
                                               "--dest=\(tempFinalOutputFileURL.path)",
                                               tempLaTeXMLFileURL.path])

            return tempFinalOutputFileURL
        } catch {
            // Clean-up the temporary working directory before throwing `Error`
            //
            // (Cannot `defer` here because then the clean-up for the success case would happen before the file
            // could be chunk-streamed in a `Response`, where `transform(...)` is called
            // up-stream in `TeXTransformationsController`).
            cleanWorkingDirectory(at: teXDocument.workingDirectory.deletingLastPathComponent())

            // Re-throw error
            throw error
        }
    }
}
