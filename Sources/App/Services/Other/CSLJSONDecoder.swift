//
//  CSLJSONDecoder.swift
//  App
//
//  Created by Dan Browne on 04/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// Decodes CSL-JSON using a customised key decoding strategy.
final class CSLJSONDecoder: JSONDecoder {

    /// Definition of a CodingKey for CSL-JSON
    struct CSLJSONCodingKey: CodingKey {
        var stringValue: String
        var intValue: Int?

        init?(stringValue: String) {
            self.stringValue = stringValue
            self.intValue = nil
        }

        init?(intValue: Int) {
            self.stringValue = String(intValue)
            self.intValue = intValue
        }
    }

    override init() {
        super.init()

        // Custom key decoding strategy for CSL-JSON
        // Keys can be either:
        // - Already camelCased.
        // - snake_cased_keys.
        // - hyphenated-keys.
        keyDecodingStrategy = .custom { keys -> CodingKey in
            let hyphenComponents = keys.last!.stringValue.split(separator: "-")
            let underscoreComponents = keys.last!.stringValue.split(separator: "_")
            let joinedString: String

            if hyphenComponents == underscoreComponents {
                // No '-' or '_' characters in key string
                joinedString = String(hyphenComponents.first!)
            } else {
                // '-' or '_' characters in key string
                // (no keys have mixed separators, so convert to camel case as appropriate)
                if hyphenComponents.count > 1 {
                    joinedString = ([hyphenComponents.first!.lowercased()] + hyphenComponents[1...].map { $0.capitalized }).joined()
                } else {
                    joinedString = ([underscoreComponents.first!.lowercased()] + underscoreComponents[1...].map { $0.capitalized }).joined()
                }
            }
            return CSLJSONCodingKey(stringValue: String(joinedString))!
        }
    }
}
