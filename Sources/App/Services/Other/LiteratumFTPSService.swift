//
//  LiteratumFTPSService.swift
//  App
//
//  Created by Dan Browne on 26/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

/// Manages file exchanges with the Literatum Article Submission FTPS server.
final class LiteratumFTPSService: FTPSService {

    static let username: String = Environment.essentialValueForKey(.literatumFTPSUsername)
    static let password: String = Environment.essentialValueForKey(.literatumFTPSPassword)
    static let hostName: String = Environment.essentialValueForKey(.literatumFTPSHostName)

    private static let remotePathPrefix: String = Environment.essentialValueForKey(.literatumFTPSRemotePathPrefix)

    private static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyyMMddHHmmss"

        return dateFormatter
    }()

    private var depositedDocumentDefaultFileName: String {
        return "\(seriesCode)_\(groupIdentifier)_\(Self.dateFormatter.string(from: Date())).zip"
    }

    let continueOnErrors = false

    let seriesCode: String
    let groupIdentifier: String

    // MARK: - Initialization

    init(seriesCode: String, groupDOI: String) {
        self.seriesCode = seriesCode
        self.groupIdentifier = String(groupDOI.split(separator: "/").last!)
    }

    // MARK: - Public methods

    /// Uploads a file on the local system to a specified location on a remote server.
    /// - Parameters:
    ///   - localFileURL: The location of the file to upload on the local system.
    ///   - remoteFileName: Optionally, the name of the file when uploaded to the remote server can be provided.
    ///                     (This defaults to the `depositedDocumentDefaultFileName` otherwise).
    /// - Throws: An `FTPSServiceError` if any stage of the remote upload fails for some reason.
    func uploadFile(at localFileURL: URL, remoteFileName: String? = nil) throws {
        let remoteFileURL = URL(string: "ftp://\(Self.hostName)/\(Self.remotePathPrefix)")!
            .appendingPathComponent(remoteFileName ?? depositedDocumentDefaultFileName)
        try uploadFile(at: localFileURL, to: remoteFileURL)
    }

    /// Downloads a file from a specified location on a remote server to the local system.
    /// - Parameters:
    ///   - localFileURL: The location to download the file to on the local system.
    ///   - remoteFileName: Optionally, the name of the file to download from the remote server can be provided.
    ///                     (This defaults to the file name as specified in `localFileURL.lastPathComponent` otherwise).
    /// - Throws: An `FTPSServiceError` if any stage of the download from the remote server fails for some reason.
    func downloadFile(to localFileURL: URL, remoteFileName: String? = nil) throws {
        let remoteFileURL = URL(string: "ftp://\(Self.hostName)/\(Self.remotePathPrefix)")!
            .appendingPathComponent(remoteFileName ?? localFileURL.lastPathComponent)
        try downloadFile(to: localFileURL, from: remoteFileURL)
    }
}
