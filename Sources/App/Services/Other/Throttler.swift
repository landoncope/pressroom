//
//  Throttler.swift
//  App
//
//  Created by Dan Browne on 09/09/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

// Inspired by: https://www.craftappco.com/blog/2018/5/30/simple-throttling-in-swift

/// Regulates the frequency that a block of work will be executed.
/// (This can be useful for preventing a rapid succession of multiple, redundant code executions).
final class Throttler {

    private var maxInterval: TimeInterval
    private var queue: DispatchQueue
    private var timeStamp = Date.distantPast
    private var workItem = DispatchWorkItem(block: {})

    /// Initializes a `Throttler`.
    ///
    /// - Parameters:
    ///   - maxInterval: The maximum interval between the execution of blocks of work submitted to the receiver.
    ///   - queue: A `DispatchQueue` on which to execute the blocks of work submitted to the receiver.
    init(maxInterval: TimeInterval, queue: DispatchQueue = DispatchQueue.global(qos: .background)) {
        self.maxInterval = maxInterval
        self.queue = queue
    }

    /// Submit a block of work to the receiver, which will be queued for execution (and throttled appropriately).
    ///
    /// - Parameter work: The block of work to submit to the receiver.
    func throttle(work: @escaping () -> Void) {
        workItem.cancel()
        workItem = DispatchWorkItem(block: { [weak self] in
            self?.timeStamp = Date()
            work()
        })

        let executionDelay = timeStamp.timeIntervalSinceNow > maxInterval ? 0 : maxInterval
        queue.asyncAfter(deadline: .now() + executionDelay, execute: workItem)
    }
}
