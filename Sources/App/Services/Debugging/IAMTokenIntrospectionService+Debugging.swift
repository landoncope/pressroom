//
//  IAMTokenIntrospectionService+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension IAMTokenIntrospectionService {

    /// The debugging information for a given token introspection web service error
    enum IAMTokenIntrospectionDebuggingInfo: String, DebuggingInfo {
        case introspectionError
        case invalidToken
        case unexpectedResponse

        var status: HTTPResponseStatus {
            switch self {
            case .introspectionError:
                return .unauthorized
            case .invalidToken:
                return .badRequest
            case .unexpectedResponse:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return IAMTokenIntrospectionService.self
        }

        var reason: String {
            switch self {
            case .introspectionError:
                return  """
                The token introspection failed for some reason.
                """
            case .invalidToken:
                return  """
                The supplied token is invalid.
                """
            case .unexpectedResponse:
                return """
                The IAM token introspection service responded with an unexpected payload structure.
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .introspectionError:
                return ["""
                        There may be a Client-side issue preventing the token introspection from being \
                        carried out successfully.
                        """]
            case .invalidToken:
                return ["""
                        The token contains the wrong information, is misconfigured, or has expired.
                        """]
            case .unexpectedResponse:
                return ["""
                        The IAM token introspection service is misconfigured or has otherwise \
                        entered an inconsistent state.
                        """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .introspectionError:
                return ["""
                        Check the details provided in the 'underlyingError' for more context, \
                        attempt to resolve the issue based on that information, and then try again.
                        """]
            case .invalidToken:
                return ["""
                        Check the contents and format of the supplied token, and then try again.
                        """]
            case .unexpectedResponse:
                return ["""
                        Unless a fix is obvious from the stack trace (or other debugging info), \
                        please report this error to the Manuscripts team.
                        """]
            }
        }
    }

    /// The underlying error information when an introspection error is received from the service.
    struct UnderlyingIntrospectionError: AbortError {
        let status: HTTPResponseStatus
        let identifier: String
        let reason: String

        init(status: HTTPResponseStatus,
             reason: String) {
            self.status = status
            self.identifier = "underlyingIntrospectionError"
            self.reason = reason
        }
    }
}
