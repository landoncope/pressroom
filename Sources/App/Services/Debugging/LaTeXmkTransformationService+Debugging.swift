//
//  LaTeXmkTransformationService+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension LaTeXmkTransformationService {

    /// The debugging information for a given LaTeXmkTransformationService error
    enum LaTeXmkTransformationDebuggingInfo: String, DebuggingInfo {
        // LaTeX installation
        case missingLaTeXInstallation
        case incorrectlyLocatedLaTeXInstallation
        // Executables
        case missingLaTeXmkExecutable
        case missingPdfLaTeXExecutable
        // Client error
        case missingReferencedFile
        // Other
        case genericLaTeXmkServiceFailure

        var status: HTTPResponseStatus {
            switch self {
            case .missingLaTeXInstallation,
                 .incorrectlyLocatedLaTeXInstallation,
                 .missingLaTeXmkExecutable,
                 .missingPdfLaTeXExecutable,
                 .genericLaTeXmkServiceFailure:
                return .internalServerError
            case .missingReferencedFile:
                return .badRequest
            }
        }

        var occurrenceClass: AnyClass {
            return LaTeXmkTransformationService.self
        }

        var reason: String {
            switch self {
            case .missingLaTeXInstallation:
                return """
                The document couldn't be compiled becase the LaTeX installation is missing.
                """
            case .incorrectlyLocatedLaTeXInstallation:
                return """
                The document couldn't be compiled becase the LaTeX installation cannot be found \
                in the expected location on disk.
                """
            case .missingLaTeXmkExecutable:
                return """
                The document couldn't be compiled becase the 'latexmk' executable is missing.
                """
            case .missingPdfLaTeXExecutable:
                return """
                The document couldn't be compiled becase the 'pdflatex' executable is missing.
                """
            case .missingReferencedFile:
                return """
                The document couldn't be compiled because a file referenced by another TeX file is missing.
                """
            case .genericLaTeXmkServiceFailure:
                return """
                The document couldn't be compiled because an error occured within the 'LaTeXmkTransformationService'.
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .missingLaTeXInstallation:
                return ["""
                    The LaTeX installation is missing entirely, or has become corrupted in some way.
                    """]
            case .incorrectlyLocatedLaTeXInstallation:
                return ["""
                    The LaTeX installation was not installed in the expected location on disk.
                    """]
            case .missingLaTeXmkExecutable:
                return ["""
                    The 'latexmk' executable appears to be missing.
                    """]
            case .missingPdfLaTeXExecutable:
                return ["""
                    The 'pdflatex' executable appears to be missing.
                    """]
            case .missingReferencedFile:
                return ["""
                    An incomplete compressed document container (or a single TeX file from a larger source \
                    document has been attached to the Request).
                    """]
            case .genericLaTeXmkServiceFailure:
                return ["""
                    Proccessing the document using the 'LaTeXmkTransformationService' failed for some reason.
                    """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .missingLaTeXInstallation,
                 .incorrectlyLocatedLaTeXInstallation,
                 .missingLaTeXmkExecutable,
                 .missingPdfLaTeXExecutable,
                 .genericLaTeXmkServiceFailure:
                return ["""
                    Please report this error to the Manuscripts team.
                    """]
            case .missingReferencedFile:
                return ["""
                    Check that all the expected documents are inside the compressed container and try again.
                    """]
            }
        }
    }
}
