//
//  ProcessInfo+Extensions.swift
//  App
//
//  Created by Dan Browne on 12/06/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

extension ProcessInfo {

    /// Returns a modified form of the `environment` where the value for the `PATH` key
    /// has been prepended with the provided path `String`.
    ///
    /// N.B: If there is no value for the `PATH` key in `environment`, then it will be set to `path`.
    ///
    /// - Parameter path: A `String` to prepend to the value of the `PATH` key in `environment`.
    /// - Returns: The modified form of `environment` with `path` prepended to the value of the `PATH` key.
    func prependingToEnvironmentPath(_ path: String) -> [String: String] {
        var currentEnvironment = environment
        var newPath = path
        if let currentPath = currentEnvironment["PATH"] {
            newPath += ":\(currentPath)"
        }
        currentEnvironment["PATH"] = newPath

        return currentEnvironment
    }
}
