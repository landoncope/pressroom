//
//  OpenAPIDocument+PressroomServer.swift
//  App
//
//  Created by Dan Browne on 12/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Swiftgger

// MARK: - PressroomServer OpenAPIDocument helper methods

extension OpenAPIDocument {

    /// Generates an HTML representation of an `OpenAPIDocument`.
    ///
    /// - Returns: An HTML `String` representation of the document.
    // swiftlint:disable:next function_body_length
    func toHTMLString() -> String {
        var endpointsHTML = ""

        for (endpointURL, endpoint) in paths.sorted(by: { $0.key < $1.key }) {

            let getOp = endpoint.get
            let postOp = endpoint.post
            let endpointTagName = getOp?.tags?.first != nil ? getOp?.tags?.first : postOp?.tags?.first

            endpointsHTML.append("<hr></hr>")
            endpointsHTML.append("<details>")
            endpointsHTML.append("<summary><h2>\(endpointURL)</h2></summary>")
            let tag = (tags?.filter { tag -> Bool in
                tag.name == endpointTagName
                }.first)!
            endpointsHTML.append("<p>\(tag.description!)</p>")

            if let getOperation = getOp {
                endpointsHTML.append(getOperation.toHTMLString(httpMethod: "GET", parentDocument: self))
            }

            if let postOperation = postOp {
                endpointsHTML.append(postOperation.toHTMLString(httpMethod: "POST", parentDocument: self))
            }
            endpointsHTML.append("</details>")
        }

        let htmlPage = """
        <!DOCTYPE html>
        <html>
        <head>
        <title>\(info.title) - v\(info.version)</title>
        <style>
        body {font-family: Arial, Helvetica, sans-serif;}
        h4 {background-color: gray; color: white; padding: 0.5em 0.5em;}
        p {line-height: 1.75em; padding: 0em 0.5em;}
        pre {background-color: #333333; color: white;}

        details > summary {
        margin: 1.5em 0em;
        padding: 0.5em 0.5em;
        width: auto;
        background-color: lightgray;
        border: none;
        transition: 0.2s box-shadow;
        cursor: pointer;
        }

        details > summary:hover {
        box-shadow: 3px 3px 4px darkgray;
        }
        </style>
        </head>
        <body>
        <h1>\(info.title)</h1>
        <p><b>Version:</b> \(info.version)</p>
        <p><b>Description:</b> \(info.description!)</p>

        <h1>Endpoints</h1>
        \(endpointsHTML)
        </body>
        </html>
        """

        return htmlPage
    }
}

// MARK: - PressroomServer OpenAPIOperation helper methods

extension OpenAPIOperation {

    /// Generates an HTML representation of an `OpenAPIOperation`.
    ///
    /// - Parameters:
    ///   - httpMethod: The HTTP method corresponding to the operation.
    ///   - parentDocument: The parent `OpenAPIDocument` to which the operation belongs.
    /// - Returns: An HTML `String` representation of the operation.
    func toHTMLString(httpMethod: String, parentDocument: OpenAPIDocument) -> String {
        var operationHTML = ""

        operationHTML.append("<h3>\(httpMethod.uppercased()): \(summary!)</h3>")
        operationHTML.append("<p>\(description!)</p>")
        if parameters?.count ?? 0 > 0 {
            operationHTML.append("<h4>Headers</h4>")
        }
        for parameter in parameters ?? [] {
            operationHTML.append("<details>")
            operationHTML.append("<summary>\(parameter.name!)</summary>")
            operationHTML.append("<p>\(parameter.description!)</p>")
            operationHTML.append("<p><b>Required?:</b> \(parameter.required)</p>")
            operationHTML.append("<p><b>Allow empty?:</b> \(parameter.allowEmptyValue)</p>")
            operationHTML.append("<p><b>Deprecated?:</b> \(parameter.deprecated)</p>")
            operationHTML.append("</details>")
        }

        if let responses = responses {
            if responses.count > 0 {
                operationHTML.append("<h4>Responses</h4>")
            }
            for (status, response) in responses.sorted(by: { $0.key < $1.key }) {
                operationHTML.append("<p><b>Status:</b> \(status)</p>")
                operationHTML.append("<p>\(response.description!)</p>")

                for (_, item) in response.content ?? [:] {
                    let schemaName = String((item.schema?.ref?.split(separator: "/").last)!)
                    let schema = (parentDocument.components?.schemas?[schemaName])!
                    operationHTML.append("<details>")
                    operationHTML.append("<summary>\(schemaName): \(schema.type!.capitalized)</summary>")
                    let objectEncoder = JSONEncoder()
                    objectEncoder.outputFormatting = .prettyPrinted
                    if let data = try? objectEncoder.encode(schema), let output = String(data: data, encoding: .utf8) {
                        operationHTML.append("<pre>\(output)</pre>")
                    }
                    operationHTML.append("</details>")
                }
            }
        }

        return operationHTML
    }
}
