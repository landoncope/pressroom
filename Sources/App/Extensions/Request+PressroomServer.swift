//
//  Request+PressroomServer.swift
//  App
//
//  Created by Dan Browne on 05/07/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import Foundation

extension Request {

    /// The current version of PressroomServer, for routing purposes
    static let pressroomServerVersion = "v1"

    static let pressroomHeaders = PressroomHeaders()

    /// Pressroom API `Request` headers
    struct PressroomHeaders: PropertyIterable {
        // API authentication
        let apiKey = HTTPHeaderName("Pressroom-API-Key")

        // Error handling
        let continueOnErrors = HTTPHeaderName("Pressroom-Continue-On-Errors")

        // Document compilation
        let digitalObjectType = HTTPHeaderName("Pressroom-Digital-Object-Type")
        let edifixEditorialStyle = HTTPHeaderName("Pressroom-Edifix-Editorial-Style")
        let edifixSecret = HTTPHeaderName("Pressroom-Edifix-Secret")
        let equationsTypesettingTimeout = HTTPHeaderName("Pressroom-Math-Typesetting-Timeout")
        let enrichDocumentMetadata = HTTPHeaderName("Pressroom-Enrich-Document-Metadata")
        let enrichedContentSimilarityThreshold = HTTPHeaderName("Pressroom-Enriched-Content-Similarity-Threshold")
        let eXtylesArcEditorialStyle = HTTPHeaderName("Pressroom-ExtylesArc-Editorial-Style")
        let eXtylesArcSecret = HTTPHeaderName("Pressroom-ExtylesArc-Secret")
        let primaryFile = HTTPHeaderName("Pressroom-Primary-File")
        let regenerateProjectBundleObjectIDs = HTTPHeaderName("Pressroom-Regenerate-Project-Bundle-Model-Object-IDs")
        let targetFileExtension = HTTPHeaderName("Pressroom-Target-File-Extension")
        let targetJATSOutputFormat = HTTPHeaderName("Pressroom-Target-JATS-Output-Format")
        let targetJATSVersion = HTTPHeaderName("Pressroom-Target-JATS-Version")
        let targetLaTeXMLOutputFormat = HTTPHeaderName("Pressroom-Target-LaTeXML-Output-Format")
        let teXContainerProcessingMethod = HTTPHeaderName("Pressroom-TeX-Container-Processing-Method")

        // Document fragment compilation
        let documentFragmentInputFormat = HTTPHeaderName("Pressroom-Document-Fragment-Input-Format")
        let documentFragmentOutputFormat = HTTPHeaderName("Pressroom-Document-Fragment-Output-Format")

        // Bibliographic items compilation
        let sourceBibliographyFormat = HTTPHeaderName("Pressroom-Source-Bibliography-Format")
        let targetBibliographyFormat = HTTPHeaderName("Pressroom-Target-Bibliography-Format")

        // JATS / EM Manifest metadata XML document compilation
        let jatsArchiveURL = HTTPHeaderName("Pressroom-JATS-Archive-URL")
        let jatsDocumentProcessingLevel = HTTPHeaderName("Pressroom-JATS-Document-Processing-Level")
        let jatsFileList = HTTPHeaderName("Pressroom-JATS-File-List")
        let jatsGroupDOI = HTTPHeaderName("Pressroom-JATS-Group-DOI")
        let jatsJournalCode = HTTPHeaderName("Pressroom-JATS-Journal-Code")
        let jatsInputFormat = HTTPHeaderName("Pressroom-JATS-Input-Format")
        let jatsISSN = HTTPHeaderName("Pressroom-JATS-ISSN")
        let jatsSeriesCode = HTTPHeaderName("Pressroom-JATS-Series-Code")
        let jatsSubmissionIdentifier = HTTPHeaderName("Pressroom-JATS-Submission-Identifier")
        let jatsSubmissionDOI = HTTPHeaderName("Pressroom-JATS-Submission-DOI")
        let jatsURL = HTTPHeaderName("Pressroom-JATS-URL")
        let jatsVendorName = HTTPHeaderName("Pressroom-JATS-Vendor-Name")

        // Document deposit services
        let depositNotificationCallbackURL = HTTPHeaderName("Pressroom-Deposit-Notification-Callback-URL")
        let depositSubmissionIdentifier = HTTPHeaderName("Pressroom-Deposit-Submission-Identifier")
        let depositSubmissionJournalName = HTTPHeaderName("Pressroom-Deposit-Submission-Journal-Name")

        func allHeaders() -> [HTTPHeaderName] {
            return propertyValues(returnType: HTTPHeaderName.self)
        }
    }
}
