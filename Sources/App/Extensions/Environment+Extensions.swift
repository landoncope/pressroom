//
//  Environment+Extensions.swift
//  App
//
//  Created by Dan Browne on 06/09/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

extension Environment {

    enum VariableKey: String {

        // AuthMiddleware
        case apiKey = "MPPressroomServerAPIKey"
        case jwtSecret = "MPPressroomServerJWTSecret"

        // EEO deposit service
        case eeoDepositServiceSecret = "MPPressroomServerEEODepositServiceSecret"

        // eXtylesArc and Edifix
        case edifixServiceSecret = "MPPressroomServerEdifixSecret"
        case eXtylesArcServiceSecret = "MPPressroomServerExtylesArcSecret"

        // IAM token introspection
        case tokenIntrospectionSecret = "MPPressroomServerIAMTokenIntrospectionSecret"

        // Sachs transformation service
        case sachsTransformationServicePATH = "MPPressroomServerSachsTransformationServicePATH"

        // LaTeXmk transformation service
        case laTeXmkTransformationServicePATH = "MPPressroomServerLaTeXmkTransformationServicePATH"

        // Literatum Digital Object SFTP
        case literatumSFTPUsername = "MPPressroomServerLiteratumSFTPUsername"
        case literatumSFTPHostName = "MPPressroomServerLiteratumSFTPHostName"
        case literatumSFTPIdentityFilePath = "MPPressroomServerLiteratumSFTPIdentityFilePath"
        case literatumSFTPRemotePathPrefix = "MPPressroomServerLiteratumSFTPRemotePathPrefix"

        // Literatum Article Submission FTPS
        case literatumFTPSUsername = "MPPressroomServerLiteratumFTPSUsername"
        case literatumFTPSPassword = "MPPressroomServerLiteratumFTPSPassword"
        case literatumFTPSHostName = "MPPressroomServerLiteratumFTPSHostName"
        case literatumFTPSRemotePathPrefix = "MPPressroomServerLiteratumFTPSRemotePathPrefix"

        // Server config
        case maxContentLengthBytes = "MPPressroomServerMaxContentLengthBytes"
        case serverCORSOrigin = "MPPressroomServerCORSOrigin"
        case serverPort = "MPPressroomServerPort"
        case swiftyBeaverAppID = "MPPressroomServerSwiftyBeaverAppID"
        case swiftyBeaverAppSecret = "MPPressroomServerSwiftyBeaverAppSecret"
        case swiftyBeaverEncryptionKey = "MPPressroomServerSwiftyBeaverEncryptionKey"
    }

    /// Returns the environment variable for a given key,
    /// passed in via ProcessInfo as an essential variable for proper server execution.
    /// (Therefore, this method triggers an assertion failure if the environment variable isn't present).
    ///
    /// - Parameter key: The environment variable key.
    /// - Returns: The environment variable value for the given key.
    static func essentialValueForKey(_ key: VariableKey) -> String {
        guard let environmentVariable = Environment.get(key.rawValue) else {
            // If no env var has been set, trigger an assertion failure since nothing will work!
            assertionFailure("No PressroomServer environment variable has been set for the key '\(key)'.")
            return ""
        }
        return environmentVariable
    }

    /// Returns the environment variable for a given key as an `Int`,
    /// passed in via ProcessInfo as an essential variable for proper server execution.
    /// (Therefore, this method triggers an assertion failure if the environment variable isn't present
    /// or cannot be represented as an `Int`).
    ///
    /// - Parameter key: The environment variable key.
    /// - Returns: The environment variable `Int` value for the given key.
    static func essentialValueForKey(_ key: VariableKey) -> Int {
        let envVar: String = Environment.essentialValueForKey(key)
        guard let intEnvVar = Int(envVar) else {
            assertionFailure("'\(key)' environment variable cannot be represented as an integer, currently it is set to '\(envVar)'.")
            return -1
        }
        return intEnvVar
    }
}
