//
//  MediaType+PressroomServer.swift
//  App
//
//  Created by Dan Browne on 12/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

extension MediaType {

    /// The file path extension for a Manuscripts project bundle document container.
    static let manuscriptsProjectBundlePathExtension = "manuproj"

    /// CSL-JSON file.
    static let cslJSON = MediaType(type: "application", subType: "vnd.citationstyles.csl+json")

    /// Creates a `MediaType` from a file path extension, if possible (handles PressroomServer specific use-cases).
    ///
    /// - Parameter pathExtension: File extension (i.e., "md", "docx", "pdf", etc).
    /// - Returns: A `MediaType`, if one exists for the path extension provided.
    static func type(for fileExtension: String) -> MediaType? {
        // Special case for 'manuproj' files (i.e. a zipped container),
        // otherwise just use existing `fileExtension(...)` method
        if fileExtension == MediaType.manuscriptsProjectBundlePathExtension {
            return .zip
        } else {
            return MediaType.fileExtension(fileExtension)
        }
    }
}
