//
//  Response+PressroomServer.swift
//  App
//
//  Created by Dan Browne on 21/09/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension Response {

    static let pressroomHeaders = PressroomHeaders()

    /// Pressroom API `Response` headers
    struct PressroomHeaders: PropertyIterable {
        let compiledWithErrors = HTTPHeaderName("Pressroom-Compiled-With-Errors")
        let primaryFileSelectionCriterion = HTTPHeaderName("Pressroom-Primary-File-Selection-Criterion")
        let projectBundleObjectIDsRegenerated = HTTPHeaderName("Pressroom-Project-Bundle-Model-Object-IDs-Regenerated")

        func allHeaders() -> [HTTPHeaderName] {
            return propertyValues(returnType: HTTPHeaderName.self)
        }
    }
}
