//
//  Route+OpenAPIDefinable.swift
//  App
//
//  Created by Dan Browne on 16/08/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import Swiftgger
import Foundation

// MARK: - Associate an `APIController` or `APIObject`s with a `Route`

extension Route {

    /// Associates an `APIController` object to the given `Route`.
    ///
    /// - Parameters:
    ///   - name: The name of the `APIController`.
    ///   - description: A description of the `APIController`.
    ///   - externalDocs: A link to any external documentation or relevant reference material.
    ///   - actions: An array of `APIAction`s for the controller.
    func setAPIController(name: String, description: String, externalDocs: APILink?, actions: [APIAction]) {
        extend.set("APIController",
                   to: APIController(name: name,
                                     description: description,
                                     externalDocs: externalDocs,
                                     actions: actions))
    }

    /// The `APIController` associated with the given `Route`.
    ///
    /// - Returns: The `APIController` that was set for a given `Route`,
    ///            or an empty definition if none was originally set.
    func apiController() -> APIController {
        return extend.get("APIController", default: APIController(name: "N/A", description: "N/A"))
    }

    /// Associates an array of `APIObject`s representing the array of objects passed in, to the given `Route`.
    ///
    /// - Parameter objects: An array of objects to represent as `APIObject`s for the purposes of defining a schema.
    func setAPIObjects(_ objects: [Any]) {
        let apiObjects = objects.map { object in
            APIObject(object: object)
        }

        extend.set("APIObjects", to: apiObjects)
    }

    /// The array of `APIObject`a associated with the given `Route`.
    ///
    /// - Returns: The `APIObject` array that was set for a given `Route`,
    ///            or an empty definition if none was originally set.
    func apiObjects() -> [APIObject] {
        return extend.get("APIObjects", default: [APIObject(object: "N/A")])
    }

    /// A String representing the route for an `APIAction` object - e.g. `/v1/document/compile`
    var apiActionRoute: String {
        return Array(path.dropFirst()).readable
    }
}
