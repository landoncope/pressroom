//
//  String+Extensions.swift
//  App
//
//  Created by Dan Browne on 21/02/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import StringMetric

enum StringSimilarity {
    case equal
    case containsTarget
    case containedByTarget
    case jaroWinklerDistance(Double)
}

extension String {

    /// Returns the `StringSimilarity` between the receiver and another `target` string.
    /// - Parameters:
    ///   - target: The target `String` to compare against.
    ///   - reversedComparison: Whether the string similarity should be computed using a reversed receiver and target.
    ///                         (This is only taken into account if the similarity is computed using the Jaro-Winkler algorithm).
    func similarity(to target: String, reversedComparison: Bool = false) -> StringSimilarity {
        if self == target {
            return .equal
        } else if self.contains(target) {
            return .containsTarget
        } else if target.contains(self) {
            return .containedByTarget
        } else {
            if reversedComparison {
                return .jaroWinklerDistance(String(self.reversed()).distance(between: String(target.reversed())))
            } else {
                return .jaroWinklerDistance(self.distance(between: target))
            }
        }
    }
}
