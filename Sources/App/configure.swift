//
//  configure.swift
//  App
//
//  Created by Dan Browne on 04/06/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import SwiftyBeaver
import SwiftyBeaverVapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    PressroomServerConfig.shared.router = router

    // Configure middlewares and server logging
    configureMiddleware(&config, &services)
    try configureLogging(&config, &services, env)

    /// Configure the server
    /// Exposes PressroomServer via the IP address of the machine it's running on
    /// (and/or any configured public hostnames), on a given port
    let port: Int = Environment.essentialValueForKey(.serverPort)

    // Configure the server
    // (Ensures `maxBodySize` >> `maxContentLength` on `PressroomServerConfig`,
    // which ensure Vapor doesn't close the connection before PressroomErrorMiddleware has fired!)
    let serverConfig = NIOServerConfig.default(hostname: "0.0.0.0",
                                               port: port,
                                               maxBodySize: PressroomServerConfig.shared.maxContentLength * 3)
    services.register(serverConfig)
}

/// Configure and register the middlewares for PressroomServer.
///
/// - Parameters:
///   - config: The application's `Config`.
///   - services: The application's `Services`.
private func configureMiddleware(_ config: inout Config, _ services: inout Services) {
    /// Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // Configure CORS settings middleware
    let corsOrigin: String = Environment.essentialValueForKey(.serverCORSOrigin)
    let allowedHeaders = [.accept,
                          .authorization,
                          .contentType,
                          .origin,
                          .xRequestedWith] + Request.pressroomHeaders.allHeaders()
    let corsConfig = CORSMiddleware.Configuration(allowedOrigin: .custom(corsOrigin),
                                                  allowedMethods: [.POST, .PUT, .OPTIONS],
                                                  allowedHeaders: allowedHeaders)
    // Use a AuthMiddleware to check each request for a valid JWT or API Key -
    // check for a valid JWT secret and API key env vars being set first
    _ = AuthMiddleware.jwtSecret
    _ = AuthMiddleware.apiKey
    middlewares.use(CORSMiddleware(configuration: corsConfig))
    middlewares.use(AuthMiddleware.self)
    middlewares.use(PressroomErrorMiddleware.self) // Catches errors and converts to HTTP response

    services.register(middlewares)
}

/// Configure and register the logging for PressroomServer.
///
/// - Parameters:
///   - config: The application's `Config`.
///   - services: The application's `Services`.
///   - env: The application's `Environment`.
private func configureLogging(_ config: inout Config, _ services: inout Services, _ env: Environment) throws {
    // Configure logging
    let consoleLoggingDestination = ConsoleDestination()
    let fileLoggingDestination = FileDestination()
    let cloudLoggingDestination: SBPlatformDestination

    // Environment-dependent analyticsUserName
    if env.isRelease {
        let appID: String = Environment.essentialValueForKey(.swiftyBeaverAppID)
        let appSecret: String = Environment.essentialValueForKey(.swiftyBeaverAppSecret)
        let encryptionKey: String = Environment.essentialValueForKey(.swiftyBeaverEncryptionKey)
        cloudLoggingDestination = SBPlatformDestination(appID: appID,
                                                        appSecret: appSecret,
                                                        encryptionKey: encryptionKey)
        cloudLoggingDestination.analyticsUserName = "com.manuscripts.pressroomserver_prod"
    } else {
        cloudLoggingDestination = SBPlatformDestination(appID: "PLACEHOLDER-SWIFTYBEAVER-APP-ID",
                                                        appSecret: "PLACEHOLDER-SWIFTYBEAVER-APP-SECRET",
                                                        encryptionKey: "PLACEHOLDER-SWIFTYBEAVER-ENCRYPTION-KEY")
        cloudLoggingDestination.analyticsUserName = "com.manuscripts.pressroomserver_dev"
    }

    try services.register(SwiftyBeaverProvider(destinations: [consoleLoggingDestination,
                                                              fileLoggingDestination,
                                                              cloudLoggingDestination]))
    config.prefer(SwiftyBeaverVapor.self, for: Logger.self)
}
