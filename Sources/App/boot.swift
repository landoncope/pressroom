//
//  boot.swift
//  App
//
//  Created by Dan Browne on 04/06/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import MPFoundation

/// Called after your application has initialized.
public func boot(_ app: Application) throws {

    // Configure initial state of Manuscripts dependencies after initializing the Vapor application
    DocumentTransformationsController.configureManuscriptsDependenciesInitialState()

    // Ensure that a valid TeX distribution is installed and accessible, and persist its related bookmark `Data`
    MPLaTeXInstallationVerifier().ensureTeXDistributionIsAccessible({ _, laTeXInstallationRootBookmarkData, error in
        if let laTeXInstallationRootBookmarkData = laTeXInstallationRootBookmarkData {
            PressroomServerConfig.shared.laTeXInstallationRootBookmarkData = laTeXInstallationRootBookmarkData
        } else if error != nil {
            assertionFailure(error!.localizedDescription)
        }
    }, afterError: nil)
}
