//
//  AuthMiddleware.swift
//  App
//
//  Created by Dan Browne on 10/08/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor
import JWT

/// A `Middleware` for validating the API keys or Bearer Tokens (JWTs or IAM tokens),
/// either of which are expected as part of incoming `Request`s to the PressroomServer API.
final class AuthMiddleware: Middleware, ServiceType {

    // MARK: Struct and Enum definitions

    /// Structure of `AuthMiddleware` default JSON response.
    struct AuthorizationErrorResponse: Codable {
        /// Always `true` to indicate this is a non-typical JSON response.
        let error: Bool = true

        /// The reason for the error.
        var reason: String
    }

    /// Structure of `AuthMiddleware` in-depth JSON response.
    struct DetailedAuthorizationErrorResponse: Codable {
        /// Always `true` to indicate this is a non-typical JSON response.
        let error: Bool = true

        /// The Pressroom error describing the issue in-depth
        var errorObject: PressroomError
    }

    /// The payload structure expected for authenticating a PressroomServer API `Request`.
    ///
    /// This structure mirrors the `LoginTokenPayload` found in the manuscripts-api repo.
    struct PressroomServerJWTPayload: JWTPayload {
        var tokenId: String
        var userId: String
        var appId: String
        var expiry: Date?
        var wayfLocalID: String?

        /// Additional payload verification (following JWT signature verification).
        ///
        /// - Throws: A `JWTError` if any of the payload verification checks fail.
        func verify(using signer: JWTSigner) throws {
            // Check that the payload has not expired
            if let expiryDate = expiry, expiryDate.timeIntervalSinceNow < 0.0 {
                throw JWTError(identifier: "invalidPayload",
                               reason: "JWT has expired (i.e. its 'expiry' date is in the past)")
            }
        }
    }

    /// Creates a new instance of `AuthMiddleware` for the supplied `Container`.
    ///
    /// - Parameter worker: The `Container` requesting the `Service`.
    /// - Returns: An instance of `AuthMiddleware`.
    /// - Throws: An `Error` if the `Service` instance cannot be created for some reason.
    static func makeService(for worker: Container) throws -> Self {
        return try .default(environment: worker.environment, log: worker.make())
    }

    /// Create a default `AuthMiddleware`. Logs errors to a `Logger` based on `Environment`
    /// and converts `Error` to `Response` based on conformance to `AbortError` and `Debuggable`.
    ///
    /// - Parameters:
    ///   - environment: The environment to respect when presenting errors.
    ///   - log: Log destination.
    /// - Returns: An instance of `AuthMiddleware`
    static func `default`(environment: Environment, log: Logger) -> Self {
        return .init { request, error in
            // Log the error
            log.report(error: error, verbose: !environment.isRelease)

            // Variables to determine
            let status: HTTPResponseStatus
            let reason: String
            let headers: HTTPHeaders
            var pressroomError: PressroomError?

            /// Inspect the error type
            switch error {

            case let pressroom as PressroomError:
                reason = pressroom.reason
                status = .unauthorized
                headers = pressroom.headers
                pressroomError = pressroom
            case let abort as AbortError:
                reason = abort.reason
                status = abort.status
                headers = abort.headers
            case let debuggable as Debuggable:
                reason = debuggable.reason
                status = .unauthorized
                headers = [:]
            default:
                // not an abort error, and not debuggable or in dev mode
                // just deliver a generic 500 to avoid exposing any sensitive error info
                reason = "Something went wrong."
                status = .internalServerError
                headers = [:]
            }

            // Create a Response with appropriate status
            let response = request.response(http: .init(status: status, headers: headers))

            // Attempt to serialize the error to json
            do {
                if let pressroomError = pressroomError {
                    let errorResponse = DetailedAuthorizationErrorResponse(errorObject: pressroomError)
                    response.http.body = try HTTPBody(data: JSONEncoder().encode(errorResponse))
                } else {
                    let errorResponse = AuthorizationErrorResponse(reason: reason)
                    response.http.body = try HTTPBody(data: JSONEncoder().encode(errorResponse))
                }
                response.http.headers.replaceOrAdd(name: .contentType, value: MediaType.json.serialize())
            } catch {
                response.http.body = HTTPBody(string: "Oops: \(error)")
                response.http.headers.replaceOrAdd(name: .contentType, value: MediaType.plainText.serialize())
            }

            return response
        }
    }

    /// The "secret" to use when verifying the signature of JWTs received in the Authorization header of a `Request`.
    static var jwtSecret: String {
        return Environment.essentialValueForKey(.jwtSecret)
    }

    /// The valid API key string for PressroomServer API requests
    static var apiKey: String {
        return Environment.essentialValueForKey(.apiKey)
    }

    /// An array of `Route` paths to allow to passthrough the middleware without affect
    /// (i.e. these route paths will not be authorized).
    private static var passthroughRoutePaths: [String] {
        return ["/\(Request.pressroomServerVersion)/docs"]
    }

    /// Error-handling closure.
    private let closure: (Request, Error) -> (Response)

    /// Create a new `AuthMiddleware`.
    ///
    /// - parameters:
    ///     - closure: Error-handling closure. Converts `Error` to `Response`.
    init(_ closure: @escaping (Request, Error) -> (Response)) {
        self.closure = closure
    }

    /// Called when each `Request` passes through this middleware.
    ///
    /// - Parameters:
    ///   - request: The incoming `Request`.
    ///   - next: The next `Responder` in the chain, potentially another middleware or the main router.
    /// - Returns: An asynchronous `Response`.
    /// - Throws: An `Error` if there is an issue when validating the Bearer Token or API-key
    ///           that is expected in the `Request`.
    func respond(to request: Request, chainingTo next: Responder) throws -> Future<Response> {
        let response: Future<Response>

        // Only authenticate via the middleware if the path of the `Request` is not an OPTIONS request
        // and does not contain one of the `passthroughRoutePath`s
        var shouldAuthenticateRequest = (request.http.method != .OPTIONS)

        if shouldAuthenticateRequest {
            for path in AuthMiddleware.passthroughRoutePaths {
                shouldAuthenticateRequest = (request.http.url.path != path)
                if shouldAuthenticateRequest == false {
                    break
                }
            }
        }

        if shouldAuthenticateRequest {
            // 1) Check there is at least one auth method attached to `request`
            response = verifyAuthenticationPresent(request)
                .then({[unowned self] _ -> Future<Void> in
                    // 2) Attempt to auth against a potential API-key
                    return self.authAgainstPotentialAPIKey(request)
                }).thenIfError({ authError -> Future<Void> in
                    // 3) If any error at this stage is NOT 'noAuthenticationMethod',
                    // attempt to auth against Bearer Token (i.e. a JWT or IAM token).
                    // Otherwise, just return the existing 'noAuthenticationMethod' error
                    if let previousError = authError as? PressroomError,
                        previousError.identifier != AuthMiddlewareDebuggingInfo.noAuthenticationMethod.rawValue {
                        do {
                            return try self.authAgainstPotentialBearerToken(request)
                                .thenIfError({ error -> Future<Void> in
                                    // 4) If 'authAgainstPotentialBearerToken(…)' errors and 'previousError'
                                    // is 'invalidAPIKeyHeader', then report that error instead.
                                    // Othwerwise, report the bearer token error
                                    if previousError.identifier == AuthMiddlewareDebuggingInfo.invalidAPIKeyHeader.rawValue {
                                        return request.eventLoop.newFailedFuture(error: previousError)
                                    } else {
                                        return request.eventLoop.newFailedFuture(error: error)
                                    }
                                })
                        } catch {
                            return request.eventLoop.newFailedFuture(error: error)
                        }
                    } else {
                        return request.eventLoop.newFailedFuture(error: authError)
                    }
                }).flatMap({ _ -> Future<Response> in
                    return try next.respond(to: request)
                })
        } else {
            // Pass the `Request` off to next responder in chain if no authentication required
            response = try next.respond(to: request)
        }

        return response.mapIfError { error in
            return self.closure(request, error)
        }
    }

    // MARK: - Private methods

    /// Attempt to authenticate against a Bearer token that may be attached to a `Request`.
    ///
    /// - Parameter request: The `Request` to inspect.
    /// - Returns: A `Future<Void>` which is fulfilled successfully if the token is valid.
    /// - Throws: A `PressroomError` if the token is invalid in some way.
    private func authAgainstPotentialBearerToken(_ request: Request) throws -> Future<Void> {
        // Bearer Token error logic steps
        if !request.http.headers.contains(name: .authorization) {

            // Check for presence of Authorization header
            let noAuthHeaderError = PressroomError.error(for: AuthMiddlewareDebuggingInfo.noAuthorizationHeader,
                                                         sourceLocation: .capture())

            return request.eventLoop.newFailedFuture(error: noAuthHeaderError)
        } else if let authHeader = request.http.headers.firstValue(name: .authorization),
            (authHeader.split(separator: " ").first != "Bearer"
                || authHeader.split(separator: " ").first == authHeader.split(separator: " ").last) {

            // Check Authorization header value is valid format, i.e. "Bearer <TOKEN>"
            let invalidAuthHeaderError = PressroomError.error(for: AuthMiddlewareDebuggingInfo.invalidAuthorizationHeaderFormat,
                                                              sourceLocation: .capture())

            return request.eventLoop.newFailedFuture(error: invalidAuthHeaderError)
        } else {

            // Check Bearer Token (verify signature, decode headers and payload, verify payload)
            let token = String(request.http.headers.firstValue(name: .authorization)!.split(separator: " ").last!)
            let signer = JWTSigner.hs256(key: Data(AuthMiddleware.jwtSecret.utf8))

            do {
                _ = try JWT<PressroomServerJWTPayload>(from: token, verifiedUsing: signer)
                return request.eventLoop.newSucceededFuture(result: ())
            } catch {

                // Transform `DecodingError` to `JWTError` - can happen when decoding the JWT header or payload
                // Otherwise, just return the relevant error directly
                if let decodingError = error as? DecodingError {
                    // Construct a more user-friendly version of the `DecodingError`
                    // for the `Response` for debugging purposes
                    let jwtDecodingError = PressroomError.error(for: AuthMiddlewareDebuggingInfo.jwtDecodingError,
                                                                sourceLocation: .capture(),
                                                                underlyingError: decodingError)

                    return request.eventLoop.newFailedFuture(error: jwtDecodingError)
                } else {

                    guard let jwtError = error as? JWTError else {
                        return request.eventLoop.newFailedFuture(error: error)
                    }

                    // If the token appears to be an IAM token, then introspect it
                    // (i.e. the error shows it's not base64-encoded, so it's not a JWT)
                    if jwtError.identifier == "invalidJWT" {
                        return try IAMTokenIntrospectionService().result(for: token,
                                                                         originatingRequest: request)
                    } else {
                        // The error was another type of JWT error
                        return request.eventLoop.newFailedFuture(error: jwtError)
                    }
                }
            }
        }
    }

    /// Attempt to authenticate against a 'Pressroom-API-Key' header that may be attached to a `Request`.
    ///
    /// - Parameter request: The `Request` to inspect.
    /// - Returns: A `Future<Void>` which is fulfilled successfully if an API-key is identified and is valid.
    private func authAgainstPotentialAPIKey(_ request: Request) -> Future<Void> {
        // API key error logic steps
        if !request.http.headers.contains(name: Request.pressroomHeaders.apiKey) {
            // No API-Key header
            let error = PressroomError.error(for: AuthMiddlewareDebuggingInfo.noAPIKeyHeader,
                                             sourceLocation: .capture())

            return request.eventLoop.newFailedFuture(error: error)
        } else if let apiKeyHeader = request.http.headers.firstValue(name: Request.pressroomHeaders.apiKey),
            apiKeyHeader != AuthMiddleware.apiKey {
            // API-key not valid
            let error = PressroomError.error(for: AuthMiddlewareDebuggingInfo.invalidAPIKeyHeader,
                                             sourceLocation: .capture())

            return request.eventLoop.newFailedFuture(error: error)
        }

        return request.eventLoop.newSucceededFuture(result: ())
    }

    /// Verify that there is at least one authentication method attached to a `Request`.
    ///
    /// N.B: This method does no validation of an intended authentication method,
    /// it simply checks that either an API-key or Bearer token is attached to `request`.
    ///
    /// - Parameter request: The `Request` to inspect.
    /// - Returns: A `Future<Void>` which is fulfilled successfully if there is at least one authentication
    ///            method found attached to `request`.
    private func verifyAuthenticationPresent(_ request: Request) -> Future<Void> {

        if !request.http.headers.contains(name: Request.pressroomHeaders.apiKey)
            && !request.http.headers.contains(name: .authorization) {
            let error = PressroomError.error(for: AuthMiddlewareDebuggingInfo.noAuthenticationMethod,
                                             sourceLocation: .capture())

            return request.eventLoop.newFailedFuture(error: error)
        }

        return request.eventLoop.newSucceededFuture(result: ())
    }
}
