//
//  PressroomErrorMiddleware.swift
//  App
//
//  Created by Dan Browne on 08/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

/// Captures all errors and transforms them into an internal server error HTTP response.
public final class PressroomErrorMiddleware: Middleware, ServiceType {
    // MARK: - Struct defintions

    /// Structure of `PressroomErrorMiddleware` default response.
    struct ErrorResponse: Encodable {
        /// Always `true` to indicate this is a non-typical JSON response.
        let error: Bool = true

        /// The reason for the error.
        var reason: String
    }

    /// Structure of `PressroomErrorMiddleware` in-depth response.
    struct DetailedErrorResponse: Encodable {
        /// Always `true` to indicate this is a non-typical JSON response.
        let error: Bool = true

        /// The Pressroom error describing the issue in-depth
        var errorObject: PressroomError
    }

    /// See `ServiceType`.
    public static func makeService(for worker: Container) throws -> Self {
        return try .default(environment: worker.environment, log: worker.make())
    }

    /// Create a default `PressroomErrorMiddleware`. Logs errors to a `Logger` based on `Environment`
    /// and converts `Error` to `Response` based on conformance to `AbortError` and `Debuggable`.
    ///
    /// - parameters:
    ///     - environment: The environment to respect when presenting errors.
    ///     - log: Log destination.
    // swiftlint:disable:next function_body_length
    public static func `default`(environment: Environment, log: Logger) -> Self {

        return .init { req, error in
            // log the error
            log.report(error: error, verbose: !environment.isRelease)

            // variables to determine
            let status: HTTPResponseStatus
            let reason: String
            let headers: HTTPHeaders
            var pressroomError: PressroomError?

            // inspect the error type
            switch error {

            case let pressroom as PressroomError:
                // this is a Pressroom error, we should use its properties
                reason = pressroom.reason
                status = pressroom.status
                headers = pressroom.headers
                pressroomError = pressroom
            case let abort as AbortError:
                // this is an abort error, we should use its status, reason, and headers
                reason = abort.reason
                status = abort.status
                headers = abort.headers
            case let validation as ValidationError:
                // this is a validation error
                reason = validation.reason
                status = .badRequest
                headers = [:]
            case let debuggable as Debuggable where !environment.isRelease:
                // if not release mode, and error is debuggable, provide debug
                // info directly to the developer
                reason = debuggable.reason
                status = .internalServerError
                headers = [:]
            default:
                // not an abort error, and not debuggable or in dev mode
                // just deliver a generic 500 to avoid exposing any sensitive error info
                reason = "Something went wrong."
                status = .internalServerError
                headers = [:]
            }

            // create a Response with appropriate status
            let res = req.response(http: .init(status: status, headers: headers))

            // attempt to serialize the error to json
            do {
                if let pressroomError = pressroomError {
                    let errorResponse = DetailedErrorResponse(errorObject: pressroomError)
                    res.http.body = try HTTPBody(data: JSONEncoder().encode(errorResponse))
                } else {
                    let errorResponse = ErrorResponse(reason: reason)
                    res.http.body = try HTTPBody(data: JSONEncoder().encode(errorResponse))
                }
                res.http.headers.replaceOrAdd(name: .contentType, value: MediaType.json.serialize())
            } catch {
                res.http.body = HTTPBody(string: "Oops: \(error)")
                res.http.headers.replaceOrAdd(name: .contentType, value: MediaType.plainText.serialize())
            }
            return res
        }
    }

    /// Error-handling closure.
    private let closure: (Request, Error) -> (Response)

    /// Create a new `PressroomErrorMiddleware`.
    ///
    /// - parameters:
    ///     - closure: Error-handling closure. Converts `Error` to `Response`.
    public init(_ closure: @escaping (Request, Error) -> (Response)) {
        self.closure = closure
    }

    /// See `Middleware`.
    public func respond(to req: Request, chainingTo next: Responder) throws -> Future<Response> {
        let response: Future<Response>
        do {
            if let contentLengthHeader = req.http.headers.firstValue(name: .contentLength) {
                guard let contentLength = UInt(contentLengthHeader),
                    contentLength < PressroomServerConfig.shared.maxContentLength else {
                        let maxContentLengthMB = PressroomServerConfig.shared.maxContentLength / (1024 * 1024)
                        throw PressroomError.init(.badRequest,
                                                  typeIdentifier: "\(type(of: self).self)Error",
                                                  identifier: "maximumContentLengthExceeded",
                                                  reason: """
                                                            The Request was rejected because it was larger than \
                                                            \(maxContentLengthMB)MB! Requests larger than \
                                                            \(maxContentLengthMB * 3)MB will fail without warning!
                                                            """,
                                                  sourceLocation: .capture(),
                                                  possibleCauses: ["A large document has been attached to the Request."],
                                                  // swiftlint:disable:next line_length
                                                  suggestedFixes: ["Attach a document smaller than \(maxContentLengthMB)MB to the Request."])
                }
            }
            response = try next.respond(to: req)
        } catch {
            response = req.eventLoop.newFailedFuture(error: error)
        }

        return response.mapIfError { error in
            return self.closure(req, error)
        }
    }
}
