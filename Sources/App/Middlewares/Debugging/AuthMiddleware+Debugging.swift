//
//  AuthMiddleware+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor
import Swiftgger

extension AuthMiddleware {

    /// The debugging information for a given authentication error
    enum AuthMiddlewareDebuggingInfo: String, DebuggingInfo, CaseIterable {
        // API-key errors
        case invalidAPIKeyHeader
        case noAPIKeyHeader

        // Bearer token errors
        case invalidAuthorizationHeaderFormat
        case noAuthorizationHeader
        case jwtDecodingError

        // No authentication method
        case noAuthenticationMethod

        var status: HTTPResponseStatus {
            // All cases are 401 Unauthorized
            return .unauthorized
        }

        var occurrenceClass: AnyClass {
            return AuthMiddleware.self
        }

        var reason: String {
            switch self {
            case .invalidAPIKeyHeader:
                return """
                Request authorization failed because the '\(Request.pressroomHeaders.apiKey.stringValue)' \
                header value was invalid!
                """
            case .noAPIKeyHeader:
                return """
                Request authorization failed because no '\(Request.pressroomHeaders.apiKey.stringValue)' header \
                was attached to the Request!
                """
            case .invalidAuthorizationHeaderFormat:
                return """
                Request authorization failed because the '\(HTTPHeaderName.authorization.stringValue)' header \
                value was of an invalid format!
                """
            case .noAuthorizationHeader:
                return """
                Request authorization failed because no '\(HTTPHeaderName.authorization.stringValue)' header \
                was attached to the Request!
                """
            case .jwtDecodingError:
                return """
                Request authorization failed because there was a problem decoding the Bearer token \
                attached to the Request!
                """
            case .noAuthenticationMethod:
                return """
                Request authorization failed because no authentication method was attached to the Request!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .invalidAPIKeyHeader:
                return ["""
                    The provided API-key is out-of-date or contains a typo.
                    """]
            case .noAPIKeyHeader:
                return ["""
                    The '\(Request.pressroomHeaders.apiKey.stringValue)' header was omitted from the Request \
                    for some reason.
                    """]
            case .invalidAuthorizationHeaderFormat:
                return ["""
                    The provided Bearer token is of an invalid format. It should be 'Bearer <TOKEN>'.
                    """]
            case .noAuthorizationHeader:
                return ["""
                    The '\(HTTPHeaderName.authorization.stringValue)' header was omitted from the Request \
                    for some reason.
                    """]
            case .jwtDecodingError:
                return ["""
                    The decoded JSON of the attached JWT cannot be decoded correctly.
                    """]
            case .noAuthenticationMethod:
                return ["""
                    Neither a '\(Request.pressroomHeaders.apiKey.stringValue)' header nor a \
                    '\(HTTPHeaderName.authorization.stringValue)' header was attached to the Request.
                    """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .invalidAPIKeyHeader,
                 .noAPIKeyHeader:
                return ["""
                    Check the '\(Request.pressroomHeaders.apiKey.stringValue)' header is correct and attached \
                    to the Request, and try again.
                    """]
            case .invalidAuthorizationHeaderFormat,
                 .noAuthorizationHeader:
                return ["""
                    Check the '\(HTTPHeaderName.authorization.stringValue)' header is correct and attached \
                    to the Request, and try again.
                    """]
            case .jwtDecodingError:
                return ["""
                    Check the structure of the JWT is correct before it was encoded, and try again.
                    """]
            case .noAuthenticationMethod:
                return ["""
                    Check that either a '\(Request.pressroomHeaders.apiKey.stringValue)' header or a \
                    '\(HTTPHeaderName.authorization.stringValue)' header is attached to the Request, and try again.
                    """]
            }
        }

        static var apiResponse: APIResponse {
            return APIResponse(code: String(HTTPStatus.unauthorized.code),
                               description: """
                Unauthorized response notifying the Client that there is a problem with the \
                'Authorization' status of the Request. The error can be one of: \(allCases.map { $0.rawValue }).
                The response is formatted as a \
                '\(String(describing: AuthMiddleware.AuthorizationErrorResponse.self))' \
                object.
                """,
                object: AuthMiddleware.AuthorizationErrorResponse.self,
                contentType: MediaType.json.serialize())
        }
    }
}
