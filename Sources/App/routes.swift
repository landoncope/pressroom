//
//  routes.swift
//  App
//
//  Created by Dan Browne on 04/06/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import Swiftgger

/// Register your application's routes here.
public func routes(_ router: Router) throws {

    // Configure the various routes
    router.compileBibliographyRoute()
    router.compileDocumentRoute()
    router.compileDocumentDepositRoute()
    router.compileDocumentRouteLegacy()
    router.compileDocumentFragmentRoute()
    router.compileDocumentJatsRoute()
    router.compileDocumentTeXRoute()
    router.receiveSubmissionsRoute()
    router.versionRoute()
    router.docsRoute()
}

// MARK: - PressroomServer Router extension

// swiftlint:disable line_length
extension Router {

    func compileBibliographyRoute() {
        let bibliographyTransformationsController = BibliographyTransformationsController()
        let bibliographyRoute = post(Request.pressroomServerVersion, "compile", "bibliography", use: bibliographyTransformationsController.compileBibliography)
        bibliographyRoute.setAPIController(name: "Bibliography",
                                           description: "This endpoint manages bibliography transformation operations.",
                                           externalDocs: nil,
                                           actions: [APIAction(method: .post,
                                                               route: bibliographyRoute.apiActionRoute,
                                                               summary: "Compiles a bibliography.",
                                                               description: "Compiles a received bibliography present in the Request body into another format based on options present.",
                                                               parameters: [APIParameter.authorizationHeaderParameter,
                                                                            APIParameter.apiKeyHeaderParameter,
                                                                            APIParameter.edifixEditorialStyleParameter,
                                                                            APIParameter.edifixSecretParameter,
                                                                            APIParameter.sourceBibliographyFormatParameter,
                                                                            APIParameter.targetBibliographyFormatParameter],
                                                               request: APIRequest(object: nil,
                                                                                   description: "A request containing an attached bibliography of arbitrary format, to compile into a given alternative format and return to the Client via the received Response.",
                                                                                   contentType: MediaType.urlEncodedForm.serialize()),
                                                               responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                       description: "A Response containing the compiled bibliography data either in the format as specified in the '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header, or CSL-JSON if the '\(Request.pressroomHeaders.sourceBibliographyFormat.stringValue)' header was specified instead.",
                                                                object: nil,
                                                                contentType: "Set to the 'Content-Type' corresponding to the representation of the bibliography format specified in the '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header, or '\(MediaType.json.serialize())' if the '\(Request.pressroomHeaders.sourceBibliographyFormat.stringValue)' header was specified instead."),
                                                                           AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + BibliographyTransformationsController.BibliographyDebuggingInfo.apiResponses,
                                                               authorization: true)])

        bibliographyRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func compileDocumentRoute() {
        let documentTransformationsController = DocumentTransformationsController()
        let documentRoute = post(Request.pressroomServerVersion, "compile", "document", use: documentTransformationsController.compileManuscript)
        documentRoute.setAPIController(name: "Document",
                                       description: "This endpoint manages document transformation operations.",
                                       externalDocs: nil,
                                       actions: [APIAction(method: .post,
                                                           route: documentRoute.apiActionRoute,
                                                           summary: "Compiles a document.",
                                                           description: "Compiles a received document into another format based on options present.",
                                                           parameters: [APIParameter.authorizationHeaderParameter,
                                                                        APIParameter.apiKeyHeaderParameter,
                                                                        APIParameter.continueOnErrorsParameter,
                                                                        APIParameter.equationsTypesettingTimeoutParameter,
                                                                        APIParameter.enrichDocumentMetadataParameter,
                                                                        APIParameter.enrichedContentSimilarityThresholdParameter,
                                                                        APIParameter.targetFileExtensionParameter,
                                                                        APIParameter.targetLaTeXMLOutputFormatParameter,
                                                                        APIParameter.teXContainerProcessingMethodParameter,
                                                                        APIParameter.primaryFileParameter,
                                                                        APIParameter.regenerateProjectBundleObjectIDsParameter,
                                                                        APIParameter.jatsArchiveURLParameter,
                                                                        APIParameter.jatsFileListParameter,
                                                                        APIParameter.jatsJournalCodeParameter,
                                                                        APIParameter.jatsSubmissionIdentifierParameter,
                                                                        APIParameter.jatsURLParameter,
                                                                        APIParameter.jatsVendorNameParameter,
                                                                        APIParameter.attachedDocumentParameter],
                                                           request: APIRequest(object: nil,
                                                                               description: "A request containing an attached document of arbitrary format, to compile into a given alternative format and return to the Client via the received Response.",
                                                                               contentType: MediaType.urlEncodedForm.serialize()),
                                                           responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                   description: "A streamed (i.e. 'Transfer-Encoding = chunked') Response containing the compiled document data. For '\(Request.pressroomHeaders.targetFileExtension.stringValue)' request header values of 'md', 'tex', 'html', 'xml' (for JATS) or 'go.xml' (for EM Manifest metadata XML), document data will be a zipped container including all associated document files such as figure images. For values of 'docx' and 'pdf' the document data will be an uncompressed single file. If a '\(Request.pressroomHeaders.primaryFile.stringValue)' header was provided in the originating request, a '\(Response.pressroomHeaders.primaryFileSelectionCriterion.stringValue)' header will be attached to the response indicating the selection criterion used to determine the 'primary' file for a compressed document container. If a '\(Request.pressroomHeaders.continueOnErrors.stringValue)' header was provided  in the originating request, a '\(Response.pressroomHeaders.compiledWithErrors.stringValue)' header will be attached to the response if there were errors while transforming the document.'",
                                                                                   object: nil,
                                                                                   contentType: "Set to the 'Content-Type' corresponding to the '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header, or is set to 'application/zip' depending on the configured options."),
                                                                       AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + DocumentTransformationsController.DocumentDebuggingInfo.apiResponses,
                                                           authorization: true)])

        documentRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func compileDocumentRouteLegacy() {
        let documentTransformationsController = DocumentTransformationsController()
        let documentRouteLegacy = post(Request.pressroomServerVersion, "document", "compile", use: documentTransformationsController.compileManuscript)
        documentRouteLegacy.setAPIController(name: "Document (legacy)",
                                             description: "This endpoint manages document transformation operations.",
                                             externalDocs: nil,
                                             actions: [APIAction(method: .post,
                                                                 route: documentRouteLegacy.apiActionRoute,
                                                                 summary: "Compiles a document (legacy endpoint).",
                                                                 description: "This endpoint has been deprecated in favor of the new and functionally-identical document compilation endpoint '/compile/document', and will be removed in a future release.")])
    }

    func compileDocumentFragmentRoute() {
        let fragmentTransformationsController = FragmentTransformationsController()
        let fragmentDocumentRoute = post(Request.pressroomServerVersion, "compile", "document", "fragment", use: fragmentTransformationsController.compileFragment)
        fragmentDocumentRoute.setAPIController(name: "Document (fragment)",
                                               description: "This endpoint manages document fragment transformation operations.",
                                               externalDocs: nil,
                                               actions: [APIAction(method: .post,
                                                                   route: fragmentDocumentRoute.apiActionRoute,
                                                                   summary: "Compiles a document fragment.",
                                                                   description: "Compiles a received document fragment into another format based on options present.",
                                                                   parameters: [APIParameter.authorizationHeaderParameter,
                                                                                APIParameter.apiKeyHeaderParameter,
                                                                                APIParameter.documentFragmentInputFormatParameter,
                                                                                APIParameter.documentFragmentOutputFormatParameter],
                                                                   request: APIRequest(object: nil,
                                                                                       description: "A request containing a document fragment in its body, to compile into a given output format and return to the Client via the received Response.",
                                                                                       contentType: MediaType.urlEncodedForm.serialize()),
                                                                   responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                           description: "A response containing the compiled document fragment in the format as specified by the '\(Request.pressroomHeaders.documentFragmentOutputFormat.stringValue)' header.",
                                                                    object: nil,
                                                                    contentType: MediaType.plainText.serialize()),
                                                                               AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + FragmentTransformationsController.FragmentDebuggingInfo.apiResponses,
                                                                   authorization: true)])

        fragmentDocumentRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func compileDocumentDepositRoute() {
        let documentDepositController = DocumentDepositsController()
        let documentDepositRoute = post(Request.pressroomServerVersion, "compile", "document", "deposit", use: documentDepositController.depositManuscript)
        documentDepositRoute.setAPIController(name: "Document (Deposit Services)",
                                              description: "This endpoint manages document transformation operations and deposits to external services and REST APIs.",
                                              externalDocs: nil,
                                              actions: [APIAction(method: .post,
                                                                  route: documentDepositRoute.apiActionRoute,
                                                                  summary: "Compiles a document.",
                                                                  description: "Compiles a received Manuscripts Project Bundle file into a container archive based on options present, which is also submitted to a relevant external service or REST API.",
                                                                  parameters: [APIParameter.apiKeyHeaderParameter,
                                                                               APIParameter.attachedProjectBundleParameter,
                                                                               APIParameter.authorizationHeaderParameter,
                                                                               APIParameter.jatsSubmissionDOIParameter,
                                                                               APIParameter.depositSubmissionIdentifierParameter,
                                                                               APIParameter.depositSubmissionJournalNameParameter,
                                                                               APIParameter.depositNotificationCallbackURLParameter],
                                                                  request: APIRequest(object: nil,
                                                                                      description: "A request containing an attached Manuscripts Project Bundle (i.e. .manuproj) file, to compile into a compressed document container and return to the Client via the received Response. Depending on the configuration option headers present on the Request, the compressed container will also be submitted to a relevant external service or REST API.",
                                                                                      contentType: MediaType.urlEncodedForm.serialize()),
                                                                  responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                          description: "A streamed (i.e. 'Transfer-Encoding = chunked') Response containing the compiled document data.",
                                                                                          object: nil,
                                                                                          contentType: MediaType.zip.serialize()),
                                                                              AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + DocumentDepositsController.DocumentDepositsDebuggingInfo.apiResponses,
                                                                  authorization: true)])

        documentDepositRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func compileDocumentJatsRoute() {
        let sachsTransformationsController = SachsTransformationsController()
        let jatsDocumentRoute = post(Request.pressroomServerVersion, "compile", "document", "jats", use: sachsTransformationsController.compileManuscript)
        jatsDocumentRoute.setAPIController(name: "Document (JATS)",
                                           description: "This endpoint manages JATS document transformation operations.",
                                           externalDocs: nil,
                                           actions: [APIAction(method: .post,
                                                               route: jatsDocumentRoute.apiActionRoute,
                                                               summary: "Compiles a document.",
                                                               description: "Compiles a received Manuscripts Project Bundle file (or MS Word .docx file) into various JATS XML-derived formats, HTML or a PDF based on options present.",
                                                               parameters: [APIParameter.apiKeyHeaderParameter,
                                                                            APIParameter.attachedProjectBundleOrDOCXParameter,
                                                                            APIParameter.authorizationHeaderParameter,
                                                                            APIParameter.digitalObjectTypeParameter,
                                                                            APIParameter.eXtylesArcEditorialStyleParameter,
                                                                            APIParameter.eXtylesArcSecretParameter,
                                                                            APIParameter.jatsDocumentProcessingLevelParameter,
                                                                            APIParameter.jatsGroupDOIParameter,
                                                                            APIParameter.jatsInputFormatParameter,
                                                                            APIParameter.jatsISSNParameter,
                                                                            APIParameter.jatsSeriesCodeParameter,
                                                                            APIParameter.jatsSubmissionDOIParameter,
                                                                            APIParameter.targetJATSOutputFormatParameter,
                                                                            APIParameter.targetJATSVersionParameter],
                                                               request: APIRequest(object: nil,
                                                                                   description: "A request containing an attached Manuscripts Project Bundle (i.e. .manuproj) file (or MS Word .docx file), to compile into a JATS XML-derived format, HTML or a PDF compressed document container and return to the Client via the received Response. N.B. MS Word to JATS XML transformations can be long-running, and therefore there may be some delay in this endpoint returning the compiled JATS XML in this case. Clients should therefore consider extending (or disabling) any request timeouts for this use case.",
                                                                                   contentType: MediaType.urlEncodedForm.serialize()),
                                                               responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                       description: "A streamed (i.e. 'Transfer-Encoding = chunked') Response containing the compiled document data. A '\(Response.pressroomHeaders.compiledWithErrors.stringValue)' header will be attached to the response if there were recoverable warnings or errors while transforming the Manuscripts Project Bundle or MS Word .docx file to JATS XML.",
                                                                object: nil,
                                                                contentType: MediaType.zip.serialize()),
                                                                           AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + SachsTransformationsController.SachsDocumentDebuggingInfo.apiResponses,
                                                               authorization: true)])

        jatsDocumentRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func compileDocumentTeXRoute() {
        let teXTransformationsController = TeXTransformationsController()
        let teXDocumentRoute = post(Request.pressroomServerVersion, "compile", "document", "latexml", use: teXTransformationsController.compileManuscript)
        teXDocumentRoute.setAPIController(name: "Document (LaTeXML)",
                                          description: "This endpoint manages TeX document transformation operations (using LaTeXML under-the-hood).",
                                          externalDocs: nil,
                                          actions: [APIAction(method: .post,
                                                              route: teXDocumentRoute.apiActionRoute,
                                                              summary: "Compiles a document.",
                                                              description: "Compiles a received TeX document into another format based on options present.",
                                                              parameters: [APIParameter.authorizationHeaderParameter,
                                                                           APIParameter.apiKeyHeaderParameter,
                                                                           APIParameter.equationsTypesettingTimeoutParameter,
                                                                           APIParameter.targetLaTeXMLOutputFormatParameter,
                                                                           APIParameter.attachedCompressedTexContainerParameter],
                                                              request: APIRequest(object: nil,
                                                                                  description: "A request containing an attached TeX document, to compile into a given alternative format and return to the Client via the received Response.",
                                                                                  contentType: MediaType.urlEncodedForm.serialize()),
                                                              responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                      description: "A streamed (i.e. 'Transfer-Encoding = chunked') Response containing the compiled document data.",
                                                                                      object: nil,
                                                                                      contentType: "Set to the 'Content-Type' corresponding to the '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header, or is set to 'application/zip' depending on the configured options."),
                                                                          AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + TeXTransformationsController.TeXDocumentDebuggingInfo.apiResponses,
                                                              authorization: true)])

        teXDocumentRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func receiveSubmissionsRoute() {
        let receivedSubmissionsController = ReceivedSubmissionsController()
        let receivedSubmissionsRoute = post(Request.pressroomServerVersion, "receive", "submission", use: receivedSubmissionsController.receiveSubmission)
        receivedSubmissionsRoute.setAPIController(name: "Receive Submission",
                                                  description: "This endpoint further processes submissions that have been received by the EEO Deposit Service API.",
                                                  externalDocs: nil,
                                                  actions: [APIAction(method: .post,
                                                                      route: receivedSubmissionsRoute.apiActionRoute,
                                                                      summary: "Further processes received EEO submissions.",
                                                                      description: "The DOCX representation of the submission is processed further.",
                                                                      parameters: [APIParameter.authorizationHeaderParameter,
                                                                                   APIParameter.apiKeyHeaderParameter],
                                                                      request: APIRequest(object: EEOReceivedSubmission.self,
                                                                                          description: "A request containing a representation of the received submission after ingestion.",
                                                                                          contentType: MediaType.json.serialize()),
                                                                      responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                              description: "A streamed (i.e. 'Transfer-Encoding = chunked') Response containing the gateway bundle data.",
                                                                                              object: nil,
                                                                                              contentType: MediaType.zip.serialize()),
                                                                                  AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse] + ReceivedSubmissionsController.ReceivedSubmissionDebuggingInfo.apiResponses,
                                                                      authorization: true)])

        receivedSubmissionsRoute.setAPIObjects([APIObject.exampleAuthorizationErrorResponse])
    }

    func versionRoute() {
        let apiVersionInfoController = APIVersionInfoController()
        let versionRoute = get(Request.pressroomServerVersion, "version", use: apiVersionInfoController.versionInfo)
        versionRoute.setAPIController(name: "Version",
                                      description: "This endpoint presents server version debugging information.",
                                      externalDocs: nil,
                                      actions: [APIAction(method: .get,
                                                          route: versionRoute.apiActionRoute,
                                                          summary: "Presents server version information.",
                                                          description: "This route is useful for debugging purposes.",
                                                          parameters: [APIParameter.authorizationHeaderParameter,
                                                                       APIParameter.apiKeyHeaderParameter],
                                                          request: APIRequest(object: nil,
                                                                              description: "A simple GET request.",
                                                                              contentType: nil),
                                                          responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                                  description: "A Response containing PressroomServer version debugging information.",
                                                                                  object: APIVersionInfoController.APIDebuggingInfo.self,
                                                                                  contentType: MediaType.json.serialize()),
                                                                      AuthMiddleware.AuthMiddlewareDebuggingInfo.apiResponse],
                                                          authorization: true)])

        versionRoute.setAPIObjects([APIObject.exampleAPIDebuggingInfo,
                                    APIObject.exampleAuthorizationErrorResponse])
    }

    func docsRoute() {
        let openAPIDocumentationController = OpenAPIDocumentationController()
        let docsRoute = get(Request.pressroomServerVersion, "docs", use: openAPIDocumentationController.serverDocumentation)
        docsRoute.setAPIController(name: "Docs",
                                   description: "This endpoint presents PressroomServer API documentation.",
                                   externalDocs: nil,
                                   actions: [APIAction(method: .get,
                                                       route: docsRoute.apiActionRoute,
                                                       summary: "Presents PressroomServer API documentation in OpenAPI 3.x format.",
                                                       description: "This endpoint presents the documentation that you are currently reading.",
                                                       parameters: [APIParameter.htmlRenderingParameter],
                                                       request: APIRequest(object: nil,
                                                                           description: "A simple GET request.",
                                                                           contentType: nil),
                                                       responses: [APIResponse(code: String(HTTPStatus.ok.code),
                                                                               description: "A Response containing PressroomServer documentation in OpenAPI 3.x format.",
                                                                               object: nil,
                                                                               contentType: MediaType.json.serialize())],
                                                       authorization: true)])
    }
}
