//
//  DocumentTransformationsController+PreImportTransformations.swift
//  App
//
//  Created by Dan Browne on 02/12/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

extension DocumentTransformationsController {

    /// Preprocess the document represented by a `PrimaryFileCandidate`, if necessary.
    /// (I.e. a transformation to a different format before the import or compilation logic has executed
    /// downstream, generating the final document format for a returned `Response`).
    /// - Parameter candidate: A `PrimaryFileCandidate` for the source document.
    func preprocessIfNecessary(_ candidate: PrimaryFileCandidate) throws -> URL {
        let candidateFileURL = try candidate.ensuredFileURL()

        // If the `candidate` is an RTF file, transform it into a MS Word DOCX
        if candidateFileURL.pathExtension == "rtf" {
            return try LibreOfficeTransformationService().transform(candidate, outputFormat: .docx)
        }

        return candidateFileURL
    }
}
