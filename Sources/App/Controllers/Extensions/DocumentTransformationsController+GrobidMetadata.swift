//
//  DocumentTransformationsController+GrobidMetadata.swift
//  App
//
//  Created by Dan Browne on 21/03/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import MPFoundation

// MARK: - Grobid metadata extraction

//swiftlint:disable file_length cyclomatic_complexity function_body_length

extension DocumentTransformationsController {

    // MARK: - Public methods

    /// Compile the document content that was previously-imported using a `MPManuscriptsPackageController`
    /// into a Manuscripts Project Bundle whose metadata has been enriched based on an API call to Grobid
    /// to extract metadata from an intermediate PDF representation of the original document.
    ///
    /// - Parameters:
    ///   - originatingRequest: The `Request` containing the document export format information in its headers.
    ///   - packageController: A `MPManuscriptsPackageController` containing imported document content
    ///                        to use for the document compile operation.
    ///   - candidate: A `PrimaryFileCandidate` representing the imported document
    ///                that is attached to the originating `Request`.
    /// - Returns: A `Future` that is fulfilled with a `FileURLPrimaryFileCandidatePackageControllerTuple`,
    ///            or an error if it fails for some reason.
    func compileEnrichedProjectBundle(originatingRequest: Request,
                                      using packageController: MPManuscriptsPackageController,
                                      candidate: PrimaryFileCandidate) -> Future<FileURLPrimaryFileCandidatePackageControllerTuple> {

        // 1): Compile the document to an intermediate PDF
        return compileDocumentToPDF(originatingRequest: originatingRequest,
                                    using: packageController,
                                    candidate: candidate).then {[unowned self] fileURL -> Future<Response> in
                                        // 2): Attempt to extract metadata from intermediate PDF using Grobid
                                        return self.extractPDFMetadata(from: fileURL,
                                                                       originatingRequest: originatingRequest)
            }.then {[unowned self] grobidResponse -> Future<PrimaryFileCandidate> in
                // 3): Write response XML to disk in temporary working directory
                return self.candidateForGrobidXMLResponse(grobidResponse,
                                                          originatingRequest: originatingRequest,
                                                          sourceCandidate: candidate)
            }.then { fileURLCandidate -> Future<PrimaryFileCandidatePackageControllerTuple> in
                // 4): Configure a _new_ package controller for Grobid XML -> Project Bundle import & compile operations
                //
                // A `Promise` used for notification of if the pkgc configuration has completed successfully
                // (Needed due to the fact that the configuration step is executed on the main-queue,
                // so the `Future` cannot be returned directly)
                let promise = originatingRequest.eventLoop.newPromise(PrimaryFileCandidatePackageControllerTuple.self)

                // Configure the Manuscripts package controller, ready for import operations (requires main-queue)
                DispatchQueue.main.async {[weak self] in
                    self?.configurePackageController(candidate: fileURLCandidate, mainQueuePromise: promise)
                }

                return promise.futureResult
            }.then { candidatePkgcTuple -> Future<PrimaryFileCandidatePackageControllerTuple> in
                // 5): Import Grobid XML content
                //
                // A `Promise` used for notification of if the content import has completed successfully
                // (Needed due to the fact that the import content step is executed on the main-queue,
                // so the `Future` cannot be return directly)
                let promise = originatingRequest.eventLoop.newPromise(PrimaryFileCandidatePackageControllerTuple.self)

                // Import the content from the received document that is now stored on disk (requires main-queue)
                DispatchQueue.main.async {[weak self] in
                    self?.importContent(from: candidatePkgcTuple.candidate,
                                        using: candidatePkgcTuple.pkgc,
                                        request: originatingRequest,
                                        mainQueuePromise: promise)
                }
                return promise.futureResult
            }.then {[unowned self] metadataPkgcTuple -> Future<PrimaryFileCandidatePackageControllerTuple> in
                // 6): Enrich the `packageController`'s `primaryManuscript`
                // (which contains the imported document that was attached to `originatingRequest`),
                // with the imported metadata extracted by Grobid from the intermediate PDF,
                // that is contained within `metadataPkgcTuple.pkgc`.

                // A `Promise` used for notification of if the metadata enrichment has completed successfully
                // (Needed due to the fact that the enrichment step is executed on the main-queue to avoid CBL
                // multiple-thread access type assertions, and so the `Future` cannot be returned directly)
                let promise = originatingRequest.eventLoop.newPromise(Void.self)

                DispatchQueue.main.async {[weak self] in
                    self?.enrichMetadata(for: packageController,
                                         using: metadataPkgcTuple.pkgc,
                                         originatingRequest: originatingRequest,
                                         mainQueuePromise: promise)

                    // Close the `metadataPkgcTuple.pkgc`'s internal database connections as it is no longer needed
                    try? metadataPkgcTuple.pkgc.close()
                }

                // When enrichment has completed, fulfill a `Future` with the `metadataPkgcTuple` to pass it on
                return promise.futureResult.then({ () -> Future<PrimaryFileCandidatePackageControllerTuple> in
                    return originatingRequest.eventLoop.newSucceededFuture(result: metadataPkgcTuple)
                })
            }.then {[unowned self] metadataPkgcTuple -> Future<FileURLPrimaryFileCandidatePackageControllerTuple> in
                // 7): Compile to Project Bundle (original `Request` can be used here as it contains the original
                // Client intention of a target file extension header of 'manuproj')
                return self.compileDocument(request: originatingRequest,
                                            using: packageController,
                                            candidate: metadataPkgcTuple.candidate)
        }.catchFlatMap {[unowned self] error -> Future<FileURLPrimaryFileCandidatePackageControllerTuple> in
            guard (error as NSError).domain == NSURLErrorDomain else {
                throw error
            }

            let metadataExtractionError = PressroomError.error(for: DocumentDebuggingInfo.documentMetadataExtractionFailure,
                                                               sourceLocation: .capture(),
                                                               underlyingError: error)
            guard originatingRequest.http.headers.contains(name: Request.pressroomHeaders.continueOnErrors) else {
                // Re-throw Grobid connection failure errors as a `documentMetadataExtractionFailure`
                // with an underlyingError containing the original network error
                // (if `continueOnErrors` header isn't present)
                throw metadataExtractionError
            }

            candidate.compilationError = metadataExtractionError

            return self.compileDocument(request: originatingRequest,
                                        using: packageController,
                                        candidate: candidate)
        }
    }

    // MARK: Private helper methods

    /// Compile the document content that was previously-imported using a `MPManuscriptsPackageController` into a PDF
    /// to use as an intermediate PDF representation of the original document for metadata extraction purposes.
    ///
    /// - Parameters:
    ///   - originatingRequest: The originating `Request` which contains the received document.
    ///   - packageController: A `MPManuscriptsPackageController` containing imported document content
    ///                        to use for the document compile operation.
    ///   - candidate: A `PrimaryFileCandidate` representing the imported document
    ///                that is attached to the originating `Request`.
    /// - Returns: A `Future` that is fulfilled with a file `URL` to the PDF on disk,
    ///            or an error if it fails for some reason.
    private func compileDocumentToPDF(originatingRequest: Request,
                                      using packageController: MPManuscriptsPackageController,
                                      candidate: PrimaryFileCandidate) -> Future<URL> {

        // If candidate check result '.valid', a `PDFTransformable` service can be used directly,
        // If candidate check result '.alreadyPDF', return the existing candidate file `URL` without compiling
        // (Otherwise, use the normal compile operation flow)
        switch candidate.pdfTransformSuitability() {
        case .validOfficeCandidate, .validTeXCandidate:
            do {
                return try compiledPDF(originatingRequest, candidate: candidate).flatMap { tuple -> Future<URL> in
                    // Map to a Future<URL> as the other components of the tuple are not required here
                    return originatingRequest.eventLoop.newSucceededFuture(result: tuple.fileURL)
                }
            } catch {
                return originatingRequest.eventLoop.newFailedFuture(error: error)
            }
        case .alreadyPDF:
            return originatingRequest.eventLoop.newSucceededFuture(result: (try? candidate.ensuredFileURL())!)
        case .invalidCandidate:
            // Modify the original request headers to compile document to a PDF (originally "manuproj")
            let pdfCompileRequest = Request(http: originatingRequest.http, using: originatingRequest.sharedContainer)
            pdfCompileRequest.http.headers.replaceOrAdd(name: Request.pressroomHeaders.targetFileExtension,
                                                        value: "pdf")

            return compileDocument(request: pdfCompileRequest,
                                   using: packageController,
                                   candidate: candidate).flatMap { tuple in
                                    // Map to a Future<URL> as the other components of the tuple are not required here
                                    return originatingRequest.eventLoop.newSucceededFuture(result: tuple.fileURL)
            }
        }
    }

    /// Extract metadata from a PDF stored on-disk using the Grobid web service.
    ///
    /// - Parameters:
    ///   - pdfFileURL: A file `URL` to a PDF file, stored inside a temporary working directory.
    ///   - originatingRequest: The originating `Request` which contains the received document.
    /// - Returns: A `Future` that is fulfilled with a `Response` containing Grobid XML in its body,
    ///            or an error if the metadata extraction fails for some reason.
    private func extractPDFMetadata(from pdfFileURL: URL, originatingRequest: Request) -> Future<Response> {
        let grobidService = GrobidMetadataService()
        do {
            return try grobidService.metadataResponse(for: pdfFileURL,
                                                      endpoint: .processHeaderDocument,
                                                      originatingRequest: originatingRequest)
        } catch {
            return originatingRequest.eventLoop.newFailedFuture(error: error)
        }
    }

    /// Write the Grobid XML response to disk in a temporary ingest working directory.
    ///
    /// - Parameters:
    ///   - response: The `Response` containing Grobid XML in its body.
    ///   - originatingRequest: The originating `Request` which contains the received document.
    ///   - sourceCandidate: The `PrimaryFileCandidate` representing the original source document.
    /// - Returns: A `Future` that is fulfilled with a `PrimaryFileCandidate` containing a file `URL`
    ///            for the saved document on disk, or an error if it fails for some reason.
    private func candidateForGrobidXMLResponse(_ response: Response,
                                               originatingRequest: Request,
                                               sourceCandidate: PrimaryFileCandidate) -> Future<PrimaryFileCandidate> {
        do {
            let workingDirectoryFileURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(UUID().uuidString),
                                              isDirectory: true)
            let fileExtension = MPImportServiceContentGrobidXML.grobidXMLFileExtension
            let grobidFileURL = workingDirectoryFileURL.appendingPathComponent("grobid_response.\(fileExtension)")
            guard let grobidXMLData = response.http.body.data, response.http.status == .ok else {
                let metadataExtractionError = PressroomError.error(for: DocumentDebuggingInfo.documentMetadataExtractionFailure,
                                                                   sourceLocation: .capture())
                guard originatingRequest.http.headers.contains(name: Request.pressroomHeaders.continueOnErrors) else {
                    return originatingRequest.eventLoop.newFailedFuture(error: metadataExtractionError)
                }

                sourceCandidate.compilationError = metadataExtractionError

                return originatingRequest.eventLoop.newSucceededFuture(result: sourceCandidate)
            }

            try FileManager.default.createDirectory(at: workingDirectoryFileURL,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
            let grobidXMLString = String(data: grobidXMLData, encoding: .utf8)
            try grobidXMLString?.write(to: grobidFileURL, atomically: true, encoding: .utf8)

            // If `sourceDocumentCandidate` transformed to an intermediate PDF with an error, copy it
            let grobidXMLCandidate = PrimaryFileCandidate(fileURL: grobidFileURL)
            grobidXMLCandidate.compilationError = sourceCandidate.compilationError

            return originatingRequest.eventLoop.newSucceededFuture(result: grobidXMLCandidate)
        } catch {
            let error = PressroomError.error(for: DocumentDebuggingInfo.documentMetadataIngestFailure,
                                             sourceLocation: .capture(),
                                             underlyingError: error)
            return originatingRequest.eventLoop.newFailedFuture(error: error)
        }
    }

    /// Enrich the metadata of a manuscript associated with a given `MPManuscriptsPackageController`
    /// using a donor `MPManuscriptsPackageController` containing richer metadata.
    ///
    /// - Parameters:
    ///   - packageController: A `MPManuscriptsPackageController` whose metadata will be enriched.
    ///   - enrichedPackageController: A donor `MPManuscriptsPackageController`.
    ///   - originatingRequest: The originating `Request` which contains the received document.
    ///   - mainQueuePromise: A `Promise` tracking the state of the operations
    ///                       that have been dispatched on to the main queue.
    private func enrichMetadata(for packageController: MPManuscriptsPackageController,
                                using enrichedPackageController: MPManuscriptsPackageController,
                                originatingRequest: Request,
                                mainQueuePromise: Promise<Void>) {

        let originalManuscript = packageController.manuscriptsController.primaryManuscript
        let enrichedManuscript = enrichedPackageController.manuscriptsController.primaryManuscript

        // Use `enrichedManuscript.tile` if it exists and is not empty,
        // AND the two titles are not already equal to one another.
        if let enrichedTitle = enrichedManuscript?.title,
            enrichedTitle.count > 0,
            enrichedTitle != originalManuscript?.title {
            originalManuscript?.title = enrichedTitle
        }

        // Use `enrichedManuscript.abstract` in favor of any existing abstract that may be present
        if let enrichedAbstract = enrichedManuscript?.abstract {
            persistCopy(of: enrichedAbstract, to: packageController)
        }

        removeDuplicateBodyContent(from: packageController,
                                   referringTo: enrichedPackageController,
                                   originatingRequest: originatingRequest)

        // Handle PDF source doc & no Grobid-extracted abstract `MPSection` special case
        // (I.e. `packageController` will have zero sections even after enrichment,
        // so there needs to be a dummy section so when `compileTreeItems(…)` is called it doesn't assert)
        if packageController.sectionsController.orderedNodes.count == 0 {
            let dummySection = packageController.sectionsController.newSection(withParent: nil)
            dummySection.saveSilently()
        }

        // Attempt to enrich contributors by type, if there are enriched contributors present
        if enrichedPackageController.contributorsController.allContributors.count > 0 {
            if packageController.contributorsController.allAuthors.count == 0 {
                persistCopy(of: enrichedPackageController.contributorsController.allAuthors,
                            to: packageController)
            }
            if packageController.contributorsController.allEditors.count == 0 {
                persistCopy(of: enrichedPackageController.contributorsController.allEditors,
                            to: packageController)
            }
            if packageController.contributorsController.allTranslators.count == 0 {
                persistCopy(of: enrichedPackageController.contributorsController.allTranslators,
                            to: packageController)
            }
        }

        // Fulfill the promise
        mainQueuePromise.succeed()
    }

    /// Persist a copy of an abstract `MPSection` to a given `MPManuscriptsPackageController`.
    ///
    /// - Parameters:
    ///   - abstract: An abstract `MPSection` to make a copy of, and then persist.
    ///   - packageController: A `MPManuscriptsPackageController` on which to persist the copied abstract `MPSection`.
    private func persistCopy(of abstract: MPSection, to packageController: MPManuscriptsPackageController) {

        if let existingPrimaryManuscript = packageController.manuscriptsController.primaryManuscript {
            // Delete the existing abstract (if one exists)
            existingPrimaryManuscript.abstract?.delete()

            // Dispatch the initialization of the abstract copy to the same queue
            // that the previous `delete()` call occured on to prevent CBL cross-thread access assertions
            mp_dispatch_sync(existingPrimaryManuscript.database.manager.dispatchQueue, UInt(packageController.serverQueueToken)) {
                // Copy the abstract section (along with the paragraph elements and styles contained within it)
                let abstractCopy = MPSection(copyOf: abstract,
                                             controller: packageController.sectionsController)

                for paragraphElement in abstract.elements ?? [] {
                    let paraStyleCopy = packageController.stylesController.defaultParagraphStyle(forXHTMLElement: "p")
                    paraStyleCopy?.saveSilently()
                    let paragraphElementCopy = MPParagraphElement(copyOf: paragraphElement,
                                                                  controller: packageController.elementsController)
                    paragraphElementCopy.paragraphStyle = paraStyleCopy
                    paragraphElementCopy.saveSilently()
                    abstractCopy.elements?.append(paragraphElementCopy)
                }
                abstractCopy.saveSilently()
            }
        }
    }

    /// Persist a copy of an array of `MPContributor`s to a given `MPManuscriptsPackageController`.
    ///
    /// - Parameters:
    ///   - contributors: An array of `MPContributor`s to make copies of, and then persist.
    ///   - packageController: A `MPManuscriptsPackageController` on which to persist the copied `MPContributor`s.
    private func persistCopy(of contributors: [MPContributor], to packageController: MPManuscriptsPackageController) {
        var affiliationCopiesByOriginalDocumentID = [String: MPAffiliation]()

        for contributor in contributors {
            let contributorCopy = MPContributor(copyOf: contributor,
                                                controller: packageController.contributorsController)
            for affiliation in contributor.affiliations ?? [] {
                // If an affiliation already has an existing copy, use it
                // Otherwise, create an affiliation copy and add it to the map of existing affiliations
                // (This avoids creating duplicates when affiliations are shared between contributors)
                guard let affiliationDocumentID = affiliation.documentID else {
                    return
                }
                if let existingAffiliationCopy = affiliationCopiesByOriginalDocumentID[affiliationDocumentID] {
                    contributorCopy.affiliations?.append(existingAffiliationCopy)
                } else {
                    let affiliatonCopy = MPAffiliation(copyOf: affiliation,
                                                       controller: packageController.affiliationsController)
                    affiliatonCopy.saveSilently()
                    contributorCopy.affiliations?.append(affiliatonCopy)
                    affiliationCopiesByOriginalDocumentID[affiliationDocumentID] = affiliatonCopy
                }
            }
            contributorCopy.saveSilently()
        }
    }

    /// Removes any content from an enriched `MPManuscriptsPackageController` that is now duplicated in its body.
    ///
    /// - Parameters:
    ///   - packageController: A `MPManuscriptsPackageController` whose metadata has been enriched,
    ///                        and likely contains redundant content in its manuscript body.
    ///   - enrichedPackageController: A donor `MPManuscriptsPackageController`.
    ///   - originatingRequest: The originating `Request` which contains the received document.
    private func removeDuplicateBodyContent(from packageController: MPManuscriptsPackageController,
                                            referringTo enrichedPackageController: MPManuscriptsPackageController,
                                            originatingRequest: Request) {
        let enrichedManuscript = enrichedPackageController.manuscriptsController.primaryManuscript
        let thresholdHeader = originatingRequest.http.headers
            .firstValue(name: Request.pressroomHeaders.enrichedContentSimilarityThreshold)
        let similarityThreshold = 0.0..<1.0 ~= (Double(thresholdHeader ?? "0.95") ?? 0.95)
            ? (Double(thresholdHeader ?? "0.95") ?? 0.95) : 0.95

        // Title body content removal
        var shouldBreakTitleSearch = false
        for section in packageController.sectionsController.orderedSections {
            for sectionElement in section.elements ?? [] {
                guard let sectionBlockElement = sectionElement as? MPBlockElement else {
                    continue
                }

                if let enrichedTitle = enrichedManuscript?.title,
                    let sectionBlockInnerHTML = sectionBlockElement.innerHTML {
                    let similarity = sectionBlockInnerHTML.similarity(to: enrichedTitle)
                    switch similarity {
                    case .equal,
                         .containsTarget,
                         .containedByTarget:
                        section.deleteElement(sectionElement, deleteContainedObject: false)
                        shouldBreakTitleSearch = true
                    case .jaroWinklerDistance(let distance):
                        if distance > similarityThreshold {
                            section.deleteElement(sectionElement, deleteContainedObject: false)
                            shouldBreakTitleSearch = true
                        }
                    }
                }
                if shouldBreakTitleSearch {
                    break
                }
            }
            if section.children.count == 0 {
                section.delete()
            }
            if shouldBreakTitleSearch {
                break
            }
        }

        // Abstract body content removal
        var shouldBreakAbstractSearch = false
        let sharedPackageController = MPSharedPackageController.sharedPackageController()
        if let enrichedAbstract = enrichedManuscript?.abstract {
            for enrichedAbstractElement in enrichedAbstract.elements ?? [] {

                guard let abstractBlockElement = enrichedAbstractElement as? MPBlockElement,
                    let abstractBlockInnerHTML = abstractBlockElement.innerHTML else {
                        continue
                }

                for section in packageController.sectionsController.orderedSections {
                    for sectionElement in section.elements ?? [] {
                        guard let sectionBlockElement = sectionElement as? MPBlockElement,
                            let sectionBlockInnerHTML = sectionBlockElement.innerHTML,
                            section.category != sharedPackageController.sectionCategoriesController.sectionCategoryAbstract else {
                            continue
                        }

                        let similarity = sectionBlockInnerHTML.similarity(to: abstractBlockInnerHTML,
                                                                          reversedComparison: true)
                        switch similarity {
                        case .equal,
                             .containsTarget,
                             .containedByTarget:
                            section.deleteElement(sectionElement, deleteContainedObject: false)
                            shouldBreakAbstractSearch = true
                        case .jaroWinklerDistance(let distance):
                            if distance > similarityThreshold {
                                section.deleteElement(sectionElement, deleteContainedObject: false)
                                shouldBreakAbstractSearch = true
                            }
                        }
                        if shouldBreakAbstractSearch {
                            break
                        }
                    }

                    if section.children.count == 0 {
                        section.delete()
                    }
                    if shouldBreakAbstractSearch {
                        break
                    }
                }
            }
        }
    }
}
