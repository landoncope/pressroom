//
//  ReceivedSubmissionsController.swift
//  App
//
//  Created by Dan Browne on 08/04/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import MPFoundation
import Vapor

final class ReceivedSubmissionsController: ContentPersistable, ContentTransformable {

    let sachsController = SachsTransformationsController()

    // MARK: - Public methods

    /// Receives notification of a submission to the EEO web service, and transforms it into a `gateway-bundle` before returning
    /// it to the Client in a `Response`.
    /// - Parameter request: The `Request` received by Pressroom, containing a JSON record of the received submission.
    /// - Returns: A `Future<Response>` that is fulfilled once the attachment from the `submission` has been transformed into a
    ///            `gateway-bundle` JATS XML compressed document container, including any image attachments from the `submission`.
    /// - Throws: An `Error` if the document transformation failed for some reason.
    func receiveSubmission(_ request: Request) throws -> Future<Response> {

        let receivedSubmissionService = EEOReceivedSubmissionService()

        return try receivedSubmissionService.decodeSubmission(from: request).flatMap({ receivedSubmission -> Future<Response> in
            let fileFuture = try self.fetchDocumentAttachment(from: receivedSubmission,
                                                              using: receivedSubmissionService,
                                                              originatingRequest: request)

            return fileFuture.flatMap { file -> Future<Response> in
                let format = try self.mainDocumentFormat(for: receivedSubmission)
                return try self.prepareGatewayBundleSubmission(from: receivedSubmission,
                                                               attachment: file,
                                                               format: format,
                                                               originatingRequest: request)
            }
        }).catchMap({ error -> Response in
            // If the `Error` is already a `PressroomError`,
            // simply re-throw the error as-is.
            // Otherwise, throw a constructed `PressroomError`.
            if let pressroomError = error as? PressroomError {
                throw pressroomError
            } else {
                throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        })
    }

    // MARK: - Private methods

    /// Asynchronously fetches the primary document attachment from a given `submission`.
    /// - Parameters:
    ///   - submission: A `EEOReceivedSubmission`, previously decoded from the `originatingRequest`.
    ///   - service: An instance of `EEOReceivedSubmissionService`, used for fetching attachments.
    ///   - originatingRequest: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<File>` that is fulfilled once the attachment from the `submission` has been transformed into a
    ///            `gateway-bundle` JATS XML compressed document container, including any image attachments from the `submission`.
    /// - Throws: An `Error` if the document transformation failed for some reason.
    private func fetchDocumentAttachment(from submission: EEOReceivedSubmission,
                                         using service: EEOReceivedSubmissionService,
                                         originatingRequest: Request) throws -> Future<File> {

        let format = try mainDocumentFormat(for: submission)
        let attachments = try service.fetchAttachments(from: submission,
                                                       designation: try mainDocumentDesignation(for: submission),
                                                       format: format,
                                                       originatingRequest: originatingRequest)

        switch format {
        case .docx:

            guard let docxAttachment = attachments.first?.value else {
                throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.missingDOCXMainDocumentAttachment,
                                           sourceLocation: .capture())
            }

            guard attachments.count == 1 else {
                throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.multipleDOCXMainDocumentAttachments,
                                           sourceLocation: .capture())
            }

            return docxAttachment
        case .xml:

            guard let jatsAttachment = attachments.first?.value else {
                throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.missingJATSMainDocumentAttachment,
                                           sourceLocation: .capture())
            }

            guard attachments.count == 1 else {
                throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.multipleJATSMainDocumentAttachments,
                                           sourceLocation: .capture())
            }

            // Rename article JATS (for `sachs` heuristic purposes)
            return jatsAttachment.map { jatsFile -> File in
                return File(data: jatsFile.data,
                            filename: "\(String(jatsFile.filename.split(separator: ".").first!)).XML")
            }
        }
    }

    /// Prepares a `gateway-bundle` JATS XML compressed document container, before returning it to the Client in a `Response`.
    /// - Parameters:
    ///   - submission: A `EEOReceivedSubmission`, previously decoded from the `originatingRequest`.
    ///   - attachment: The attachment `File` decoded from the provided `submission`.
    ///   - format: The expected file format of the attachment decoded from the provided `submission`.
    ///   - originatingRequest: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<Response>` that is fulfilled once the attachment from the `submission` has been transformed into a
    ///            `gateway-bundle` JATS XML compressed document container, including any image attachments from the `submission`.
    /// - Throws: An `Error` if the document transformation failed for some reason.
    private func prepareGatewayBundleSubmission(from submission: EEOReceivedSubmission,
                                                attachment: File,
                                                format: EEOReceivedSubmissionService.DocumentFormat,
                                                originatingRequest: Request) throws -> Future<Response> {

        let attachmentContent = try encodeContent(from: ArbitrarilyFormattedDocument(file: attachment))

        // Write the attachment to disk in temp location
        let fileURL = attachmentContent.workingDirectory.appendingPathComponent(attachmentContent.content.file.filename)
        try attachmentContent.content.file.data.write(to: fileURL, options: .atomic)

        switch format {
        case .docx:
            return try prepareGatewayBundle(docxContent: attachmentContent,
                                            submission: submission,
                                            originatingRequest: originatingRequest)
        case .xml:
            return try prepareGatewayBundle(jatsContent: attachmentContent,
                                            submission: submission,
                                            originatingRequest: originatingRequest)
        }
    }

    /// Prepares a `gateway-bundle` JATS XML compressed document container, before returning it to the Client in a `Response`.
    /// - Parameters:
    ///   - docxContent: A MS Word document file, previously encoded as `PersistableContent<ArbitrarilyFormattedDocument>`.
    ///   - submission: A `EEOReceivedSubmission`, previously decoded from a `Request`.
    ///   - originatingRequest: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<Response>` that is fulfilled once the MS Word document file has been transformed into a
    ///            `gateway-bundle` JATS XML compressed document container, including any image attachments from the `submission`.
    /// - Throws: An `Error` if the document transformation failed for some reason.
    private func prepareGatewayBundle(docxContent: PersistableContent<ArbitrarilyFormattedDocument>,
                                      submission: EEOReceivedSubmission,
                                      originatingRequest: Request) throws -> Future<Response> {

        // DOCX -> JATS (via eXtylesArc)
        originatingRequest.http.headers.add(name: Request.pressroomHeaders.eXtylesArcSecret,
                                            value: Environment.essentialValueForKey(.eXtylesArcServiceSecret))
        originatingRequest.http.headers.add(name: Request.pressroomHeaders.jatsDocumentProcessingLevel, value: "full_text")

        return try sachsController.compileManuscript(from: docxContent, request: originatingRequest)
            .flatMap({ response -> Future<Response> in
                let jatsFile = File(data: response.http.body.data!, filename: "docx_to_jats.zip")
                let jatsContent = try self.encodeContent(from: ArbitrarilyFormattedDocument(file: jatsFile))

                // Write the attachment to disk in temp location
                let fileURL = jatsContent.workingDirectory.appendingPathComponent(jatsContent.content.file.filename)
                try jatsContent.content.file.data.write(to: fileURL, options: .atomic)

                // JATS -> gateway-bundle
                let options = SachsTransformationService.ConfigurationOptions(digitalObjectType: nil,
                                                                              doi: submission.metadata.doi,
                                                                              groupDOI: nil,
                                                                              inputFormat: .extylesArcJATS,
                                                                              issn: submission.metadata.digitalISSN,
                                                                              outputFormat: .gatewayBundle,
                                                                              jatsVersion: nil,
                                                                              processingLevel: .fullText)

                let gatewayBundleResult = try SachsTransformationService().transform(jatsContent, options: options)
                let response = try self.chunkedResponse(forFileContentsAt: gatewayBundleResult.fileURL,
                                                        request: originatingRequest)

                return originatingRequest.eventLoop.newSucceededFuture(result: response)
            })
    }

    /// Prepares a `gateway-bundle` JATS XML compressed document container, before returning it to the Client in a `Response`.
    /// - Parameters:
    ///   - jatsContent: A JATS XML file, previously encoded as `PersistableContent<ArbitrarilyFormattedDocument>`.
    ///   - submission: A `EEOReceivedSubmission`, previously decoded from a `Request`.
    ///   - originatingRequest: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<Response>` that is fulfilled once the JATS XML file has been transformed into a `gateway-bundle`
    ///            JATS XML compressed document container, including any image attachments from the `submission`.
    /// - Throws: An `Error` if the document transformation failed for some reason.
    private func prepareGatewayBundle(jatsContent: PersistableContent<ArbitrarilyFormattedDocument>,
                                      submission: EEOReceivedSubmission,
                                      originatingRequest: Request) throws -> Future<Response> {

        // Fetch image attachments (if any)
        let imageAttachmentsDict = try EEOReceivedSubmissionService().fetchAttachments(from: submission,
                                                                                       designation: .image,
                                                                                       originatingRequest: originatingRequest)
        let imageFilesFuture = imageAttachmentsDict.values.flatten(on: originatingRequest)

        // Build JATS archive
        return imageFilesFuture.flatMap { imageFiles -> Future<Response> in

            let imagesDirectoryFileURL = jatsContent.workingDirectory.appendingPathComponent("Images")
            try FileManager.default.createDirectory(at: imagesDirectoryFileURL,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)

            for imageFile in imageFiles {
                let filenameNoExtension = String(imageFile.filename.split(separator: ".").first!)
                let fileURL = imagesDirectoryFileURL.appendingPathComponent(filenameNoExtension)
                try imageFile.data.write(to: fileURL)
            }

            let builtJATSArchiveFileURL = jatsContent.workingDirectory.appendingPathComponent("built_jats_archive.zip")
            try MPCompressor.compressDirectory(at: jatsContent.workingDirectory,
                                               to: builtJATSArchiveFileURL,
                                               includeRootDirectory: false)

            // Clean up intermediates
            let articleJATSFileURL = jatsContent.workingDirectory.appendingPathComponent(jatsContent.content.file.filename)
            try FileManager.default.removeItem(at: imagesDirectoryFileURL)
            try FileManager.default.removeItem(at: articleJATSFileURL)

            let builtJATSArchiveFile = File(data: try Data(contentsOf: builtJATSArchiveFileURL),
                                            filename: builtJATSArchiveFileURL.lastPathComponent)
            let builtDocument = ArbitrarilyFormattedDocument(file: builtJATSArchiveFile)
            let builtContent: PersistableContent<ArbitrarilyFormattedDocument> = (builtDocument,
                                                                                  jatsContent.workingDirectory)

            // JATS -> gateway-bundle
            let options = SachsTransformationService.ConfigurationOptions(digitalObjectType: nil,
                                                                          doi: submission.metadata.doi,
                                                                          groupDOI: nil,
                                                                          inputFormat: .extylesArcJATS,
                                                                          issn: submission.metadata.digitalISSN,
                                                                          outputFormat: .gatewayBundle,
                                                                          jatsVersion: nil,
                                                                          processingLevel: .fullText)

            let gatewayBundleResult = try SachsTransformationService().transform(builtContent, options: options)
            let response = try self.chunkedResponse(forFileContentsAt: gatewayBundleResult.fileURL,
                                                    request: originatingRequest)

            return originatingRequest.eventLoop.newSucceededFuture(result: response)
        }
    }

    /// Determines the expected 'Main Document' file format for a given `EEOReceivedSubmission`.
    /// - Parameter submission: A `EEOReceivedSubmission` previously decoded from a `Request`.
    /// - Returns: The relevant `EEOReceivedSubmissionService.DocumentFormat` for the `submission`.
    /// - Throws: An `Error` if an unexpected Depository Code is encountered.
    private func mainDocumentFormat(for submission: EEOReceivedSubmission) throws -> EEOReceivedSubmissionService.DocumentFormat {
        if submission.depositoryCode == "S1" {
            return .docx
        } else if submission.depositoryCode == "authorea" {
            return .xml
        } else {
            throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.unexpectedDepositoryCode,
                                       sourceLocation: .capture())
        }
    }

    /// Determines the expected 'Main Document' designation for a given `EEOReceivedSubmission`.
    /// - Parameter submission: A `EEOReceivedSubmission` previously decoded from a `Request`.
    /// - Returns: The relevant `EEOReceivedSubmissionService.DocumentDesignation` for the `submission`.
    /// - Throws: An `Error` if an unexpected Depository Code is encountered.
    private func mainDocumentDesignation(for submission: EEOReceivedSubmission) throws -> EEOReceivedSubmissionService.DocumentDesignation {
        if submission.depositoryCode == "S1" {
            return .mainDocument
        } else if submission.depositoryCode == "authorea" {
            return .articleJATS
        } else {
            throw PressroomError.error(for: ReceivedSubmissionDebuggingInfo.unexpectedDepositoryCode,
            sourceLocation: .capture())
        }
    }
}
