//
//  DocumentDepositsController.swift
//  App
//
//  Created by Dan Browne on 20/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import MPFoundation
import Vapor

final class DocumentDepositsController: ContentPersistable, ContentTransformable {

    /// Transforms a received Manuscripts Project Bundle file into an `EEOSubmission`
    /// (i.e. a combination of JATS XML and preview PDF representations inside an archive container),
    /// which is then submitted to Literatum via the EEO Deposit Service REST API.
    /// - Parameter request: The `Request` containing the document content and export configuration options
    /// - Returns: A `Response` once the requisite document compilation and export operations
    ///            have either completed, or failed in some way.
    /// - Throws: An `Error` if any of the internal operations failed for some reason.
    func depositManuscript(_ request: Request) throws -> Future<Response> {

        // swiftlint:disable:next line_length
        return try decodeContent(in: request, contentType: ArbitrarilyFormattedDocument.self).flatMap({[unowned self] receivedDocument -> Future<Response> in

            // Only Manuscript Project Bundles are accepted
            if MediaType.type(for: receivedDocument.content.file.ext ?? "_") == .zip {

                // Write the received Manuscripts Project Bundle to disk in temp location
                let fileURL = receivedDocument.workingDirectory.appendingPathComponent(receivedDocument.content.file.filename)
                try receivedDocument.content.file.data.write(to: fileURL, options: .atomic)

                let submission = try self.prepareEEOSubmission(from: receivedDocument, request: request)

                // Only attempt the document deposit in production
                guard try Environment.detect().isRelease else {
                    let response = request.response()
                    response.http.contentType = .json

                    return request.eventLoop.newSucceededFuture(result: response)
                }

                return try self.eeoDeposit(submission, request: request)

            } else {
                // Any other document formats are rejected
                throw PressroomError.error(for: DocumentDepositsDebuggingInfo.invalidDocumentContentType,
                                           sourceLocation: .capture())
            }
        }).catchMap({ error -> Response in
            // If the `Error` is already a `PressroomError`,
            // simply re-throw the error as-is.
            // Otherwise, throw a constructed `PressroomError`.
            if let pressroomError = error as? PressroomError {
                throw pressroomError
            } else {
                throw PressroomError.error(for: DocumentDepositsDebuggingInfo.genericDepositSubmissionFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        })
    }

    // MARK: - Private methods

    // swiftlint:disable:next function_body_length
    private func prepareEEOSubmission(from receivedDocument: PersistableContent<ArbitrarilyFormattedDocument>,
                                      request: Request) throws -> EEODepositService.EEOSubmission {
        let sachsTransformationService = SachsTransformationService()
        guard let jatsSubmissionDOI = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsSubmissionDOI) else {
            throw PressroomError.error(for: DocumentDepositsDebuggingInfo.missingJATSSubmissionDOI,
                                       sourceLocation: .capture())
        }
        guard let idSuffix = jatsSubmissionDOI.split(separator: "/").last else {
            throw PressroomError.error(for: DocumentDepositsDebuggingInfo.missingJATSSubmissionDOI,
                                       sourceLocation: .capture())
        }
        let jatsSubmissionID = String(idSuffix)
        guard let eeoID = request.http.headers.firstValue(name: Request.pressroomHeaders.depositSubmissionIdentifier) else {
            throw PressroomError.error(for: DocumentDepositsDebuggingInfo.missingDepositSubmissionIdentifier,
                                       sourceLocation: .capture())
        }
        guard let eeoJournalName = request.http.headers.firstValue(name: Request.pressroomHeaders.depositSubmissionJournalName) else {
            throw PressroomError.error(for: DocumentDepositsDebuggingInfo.missingDepositSubmissionJournalName,
                                       sourceLocation: .capture())
        }
        guard let callbackURL = request.http.headers.firstValue(name: Request.pressroomHeaders.depositNotificationCallbackURL) else {
            throw PressroomError.error(for: DocumentDepositsDebuggingInfo.missingDepositNotificationCallbackURL,
                                       sourceLocation: .capture())
        }

        let jatsOptions = SachsTransformationService.ConfigurationOptions(digitalObjectType: nil,
                                                                          doi: jatsSubmissionDOI,
                                                                          groupDOI: nil,
                                                                          inputFormat: .manuscriptsProjectBundle,
                                                                          issn: nil,
                                                                          outputFormat: .literatumJATS,
                                                                          jatsVersion: nil,
                                                                          processingLevel: .frontMatterOnly)
        let jatsArchiveResult = try sachsTransformationService.transform(receivedDocument, options: jatsOptions)
        let decompressedJATSFileURL = receivedDocument.workingDirectory.appendingPathComponent("\(jatsSubmissionID).xml")
        try MPDecompressor.decompressZIP(at: jatsArchiveResult.fileURL, to: receivedDocument.workingDirectory)
        let frontMatterJATSFile = File(data: try Data(contentsOf: decompressedJATSFileURL), filename: "\(jatsSubmissionID).xml")

        let pdfOptions = SachsTransformationService.ConfigurationOptions(digitalObjectType: nil,
                                                                         doi: nil,
                                                                         groupDOI: nil,
                                                                         inputFormat: .manuscriptsProjectBundle,
                                                                         issn: nil,
                                                                         outputFormat: .pdf,
                                                                         jatsVersion: nil,
                                                                         processingLevel: .fullText)
        let pdfResult = try sachsTransformationService.transform(receivedDocument, options: pdfOptions)
        let reviewPDFFile = File(data: try Data(contentsOf: pdfResult.fileURL), filename: "\(jatsSubmissionID).pdf")

        _ = cleanTemporaryContent(at: receivedDocument.workingDirectory)

        return EEODepositService.EEOSubmission(frontMatterJATS: frontMatterJATSFile,
                                               reviewPDF: reviewPDFFile,
                                               identifier: eeoID,
                                               journalName: eeoJournalName,
                                               notificationCallbackURL: callbackURL)
    }

    private func eeoDeposit(_ submission: EEODepositService.EEOSubmission, request: Request) throws -> Future<Response> {
        let service = EEODepositService()

        return try service.authenticate(originatingRequest: request).flatMap({ authAPIResponse -> Future<Response> in
            return try service.deposit(submission: submission,
                                       accessToken: authAPIResponse.accessToken!,
                                       originatingRequest: request)
        })
    }
}
