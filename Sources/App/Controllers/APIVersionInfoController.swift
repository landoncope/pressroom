//
//  APIVersionInfoController.swift
//  App
//
//  Created by Dan Browne on 08/08/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import Foundation

final class APIVersionInfoController {

    /// Debugging information to display to the Client via a `Response`.
    struct APIDebuggingInfo: Content {
        let versionNumber: String
        let buildNumber: String
    }

    /// Debugging endpoint for displaying PressroomServer version information.
    ///
    /// - Parameter req: The `Request` received by the server.
    /// - Returns: A `APIDebuggingInfo` containing debugging information from the main `Bundle`.
    /// - Throws: An `Abort` error if the `Request` does not contain a valid PressroomServer API key
    ///           or JWT (see `AuthMiddleware`).
    func versionInfo(_ req: Request) throws -> APIDebuggingInfo {

        // Return the version and build numbers in a `APIDebuggingInfo` object
        // (other info might also be returned at a future date)
        guard let bundleShortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
            let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else {
                return APIDebuggingInfo(versionNumber: "Unknown", buildNumber: "Unknown")
        }

        return APIDebuggingInfo(versionNumber: bundleShortVersion, buildNumber: bundleVersion)
    }
}
