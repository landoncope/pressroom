//
//  SachsTransformationsController.swift
//  App
//
//  Created by Dan Browne on 15/02/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

// swiftlint:disable file_length function_body_length cyclomatic_complexity

import Foundation
import Vapor

final class SachsTransformationsController: ContentPersistable, ContentTransformable {

    // MARK: - Private variables

    private let retryQueryThrottler = Throttler(maxInterval: 2.0)

    // MARK: - Public methods

    /// Compiles a JATS-flavored document container from received a MS Word document or Manuscripts Project Bundle
    /// contained in a `Request`, and then streams it back to the Client via a chunked `Response`.
    ///
    /// - Parameter request: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<Response>` that is fulfilled once the received MS Word document or
    ///            Manuscripts Project Bundle has been transformed into a JATS XML response.
    /// - Throws: An `Error` if the transformation fails for some reason.
    func compileManuscript(_ request: Request) throws -> Future<Response> {

        // swiftlint:disable:next line_length
        return try decodeContent(in: request, contentType: ArbitrarilyFormattedDocument.self).flatMap({[unowned self] receivedDocument -> Future<Response> in
            return try self.compileManuscript(from: receivedDocument, request: request)
        })
    }

    /// Compiles a JATS-flavored document container from received a MS Word document or Manuscripts Project Bundle,
    /// and then streams it back to the Client via a chunked `Response`.
    ///
    /// - Parameters:
    ///   - receivedDocument: The received document content, as decoded from `request`.
    ///   - request: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<Response>` that is fulfilled once the received MS Word document or
    ///            Manuscripts Project Bundle has been transformed into a JATS XML response.
    /// - Throws: An `Error` if the transformation fails for some reason.
    func compileManuscript(from receivedDocument: PersistableContent<ArbitrarilyFormattedDocument>,
                           request: Request) throws -> Future<Response> {
        // Check for missing 'Pressroom-JATS-Document-Processing-Level' header
        guard let levelHeader = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsDocumentProcessingLevel) else {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingDocumentProcessingLevel,
                                       sourceLocation: .capture())
        }

        // Check for invalid 'Pressroom-JATS-Document-Processing-Level' header
        guard let processingLevel = DocumentProcessingLevel(rawValue: levelHeader) else {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.invalidDocumentProcessingLevel,
                                       sourceLocation: .capture())
        }

        if MediaType.type(for: receivedDocument.content.file.ext ?? "_") == .zip {
            // Project bundle as source document
            return try self.compileJATSFromProjectBundle(request, decodedContent: receivedDocument, processingLevel: processingLevel)
        } else if MediaType.type(for: receivedDocument.content.file.ext ?? "_") == MediaType.type(for: "docx") {
            // MS Word .docx as source document
            return try self.compileJATSFromMSWord(request, decodedFile: receivedDocument.content.file, processingLevel: processingLevel)
        } else {
            // Any other document formats are rejected
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.invalidDocumentContentType,
                                       sourceLocation: .capture())
        }
    }

    // MARK: - Private methods

    /// Transforms a MS Word .docx file that has been decoded from a received `Request` into
    /// a JATS XML response inside a compressed container.
    ///
    /// - Parameters:
    ///   - request: The originating `Request` that was received by Pressroom.
    ///   - decodedFile: The received MS Word .docx file, that has been decoded from the originating request.
    ///   - processingLevel: The document processing level controlling the content of the generated JATS.
    /// - Returns: A `Future<Response>` that is fulfilled once the transformation service
    ///            has transformed the MS Word .docx file into a JATS XML response.
    /// - Throws: An `Error` if the transformation fails for some reason.
    private func compileJATSFromMSWord(_ request: Request,
                                       decodedFile: File,
                                       processingLevel: DocumentProcessingLevel) throws -> Future<Response> {

        guard let secret = request.http.headers.firstValue(name: Request.pressroomHeaders.eXtylesArcSecret) else {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingExtylesArcSecret,
                                       sourceLocation: .capture())
        }
        var eXtylesArcSecret: ServiceSecret!
        do {
            eXtylesArcSecret = try ServiceSecret(encoded: secret, secretType: .threeParts)
        } catch {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.genericCompileFailure,
                                       sourceLocation: .capture(),
                                       underlyingError: error)
        }

        // swiftlint:disable:next line_length
        let styleHeader = request.http.headers.firstValue(name: Request.pressroomHeaders.eXtylesArcEditorialStyle) ?? ExtylesArcTransformationService.EditorialStyle.noFormat.rawValue
        guard let style = ExtylesArcTransformationService.EditorialStyle(rawValue: styleHeader) else {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.invalidExtylesArcEditorialStyle,
                                       sourceLocation: .capture())
        }

        let extylesArcService = ExtylesArcTransformationService(secret: eXtylesArcSecret)
        var sessionToken = ""

        return try extylesArcService.logIn(originatingRequest: request).map({ authResponse -> String in
            return authResponse.token
        }).flatMap({ token -> Future<ExtylesArcTransformationService.CreateJobResponse> in
            // Persist copy of token for downstream usage
            sessionToken = token

            return try extylesArcService.createJob(documentProcessingLevel: processingLevel,
                                                   editorialStyle: style,
                                                   file: decodedFile,
                                                   token: token,
                                                   originatingRequest: request)
        }).flatMap({[unowned self] createJobResponse -> Future<Response> in
            let queryJobFuture = try self.queryMSWordToJATSJob(request,
                                                               service: extylesArcService,
                                                               token: sessionToken,
                                                               jobId: ExtylesArcTransformationService.JobId(jobId: createJobResponse.jobId))

            // Invalidate current session via `logOut(…)`, regardless of the result of `queryMSWordToJATSJob(…)`
            queryJobFuture.whenComplete {
                _ = try? extylesArcService.logOut(token: sessionToken, originatingRequest: request)
            }

            return queryJobFuture
        }).catchMap({ error -> Response in
            // If the `Error` is already a `PressroomError`,
            // simply re-throw the error as-is.
            // Otherwise, throw a constructed `PressroomError`.
            if let pressroomError = error as? PressroomError {
                throw pressroomError
            } else {
                throw PressroomError.error(for: SachsDocumentDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        })
    }

    /// Retreives the JATS XML representation of a source MS Word .docx file, as attached to a `Response`.
    ///
    /// - Parameters:
    ///   - request: The originating `Request` that was received by Pressroom.
    ///   - service: An instance of `ExtylesArcTransformationService`.
    ///   - token: A current, valid session token for the eXtyles Arc web service API.
    ///   - jobId: A `JobId` representing the ID of transformation job that was previously submitted
    ///            to eXtyles Arc for processing.
    /// - Returns: A `Future<Response>` that is fulfilled once eXtyles has completed processing the source MS Word .docx file.
    ///            (This may take some time depending on the source document).
    /// - Throws: An `Error` if the transformation job failed for some reason.
    private func queryMSWordToJATSJob(_ request: Request,
                                      service: ExtylesArcTransformationService,
                                      token: String,
                                      jobId: ExtylesArcTransformationService.JobId) throws -> Future<Response> {
        // Job status check for long-running MS Word -> JATS jobs using eXtyles Arc
        return try service.checkJob(jobId, token: token, originatingRequest: request)
            .flatMap({[unowned self] apiResponse -> Future<Response> in
                // If the job has finished, return the JATS XML zipped response
                if apiResponse.status == .finished {
                    return try service.getJob(jobId, token: token, originatingRequest: request)
                } else if apiResponse.status == .error {
                    // If there was an error in compiling the JATS XML, return the error
                    let response = request.response()
                    response.http.contentType = .json
                    try response.content.encode(apiResponse)

                    return request.eventLoop.newSucceededFuture(result: response)
                } else {
                    // Use a `Promise` since `Throttler` cannot return the `Future<Response>` directly
                    let retryQueryPromise = request.eventLoop.newPromise(Response.self)

                    // Otherwise, the job is still in progress - query again (throttled)
                    self.retryQueryThrottler.throttle {
                        do {
                            let responseFuture = try self.queryMSWordToJATSJob(request,
                                                                               service: service,
                                                                               token: token,
                                                                               jobId: jobId)
                            responseFuture.whenSuccess({ response in
                                retryQueryPromise.succeed(result: response)
                            })
                        } catch {
                            retryQueryPromise.fail(error: error)
                        }
                    }

                    return retryQueryPromise.futureResult
                }
            })
    }

    /// Transforms a Manuscripts Project Bundle that has been decoded from a received `Request` into
    /// a JATS XML response inside a compressed container.
    ///
    /// - Parameters:
    ///   - request: The originating `Request` that was received by Pressroom.
    ///   - decodedContent: The received document, that has been decoded from the originating request.
    ///   - processingLevel: The document processing level controlling the content of the generated JATS.
    /// - Returns: A `Future<Response>` that is fulfilled once the transformation service
    ///            has transformed the Project Bundle into a JATS XML response.
    /// - Throws: An `Error` if the transformation fails for some reason.
    private func compileJATSFromProjectBundle(_ request: Request,
                                              decodedContent: PersistableContent<ArbitrarilyFormattedDocument>,
                                              processingLevel: DocumentProcessingLevel) throws -> Future<Response> {

        let transformationService = SachsTransformationService()

        return try sachsResult(for: decodedContent,
                              using: transformationService,
                              processingLevel: processingLevel,
                              request: request).thenThrowing({ sachsResult -> Response in
            // Init the response
            let response = try self.chunkedResponse(forFileContentsAt: sachsResult.fileURL, request: request)

            // Add any errors/warnings (that didn't otherwise cause a transformation failure)
            // to the `Response` being returned to the Client
            if let jatsError = String(data: sachsResult.standardError, encoding: .utf8), jatsError.count > 0 {
                let headerSafeError = jatsError.components(separatedBy: .newlines).joined(separator: " /// ")
                response.http.headers.add(name: Response.pressroomHeaders.compiledWithErrors,
                                          value: headerSafeError)
            }

            // If a Digitial Object file has been generated, upload to Literatum
            try self.depositResultToLiteratumIfRelevant(sachsResult, request: request)

            // Clean-up the temporary working directory before returning
            transformationService.cleanWorkingDirectory(at: sachsResult.fileURL.deletingLastPathComponent())

            return response
        }).catchMap({ error -> Response in
            // Throw the `Abort` corresponding with the `PressroomError` already thrown earlier -
            // this is exposed in the `Response`
            try self.handleAbort(for: error)

            return request.response()
        })
    }

    /// Returns a `SachsResult` for a specified Manuscripts Project Bundle previously decoded from a `Request`.
    ///
    /// - Parameters:
    ///   - receivedDocument: The received document, that has been decoded from the originating request.
    ///   - transformationService: An instance of `SachsTransformationService` to use to complete the transformation.
    ///   - processingLevel: The document processing level controlling the content of the generated document.
    ///   - request: The originating `Request` that was received by Pressroom.
    /// - Returns: A `Future<SachsResult>` that is fulfilled once the transformation service
    ///            has transformed the Project Bundle into a JATS XML response.
    /// - Throws: An `Error` if the transformation fails for some reason.
    private func sachsResult(for receivedDocument: PersistableContent<ArbitrarilyFormattedDocument>,
                             using transformationService: SachsTransformationService,
                             processingLevel: DocumentProcessingLevel,
                             request: Request) throws -> Future<SachsResult> {

        let options = try validateHeaders(for: request, processingLevel: processingLevel)

        // Write the received Manuscripts Project Bundle to disk in temp location
        let receivedDocumentFileURL = receivedDocument.workingDirectory.appendingPathComponent(receivedDocument.content.file.filename)
        try receivedDocument.content.file.data.write(to: receivedDocumentFileURL, options: .atomic)

        // Transform the received Manuscripts Project Bundle into JATS or HTML
        do {
            let jatsResult = try transformationService.transform(receivedDocument, options: options)
            return request.eventLoop.newSucceededFuture(result: jatsResult)
        } catch {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.genericCompileFailure,
                                       sourceLocation: .capture(),
                                       underlyingError: error)
        }
    }

    /// Validates the headers attached to a received `Request` to ensure that
    /// all required headers are provided for a given output format.
    /// - Parameters:
    ///   - request: The originating `Request` that was received by Pressroom.
    ///   - processingLevel: The document processing level controlling the content of the generated document.
    /// - Returns: A `SachsTransformationService.ConfigurationOptions` instance to provide to `SachsTransformationService`.
    /// - Throws: An `Error` if the validation fails for some reason.
    private func validateHeaders(for request: Request,
                                 processingLevel: DocumentProcessingLevel) throws -> SachsTransformationService.ConfigurationOptions {

        // If the JATS input format header isn't provided, default to Manuscripts Project Bundle input
        let jatsInputFormat = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsInputFormat) ?? "manuproj"

        // If the target JATS output format header isn't provided, default to JATS XML output
        let targetJATSOutputFormat = request.http.headers.firstValue(name: Request.pressroomHeaders.targetJATSOutputFormat) ?? "jats"

        // Check for invalid JATS input format header before initializing the Sachs transform service
        guard let inputFormat = SachsTransformationService.InputFormat(rawValue: jatsInputFormat) else {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.invalidJATSInputFormat,
                                       sourceLocation: .capture())
        }

        // Check for invalid target JATS output format header before initializing the Sachs transform service
        guard let outputFormat = SachsTransformationService.OutputFormat(rawValue: targetJATSOutputFormat) else {
            throw PressroomError.error(for: SachsDocumentDebuggingInfo.invalidTargetJATSOutputFormat,
                                       sourceLocation: .capture())
        }

        // Optional JATS version, submission ID or submission DOI headers which may be provided
        let targetJATSVersion = request.http.headers.firstValue(name: Request.pressroomHeaders.targetJATSVersion)
        let digitalObjectType = request.http.headers.firstValue(name: Request.pressroomHeaders.digitalObjectType)
        let groupDOI = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsGroupDOI)
        let issn = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsISSN)
        let seriesCode = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsSeriesCode)
        let submissionDOI = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsSubmissionDOI)

        // Submission ID and DOI are _required_ when using a 'literatum-jats', 'literatum-do'
        // OR 'wileyml-bundle' output formats
        if outputFormat == .literatumJATS
            || outputFormat == .literatumDO
            || outputFormat == .jatsBundle
            || outputFormat == .wileyMLBundle
            || outputFormat == .gatewayBundle {
            guard submissionDOI != nil, submissionDOI?.split(separator: "/").last != nil else {
                throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingSubmissionDOI,
                                           sourceLocation: .capture())
            }

            // Additionally, Digital Object type is _required_ when using a 'literatum-do' output format
            if outputFormat == .literatumDO && digitalObjectType == nil {
                throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingDigitalObjectType,
                                           sourceLocation: .capture())
            }

            // Additionally, 'series code' and 'group identifier' are _required_ when using a
            // 'wileyml-bundle' output format
            if outputFormat == .wileyMLBundle {
                if seriesCode == nil {
                    throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingSeriesCode,
                                               sourceLocation: .capture())
                } else if groupDOI == nil {
                    throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingGroupDOI,
                                               sourceLocation: .capture())
                }
            }

            // Additionally, An 'extyles' input format and  'issn' are _required_ when using a
            // 'gateway-bundle' output format
            if outputFormat == .gatewayBundle {
                guard inputFormat == .extylesArcJATS else {
                    throw PressroomError.error(for: SachsDocumentDebuggingInfo.invalidJATSInputFormat,
                                               sourceLocation: .capture())
                }
                guard issn != nil else {
                    throw PressroomError.error(for: SachsDocumentDebuggingInfo.missingISSN,
                                               sourceLocation: .capture())
                }
            }
        }

        return SachsTransformationService.ConfigurationOptions(digitalObjectType: digitalObjectType,
                                                               doi: submissionDOI,
                                                               groupDOI: groupDOI,
                                                               inputFormat: inputFormat,
                                                               issn: issn,
                                                               outputFormat: outputFormat,
                                                               jatsVersion: targetJATSVersion,
                                                               processingLevel: processingLevel)
    }
}

// MARK: Literatum Digital Object file uploading

fileprivate extension SachsTransformationsController {

    /// Deposit a generated document to a Literatum SFTP server for further processing,
    /// if the `Pressroom-Target-JATS-Output-Format` header is set to `literatum-do` or `wileyml-bundle`.
    /// NOTE: No file uploads will occur whilst running tests or in debug mode (i.e. this method call will do nothing).
    /// - Parameters:
    ///   - sachsResult: The document that was generated by `sachs`.
    ///   - request: The originating request from a Client.
    func depositResultToLiteratumIfRelevant(_ sachsResult: SachsResult, request: Request) throws {

        if let targetJATSOutputFormat = request.http.headers.firstValue(name: Request.pressroomHeaders.targetJATSOutputFormat),
            let outputFormat = SachsTransformationService.OutputFormat(rawValue: targetJATSOutputFormat),
            try Environment.detect().isRelease {

            if outputFormat == .literatumDO {
                let submissionDOI = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsSubmissionDOI)!
                let sftpService = LiteratumSFTPService(submissionDOI: submissionDOI)
                try sftpService.uploadFile(at: sachsResult.fileURL)
            } else if outputFormat == .wileyMLBundle {
                let seriesCode = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsSeriesCode)!
                let groupDOI = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsGroupDOI)!
                let ftpsService = LiteratumFTPSService(seriesCode: seriesCode, groupDOI: groupDOI)
                try ftpsService.uploadFile(at: sachsResult.fileURL)
            }
        }
    }
}
