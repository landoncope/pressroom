//
//  TeXTransformationsController.swift
//  App
//
//  Created by Dan Browne on 15/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import MPFoundation
import Vapor

final class TeXTransformationsController: ContentPersistable, ContentIteratable, ContentTransformable {

    // MARK: - Private variables

    /// The default filename for the transformed document container inside the temporary working directory.
    private static let transformedContainerFilename = "compressed_container.zip"

    /// The valid file extensions that are accepted as TeX files.
    private static let teXFileExtensions = ["tex"]

    /// The valid file extensions that are accepted as TeX-related auxiliary files.
    private static let teXRelatedAuxFileExtensions = ["bib", "cls"]

    /// The invalid file extensions, that are not accepted as unrelated auxiliary files.
    private static let invalidAuxFileExtensions = ["md", "docx", "html", "manuproj", "xml"]

    // MARK: - Public methods

    // swiftlint:disable:next function_body_length
    func compileManuscript(_ req: Request) throws -> Future<Response> {

        var transformationService: LaTeXMLTransformationService!

        return try decodeContent(in: req, contentType: ArbitrarilyFormattedDocument.self).thenThrowing { receivedDocument -> URL in
            var transformedFileURL: URL!

            // Check for missing and/or invalid target LaTeXML output format header
            guard let targetFileExtension = req.http.headers.firstValue(name: Request.pressroomHeaders.targetLaTeXMLOutputFormat) else {
                throw PressroomError.error(for: TeXDocumentDebuggingInfo.missingTargetLaTeXMLOutputFormat,
                                           sourceLocation: .capture())
            }
            guard let outputFormat = LaTeXMLTransformationService.OutputFormat(rawValue: targetFileExtension) else {
                throw PressroomError.error(for: TeXDocumentDebuggingInfo.invalidTargetLaTeXMLOutputFormat,
                                           sourceLocation: .capture())
            }
            transformationService = LaTeXMLTransformationService(outputFormat: outputFormat)

            if receivedDocument.content.file.contentType == .zip {
                // Decompress zip container
                let decompressedTeXContainerFileURL = try self.decompressContent(receivedDocument)

                // Firstly, check for an empty container
                let containerContents = self.decompressedContainerContents(for: decompressedTeXContainerFileURL)
                if containerContents.count == 0 {
                    throw PressroomError.error(for: TeXDocumentDebuggingInfo.emptyDocumentContainerAttached,
                                               sourceLocation: .capture())
                }

                // Determine the TeX and auxiliary files inside the decompressed container
                let teXFileURLs = containerContents.filter { fileURL in
                    return type(of: self).teXFileExtensions.contains(fileURL.pathExtension)
                }
                let auxiliaryFileURLs = containerContents.filter { fileURL in
                    return !(type(of: self).teXFileExtensions.contains(fileURL.pathExtension))
                }

                // Check that there is at least one TeX file in the container
                if teXFileURLs.count == 0 {
                    throw PressroomError.error(for: TeXDocumentDebuggingInfo.invalidContainerContents,
                                               sourceLocation: .capture())
                }

                // Check that if auxiliary files are present, they are all of valid file formats
                for fileURL in auxiliaryFileURLs {
                    // Throw `PressroomError` if a file is in the container that isn't a valid auxiliary file format
                    if type(of: self).invalidAuxFileExtensions.contains(fileURL.pathExtension) {
                        throw PressroomError.error(for: TeXDocumentDebuggingInfo.invalidContainerContents,
                                                   sourceLocation: .capture())
                    }
                }

                // Transform each TeX file in turn
                for fileURL in teXFileURLs {
                    let documentContent = ArbitrarilyFormattedDocument(file: File(data: try Data(contentsOf: fileURL),
                                                                                  filename: fileURL.lastPathComponent))
                    let teXDocument = PersistableContent(content: documentContent,
                                                         workingDirectory: decompressedTeXContainerFileURL)

                    // Transform a received TeX document to a given HTML-like output format.
                    do {
                        transformedFileURL = try transformationService.transform(teXDocument)
                    } catch {
                        throw PressroomError.error(for: TeXDocumentDebuggingInfo.genericCompileFailure,
                                                   sourceLocation: .capture(),
                                                   underlyingError: error)
                    }
                }

                // Move non TeX-related auxiliary files into the directory that will be compressed
                let transformedContainerFileURL = transformedFileURL.deletingLastPathComponent()
                for fileURL in auxiliaryFileURLs
                    where !(type(of: self).teXRelatedAuxFileExtensions.contains(fileURL.pathExtension)) {
                    // swiftlint:disable:next line_length
                    try FileManager.default.moveItem(at: fileURL, to: transformedContainerFileURL.appendingPathComponent(fileURL.lastPathComponent))
                }

                // Compress transformed files and return transformed container file URL
                // swiftlint:disable:next line_length
                let compressedContainerFileURL = receivedDocument.workingDirectory.appendingPathComponent(TeXTransformationsController.transformedContainerFilename)
                try MPCompressor.compressDirectory(at: transformedContainerFileURL,
                                                   to: compressedContainerFileURL,
                                                   includeRootDirectory: false)

                return compressedContainerFileURL

            } else {
                // Only compressed containers are accepted
                throw PressroomError.error(for: TeXDocumentDebuggingInfo.invalidDocumentContentType,
                                           sourceLocation: .capture())
            }
            }.thenThrowing { compiledDocumentFileURL -> Response in
                // Init the response
                let response = try self.chunkedResponse(forFileContentsAt: compiledDocumentFileURL, request: req)

                // Clean-up the temporary working directory before returning
                transformationService.cleanWorkingDirectory(at: compiledDocumentFileURL.deletingLastPathComponent())

                return response
            }.catchMap { error -> Response in
                // Throw the `Abort` corresponding with the `PressroomError` already thrown earlier -
                // this is exposed in the `Response`
                try self.handleAbort(for: error)

                return req.response()
        }
    }
}
