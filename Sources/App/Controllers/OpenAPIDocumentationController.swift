//
//  OpenAPIDocumentationController.swift
//  App
//
//  Created by Dan Browne on 16/08/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import Swiftgger
import Foundation

final class OpenAPIDocumentationController {

    /// PressroomServer OpenAPI 3.x documentation endpoint.
    ///
    /// - Parameter req: The `Request` received by the server.
    /// - Returns: JSON documentation of the PressroomServer API in OpenAPI 3.x format,
    ///            or an HTML representation depending on `Request` query parameters.
    /// - Throws: An `Abort` error if the `Request` does not contain a valid PressroomServer API key
    ///           or JWT (see `AuthMiddleware`).
    func serverDocumentation(_ req: Request) throws -> Future<Response> {

        // Initialise the `OpenAPIBuilder` with "root" API information
        let bundleShortVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "Unknown"
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "Unknown"
        let openAPIBuilder = OpenAPIBuilder(title: "PressroomServer API",
                                            version: "\(bundleShortVersion) (\(bundleVersion))",
                                            // swiftlint:disable:next line_length
                                            description: "This is the API documentation for Pressroom - the document transformation microservice used in manuscripts.io and Manuscripts for Mac.",
                                            termsOfService: nil,
                                            contact: nil,
                                            license: nil,
                                            authorizations: nil)

        addAPIControllers(to: openAPIBuilder)
        addAPIObjects(to: openAPIBuilder)

        // Compile the server documentation into an `OpenAPIDocument`
        let openAPIDocument = openAPIBuilder.built()

        // Return the documentation to the Client depending on the `Request` query parameters
        let response = req.response()
        if (try? req.query.get(Bool.self, at: "html")) != nil {
            try response.content.encode(openAPIDocument.toHTMLString(), as: .html)
        } else {
            try response.content.encode(openAPIDocument, as: .json)
        }
        return req.eventLoop.newSucceededFuture(result: response)
    }

    // MARK: - Private methods

    /// Add the `APIController` describing each route to an `OpenAPIBuilder`.
    ///
    /// - Parameter openAPIBuilder: The `OpenAPIBuilder` to add the `APIController`s to.
    private func addAPIControllers(to openAPIBuilder: OpenAPIBuilder) {
        guard let routes = PressroomServerConfig.shared.router?.routes else {
            return
        }
        for route in routes {
            _ = openAPIBuilder.add(route.apiController())
        }
    }

    /// Add the collated `APIObject` arrays specified for each route to the `OpenAPIBuilder`.
    ///
    /// - Parameter openAPIBuilder: The `OpenAPIBuilder` to add the `APIObject`s to.
    private func addAPIObjects(to openAPIBuilder: OpenAPIBuilder) {
        guard let routes = PressroomServerConfig.shared.router?.routes else {
            return
        }
        let apiObjects = routes.flatMap { route in
            route.apiObjects()
        }

        _ = openAPIBuilder.add(apiObjects)
    }
}

// MARK: - `OpenAPIDocument` `ResponseEncodable` extension

extension OpenAPIDocument: ResponseEncodable {

    public func encode(for req: Request) throws -> Future<Response> {
        let res = req.response()
        try res.content.encode(json: self, using: JSONEncoder())
        return req.eventLoop.newSucceededFuture(result: res)
    }
}
