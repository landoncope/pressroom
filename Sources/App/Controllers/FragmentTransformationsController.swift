//
//  FragmentTransformationsController.swift
//  App
//
//  Created by Dan Browne on 05/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

/// Controls document fragment transformation operations for PressroomServer
final class FragmentTransformationsController: ContentTransformable {

    // MARK: - Public methods

    /// Compiles a document fragment from received data contained in a `Request`,
    /// and then returns it back to the Client via a `Response`.
    ///
    /// The compiled document fragment format is determined based on `Request` headers information.
    /// - Parameter request: The `Request` containing the document fragment, and input and output format information.
    /// - Returns: A `Response` once the document fragment transformation operation has either completed,
    ///            or failed in some way.
    /// - Throws: An `Error` if any of the internal operations failed for some reason.
    func compileFragment(_ request: Request) throws -> Future<Response> {

        // Check input format header presence and validity
        guard let inputFormatHeader = request.http.headers.firstValue(name: Request.pressroomHeaders.documentFragmentInputFormat) else {
            throw PressroomError.error(for: FragmentDebuggingInfo.missingInputFormatHeader, sourceLocation: .capture())
        }

        guard let inputFormat = FragmentTransformationService.InputFormat(rawValue: inputFormatHeader) else {
            throw PressroomError.error(for: FragmentDebuggingInfo.invalidInputFormat, sourceLocation: .capture())
        }

        // Check output format header presence and validity
        guard let outputFormatHeader = request.http.headers.firstValue(name: Request.pressroomHeaders.documentFragmentOutputFormat) else {
            throw PressroomError.error(for: FragmentDebuggingInfo.missingOutputFormatHeader, sourceLocation: .capture())
        }

        guard let outputFormat = FragmentTransformationService.OutputFormat(rawValue: outputFormatHeader) else {
            throw PressroomError.error(for: FragmentDebuggingInfo.invalidOutputFormat, sourceLocation: .capture())
        }

        // Attempt to transform the document fragment attached to `request` based on provided format settings
        return request.http.body.consumeData(on: request).thenThrowing({ decodedFragmentData -> String in
            do {
                return try FragmentTransformationService().transform(decodedFragmentData,
                                                                     from: inputFormat,
                                                                     to: outputFormat)
            } catch {
                throw PressroomError.error(for: FragmentDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        }).thenThrowing({ compiledFragmentStringRepresentation -> Response in
            let response = request.response()
            try response.content.encode(compiledFragmentStringRepresentation)

            return response
        }).catchMap({ error -> Response in
            try self.handleAbort(for: error)

            return request.response()
        })
    }
}
