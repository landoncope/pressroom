//
//  ReceivedSubmissionsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 14/04/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension ReceivedSubmissionsController {

    /// The debugging information for a given document submission reception error
    enum ReceivedSubmissionDebuggingInfo: String, DebuggingInfo {
        case unexpectedDepositoryCode
        case missingJATSMainDocumentAttachment
        case multipleJATSMainDocumentAttachments
        case missingDOCXMainDocumentAttachment
        case multipleDOCXMainDocumentAttachments
        case genericCompileFailure

        var status: HTTPResponseStatus {
            switch self {
            case .missingJATSMainDocumentAttachment,
                 .multipleJATSMainDocumentAttachments,
                 .missingDOCXMainDocumentAttachment,
                 .multipleDOCXMainDocumentAttachments,
                 .unexpectedDepositoryCode:
                return .badRequest
            case .genericCompileFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return ReceivedSubmissionsController.self
        }

        var reason: String {
            switch self {
            case .missingJATSMainDocumentAttachment:
                return """
                The submission reception failed because no document attachment with a 'designation' of \
                'Article JATS' and a 'format' of 'xml' was found in the Request.
                """
            case .multipleJATSMainDocumentAttachments:
                return """
                The submission reception failed because multiple document attachments with a 'designation' of \
                'Article JATS' and a 'format' of 'xml' were found in the Request.
                """
            case .missingDOCXMainDocumentAttachment:
                return """
                The submission reception failed because no document attachment with a 'designation' of \
                'Main Document' and a 'format' of 'docx' was found in the Request.
                """
            case .multipleDOCXMainDocumentAttachments:
                return """
                The submission reception failed because multiple document attachments with a 'designation' of \
                'Main Document' and a 'format' of 'docx' were found in the Request.
                """
            case .unexpectedDepositoryCode:
                return """
                The submission reception failed because an unexpected Depository Code was included in the request.
                """
            case .genericCompileFailure:
                return """
                The document couldn't be compiled because an error occured!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .missingJATSMainDocumentAttachment:
                return ["""
                    A document attachment with a 'designation' of 'Article JATS' and a 'format' of 'xml' \
                    was missing from the Request for some reason.
                    """]
            case .multipleJATSMainDocumentAttachments:
                return ["""
                    Multiple document attachments with a 'designation' of 'Article JATS' and a 'format' of 'xml' \
                    were found in the Request for some reason.
                    """]
            case .missingDOCXMainDocumentAttachment:
                return ["""
                    A document attachment with a 'designation' of 'Main Document' and a 'format' of 'docx' \
                    was missing from the Request for some reason.
                    """]
            case .multipleDOCXMainDocumentAttachments:
                return ["""
                    Multiple document attachments with a 'designation' of 'Main Document' and a 'format' of 'docx' \
                    were found in the Request for some reason.
                    """]
            case .unexpectedDepositoryCode:
                return ["""
                    An unexpected Depository Code was included in the Request for some reason.
                    """]
            case .genericCompileFailure:
                return ["""
                    An internal step of the document transformation failed for some reason.
                    """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .missingJATSMainDocumentAttachment,
                 .multipleJATSMainDocumentAttachments:
                return ["""
                    The Request should contain one document attachment with a 'designation' of 'Main Document' and a 'format' of 'xml' \
                    (i.e JATS XML).
                    """]
            case .missingDOCXMainDocumentAttachment,
                 .multipleDOCXMainDocumentAttachments:
                return ["""
                    The Request should contain one document attachment with a 'designation' of 'Main Document' and a 'format' of 'docx'.
                    """]
            case .unexpectedDepositoryCode:
                return ["""
                    The Request should contain a Depository Code with a value of either 'authorea' or 'S1'.
                    """]
            case .genericCompileFailure:
                return ["""
                    Unless a fix is obvious from the stack trace (or other debugging info), \
                    please report this error to the Manuscripts team.
                    """]
            }
        }
    }
}
