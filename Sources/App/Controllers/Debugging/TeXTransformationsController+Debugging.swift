//
//  TeXTransformationsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension TeXTransformationsController {

    /// The debugging information for a given document transformation error
    enum TeXDocumentDebuggingInfo: String, DebuggingInfo {
        case invalidDocumentContentType
        case emptyDocumentContainerAttached
        case invalidContainerContents
        case missingTargetLaTeXMLOutputFormat
        case invalidTargetLaTeXMLOutputFormat
        case genericCompileFailure

        var status: HTTPResponseStatus {
            switch self {
            case .invalidDocumentContentType,
                 .emptyDocumentContainerAttached,
                 .invalidContainerContents,
                 .missingTargetLaTeXMLOutputFormat,
                 .invalidTargetLaTeXMLOutputFormat:
                return .badRequest
            case .genericCompileFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return TeXTransformationsController.self
        }

        var reason: String {
            switch self {
            case .invalidDocumentContentType:
                return """
                The document couldn't be compiled because a document container with an invalid \
                '\(HTTPHeaderName.contentType.stringValue)' was attached to the Request!
                """
            case .emptyDocumentContainerAttached:
                return """
                The document couldn't be compiled because an empty document container was \
                attached to the Request!
                """
            case .invalidContainerContents:
                return """
                The document couldn't be compiled because a document container with invalid contents \
                was attached to the Request!
                """
            case .missingTargetLaTeXMLOutputFormat:
                return """
                The document couldn't be compiled because no \
                '\(Request.pressroomHeaders.targetLaTeXMLOutputFormat.stringValue)' header \
                was attached to the Request!
                """
            case .invalidTargetLaTeXMLOutputFormat:
                return """
                The document couldn't be compiled because an invalid document output format was used!
                """
            case .genericCompileFailure:
                return """
                The document couldn't be compiled because an error occured!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .invalidDocumentContentType:
                return ["""
                    The document container that was attached to the Request did not have a \
                    '\(HTTPHeaderName.contentType.stringValue)' of '\(MediaType.zip.description).'
                    """]
            case .emptyDocumentContainerAttached:
                return ["""
                        The document container attached to the Request appears to be empty.
                        """]
            case .invalidContainerContents:
                return ["""
                        The document container that was attached to the Request contained files \
                        that were not TeX-related files, contained disallowed auxiliary files, \
                        or contained no TeX files at all.'
                        """]
            case .missingTargetLaTeXMLOutputFormat:
                return ["""
                    The '\(Request.pressroomHeaders.targetLaTeXMLOutputFormat.stringValue)' header \
                    was not attached to the Request for some reason.
                    """]
            case .invalidTargetLaTeXMLOutputFormat:
                return ["""
                    An unsupported format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.targetLaTeXMLOutputFormat.stringValue)' header \
                    must be one of: 'html', 'html4', 'html5', 'xhtml' or 'xml'.
                    """]
            case .genericCompileFailure:
                return ["""
                        An internal step of the document transformation failed for some reason.
                        """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .invalidDocumentContentType:
                return ["""
                    Check that a document container with a '\(HTTPHeaderName.contentType.stringValue)' \
                    of '\(MediaType.zip.description) is being attached to the Request.
                    """]
            case .emptyDocumentContainerAttached:
                return ["""
                        Check that the document is being compressed correctly \
                        and the resulting container is not empty.
                        """]
            case .invalidContainerContents:
                return ["""
                        Check that a document container with only TeX files (except for auxilary files \
                        such as figures or stylesheets) is being attached to the Request.
                        """]
            case .missingTargetLaTeXMLOutputFormat:
                return ["""
                    Check that the '\(Request.pressroomHeaders.targetLaTeXMLOutputFormat.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .invalidTargetLaTeXMLOutputFormat:
                return ["""
                    Check that a valid document output format is used in the \
                    '\(Request.pressroomHeaders.targetLaTeXMLOutputFormat.stringValue)' header.
                    """]
            case .genericCompileFailure:
                return ["""
                        Unless a fix is obvious from the stack trace (or other debugging info), \
                        please report this error to the Manuscripts team.
                        """]
            }
        }
    }
}
