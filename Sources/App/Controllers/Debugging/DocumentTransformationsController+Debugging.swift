//
//  DocumentTransformationsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

// swiftlint:disable type_body_length

extension DocumentTransformationsController {

    /// The debugging information for a given document transformation error
    enum DocumentDebuggingInfo: String, DebuggingInfo {
        case documentFormDataMissing
        case documentMetadataIngestFailure
        case documentMetadataExtractionFailure
        case genericContentImportServiceFailure
        case genericDocumentIngestFailure
        case invalidEquationTypesettingTimeout
        // swiftlint:disable:next identifier_name
        case invalidMetadataExtractionTargetFileExtension
        case invalidSourceFileExtension
        case invalidTargetFileExtension
        case invalidTeXContainerProcessingMethod
        case jatsArchiveURLMissing
        case jatsFileListMissing
        case jatsJournalCodeMissing
        case jatsSubmissionIdentifierMissing
        case jatsURLMissing
        case jatsVendorNameMissing
        case packageControllerConfigFailure

        var status: HTTPResponseStatus {
            switch self {
            case .documentFormDataMissing,
                 .genericDocumentIngestFailure,
                 .invalidEquationTypesettingTimeout,
                 .invalidMetadataExtractionTargetFileExtension,
                 .invalidSourceFileExtension,
                 .invalidTargetFileExtension,
                 .invalidTeXContainerProcessingMethod,
                 .jatsArchiveURLMissing,
                 .jatsFileListMissing,
                 .jatsJournalCodeMissing,
                 .jatsSubmissionIdentifierMissing,
                 .jatsURLMissing,
                 .jatsVendorNameMissing:
                return .badRequest
            case .documentMetadataExtractionFailure:
                return .badGateway
            case .genericContentImportServiceFailure,
                 .documentMetadataIngestFailure,
                 .packageControllerConfigFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return DocumentTransformationsController.self
        }

        var reason: String {
            switch self {
            case .documentFormDataMissing:
                return """
                The document couldn't be compiled because no document multipart form data was attached to the Request!
                """
            case .documentMetadataExtractionFailure,
                 .documentMetadataIngestFailure:
                return """
                The document couldn't be compiled because an error occured during the metadata extraction stage.
                """
            case .genericContentImportServiceFailure:
                return """
                The document couldn't be compiled because an error occured during the document content import stage.
                """
            case .genericDocumentIngestFailure:
                return """
                The document couldn't be compiled because an error occured during the igestion stage.
                """
            case .invalidEquationTypesettingTimeout:
                return """
                The document couldn't be compiled because an invalid equation and \
                inline math typesetting timeout was used!
                """
            case .invalidMetadataExtractionTargetFileExtension:
                return """
                The document couldn't be compiled because an invalid target file extension was used when \
                also attempting to extract document metadata!
                """
            case .invalidSourceFileExtension:
                return """
                The document couldn't be compiled because a source document of an invalid file format was \
                attached to the Request!
                """
            case .invalidTargetFileExtension:
                return """
                The document couldn't be compiled because an invalid target file extension was used!
                """
            case .invalidTeXContainerProcessingMethod:
                return """
                The document couldn't be compiled because an invalid TeX container processing method was used!
                """
            case .jatsArchiveURLMissing:
                return """
                The document couldn't be compiled because no JATS Archive URL was attached to the Request!
                """
            case .jatsFileListMissing:
                return """
                The document couldn't be compiled because no JATS File List was attached to the Request!
                """
            case .jatsJournalCodeMissing:
                return """
                The document couldn't be compiled because no JATS Journal Code was attached to the Request!
                """
            case .jatsSubmissionIdentifierMissing:
                return """
                The document couldn't be compiled because no JATS Submission Identifier was attached to the Request!
                """
            case .jatsURLMissing:
                return """
                The document couldn't be compiled because no JATS URL was attached to the Request!
                """
            case .jatsVendorNameMissing:
                return """
                The document couldn't be compiled because no JATS Vendor Name was attached to the Request!
                """
            case .packageControllerConfigFailure:
                return """
                The document couldn't be compiled because an error occured when initializing the package controller \
                that is required for document transformations.
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .documentFormDataMissing:
                return ["""
                    The document multipart form data was not attached to the Request for some reason.
                    """]
            case .documentMetadataExtractionFailure:
                return ["""
                    The call to the metadata extraction web service failed for some reason.
                    """]
            case .documentMetadataIngestFailure:
                return ["""
                    Saving the XML response from the metadata extraction web service \
                    to a temporary file failed for some reason.
                    """]
            case .genericContentImportServiceFailure:
                return ["""
                    An internal step of the document content import failed for some reason.
                    """]
            case .genericDocumentIngestFailure:
                return ["""
                    An internal step of the document transformation failed for some reason.
                    """]
            case .invalidEquationTypesettingTimeout:
                return ["""
                    A non-numerical timeout was specified, or it was outside of the valid range. \
                    The \(Request.pressroomHeaders.equationsTypesettingTimeout.stringValue) header \
                    must be a number between 90 - 300.
                    """]
            case .invalidMetadataExtractionTargetFileExtension:
                return ["""
                    An unsupported format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header must be ommitted \
                    or set to 'manuproj' when attempting to extract document metadata.
                    """]
            case .invalidSourceFileExtension:
                return ["""
                    An unsupported source document was attached to the Request. \
                    The source document file format must be one of: 'Microsoft Word', 'HTML', \
                    'Manuscripts Project Bundle', 'Markdown', 'RTF' or 'TeX'. 'PDF' is only supported as a source \
                    document file format if the '\(Request.pressroomHeaders.enrichDocumentMetadata.stringValue)' \
                    header is also attached to the Request.
                    """]
            case .invalidTargetFileExtension:
                return ["""
                    An unsupported format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header must be one of: \
                    'docx', 'html', 'manuproj', 'md', 'pdf', 'tex', 'xml' (for JATS) or 'go.xml' \
                    (for EM Manifest metadata XML).
                    """]
            case .invalidTeXContainerProcessingMethod:
                return ["""
                    An unsupported TeX container processing method was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.teXContainerProcessingMethod.stringValue)' header must be \
                    one of: 'pandoc' or 'latexml'.
                    """]
            case .jatsArchiveURLMissing:
                return ["""
                    The \(Request.pressroomHeaders.jatsArchiveURL.stringValue)' header was not attached \
                    to the Request for some reason.
                    """]
            case .jatsFileListMissing:
                return ["""
                    The \(Request.pressroomHeaders.jatsFileList.stringValue)' header was not attached \
                    to the Request for some reason.
                    """]
            case .jatsJournalCodeMissing:
                return ["""
                    The \(Request.pressroomHeaders.jatsJournalCode.stringValue)' header was not attached \
                    to the Request for some reason.
                    """]
            case .jatsSubmissionIdentifierMissing:
                return ["""
                    The \(Request.pressroomHeaders.jatsSubmissionIdentifier.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .jatsURLMissing:
                return ["""
                    The \(Request.pressroomHeaders.jatsURL.stringValue)' header was not attached \
                    to the Request for some reason.
                    """]
            case .jatsVendorNameMissing:
                return ["""
                    The \(Request.pressroomHeaders.jatsVendorName.stringValue)' header was not attached \
                    to the Request for some reason.
                    """]
            case .packageControllerConfigFailure:
                return ["""
                    An internal step of the package controller initialization failed for some reason.
                    """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .documentFormDataMissing:
                return ["""
                    Check that the document is being attached to the Request using a 'file' header.
                    """]
            case .documentMetadataExtractionFailure,
                 .documentMetadataIngestFailure:
                return ["""
                    Try making the request again. If the failure persists, please report \
                    this error to the Manuscripts team.
                    """]
            case .invalidEquationTypesettingTimeout:
                return ["""
                    Check that a valid timeout value is used in the \
                    '\(Request.pressroomHeaders.equationsTypesettingTimeout.stringValue)' header.
                    """]
            case .invalidMetadataExtractionTargetFileExtension:
                return ["""
                    Check that a valid document format is used in the \
                    '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header, or remove the \
                    '\(Request.pressroomHeaders.enrichDocumentMetadata.stringValue)' header \
                    if metadata extraction was being attempted by mistake.
                    """]
            case .invalidSourceFileExtension:
                return ["""
                    Check that a source document of a valid file format is attached to the Request.
                    """]
            case .invalidTargetFileExtension:
                return ["""
                    Check that a valid document format is used in the \
                    '\(Request.pressroomHeaders.targetFileExtension.stringValue)' header.
                    """]
            case .invalidTeXContainerProcessingMethod:
                return ["""
                    Check that a valid TeX container processing method is used in the \
                    '\(Request.pressroomHeaders.teXContainerProcessingMethod.stringValue)' header.
                    """]
            case .jatsArchiveURLMissing,
                 .jatsFileListMissing,
                 .jatsJournalCodeMissing,
                 .jatsSubmissionIdentifierMissing,
                 .jatsURLMissing,
                 .jatsVendorNameMissing:
                return ["""
                    Check that the relevant JATS/EM Manifest metadata header is being attached to the Request, \
                    and that contains a valid value.
                    """]
            case .genericContentImportServiceFailure,
                 .genericDocumentIngestFailure,
                 .packageControllerConfigFailure:
                return ["""
                    Unless a fix is obvious from the stack trace (or other debugging info), \
                    please report this error to the Manuscripts team.
                    """]
            }
        }
    }
}
