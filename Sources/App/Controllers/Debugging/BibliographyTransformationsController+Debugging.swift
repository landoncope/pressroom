//
//  BibliographyTransformationsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension BibliographyTransformationsController {

    /// The debugging information for a given bibliography transformation error
    enum BibliographyDebuggingInfo: String, DebuggingInfo {
        case invalidContentType
        case invalidEdifixEditorialStyle
        case missingEdifixSecret
        case missingTargetBibliographyFormatHeader
        case invalidSourceBibliographyFormat
        case invalidTargetBibliographyFormat
        case missingStringRepresentation
        case genericCompileFailure

        var status: HTTPResponseStatus {
            switch self {
            case .invalidContentType:
                return .unsupportedMediaType
            case .missingEdifixSecret,
                 .missingTargetBibliographyFormatHeader,
                 .invalidEdifixEditorialStyle,
                 .invalidSourceBibliographyFormat,
                 .invalidTargetBibliographyFormat:
                return .badRequest
            case .missingStringRepresentation, .genericCompileFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return BibliographyTransformationsController.self
        }

        var reason: String {
            switch self {
            case .invalidContentType:
                return """
                The bibliography couldn't be compiled because the '\(HTTPHeaderName.contentType)' header \
                was set incorrectly!
                """
            case .missingEdifixSecret:
                return """
                The bibliography couldn't be compiled because no \
                '\(Request.pressroomHeaders.edifixSecret.stringValue)' header was attached to the Request.
                """
            case .missingTargetBibliographyFormatHeader:
                return """
                The bibliography couldn't be compiled because a target bibliography format was not specified!
                """
            case .invalidEdifixEditorialStyle:
                return """
                The bibliography couldn't be compiled because an invalid Edifix editorial style was used!
                """
            case .invalidSourceBibliographyFormat:
                return """
                The bibliography couldn't be compiled because an invalid source bibliography format was used!
                """
            case .invalidTargetBibliographyFormat:
                return """
                The bibliography couldn't be compiled because an invalid target bibliography format was used!
                """
            case .missingStringRepresentation:
                return """
                The bibliography couldn't be compiled because the attached bibliography did not have \
                a string representation when compiled to the target bibliography format!
                """
            case .genericCompileFailure:
                return """
                The bibliography wasn't compiled because an error occured!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .invalidContentType:
                return ["""
                    The '\(HTTPHeaderName.contentType)' header is not '\(MediaType.json.serialize())'.
                    """]
            case .missingEdifixSecret:
                return ["""
                    The '\(Request.pressroomHeaders.edifixSecret.stringValue)' header was not \
                    attached to the Request for some reason.
                    """]
            case .missingTargetBibliographyFormatHeader:
                return ["""
                    The '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header \
                    is missing from the Request for some reason.
                    """]
            case .invalidEdifixEditorialStyle:
                return ["""
                    An unsupported Edifix editorial style was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.edifixEditorialStyle.stringValue)' header \
                    must be a valid value (see Edifix API documentation).
                    """]
            case .invalidSourceBibliographyFormat:
                return ["""
                    An unsupported format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.sourceBibliographyFormat.stringValue)' header \
                    must be one of: 'bib', 'end', 'json' or 'ris'.
                    """]
            case .invalidTargetBibliographyFormat:
                return ["""
                    An unsupported format was specified, or there is a typo. \
                    The '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header \
                    must be one of: 'ads', 'bib', 'end', 'isi', 'jats', 'ris', 'wordbib' or 'xml'.
                    """]
            case .missingStringRepresentation:
                return ["""
                        Some (or all) of the content in the original bibliography \
                        could not be represented as a string.
                        """]
            case .genericCompileFailure:
                return ["""
                        An internal step of the bibliography transformation failed for some reason.
                        """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .invalidContentType:
                return ["""
                    Check that a '\(HTTPHeaderName.contentType)' header of '\(MediaType.json.serialize())' \
                    is attached to the Request.
                    """]
            case .missingEdifixSecret:
                return ["""
                    Check that a '\(Request.pressroomHeaders.edifixSecret.stringValue)' header \
                    is being attached to the Request.
                    """]
            case .missingTargetBibliographyFormatHeader:
                return ["""
                    Check that a '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' \
                    header is attached to the Request.
                    """]
            case .invalidEdifixEditorialStyle:
                return ["""
                    Check that a valid Edifix editorial style is used in the \
                    '\(Request.pressroomHeaders.edifixEditorialStyle.stringValue)' header.
                    """]
            case .invalidSourceBibliographyFormat:
                return ["""
                    Check that a valid bibliography format is used in the \
                    '\(Request.pressroomHeaders.sourceBibliographyFormat.stringValue)' header.
                    """]
            case .invalidTargetBibliographyFormat:
                return ["""
                    Check that a valid bibliography format is used in the \
                    '\(Request.pressroomHeaders.targetBibliographyFormat.stringValue)' header.
                    """]
            case .missingStringRepresentation:
                return ["""
                        Check the original bibiliography for any non-standard content \
                        that might not be string-representable.
                        """]
            case .genericCompileFailure:
                return ["""
                        Unless a fix is obvious from the stack trace (or other debugging info), \
                        please report this error to the Manuscripts team.
                        """]
            }
        }
    }
}
