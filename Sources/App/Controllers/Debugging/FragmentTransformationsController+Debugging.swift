//
//  FragmentTransformationsController+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension FragmentTransformationsController {

    /// The debugging information for a given document fragment transformation error
    enum FragmentDebuggingInfo: String, DebuggingInfo {
        case missingInputFormatHeader
        case missingOutputFormatHeader
        case invalidInputFormat
        case invalidOutputFormat
        case genericCompileFailure

        var status: HTTPResponseStatus {
            switch self {
            case .missingInputFormatHeader,
                 .missingOutputFormatHeader,
                 .invalidInputFormat,
                 .invalidOutputFormat:
                return .badRequest
            case .genericCompileFailure:
                return .internalServerError
            }
        }

        var occurrenceClass: AnyClass {
            return FragmentTransformationsController.self
        }

        var reason: String {
            switch self {
            case .missingInputFormatHeader, .missingOutputFormatHeader:
                return """
                The document fragment failed to compile because the format header was not attached to the Request.
                """
            case .invalidInputFormat, .invalidOutputFormat:
                return """
                The document fragment failed to compile because the format header attached to the Request was \
                an invalid value.
                """
            case .genericCompileFailure:
                return """
                The document fragment failed to compile for some reason.
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .missingInputFormatHeader:
                return ["""
                    The '\(Request.pressroomHeaders.documentFragmentInputFormat.stringValue)' header \
                    is missing from the Request for some reason.
                    """]
            case .missingOutputFormatHeader:
                return ["""
                    The '\(Request.pressroomHeaders.documentFragmentOutputFormat.stringValue)' header \
                    is missing from the Request for some reason.
                    """]
            case .invalidInputFormat:
                return ["""
                    An unsupported format was specified, or there is a typo.\
                    The '\(Request.pressroomHeaders.documentFragmentInputFormat.stringValue)' header \
                    must be one of: 'mathml'.
                    """]
            case .invalidOutputFormat:
                return ["""
                    An unsupported format was specified, or there is a typo.\
                    The '\(Request.pressroomHeaders.documentFragmentOutputFormat.stringValue)' header \
                    must be one of: 'tex'.
                    """]
            case .genericCompileFailure:
                return ["""
                        An internal step of the document fragment transformation failed for some reason.
                        """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .missingInputFormatHeader:
                return ["""
                    Check that a '\(Request.pressroomHeaders.documentFragmentInputFormat.stringValue)' \
                    header is attached to the Request.
                    """]
            case .missingOutputFormatHeader:
                return ["""
                    Check that a '\(Request.pressroomHeaders.documentFragmentOutputFormat.stringValue)' \
                    header is attached to the Request.
                    """]
            case .invalidInputFormat:
                return ["""
                    Check that a valid bibliography format is used in the \
                    '\(Request.pressroomHeaders.documentFragmentInputFormat.stringValue)' header.
                    """]
            case .invalidOutputFormat:
                return ["""
                    Check that a valid bibliography format is used in the \
                    '\(Request.pressroomHeaders.documentFragmentOutputFormat.stringValue)' header.
                    """]
            case .genericCompileFailure:
                return ["""
                        Unless a fix is obvious from the stack trace (or other debugging info), \
                        please report this error to the Manuscripts team.
                        """]
            }
        }
    }
}
