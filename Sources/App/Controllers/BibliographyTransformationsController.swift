//
//  BibliographyTransformationsController.swift
//  App
//
//  Created by Dan Browne on 17/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import MPFoundation

/// Controls bibliography transformation operations for PressroomServer -
/// handles import, bibliography compilation and client export operations.
final class BibliographyTransformationsController: ContentTransformable {

    // MARK: - Bibliography transformation operations

    /// Compiles a bibliography from received bibliography data contained in a `Request`,
    /// and then streams it back to the Client via a chunked `Response`.
    ///
    /// The exported bibliography format is determined based on `Request` headers information.
    ///
    /// - Parameter request: The `Request` containing the bibliography content and export file format information.
    /// - Returns: A `Response` once the requisite bibliography compilation
    ///            and export operations have either completed, or failed in some way.
    /// - Throws: Errors if any of the internal operations failed for some reason.
    func compileBibliography(_ request: Request) throws -> Future<Response> {

        // If a 'Pressroom-Source-Bibliography-Format' is present,
        // compile operation is assumed to be 'Some bib format -> CSL-JSON'
        // Otherwise, compile operation is assumed to be plaintext -> JATS XML or 'CSL-JSON -> Some bib format'
        if let sourceBibliographyFormat = request.http.headers.firstValue(name: Request.pressroomHeaders.sourceBibliographyFormat) {
            // Decode the attached bibliography data
            // (In an arbitrary bibliography format included in the body of the `Request`)
            return request.http.body.consumeData(on: request).thenThrowing {[unowned self] decodedBibliographyData -> String in
                // Check headers
                guard let contentTypeHeader = request.http.headers.firstValue(name: .contentType),
                    MediaType.parse(contentTypeHeader) == MediaType.plainText else {
                    throw PressroomError.error(for: BibliographyDebuggingInfo.invalidContentType,
                                               sourceLocation: .capture())
                }

                // If the input format is invalid, then throw a `PressroomError`
                guard let inputFormat = CSLJSONTransformationService.InputFormat(rawValue: sourceBibliographyFormat) else {
                    throw PressroomError.error(for: BibliographyDebuggingInfo.invalidSourceBibliographyFormat,
                                               sourceLocation: .capture())
                }

                return try self.exportBibliography(from: decodedBibliographyData, inputFormat: inputFormat)

                }.thenThrowing { compiledBibliographyStringRepresentation -> Response in
                    // Return the compiled bibliography to the Client in the `Response` body
                    let response = request.response()
                    try response.content.encode(compiledBibliographyStringRepresentation)
                    response.http.contentType = .json

                    return response

                }.catchMap {[unowned self] error -> Response in
                    // Throw the `Abort` corresponding with the BibliographyTransformationError already thrown earlier -
                    // this is exposed in the `Response`
                    try self.handleAbort(for: error)

                    return request.response()
            }
        } else {
            // Check headers
            guard let contentTypeHeader = request.http.headers.firstValue(name: .contentType),
                MediaType.parse(contentTypeHeader) == MediaType.json else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.invalidContentType,
                                           sourceLocation: .capture())
            }
            // swiftlint:disable:next line_length
            guard let targetBibliographyFormat = request.http.headers.firstValue(name: Request.pressroomHeaders.targetBibliographyFormat) else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.missingTargetBibliographyFormatHeader,
                                           sourceLocation: .capture())
            }

            if targetBibliographyFormat == "jats" {
                return try exportJATSBibliography(originatingRequest: request)
            } else {
                return try exportBibliography(targetFormat: targetBibliographyFormat, originatingRequest: request)
            }
        }
    }

    // MARK: - Private methods

    /// Transforms attached plaintext bibliography references (inside JSON included in the body of the `Request`) to JATS XML.
    ///
    /// - Parameter originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: A `Future<Response>` containing a JATS XML representation of the references,
    ///            formatted according to specified configuration options.
    /// - Throws: An `Error` if the plaintext bibliography transformation fails for some reason.
    private func exportJATSBibliography(originatingRequest: Request) throws -> Future<Response> {

        return try originatingRequest.content.decode(BibliographicPlaintextJSON.self).flatMap({ decodedBibliography -> Future<Response> in
            guard let secret = originatingRequest.http.headers.firstValue(name: Request.pressroomHeaders.edifixSecret) else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.missingEdifixSecret,
                                           sourceLocation: .capture())
            }
            var edifixSecret: ServiceSecret!
            do {
                edifixSecret = try ServiceSecret(encoded: secret, secretType: .twoParts)
            } catch {
                throw PressroomError.error(for: BibliographyDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }

            // swiftlint:disable:next line_length
            let styleHeader = originatingRequest.http.headers.firstValue(name: Request.pressroomHeaders.edifixEditorialStyle) ?? EdifixBibliographyService.Options.EditorialStyle.chicagoBibliography.rawValue
            guard let style = EdifixBibliographyService.Options.EditorialStyle(rawValue: styleHeader) else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.invalidEdifixEditorialStyle,
                                           sourceLocation: .capture())
            }
            let options = EdifixBibliographyService.Options(editorialStyle: style)
            let edifixService = EdifixBibliographyService(secret: edifixSecret)
            return try edifixService.response(for: decodedBibliography.references,
                                              options: options,
                                              originatingRequest: originatingRequest)
        }).catchMap({ error -> Response in
            // If the `Error` is already a `PressroomError`,
            // simply re-throw (i.e. 'missingStringRepresentation' error above).
            // Otherwise, throw a constructed `PressroomError`.
            if let pressroomError = error as? PressroomError {
                throw pressroomError
            } else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        })
    }

    /// Transforms attached CSL-JSON included in the body of the `Request`, to another bibliography format.
    ///
    /// - Parameters:
    ///   - targetFormat: The target bibliography format for the transformation operation.
    ///   - originatingRequest: The `Request` that was initially received by Pressroom.
    /// - Returns: A `Future<Response>` containing the references represented in the output format specified.
    /// - Throws: An `Error` if the bibliography transformation fails for some reason.
    private func exportBibliography(targetFormat: String, originatingRequest: Request) throws -> Future<Response> {

        // If the output format is unknown, then throw a `PressroomError`
        let bibutilsService = MPBibutilsService()
        let outputFormat = bibutilsService.outputFormat(for: targetFormat)
        if outputFormat == .unknown {
            throw PressroomError.error(for: BibliographyDebuggingInfo.invalidTargetBibliographyFormat,
                                       sourceLocation: .capture())
        }

        return try originatingRequest.content.decode(json: [CSLJSONItem].self,
                                          using: CSLJSONDecoder()).thenThrowing {[unowned self] decodedBibliography -> String in
                                            return try self.exportBibliography(from: decodedBibliography,
                                                                               bibutilsService: bibutilsService,
                                                                               outputFormat: outputFormat)

            }.thenIfErrorThrowing { error -> String in
                // If the `Error` is already a `PressroomError`,
                // simply re-throw (i.e. 'missingStringRepresentation' error above).
                // Otherwise, throw a constructed `PressroomError`.
                if let pressroomError = error as? PressroomError {
                    throw pressroomError
                } else {
                    throw PressroomError.error(for: BibliographyDebuggingInfo.genericCompileFailure,
                                               sourceLocation: .capture(),
                                               underlyingError: error)
                }
            }.thenThrowing { compiledBibliographyStringRepresentation -> Response in
                // Return the compiled bibliography to the Client in the `Response` body
                let response = originatingRequest.response()
                try response.content.encode(compiledBibliographyStringRepresentation)

                return response

            }.catchMap {[unowned self] error -> Response in
                // Throw the `Abort` corresponding with the BibliographyTransformationError already thrown earlier -
                // this is exposed in the `Response`
                try self.handleAbort(for: error)

                return originatingRequest.response()
        }
    }

    /// Export a bibliography to a CSL-JSON representation, based on a decoded originating bibliography.
    ///
    /// - Parameters:
    ///   - bibliographyData: `Data` (formatted as BibTeX, EndNote, CSL-JSON or RIS).
    ///   - inputFormat: The input format of the bibliography data before transformation
    ///     (it should match the format of the data in `bibliographyData`).
    /// - Returns: `Data` representation of the resulting CSL-JSON output.
    /// - Throws: A `PressroomError` if the export operation fails for some reason.
    private func exportBibliography(from bibliographyData: Data,
                                    inputFormat: CSLJSONTransformationService.InputFormat) throws -> String {
        do {
            // Export the bibliography data from the input format into a CSL-JSON representation
            let cslJSONData = try CSLJSONTransformationService().transform(bibliographyData, from: inputFormat)
            guard let exportedBibliography = String(data: cslJSONData, encoding: .utf8) else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.missingStringRepresentation,
                                           sourceLocation: .capture())
            }
            return "{\"items\": \(exportedBibliography)}"
        } catch {
            // If the `Error` is already a `PressroomError`,
            // simply re-throw (i.e. 'missingStringRepresentation' error above).
            // Otherwise, throw a constructed `PressroomError`.
            if let pressroomError = error as? PressroomError {
                throw pressroomError
            } else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        }
    }

    /// Export a bibliography to a given output format, based on a decoded originating bibliography.
    ///
    /// - Parameters:
    ///   - decodedBibliography: `BibliographicCSLJSON`, containing the decoded bibliography.
    ///   - bibutilsService: An instance of `MPBibutilsService`, used for doing the export operation.
    ///   - outputFormat: The `MPBibutilsOutputFormat` specifying the format of the exported bibliography.
    /// - Returns: The exported bibliograhy in the given output format, as a `String`.
    /// - Throws: A `PressroomError` if the export operation fails for some reason.
    private func exportBibliography(from decodedBibliography: [CSLJSONItem],
                                    bibutilsService: MPBibutilsService,
                                    outputFormat: MPBibutilsOutputFormat) throws -> String {
        do {
            // Export the bibliography data into the request target file format
            let bibliographyData = try JSONEncoder().encode(decodedBibliography)

            // TODO: This is the best current compromise: CSL-JSON -> BibTeX, BibTeX -> MODS, MODS -> final output
            // (Special-case for BibTeX output  - do the CSL-JSON -> BibTeX step only!)
            // (Special-case for MODS output    - do the CSL-JSON -> BibTeX -> MODS steps only!)
            var exportedBibliographyData: Data!
            let bibTeXIntermediateData = try CSLJSONTransformationService().transform(bibliographyData, to: .bibTeX)
            if outputFormat == .bibTeX {
                exportedBibliographyData = bibTeXIntermediateData
            } else {
                let modsIntermediateData = try bibutilsService.data(bibTeXIntermediateData, from: .bibTeX, to: .MODS)
                if outputFormat == .MODS {
                    exportedBibliographyData = modsIntermediateData
                } else {
                    exportedBibliographyData = try bibutilsService.data(modsIntermediateData,
                                                                        from: .MODS,
                                                                        to: outputFormat)
                }
            }

            guard let exportedBibliography = String(data: exportedBibliographyData, encoding: .utf8) else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.missingStringRepresentation,
                                           sourceLocation: .capture())
            }
            return exportedBibliography
        } catch {
            // If the `Error` is already a `PressroomError`,
            // simply re-throw (i.e. 'missingStringRepresentation' error above).
            // Otherwise, throw a constructed `PressroomError`.
            if let pressroomError = error as? PressroomError {
                throw pressroomError
            } else {
                throw PressroomError.error(for: BibliographyDebuggingInfo.genericCompileFailure,
                                           sourceLocation: .capture(),
                                           underlyingError: error)
            }
        }
    }
}
