//
//  DocumentTransformationsController.swift
//  App
//
//  Created by Dan Browne on 04/06/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import MPFoundation

// swiftlint:disable file_length
// swiftlint:disable type_body_length

/// Controls document transformation operations for PressroomServer -
/// handles import, document compilation and client export operations.
final class DocumentTransformationsController: ContentPersistable, ContentTransformable {

    /// A tuple for passing a file `URL` and `PrimaryFileCandidatePackageControllerTuple` between chained Vapor closures
    // swiftlint:disable:next type_name line_length
    typealias FileURLPrimaryFileCandidatePackageControllerTuple = (fileURL: URL, candidatePkgcTuple: PrimaryFileCandidatePackageControllerTuple)

    /// A tuple for passing a `PrimaryFileCandidate` and `MPManuscriptsPackageController` between chained Vapor closures
    // swiftlint:disable:next type_name
    typealias PrimaryFileCandidatePackageControllerTuple = (candidate: PrimaryFileCandidate, pkgc: MPManuscriptsPackageController)

    /// The processing method to use when the received document is a TeX container.
    ///
    /// - pandoc: The TeX container should be processed using Pandoc.
    /// - latexml: The TeX container should be processed using LaTeXML.
    enum TeXContainerProcessingMethod: String {
        case latexml
        case pandoc
    }

    // MARK: - Manuscripts dependency configuration

    /// Ensure Manuscripts dependencies are in correct initial state required for document IO.
    class func configureManuscriptsDependenciesInitialState() {
        // Ensure the sharedShoebox is initialised before any document IO takes place
        // (We need to dispatch to the main queue due to an implementation detail)
        DispatchQueue.main.async {
            MPShoeboxPackageController.sharedShoebox().ensureInitialStateInitialized()
        }
    }

    // MARK: - Document transformation operations

    /// Compiles a document from received document data contained in a `Request`,
    /// and then streams it back to the Client via a chunked `Response`.
    ///
    /// The exported document format is determined based on `Request` headers information.
    /// If no export format information is provided, a Manuscript Project Bundle
    /// (i.e. a `.manuproj` file) will be exported by default.
    ///
    /// - Parameter req: The `Request` containing the document content and (optionally)
    ///                  document export file format information.
    /// - Returns: A `Response` once the requisite document compilation and export operations
    ///            have either completed, or failed in some way.
    /// - Throws: Errors if any of the internal operations failed for some reason.
    // swiftlint:disable:next function_body_length
    func compileManuscript(_ req: Request) throws -> Future<Response> {

        // Holds the resulting MPManuscriptsPackageController
        // (set when fulfilling `configuredManscriptsPackageControllerPromise`,
        // used in `catchMap` because we can't pass in any params there)
        var pkgc: MPManuscriptsPackageController?

        // Special case 1)
        // Check for a 'Pressroom-TeX-Container-Processing-Method' header on the `Request`.
        // If one is found and is set to 'latexml' then forward the `Request` to an instance of
        // 'TeXTransformationsController' for processing. Otherwise continue processing here as normal
        // swiftlint:disable:next line_length
        guard let processingMethod = TeXContainerProcessingMethod(rawValue: req.http.headers.firstValue(name: Request.pressroomHeaders.teXContainerProcessingMethod) ?? TeXContainerProcessingMethod.pandoc.rawValue) else {
            throw PressroomError.error(for: DocumentDebuggingInfo.invalidTeXContainerProcessingMethod,
                                       sourceLocation: .capture())
        }
        switch processingMethod {
        case .latexml:
            return try TeXTransformationsController().compileManuscript(req)
        case .pandoc:
            break
        }

        // Chain the neccessary operations using `Future`s to ingest, import, compile,
        // and then stream the compiled document back to the Client in a `Response`
        //
        // Store the received document on disk in a temporary working location, before compiling into a document later
        return try candidateForDocumentRequest(request: req).flatMap {[unowned self] candidate -> Future<Response> in
            // Special case 2)
            // Check for a TeX -> PDF, RTF -> PDF or Word -> PDF transformation on the `Request`
            // (In this case, the normal flow is bypassed in favor of simply using 'pdflatex' or 'libreOffice' directly)
            if candidate.pdfTransformSuitability(of: req) == .validOfficeCandidate
                || candidate.pdfTransformSuitability(of: req) == .validTeXCandidate {
                return try self.compilePDF(req, candidate: candidate)
            }

            // A `Promise` used for notification of when the package controller configuration
            // has completed successfully, or if it has failed
            // (Needed due to the fact that the configuration step is executed on the main-queue,
            // so the `Future` cannot be returned directly)
            let pkgcPromise = req.eventLoop.newPromise(PrimaryFileCandidatePackageControllerTuple.self)

            // Configure the Manuscripts package controller, ready for import operations (requires main-queue)
            DispatchQueue.main.async {[weak self] in
                self?.configurePackageController(candidate: candidate,
                                                            mainQueuePromise: pkgcPromise)
            }

            return pkgcPromise.futureResult.then { (candidate, packageController) -> Future<PrimaryFileCandidatePackageControllerTuple> in
                // A `Promise` used for notification of when the content import
                // has completed successfully, or if it has failed
                // (Needed due to the fact that the import content step is executed on the main-queue,
                // so the `Future` cannot be return directly)
                let importContentPromise = req.eventLoop.newPromise(PrimaryFileCandidatePackageControllerTuple.self)

                // Persist the `MPManuscriptsPackageController` instance,
                // (used in `catchMap` because we can't pass in any params there)
                pkgc = packageController

                // Import the content from the received document that is now stored on disk (requires main-queue)
                DispatchQueue.main.async {[weak self] in
                    self?.importContent(from: candidate,
                                        using: packageController,
                                        request: req,
                                        mainQueuePromise: importContentPromise)
                }
                return importContentPromise.futureResult
                }.then {[unowned self] (candidate, packageController) -> Future<FileURLPrimaryFileCandidatePackageControllerTuple> in
                    // Compile the document based on the contents of the pkgc
                    // and configurations options present in the `Request`
                    return self.compileDocumentOptionallyEnrichingMetadata(request: req,
                                                                           using: packageController,
                                                                           candidate: candidate)
                }.thenThrowing {[unowned self] (fileURL, candidatePkgcTuple) -> Response in
                    // Close the package controller's internal database connections when it is no longer needed
                    try candidatePkgcTuple.pkgc.close()
                    pkgc = nil

                    // Clean-up received document ingest working directory before returning `Response`
                    if let candidateFileURL = try? candidate.ensuredFileURL() {
                        _ = self.cleanTemporaryContent(at: candidateFileURL.deletingLastPathComponent())
                    }

                    // Return a chunked `Response` streaming the compiled `.manuproj` file from disk to the Client
                    let response = try self.chunkedResponse(forFileContentsAt: fileURL, request: req)

                    // Add any `Response` headers that are present on the primary file candidate
                    // to the `Response` being returned to the Client
                    for (header, value) in candidatePkgcTuple.candidate.responseHeaders {
                        response.http.headers.add(name: header, value: value)
                    }

                    return response
                }.catchMap {[unowned self] error -> Response in
                    // Close the package controller's internal database connections when it is no longer needed
                    // (Optional used here in case it was never initialized)
                    try pkgc?.close()

                    // Throw the `Abort` corresponding with the DocumentTransformationsController.Error
                    // already thrown earlier - this is exposed in the `Response`
                    try self.handleAbort(for: error)

                    return req.response()
            }
        }
    }

    // MARK: - Private methods

    /// Write the received document to disk in a temporary ingest working directory.
    ///
    /// - Parameter request: The `Request` containing an attached document (e.g. a '.docx' file).
    /// - Returns: A `PrimaryFileCandidate` containing a file `URL` for the saved document on disk,
    ///            inside a temporary ingest working directory.
    /// - Throws: An `Abort` if the document could not be ingested correctly.
    private func candidateForDocumentRequest(request: Request) throws -> Future<PrimaryFileCandidate> {

        // Holds the resulting `PrimaryFileCandidate`
        // (Private var needed because we can't pass in any params into `catchMap`,
        // and it is Optional because an error may have been thrown during initialization)
        var receivedDocumentFileCandidate: PrimaryFileCandidate?

        return try decodeContent(in: request,
                                 // swiftlint:disable:next line_length
                                 contentType: ArbitrarilyFormattedDocument.self).thenThrowing {[unowned self] receivedDocument -> PrimaryFileCandidate in
            if receivedDocument.content.file.contentType == .zip {
                let primaryFileName = request.http.headers.firstValue(name: Request.pressroomHeaders.primaryFile)
                receivedDocumentFileCandidate = try self.decompressDocumentContainer(receivedDocument,
                                                                                     primaryFileName: primaryFileName)
            } else {
                // swiftlint:disable:next line_length
                receivedDocumentFileCandidate = PrimaryFileCandidate(fileURL: receivedDocument.workingDirectory.appendingPathComponent(receivedDocument.content.file.filename))
                try receivedDocument.content.file.data.write(to: receivedDocumentFileCandidate!.ensuredFileURL(),
                                                             options: .atomic)
            }

            return receivedDocumentFileCandidate!
        }.catchMap { outerError -> PrimaryFileCandidate in

            // Clean-up received document ingest working directory
            // (if it was set before the error occurred) before we throw an `Abort`
            do {
                if let url = try receivedDocumentFileCandidate?.ensuredFileURL() {
                    _ = self.cleanTemporaryContent(at: url)
                }
            } catch {
                // If `ensuredFileURL()` throws an error, it's because the file was an empty document container,
                // the file was missing from the request or the file in `Pressroom-Primary-File` header
                // couldn't be found in the document container
                if outerError is PressroomError {
                    throw outerError
                } else {
                    throw PressroomError.error(for: DocumentDebuggingInfo.documentFormDataMissing,
                                               sourceLocation: .capture(),
                                               underlyingError: outerError)
                }
            }

            // The received document file URL was constructed, but the file save failed for some other reason
            throw PressroomError.error(for: DocumentDebuggingInfo.genericDocumentIngestFailure,
                                       sourceLocation: .capture(),
                                       underlyingError: outerError)
        }
    }

    /// Decompress a received document container that is already located on disk.
    ///
    /// - Parameters:
    ///   - compressedDocument: `PersistableContent`, containing the decoded document and the file URL
    ///                         for the compressed document container on disk,
    ///                         inside a temporary ingest working directory.
    ///   - primaryFileName: The value of the optional `Pressroom-Primary-File` `Request` header
    ///                      which explicitly declares the 'primary' file to use for importing.
    /// - Returns: A `PrimaryFileCandidate` containing a file `URL` for the "primary" file of the document
    ///            in its decompressed state.
    /// - Throws: An `Error` if the document container could not be decompressed correctly.
    private func decompressDocumentContainer(_ compressedDocument: PersistableContent<ArbitrarilyFormattedDocument>,
                                             primaryFileName: String?) throws -> PrimaryFileCandidate {
        // Decompress the document container to a subdirectory of the working directory
        // where the container is currently located
        let decompressedDocumentFileURL = try decompressContent(compressedDocument)

        // Return the "primary" file candidate for the document,
        // as located in the decompressed document container directory
        return try PrimaryFileCandidate(for: decompressedDocumentFileURL, primaryFileName: primaryFileName)
    }

    /// Configures a `MPManuscriptsPackageController`, initialised and ready for immediate use for
    /// import operations via `mainQueuePromise` (requires the main queue).
    ///
    /// - Parameters:
    ///   - candidate: A `PrimaryFileCandidate` that is passed through unmodified
    ///                within `mainQueuePromise` if it is fulfilled successfully.
    ///   - mainQueuePromise: A `Promise` tracking the state of the operations
    ///                       that have been dispatched on to the main queue.
    func configurePackageController(candidate: PrimaryFileCandidate,
                                    mainQueuePromise: Promise<PrimaryFileCandidatePackageControllerTuple>) {
        let pkgDelegate = MPDatabasePackageControllerBlockBasedDelegate(packageController: nil,
                                                                        rootURLHandler: ({ nil })) { _ in }
        let filePath = NSTemporaryDirectory().appending(UUID().uuidString)

        do {
            let packageController = try MPManuscriptsPackageController(fileURL: URL(fileURLWithPath: filePath),
                                                                       readOnly: false,
                                                                       delegate: pkgDelegate)
            packageController.ensureInitialStateInitialized()
            mainQueuePromise.succeed(result: (candidate: candidate, pkgc: packageController))
        } catch {
            mainQueuePromise.fail(error: PressroomError.error(for: DocumentDebuggingInfo.packageControllerConfigFailure,
                                                              sourceLocation: .capture(),
                                                              underlyingError: error))
        }
    }

    /// Import the content from the received document in the ingest working directory (requires the main queue).
    ///
    /// - Parameters:
    ///   - candidate: A `PrimaryFileCandidate` that is passed through unmodified
    ///                within `mainQueuePromise` if it is fulfilled successfully.
    ///   - packageController: A `MPManuscriptsPackageController` to use for the content import operation.
    ///   - mainQueuePromise: A `Promise` tracking the state of the operations
    ///                       that have been dispatched on to the main queue.
    // swiftlint:disable:next function_body_length
    func importContent(from candidate: PrimaryFileCandidate,
                       using packageController: MPManuscriptsPackageController,
                       request: Request,
                       mainQueuePromise: Promise<PrimaryFileCandidatePackageControllerTuple>) {
        do {
            let candidateFileURL = try preprocessIfNecessary(candidate)
            // swiftlint:disable:next line_length
            let shouldRegenerateModelObjectIDs = request.http.headers.contains(name: Request.pressroomHeaders.regenerateProjectBundleObjectIDs)
            // swiftlint:disable:next line_length
            let targetFileExtension = request.http.headers.firstValue(name: Request.pressroomHeaders.targetFileExtension) ?? MediaType.manuscriptsProjectBundlePathExtension

            // Initialise the equation and inline math typesetting timeout value for the import options -
            // throw an `Abort` if the Client has passed-in an invalid timeout string via the `Request` headers
            // swiftlint:disable:next line_length
            guard let equationsTypesettingTimeout = Double(request.http.headers.firstValue(name: Request.pressroomHeaders.equationsTypesettingTimeout)
                ?? String(MPContentImportOptionsMathJaxTimeoutDefault)),
                (equationsTypesettingTimeout >= MPContentImportOptionsMathJaxTimeoutDefault
                    && equationsTypesettingTimeout <= MPContentImportOptionsMathJaxTimeoutMax) else {
                    mainQueuePromise.fail(error: PressroomError.error(for: DocumentDebuggingInfo.invalidEquationTypesettingTimeout,
                                                                      sourceLocation: .capture()))
                    return
            }

            // If a Project Bundle is being copied, configure manuscript-json import options
            // to optionally regenerate the model object IDs.
            // Otherwise, use a default set of import options, with an optionally configured
            // MathJax timeout (defaults to 90 seconds if header not set).
            var importOptions: MPContentImportOptions!
            if candidateFileURL.pathExtension == targetFileExtension
                && targetFileExtension == MediaType.manuscriptsProjectBundlePathExtension {
                importOptions = MPManuscriptJSONImportOptions.init(applicableDatabaseNames: nil,
                                                                   shouldUpdateExistingObjects: false,
                                                                   shouldRegenerateModelObjectIDs: shouldRegenerateModelObjectIDs)

                if shouldRegenerateModelObjectIDs {
                    candidate.responseHeaders.add(name: Response.pressroomHeaders.projectBundleObjectIDsRegenerated,
                                                  value: "true")
                }

            } else {
                importOptions = MPContentImportOptions(originalSourceURL: nil,
                                                       tidyProcessing: true,
                                                       readabilityProcessing: false,
                                                       failForUnexpectedContents: false,
                                                       flattenSections: false,
                                                       logProgress: !(try Environment.detect().isRelease),
                                                       overrideMainResource: false,
                                                       mathJaxTimeout: equationsTypesettingTimeout,
                                                       context: nil)
            }

            // Handle import depending on if metadata enrichment is being requested
            if request.http.headers.contains(name: Request.pressroomHeaders.enrichDocumentMetadata) {
                if targetFileExtension == MediaType.manuscriptsProjectBundlePathExtension {
                    // If the candidate file is NOT a PDF, attempt to import it
                    // Otherwise, we can skip the import as there nothing else to do
                    if candidateFileURL.pathExtension != "pdf" {
                        guard packageController.contentImportService.service(for: candidateFileURL) != nil else {
                            mainQueuePromise.fail(error: PressroomError.error(for: DocumentDebuggingInfo.invalidSourceFileExtension,
                                                                              sourceLocation: .capture()))
                            return
                        }

                        try packageController.contentImportService.imported(from: candidateFileURL,
                                                                            options: importOptions)
                    }
                } else {
                    // Throw an `Abort` if an invalid target file extension
                    // has been provided whilst attempting metadata extraction
                    // swiftlint:disable:next line_length
                    mainQueuePromise.fail(error: PressroomError.error(for: DocumentDebuggingInfo.invalidMetadataExtractionTargetFileExtension,
                                                                      sourceLocation: .capture()))
                }
            } else {
                // If there is no enrichment being requested, import the file as normal
                guard packageController.contentImportService.service(for: candidateFileURL) != nil else {
                    mainQueuePromise.fail(error: PressroomError.error(for: DocumentDebuggingInfo.invalidSourceFileExtension,
                                                                      sourceLocation: .capture()))
                    return
                }

                try packageController.contentImportService.imported(from: candidateFileURL,
                                                                    options: importOptions)
            }
            // Fulfil the promise in any case if this point is reached and no error has occurred
            mainQueuePromise.succeed(result: (candidate: candidate, pkgc: packageController))
        } catch {
            // If enrichment is being requested whilst continuing on errors, override import errors
            if request.http.headers.contains(name: Request.pressroomHeaders.enrichDocumentMetadata)
                && request.http.headers.contains(name: Request.pressroomHeaders.continueOnErrors) {
                // Record the compilation error (this will be added as a `Response` header later)
                candidate.compilationError = error

                mainQueuePromise.succeed(result: (candidate: candidate, pkgc: packageController))
            } else {
                // Clean-up received document ingest working directory (in the case where the content import fails)
                if let candidateFileURL = try? candidate.ensuredFileURL() {
                    _ = self.cleanTemporaryContent(at: candidateFileURL.deletingLastPathComponent())
                }
                mainQueuePromise.fail(error: PressroomError.error(for: DocumentDebuggingInfo.genericContentImportServiceFailure,
                                                                  sourceLocation: .capture(),
                                                                  underlyingError: error))
            }
        }
    }

    /// Compile the document content that was previously imported using a `MPManuscriptsPackageController`
    /// into another document that whose format is determined based on `Request` headers information.
    /// (Optionally enrich the compiled the metadata of the Manuscripts Project Bundle, if the relevant
    /// header was set on the `Request`).
    ///
    /// - Parameters:
    ///   - request: The `Request` containing the document export format information in its headers.
    ///   - packageController: A `MPManuscriptsPackageController` containing imported document content
    ///                        to use for the document compile operation.
    ///   - candidate: A `PrimaryFileCandidate` representing the imported document
    ///                that is attached to the originating `Request`.
    /// - Returns: A `Future` that is fulfilled with a `FileURLPrimaryFileCandidatePackageControllerTuple`,
    ///            or an error if it fails for some reason.
    private func compileDocumentOptionallyEnrichingMetadata(request: Request,
                                                            using packageController: MPManuscriptsPackageController,
                                                            // swiftlint:disable:next line_length
                                                            candidate: PrimaryFileCandidate) -> Future<FileURLPrimaryFileCandidatePackageControllerTuple> {
        // Default to Manuscript Project Bundle (i.e. `manuproj`) compilation
        // if no file-extension header is present
        let fileExtension = request.http.headers.firstValue(name: Request.pressroomHeaders.targetFileExtension)
            ?? MediaType.manuscriptsProjectBundlePathExtension

        // Check if metadata extraction should be attempted, otherwise compile document as normal
        if request.http.headers.contains(name: Request.pressroomHeaders.enrichDocumentMetadata) {
            if fileExtension == MediaType.manuscriptsProjectBundlePathExtension {
                // If target file extension is valid, compile the enriched Project Bundle
                return self.compileEnrichedProjectBundle(originatingRequest: request,
                                                         using: packageController,
                                                         candidate: candidate)
            } else {
                // Throw an `Abort` if an invalid target file extension
                // has been provided whilst attempting extraction
                let err = PressroomError.error(for: DocumentDebuggingInfo.invalidMetadataExtractionTargetFileExtension,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: err)
            }
        } else {
            // Compile the imported document contained in the package controller
            // into a document formatted based on the `Request` headers
            return self.compileDocument(request: request, using: packageController, candidate: candidate)
        }
    }

    /// Compile the document content that was previously-imported using a `MPManuscriptsPackageController`
    /// into another document that whose format is determined based on `Request` headers information.
    ///
    /// - Parameters:
    ///   - request: The `Request` containing the document export format information in its headers.
    ///   - packageController: A `MPManuscriptsPackageController` containing imported document content
    ///                        to use for the document compile operation.
    ///   - candidate: A `PrimaryFileCandidate` representing the imported document
    ///                that is attached to the originating `Request`.
    /// - Returns: A `Future` that is fulfilled with a `FileURLPrimaryFileCandidatePackageControllerTuple`,
    ///            or an error if it fails for some reason.
    // swiftlint:disable:next function_body_length cyclomatic_complexity
    func compileDocument(request: Request,
                         using packageController: MPManuscriptsPackageController,
                         candidate: PrimaryFileCandidate) -> Future<FileURLPrimaryFileCandidatePackageControllerTuple> {

        // Configure the `MPManuscriptCompiler` to use for compiling the document into
        // an arbitrary format based on `Request` headers
        let pressRoom = MPPressRoom()

        let outputOptions = MPManuscriptCompilerOutputOptions(cleanOnComplete: true, persistsOutput: false)
        // Default to Manuscript Project Bundle (i.e. `manuproj`) compilation if no file-extension header is present
        // swiftlint:disable:next line_length
        let fileExtension = request.http.headers.firstValue(name: Request.pressroomHeaders.targetFileExtension) ?? MediaType.manuscriptsProjectBundlePathExtension
        // Initialize the compileFormat object -
        // throw an `Abort` if the Client has passed-in an invalid fileExtension string via the `Request` headers
        guard let compileFormat = MPCompileFormat.init(forFileExtension: fileExtension) else {
            let error = PressroomError.error(for: DocumentDebuggingInfo.invalidTargetFileExtension,
                                             sourceLocation: .capture())
            return request.eventLoop.newFailedFuture(error: error)
        }
        outputOptions.compilerFormatKey = compileFormat.key
        outputOptions.staticResourcesServerPort = packageController.manuscriptListenerPort

        // Customize the outputOptions depending on the 'key' string of the inferred compile format object
        switch compileFormat.key {
        // For PDF compiling, we need to set `outputOptions.laTeXInstallationRootBookmarkData`
        // before attempting the compile operation
        case MPCompileFormatKeyPDF:
            outputOptions.laTeXInstallationRootBookmarkData = PressroomServerConfig.shared.laTeXInstallationRootBookmarkData
            pressRoom.backChannel = PressRoomBackChannel(controller: MPPressRoomController())
        case MPCompileFormatKeyJATS:
            guard let jatsVendorName = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsVendorName) else {
                let error = PressroomError.error(for: DocumentDebuggingInfo.jatsVendorNameMissing,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: error)
            }
            guard let jatsSubmissionID = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsSubmissionIdentifier) else {
                let error = PressroomError.error(for: DocumentDebuggingInfo.jatsSubmissionIdentifierMissing,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: error)
            }
            outputOptions.ingestVendorName = jatsVendorName
            outputOptions.ingestSubmissionIdentifier = jatsSubmissionID
        case MPCompileFormatKeyEMIngestMetadata:
            guard let jatsArchiveURL = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsArchiveURL) else {
                let error = PressroomError.error(for: DocumentDebuggingInfo.jatsArchiveURLMissing,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: error)
            }
            // swiftlint:disable:next line_length
            guard let jatsFileList = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsFileList)?.components(separatedBy: ",") else {
                let error = PressroomError.error(for: DocumentDebuggingInfo.jatsFileListMissing,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: error)
            }
            guard let jatsJournalCode = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsJournalCode) else {
                let error = PressroomError.error(for: DocumentDebuggingInfo.jatsJournalCodeMissing,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: error)
            }
            guard let jatsURL = request.http.headers.firstValue(name: Request.pressroomHeaders.jatsURL) else {
                let error = PressroomError.error(for: DocumentDebuggingInfo.jatsURLMissing,
                                                 sourceLocation: .capture())
                return request.eventLoop.newFailedFuture(error: error)
            }
            outputOptions.ingestArchiveURLString = jatsArchiveURL
            outputOptions.ingestFileList = jatsFileList
            outputOptions.ingestJournalCode = jatsJournalCode
            outputOptions.ingestJATSURLString = jatsURL
        default:
            break
        }

        // FIXME: Workaround for Manuscripts for Mac bug when formatting
        // document citations during compilation operations
        packageController.manuscriptsController.primaryManuscript.useNativeBiblatexCitations = false

        // Init a compiler based on the `compileFormat` determined previously
        let compiler = MPManuscriptCompiler.init(for: compileFormat,
                                                 pressRoom: pressRoom,
                                                 packageController: packageController,
                                                 outputOptions: outputOptions)

        // A `Promise` tracking the state of the document compile operation.
        let compilerPromise = request.eventLoop.newPromise(FileURLPrimaryFileCandidatePackageControllerTuple.self)

        // Compile the document (requires main-queue, depending on the specific compiler being used)
        DispatchQueue.main.async {
            // swiftlint:disable:next line_length
            compiler.compileTreeItems(packageController.sectionsController.orderedNodes) { (compiler, _, compileResultURL, mainResultDocumentURL, enclosedFileURLs, error) in

                // Fulfil the `compilerPromise`, depending on the returned 'mainResultDocumentURL' and 'error' values
                if error != nil {
                    compilerPromise.fail(error: error!)
                } else if mainResultDocumentURL != nil && compileResultURL != nil {
                    let currentEnvironment = try? Environment.detect()
                    if currentEnvironment?.isRelease == false {
                        let logger = try? request.sharedContainer.make(Logger.self)
                        logger?.debug("MAIN RESULT DOCUMENT PATH: \(mainResultDocumentURL!)")
                        logger?.debug("COMPILE RESULT DOCUMENT PATH: \(compileResultURL!)")
                        logger?.debug("ENCLOSED FILE PATHS: \(enclosedFileURLs ?? [])")
                        logger?.debug("COMPILER WORKING DIR: \(compiler.workingDirectoryURL)")
                    }

                    // If `mainResultDocumentURL` and `compileResultURL` are equal and
                    // `mainResultDocumentURL` is NOT a directory, succeed using that file URL
                    // Otherwise, the `compileResultURL` is the document container including auxiliary files
                    // (such as any Figures and a .bib file for TeX output)
                    // (Compress the container and succeed with its file URL instead)
                    var isDir: ObjCBool = false
                    if mainResultDocumentURL == compileResultURL
                        && FileManager.default.fileExists(atPath: mainResultDocumentURL!.path,
                                                          isDirectory: &isDir) && isDir.boolValue == false {
                        compilerPromise.succeed(result: (fileURL: mainResultDocumentURL!,
                                                         candidatePkgcTuple: (candidate: candidate,
                                                                              pkgc: packageController)))
                    } else {
                        do {
                            let compressedDocumentContainerFileURL = compileResultURL!.appendingPathComponent("compressed_container.zip")
                            try MPCompressor.compressDirectory(at: compileResultURL,
                                                               to: compressedDocumentContainerFileURL,
                                                               includeRootDirectory: false)
                            // `shouldCleanOnCompletion = false` prevents 'compressed_container.zip'
                            // from being removed too early
                            compiler.outputOptions.shouldCleanOnCompletion = false
                            compilerPromise.succeed(result: (fileURL: compressedDocumentContainerFileURL,
                                                             candidatePkgcTuple: (candidate: candidate,
                                                                                  pkgc: packageController)))
                        } catch {
                            // Catch any MPCompressor errors here
                            compilerPromise.fail(error: error)
                        }
                    }
                }
            }
        }

        return compilerPromise.futureResult
    }
}
