//
//  SFTPService.swift
//  App
//
//  Created by Dan Browne on 14/01/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

// MARK: - Enum and struct definitions

/// An enum describing the possible SFTP file operations for a remote server.
enum SFTPServiceFileOperation: String {
    case get
    case put
}

/// An enum describing the possible error states of `SFTPService`.
private enum SFTPServiceError: LocalizedError {
    case missingIdentityFile
    case missingLocalFile

    var errorDescription: String? {
        switch self {
        case .missingIdentityFile:
            return "There is no identity file present at the file URL provided."
        case .missingLocalFile:
            return "There is no file present at the file URL provided."
        }
    }
}

/// Uploads files to a remote server via the SFTP protocol.
protocol SFTPService: CommandLineTransformable {

    // MARK: - Remote server variables

    static var username: String { get }
    static var hostName: String { get }
    static var identityFileURL: URL { get }

    // MARK: - Public methods

    /// Uploads a file on the local system to a specified location on a remote server.
    /// - Parameters:
    ///   - localFileURL: The location of the file to upload on the local system.
    ///   - remoteFileURL: The location to upload the file to on the remote server.
    /// - Throws: An `SFTPServiceError` if any stage of the remote upload fails for some reason.
    func uploadFile(at localFileURL: URL, to remoteFileURL: URL) throws

    /// Downloads a file from a specified location on a remote server to the local system.
    /// - Parameters:
    ///   - localFileURL: The location to download the file to on the local system.
    ///   - remoteFileURL: The location to download the file from on the remote server.
    /// - Throws: An `SFTPServiceError` if any stage of the download from the remote server fails for some reason.
    func downloadFile(to localFileURL: URL, from remoteFileURL: URL) throws
}

// MARK: - Default implementations

extension SFTPService {

    func uploadFile(at localFileURL: URL, to remoteFileURL: URL) throws {
        try executeOperation(.put, localFileURL: localFileURL, remoteFileURL: remoteFileURL)
    }

    func downloadFile(to localFileURL: URL, from remoteFileURL: URL) throws {
        try executeOperation(.get, localFileURL: localFileURL, remoteFileURL: remoteFileURL)
    }

    // MARK: - Private methods

    /// Executes a SFTP file operation with respect to a local and remote file locations.
    /// - Parameters:
    ///   - operation: The `SFTPFileOperation` to execute via SFTP on the remote server.
    ///   - localFileURL: The location of a file on the local system.
    ///   - remoteFileURL: The location of a file on the remote server.
    /// - Throws: An `SFTPServiceError` if any stage of the file operation fails for some reason.
    private func executeOperation(_ operation: SFTPServiceFileOperation, localFileURL: URL, remoteFileURL: URL) throws {
        var isDir: ObjCBool = false
        guard FileManager.default.fileExists(atPath: localFileURL.path,
                                             isDirectory: &isDir)
            && isDir.boolValue == false else {
                throw SFTPServiceError.missingLocalFile
        }

        guard FileManager.default.fileExists(atPath: Self.identityFileURL.path,
                                             isDirectory: &isDir)
            && isDir.boolValue == false else {
                throw SFTPServiceError.missingIdentityFile
        }

        // Arguments ordering depends on get/put file operation
        var operationCommand = operation.rawValue
        switch operation {
        case .get:
            operationCommand += " \(remoteFileURL.path) \(localFileURL.path)"
        case .put:
            operationCommand += " \(localFileURL.path) \(remoteFileURL.path)"
        }

        let batchFileURL = localFileURL.deletingLastPathComponent().appendingPathComponent("batchfile")
        try operationCommand.write(to: batchFileURL,
                                     atomically: true,
                                     encoding: .utf8)

        _ = try executeCommand("/usr/bin/sftp",
                               arguments: ["-i",
                                           Self.identityFileURL.path,
                                           "-b",
                                           batchFileURL.path,
                                           "\(Self.username)@\(Self.hostName)"])
    }
}
