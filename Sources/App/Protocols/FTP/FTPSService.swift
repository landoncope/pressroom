//
//  FTPSService.swift
//  App
//
//  Created by Dan Browne on 27/03/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

// MARK: - Enum and struct definitions

/// An enum describing the possible FTPS file operations for a remote server.
enum FTPSServiceFileOperation: String {
    case get
    case put
}

/// An enum describing the possible error states of `FTPSService`.
private enum FTPSServiceError: LocalizedError {
    case missingLocalFile

    var errorDescription: String? {
        switch self {
        case .missingLocalFile:
            return "There is no file present at the file URL provided."
        }
    }
}

/// Uploads files to a remote server via the FTPS protocol.
protocol FTPSService: CommandLineTransformable {

    // MARK: - Remote server variables

    static var username: String { get }
    static var password: String { get }
    static var hostName: String { get }

    // MARK: - Public methods

    /// Uploads a file on the local system to a specified location on a remote server.
    /// - Parameters:
    ///   - localFileURL: The location of the file to upload on the local system.
    ///   - remoteFileURL: The location to upload the file to on the remote server.
    /// - Throws: An `FTPSServiceError` if any stage of the remote upload fails for some reason.
    func uploadFile(at localFileURL: URL, to remoteFileURL: URL) throws

    /// Downloads a file from a specified location on a remote server to the local system.
    /// - Parameters:
    ///   - localFileURL: The location to download the file to on the local system.
    ///   - remoteFileURL: The location to download the file from on the remote server.
    /// - Throws: An `FTPSServiceError` if any stage of the download from the remote server fails for some reason.
    func downloadFile(to localFileURL: URL, from remoteFileURL: URL) throws
}

// MARK: - Default implementations

extension FTPSService {

    func uploadFile(at localFileURL: URL, to remoteFileURL: URL) throws {
        try executeOperation(.put, localFileURL: localFileURL, remoteFileURL: remoteFileURL)
    }

    func downloadFile(to localFileURL: URL, from remoteFileURL: URL) throws {
        try executeOperation(.get, localFileURL: localFileURL, remoteFileURL: remoteFileURL)
    }

    // MARK: - Private methods

    /// Executes a FTPS file operation with respect to a local and remote file locations.
    /// - Parameters:
    ///   - operation: The `FTPSFileOperation` to execute via FTPS on the remote server.
    ///   - localFileURL: The location of a file on the local system.
    ///   - remoteFileURL: The location of a file on the remote server.
    /// - Throws: An `FTPSServiceError` if any stage of the file operation fails for some reason.
    private func executeOperation(_ operation: FTPSServiceFileOperation, localFileURL: URL, remoteFileURL: URL) throws {
        var isDir: ObjCBool = false
        guard FileManager.default.fileExists(atPath: localFileURL.path,
                                             isDirectory: &isDir)
            && isDir.boolValue == false else {
                throw FTPSServiceError.missingLocalFile
        }

        // Arguments depends on get/put file operation
        var ftpsArguments: [String]
        switch operation {
        case .get:
            ftpsArguments = ["--ftp-ssl",
                             remoteFileURL.absoluteString,
                             "-o",
                             localFileURL.path,
                             "--user",
                             "\(Self.username):\(Self.password)"]
        case .put:
            ftpsArguments = ["--ftp-ssl",
                             "-T",
                             localFileURL.path,
                             remoteFileURL.absoluteString,
                             "--user",
                             "\(Self.username):\(Self.password)"]
        }

        _ = try executeCommand("/usr/bin/curl",
                               arguments: ftpsArguments)
    }
}
