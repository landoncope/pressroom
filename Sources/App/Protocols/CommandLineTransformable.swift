//
//  CommandLineTransformable.swift
//  App
//
//  Created by Dan Browne on 14/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import MPFoundation
import Vapor

protocol PDFTransformable: CommandLineTransformable {

    /// A directory that is created at the same level as a given `PrimaryFileCandidate`,
    /// where the finished PDF is written to.
    static var transformedDocumentsDirectory: String { get }

    /// Transform a document represented by a `PrimaryFileCandidate` into a finished PDF file
    /// (i.e. any bibliographies and cross-references will be resolved if possible).
    ///
    /// The transformation is completed by a relevant command-line utility under-the-hood.
    ///
    /// - Parameters:
    ///   - candidate: A `PrimaryFileCandidate` representing the main file of the received document
    ///                (i.e. parsed from a received `Request`).
    /// - Returns: The file `URL` of the generated PDF after transformation, inside the temporary working directory.
    /// - Throws: A `CommandLineTransformationError` if any stage of the document transformation fails for some reason.
    func transform(_ candidate: PrimaryFileCandidate) throws -> URL
}

/// Capable of executing command-line utilities as part of a document transformation.
protocol CommandLineTransformable {

    /// The expected `String` pattern of a possible prompt for input, received via std-out. E.g. "Enter filename: ".
    typealias PromptPattern = String

    /// The command `String` to write to std-in in response to a particular command prompt. E.g. "example.docx".
    typealias CommandString = String

    typealias CommandLineResult = (standardOut: Data, standardError: Data)

    /// Determines if the command-line utility should continue executing when encountering errors
    /// (if at all possible).
    var continueOnErrors: Bool { get }

    /// Execute an installed command-line utility (with some optional arguments)
    /// that is being used as part of a document transformation.
    ///
    /// - Parameters:
    ///   - executablePath: The path to the command-line utility to be executed.
    ///   - arguments: Optional arguments that are passed to the command-line utility on execution.
    ///   - promptDictionary: Optional dictionary of possible prompts for input received via std-out,
    ///                       and the associated command strings to write to std-in in response.
    ///                       E.g. ["Enter file name: ": "example.docx"].
    ///                       Pattern matching for the prompt string is via checking the suffix of std-out data chunks,
    ///                       since the prompt for input should be at the end of the string.
    ///   - environment: Optional environment dictionary for the process running the command. If this isn't used,
    ///                  the environment is inherited from the process that executed this method.
    /// - Returns: `Data` representation of the resulting output of the transformation.
    /// - Throws: A `CommandLineTransformableError` if some part of the execution fails for some reason.
    func executeCommand(_ executablePath: String,
                        arguments: [String]?,
                        promptDictionary: [PromptPattern: CommandString]?,
                        environment: [String: String]?) throws -> CommandLineResult

    /// Creates a temporary working directory to hold intermediate files
    /// generated as part of the document transformation.
    ///
    /// - Returns: The file `URL` to the created working directory.
    /// - Throws: A `CommandLineTransformableError` if the directory creation fails for some reason.
    func createWorkingDirectory() throws -> URL

    /// Writes the `Data` representation of a document to a given file `URL`.
    ///
    /// - Parameters:
    ///   - documentData: the `Data` representation of the document to write to a file.
    ///   - fileURL: The file `URL` specifying where the document `Data` should be written
    ///              (e.g. relative to a temporary working directory).
    /// - Throws: A `CommandLineTransformableError` if writing the data to the given file `URL` fails for some reason.
    func write(_ documentData: Data, to fileURL: URL) throws

    /// Clean up the temporary working directory at the given file `URL`.
    ///
    /// - Parameter workingDirectoryFileURL: The file `URL` to the temporary working directory.
    func cleanWorkingDirectory(at workingDirectoryFileURL: URL)
}

/// An enum describing the possible error states of the service.
///
/// - workingDirectoryCreationFailure: The temporary working directory creation failed for some reason.
/// - temporaryFileWriteFailure: Writing the document data that was passed in to the service
///                              to a temporary file failed for some reason.
/// - promptStringInvalid: The command prompt string cannot be represented as an UTF-8 `String`.
/// - commandDataInvalid: The command string to pass to std-in cannot be represented as `Data`.
/// - terminationErrorCodeUncaughtSignal: The command-line task executed by the service
///                                       terminated with an uncaught-signal error.
/// - terminationErrorNonZeroExit: The command-line task executed by the service terminated with a non-zero exit code.
/// - terminationErrorTimedOut: The command-line task executed by the service did not terminate
///                             within a 60 second timeout period.
enum CommandLineTransformableError: LocalizedError {
    // Document persistence
    case workingDirectoryCreationFailure
    case temporaryFileWriteFailure

    // Standard-in
    case promptStringInvalid
    case commandDataInvalid

    // Task termination
    case terminationErrorCodeUncaughtSignal(status: Int32)
    case terminationErrorNonZeroExit(status: Int32, errorOutput: String?)
    case terminationErrorTimedOut

    /// A custom error description to provide more context for the error (i.e. when using `error.localizedDescription`).
    var errorDescription: String? {
        switch self {
        case .workingDirectoryCreationFailure:
            return "The command line operation couldn't be completed. The working directory failed to be created."
        case .temporaryFileWriteFailure:
            return "The command line operation couldn't be completed. A temporary file failed to write to disk."
        case .promptStringInvalid:
            return "The command prompt (i.e. std-out) cannot be represented as an UTF-8 string."
        case .commandDataInvalid:
            return "The command string to pass to std-in cannot be represented as Data."
        case .terminationErrorCodeUncaughtSignal(status: let status):
            return """
            The command line operation couldn't be completed. \
            It terminated due to an uncaught signal with status code: \(status).
            """
        case .terminationErrorNonZeroExit(status: let status, errorOutput: let errorOutput):
            if errorOutput != nil {
                return """
                The command line operation couldn't be completed.
                It terminated with status code: \(status), and errors: \(errorOutput!).
                """
            } else {
                return "The command line operation couldn't be completed.\nIt terminated with status code: \(status)."
            }
        case .terminationErrorTimedOut:
            return "The command line operation couldn't be completed. It timed-out before it completed its execution."
        }
    }
}

// MARK: - Default implementations

extension PDFTransformable {

    static var transformedDocumentsDirectory: String {
        return "transformed_documents"
    }
}

extension CommandLineTransformable {

    // swiftlint:disable:next function_body_length
    func executeCommand(_ executablePath: String,
                        arguments: [String]? = nil,
                        promptDictionary: [PromptPattern: CommandString]? = nil,
                        environment: [String: String]? = nil) throws -> CommandLineResult {
        let dispatchGroup = DispatchGroup()
        let task = Process()
        let stdOutPipe = Pipe()
        let stdErrPipe = Pipe()
        var stdInPipe: Pipe?
        var stdInError: CommandLineTransformableError?
        var outData = Data()
        var errData = Data()

        // Configure command-line utility task
        task.launchPath = executablePath
        if arguments != nil {
            task.arguments = arguments
        }
        task.standardOutput = stdOutPipe
        task.standardError = stdErrPipe

        // Configure std-in if a promptDictionary is present
        if promptDictionary != nil {
            stdInPipe = Pipe()
            task.standardInput = stdInPipe
        }

        // If a customized environment was passed in, apply it to the process
        if environment != nil {
            task.environment = environment
        }

        stdOutPipe.fileHandleForReading.readabilityHandler = { fileHandle in
            let availableData = fileHandle.availableData
            outData.append(availableData)

            // Respond to potential command prompts on std-out if `promptDictionary` is set
            if promptDictionary != nil {
                do {
                    guard let commandData = try self.commandData(correspondingTo: availableData,
                                                                 promptDictionary: promptDictionary!) else {
                                                                    return
                    }
                    task.suspend()
                    stdInPipe?.fileHandleForWriting.write(commandData)
                    task.resume()
                } catch {
                    task.terminate()
                    stdInError = error as? CommandLineTransformableError
                }
            }
        }

        stdErrPipe.fileHandleForReading.readabilityHandler = { fileHandle in
            errData.append(fileHandle.availableData)
        }

        dispatchGroup.enter()
        task.terminationHandler = { task in
            stdOutPipe.fileHandleForReading.readabilityHandler = nil
            stdErrPipe.fileHandleForReading.readabilityHandler = nil
            stdInPipe?.fileHandleForWriting.readabilityHandler = nil
            dispatchGroup.leave()
        }

        task.launch()

        // Record time-out result and errorOutput
        // (Checks for an optional env var, and sets the timeout to 60 seconds if it's not been set)
        let envVar = Environment.get("MPPressroomServerCommandLineUtilityTransformationTimeout")
        let timeout = envVar != nil ? Double(envVar!) : 60
        let timeoutResult = dispatchGroup.wait(timeout: DispatchTime.now() + timeout!)
        let errorOutput = errData.count > 0 ? String(data: errData, encoding: .utf8) : nil

        // Check for time-out
        if timeoutResult == .timedOut {
            throw CommandLineTransformableError.terminationErrorTimedOut
        }

        // Command-line task error-handling
        if task.terminationReason == .uncaughtSignal {
            if let stdInError = stdInError {
                throw stdInError
            } else {
                throw CommandLineTransformableError.terminationErrorCodeUncaughtSignal(status: task.terminationStatus)
            }
        }

        if task.terminationStatus != 0 {
            throw CommandLineTransformableError.terminationErrorNonZeroExit(status: task.terminationStatus,
                                                                            errorOutput: errorOutput)
        }

        return CommandLineResult(outData, errData)
    }

    func createWorkingDirectory() throws -> URL {
        let workingDirectoryFileURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(UUID().uuidString),
                                          isDirectory: true)
        do {
            try FileManager.default.createDirectory(at: workingDirectoryFileURL,
                                                    withIntermediateDirectories: true,
                                                    attributes: nil)
            return workingDirectoryFileURL
        } catch {
            throw CommandLineTransformableError.workingDirectoryCreationFailure
        }
    }

    func write(_ documentData: Data, to workingDirectoryFileURL: URL) throws {
        do {
            try documentData.write(to: workingDirectoryFileURL, options: .atomic)
        } catch {
            throw CommandLineTransformableError.temporaryFileWriteFailure
        }
    }

    func cleanWorkingDirectory(at workingDirectoryFileURL: URL) {
        do {
            var isDir: ObjCBool = false
            if FileManager.default.fileExists(atPath: workingDirectoryFileURL.path,
                                              isDirectory: &isDir) && isDir.boolValue == true {
                try FileManager.default.removeItem(at: workingDirectoryFileURL)
            }
        } catch {
            // This is a non-critical error, so log to std_err only, rather than throw an `Error`
            // swiftlint:disable:next line_length
            printStdErr("COMMAND_LINE_UTILITY_TRANSFORMATION_ERROR: Failed to delete temporary working directory - \(workingDirectoryFileURL)")
        }
    }

    // MARK: - Private methods

    /// Determine if the std-out stream of a `Process` contains a command prompt for input
    /// (matching an entry in `promptDictionary`) and return the `Data` representation
    /// of the corresponding command `String` to execute.
    ///
    /// - Parameters:
    ///   - potentialCommandPrompt: `Data` from the std-out of a `Process`, which may contain a prompt for input.
    ///   - promptDictionary: Optional dictionary of possible prompts for input received via std-out,
    ///                       and the associated command strings to write to std-in in response.
    ///                       E.g. ["Enter file name: ": "example.docx"].
    ///                       Pattern matching for the prompt string is via checking the suffix of std-out data chunks,
    ///                       since the prompt for input should be at the end of the string.
    /// - Returns: A `Data` representation of the command `String` corresponding to the identified prompt,
    ///            or `nil` if no prompt exists in `potentialCommandPrompt`.
    /// - Throws: A `CommandLineTransformableError` if there was a problem encoding `potentialCommandPrompt` to
    /// a `String` or encoding an identified command to `Data`.
    private func commandData(correspondingTo potentialCommandPrompt: Data,
                             promptDictionary: [PromptPattern: CommandString]) throws -> Data? {
        var stdOutBuffer = [String]()

        guard let stdOutChunk = String(data: potentialCommandPrompt, encoding: .utf8) else {
            throw CommandLineTransformableError.promptStringInvalid
        }

        // Accumulate the current and previous std-out stream chunks to mitigate alignment issues
        // when testing the suffix of the std-out stream against possible prompt patterns below
        stdOutBuffer.append(stdOutChunk)
        if stdOutBuffer.count > 2 {
            stdOutBuffer.removeFirst()
        }

        // Write command to std-in that is associated with the given prompt pattern
        if let command = promptDictionary.first(where: { stdOutBuffer.joined().hasSuffix($0.key) })?.value {
            guard let commandData = command.data(using: .utf8) else {
                throw CommandLineTransformableError.commandDataInvalid
            }

            return commandData
        }

        return nil
    }
}
