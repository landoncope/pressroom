//
//  PropertyIterable.swift
//  App
//
//  Created by Dan Browne on 22/01/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

protocol PropertyIterable {
    /// The values of properties contained within the receiver that are of a given type.
    ///
    /// - Parameter returnType: The Type of properties whose values should be returned.
    /// - Returns: An array of property values contained within the receiver that are of type `returnType`.
    func propertyValues<T>(returnType: T.Type) -> [T]
}

extension PropertyIterable {
    func propertyValues<T>(returnType: T.Type) -> [T] {
        let mirror = Mirror(reflecting: self)
        // swiftlint:disable:next force_cast
        return mirror.children.filter { $0.value is T }.map { $0.value as! T }
    }
}
