//
//  ContentTransformable.swift
//  App
//
//  Created by Dan Browne on 17/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import MPFoundation

/// A tuple, containing decoded document content and a file URL to a temporary working directory
/// where transformation operations can be performed on the
/// document content before those files are cleaned up later.
typealias PersistableContent<T> = (content: T, workingDirectory: URL) where T: Content

protocol ContentPersistable {

    /// The name used for the directory containing the contents of a decompressed content container.
    static var decompressedContainerDirectoryName: String { get }

    /// Decode the content attached to a given `Request` according to a given `Content` Type
    /// and generate a proposed temporary ingest working directory where it can be stored on disk.
    ///
    /// - Parameters:
    ///   - request: The `Request` containing attached content to be decoded and persisted to disk.
    ///   - contentType: A `Content` Type specifying how the content should be decoded.
    /// - Returns: A `PersistableContent` tuple, containing the decoded content
    ///            and its proposed temporary ingest working directory.
    /// - Throws: An `Error` if either the content decode or file URL generation steps fail for some reason.
    func decodeContent<T>(in request: Request,
                          contentType: T.Type) throws -> Future<PersistableContent<T>> where T: Content

    /// Encode some `Content` to a `PersistableContent` wrapped type.
    /// - Parameter content: Some `Content` object.
    /// - Returns: A `PersistableContent` tuple, containing the decoded content
    ///            and its proposed temporary ingest working directory.
    /// - Throws: An `Error` if the working directory file URL generation fails for some reason.
    func encodeContent<T>(from content: T) throws -> PersistableContent<T> where T: Content

    /// Decompress a compressed content container that is already located on disk.
    ///
    /// - Parameters:
    ///   - compressedContent: `PersistableContent`, containing the decoded content and the file URL
    ///                        for the proposed temporary ingest working directory on disk.
    /// - Returns: The file URL for the directory containing the content in its decompressed state.
    /// - Throws: An `Error` if the content container could not be decompressed correctly.
    func decompressContent(_ compressedContent: PersistableContent<ArbitrarilyFormattedDocument>) throws -> URL

    /// Persist content to a temporary ingest working directory on disk.
    ///
    /// - Parameters:
    ///   - content: `PersistableContent` that was previously decoded and has a
    ///              proposed temporary ingest working directory.
    ///   - fileURL: The location where the `Content` will be persisted.
    /// - Throws: An `Error` if the content persistence fails for some reason.
    //    func persistContent(_ content: PersistableContent) throws

    /// Clean up the temporary files inside the specified content ingest working directory.
    ///
    /// - Parameter ingestWorkingDirectory: The file URL representing the content ingest working directory to clean up.
    /// - Returns: A `Bool` indicating if the clean up operation succeeded
    ///            (failure is non-critical, so no error is thrown).
    func cleanTemporaryContent(at ingestWorkingDirectory: URL) -> Bool
}

protocol ContentIteratable {
    /// The contents of a decompressed document container, via a deep traversal of its directory structure.
    ///
    /// - Parameter decompressedContainerFileURL: The file `URL` to the decompressed document container.
    /// - Returns: A flat array of file `URL`s representing the container contents,
    ///            including sub-directory contents (but ignoring the directories themselves).
    func decompressedContainerContents(for decompressedContainerFileURL: URL) -> [URL]
}

protocol ContentTransformable {

    /// Stream the contents of a file back to the Client in a chunked Response.
    ///
    /// - Parameters:
    ///   - fileURL: The location of the content on disk to stream back to the Client.
    ///   - request: The originating `Request` (this is required to construct a corresponding `Response`).
    /// - Returns: A chunked `Response` that will file-stream the file contents back to the Client.
    /// - Throws: An error if the file-stream `Response` fails for some reason.
    func chunkedResponse(forFileContentsAt fileURL: URL, request: Request) throws -> Response

    /// Handle any reported errors from operations that occurred as a result of
    /// internal operations during content transformation.
    ///
    /// - Parameter contentTransformationError: An `Error` that has occurred.
    /// - Throws: An `Abort` representing any errors that may have occurred during document transformation operations.
    func handleAbort(for contentTransformationError: Error) throws
}

// MARK: - Default implementations

extension ContentPersistable {

    static var decompressedContainerDirectoryName: String {
        return "decompressed_container"
    }

    func decodeContent<T>(in request: Request,
                          contentType: T.Type) throws -> Future<PersistableContent<T>> where T: Content {
        return try request.content.decode(T.self).thenThrowing { decodedContent -> PersistableContent<T> in
            return try self.encodeContent(from: decodedContent)
        }
    }

    func encodeContent<T>(from content: T) throws -> PersistableContent<T> where T: Content {
        let workingDirectoryFileURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(UUID().uuidString),
                                          isDirectory: true)
        try FileManager.default.createDirectory(at: workingDirectoryFileURL,
                                                withIntermediateDirectories: true,
                                                attributes: nil)

        return (content: content, workingDirectory: workingDirectoryFileURL)
    }

    func decompressContent(_ compressedContent: PersistableContent<ArbitrarilyFormattedDocument>) throws -> URL {
        // Decompress the content container to a subdirectory of the working directory
        // where the container is currently located
        let decompressedContentFileURL = compressedContent.workingDirectory.appendingPathComponent(Self.decompressedContainerDirectoryName)
        let fileManager = FileManager.default
        try fileManager.createDirectory(at: decompressedContentFileURL,
                                        withIntermediateDirectories: true,
                                        attributes: nil)

        let compressedContentFileURL = compressedContent.workingDirectory.appendingPathComponent(compressedContent.content.file.filename)
        try compressedContent.content.file.data.write(to: compressedContentFileURL, options: .atomic)
        try MPDecompressor.decompressZIP(at: compressedContentFileURL, to: decompressedContentFileURL)

        // Remove compressed container (since it has now been decompressed and is redundant)
        _ = cleanTemporaryContent(at: compressedContentFileURL)

        // Flatten `decompressedContentFileURL` if it only contains a nested directory
        var isDir: ObjCBool = false
        let dirContents = try fileManager.contentsOfDirectory(at: decompressedContentFileURL,
                                                              includingPropertiesForKeys: nil,
                                                              options: .skipsHiddenFiles).filter { fileURL -> Bool in
                                                                // Exclude potential "__MACOSX" directories
                                                                fileURL.lastPathComponent != "__MACOSX"
        }

        if let potentialDir = dirContents.first,
            dirContents.count == 1,
            fileManager.fileExists(atPath: potentialDir.path,
                                   isDirectory: &isDir),
            isDir.boolValue == true {
            // Flatten the contents of the nested directory to the level above
            let nestedDirectoryContents = try fileManager.contentsOfDirectory(at: potentialDir,
                                                                              includingPropertiesForKeys: nil)
            for fileURL in nestedDirectoryContents {
                let flattenedFileURL = decompressedContentFileURL.appendingPathComponent(fileURL.lastPathComponent)
                try fileManager.moveItem(at: fileURL,
                                         to: flattenedFileURL)
            }

            // Delete the now-empty nested directory
            try fileManager.removeItem(at: potentialDir)
        }

        return decompressedContentFileURL
    }

    func cleanTemporaryContent(at ingestWorkingDirectory: URL) -> Bool {
        do {
            var directoryToCleanup: URL!
            if ingestWorkingDirectory.lastPathComponent == Self.decompressedContainerDirectoryName {
                directoryToCleanup = ingestWorkingDirectory.deletingLastPathComponent()
            } else {
                directoryToCleanup = ingestWorkingDirectory
            }
            try FileManager.default.removeItem(at: directoryToCleanup)

            return true
        } catch {
            // This is a non-critical error, so log to std_err only, rather than throw an `Error`
            printStdErr("RECEIVED_DOCUMENT_ERROR: Failed to delete ingest working directory - \(ingestWorkingDirectory)")
            return false
        }
    }
}

extension ContentIteratable {

    func decompressedContainerContents(for decompressedContainerFileURL: URL) -> [URL] {
        let directoryEnumerator = FileManager.default.enumerator(at: decompressedContainerFileURL,
                                                                 includingPropertiesForKeys: nil,
                                                                 options: .skipsHiddenFiles)
        // swiftlint:disable:next force_cast
        return (directoryEnumerator?.allObjects as! [URL]).filter {
            var isDir: ObjCBool = false
            return FileManager.default.fileExists(atPath: $0.path, isDirectory: &isDir) && isDir.boolValue == false
        }
    }
}

extension ContentTransformable {

    func chunkedResponse(forFileContentsAt fileURL: URL, request: Request) throws -> Response {
        let response = request.response()
        response.http = try request.fileio().chunkedResponse(file: fileURL.path, for: request.http)
        response.http.contentType = MediaType.type(for: fileURL.pathExtension)

        return response
    }

    func handleAbort(for contentTransformationError: Error) throws {
        if let pressroomError = contentTransformationError as? PressroomError {
            throw pressroomError
        }

        // Fallback `Abort` error if the `Error` cannot be parsed as a `PressroomError` for some reason
        throw Abort(.internalServerError, reason: String(describing: contentTransformationError))
    }
}
