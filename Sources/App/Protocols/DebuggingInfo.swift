//
//  DebuggingInfo.swift
//  App
//
//  Created by Dan Browne on 09/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor
import Swiftgger

/// Protocol for `RawRepresentable` types that represent some debugging information.
protocol DebuggingInfo: RawRepresentable, CaseIterable {
    var status: HTTPResponseStatus { get }
    var occurrenceClass: AnyClass { get }
    var reason: String { get }
    var possibleCauses: [String] { get }
    var suggestedFixes: [String] { get }

    static var apiResponses: [APIResponse] { get }
}

extension DebuggingInfo {

    static var apiResponses: [APIResponse] {
        var casesByStatus = [UInt: [Self]]()

        for enumCase in allCases {
            if casesByStatus[enumCase.status.code] == nil {
                casesByStatus[enumCase.status.code] = []
            }
            casesByStatus[enumCase.status.code]!.append(enumCase)
        }

        return casesByStatus.enumerated().map {
            APIResponse(code: String($0.element.key),
                        description: """
                Error response notifying the Client that there was a problem handling the request. \
                The error could be one of the following cases: \($0.element.value.map { $0.rawValue }).
                """)
        }
    }
}
