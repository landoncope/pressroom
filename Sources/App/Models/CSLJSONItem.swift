//
//  BibliographicCSLJSON.swift
//  App
//
//  Created by Dan Browne on 18/10/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

/// A CSL-JSON item as a `Content` conforming struct
/// (as defined in the official csl-json schema:
/// https://github.com/citation-style-language/schema/blob/master/csl-data.json).
struct CSLJSONItem: Content {

    /// Definition of the valid options for the `type` field of a `CSLJSONItem`.
    enum ItemType: String, Codable {
        case article
        case articleJournal = "article-journal"
        case articleMagazine = "article-magazine"
        case articleNewspaper = "article-newspaper"
        case bill
        case book
        case broadcast
        case chapter
        case dataset
        case entry
        case entryDictionary = "entry-dictionary"
        case entryEncyclopedia = "entry-encyclopedia"
        case figure
        case graphic
        case interview
        case legalCase = "legal_case"
        case legislation
        case manuscript
        case map
        case motionPicture = "motion_picture"
        case musicalScore = "musical_score"
        case pamphlet
        case paperConference = "paperConference"
        case patent
        case personalCommunication = "personal_communication"
        case post
        case postWeblog = "post-weblog"
        case report
        case review
        case reviewBook = "review-book"
        case song
        case speech
        case thesis
        case treaty
        case webpage
    }

    /// Definition of the `CSLJSONItem` person name object,
    /// instances of which can be present in several fields of the item JSON.
    struct ItemPersonName: Codable {
        var family: String?
        var given: String?
        var droppingParticle: String?
        var nonDroppingParticle: String?
        var suffix: String?
        var commaSuffix: StringIntBool?
        var staticOrdering: StringIntBool?
        var literal: String?
        var parseNames: StringIntBool?
    }

    /// Definition of the `CSLJSONItem` date object,
    /// instances of which can be present in several fields of the item JSON.
    struct ItemDate: Codable {
        var dateParts: [[StringIntBool]]?
        var season: StringInt?
        var circa: StringIntBool?
        var literal: String?
        var raw: String?
    }

    /// Definition of a type where a value for a CSL-JSON key can be a `String` or an `Int`.
    ///
    /// - string: The `String` case.
    /// - int: The `Int` case.
    enum StringInt {
        case string(String)
        case int(Int)
    }

    /// Definition of a type where a value for a CSL-JSON key can be a `String`, `Int` or `Bool`.
    ///
    /// - string: The `String` case.
    /// - int: The `Int` case.
    /// - bool: The `Bool` case.
    enum StringIntBool {
        case string(String)
        case int(Int)
        case bool(Bool)
    }

    // swiftlint:disable:next identifier_name
    var id: String
    var type: ItemType

    var categories: [String]?
    var language: String?
    var journalAbbreviation: String?
    var shortTitle: String?

    var author: [ItemPersonName]?
    var collectionEditor: [ItemPersonName]?
    var composer: [ItemPersonName]?
    var containerAuthor: [ItemPersonName]?
    var director: [ItemPersonName]?
    var editor: [ItemPersonName]?
    var editorialDirector: [ItemPersonName]?
    var interviewer: [ItemPersonName]?
    var illustrator: [ItemPersonName]?
    var originalAuthor: [ItemPersonName]?
    var recipient: [ItemPersonName]?
    var reviewedAuthor: [ItemPersonName]?
    var translator: [ItemPersonName]?

    var accessed: ItemDate?
    var container: ItemDate?
    var eventDate: ItemDate?
    var issued: ItemDate?
    var originalDate: ItemDate?
    var submitted: ItemDate?

    var abstract: String?
    var annote: String?
    var archive: String?
    var archiveLocation: String?
    var archivePlace: String?
    var authority: String?
    var callNumber: String?
    var chapterNumber: String?
    var citationNumber: String?
    var citationLabel: String?
    var collectionNumber: String?
    var collectionTitle: String?
    var containerTitle: String?
    var containerTitleShort: String?
    var dimensions: String?
    var DOI: String?

    var edition: StringInt?

    var event: String?
    var eventPlace: String?
    var firstReferenceNoteNumber: String?
    var genre: String?
    var ISBN: String?
    var ISSN: String?

    var issue: StringInt?

    var jurisdiction: String?
    var keyword: String?
    var locator: String?
    var medium: String?
    var note: String?

    var number: StringInt?

    var numberOfPages: String?

    var numberOfVolumes: StringInt?

    var originalPublisher: String?
    var originalPublisherPlace: String?
    var originalTitle: String?
    var page: String?
    var pageFirst: String?
    var PMCID: String?
    var PMID: String?
    var publisher: String?
    var publisherPlace: String?
    var references: String?
    var reviewedTitle: String?
    var scale: String?
    var section: String?
    var source: String?
    var status: String?
    var title: String?
    var titleShort: String?
    var URL: String?
    var version: String?

    var volume: StringInt?

    var yearSuffix: String?
}

// MARK: - Manual Codable conformances for indeterminate types

extension CSLJSONItem.StringInt: Codable {
    enum CodingKeys: String, CodingKey {
        case string, int
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let stringValue = try? container.decode(String.self) {
            self = .string(stringValue)
        } else if let intValue = try? container.decode(Int.self) {
            self = .int(intValue)
        } else {
            throw DecodingError.typeMismatch(CSLJSONItem.StringInt.self,
                                             DecodingError.Context(codingPath: container.codingPath,
                                                                   debugDescription: "Object should be a String or an Int!"))
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let value):
            try container.encode(value)
        case .int(let value):
            try container.encode(value)
        }
    }
}

extension CSLJSONItem.StringIntBool: Codable {
    enum CodingKeys: String, CodingKey {
        case string, int, bool
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let stringValue = try? container.decode(String.self) {
            self = .string(stringValue)
        } else if let intValue = try? container.decode(Int.self) {
            self = .int(intValue)
        } else if let boolValue = try? container.decode(Bool.self) {
            self = .bool(boolValue)
        } else {
            throw DecodingError.typeMismatch(CSLJSONItem.StringIntBool.self,
                                             DecodingError.Context(codingPath: container.codingPath,
                                                                   debugDescription: "Object should be a String, an Int or a Bool!"))
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let value):
            try container.encode(value)
        case .int(let value):
            try container.encode(value)
        case .bool(let value):
            try container.encode(value)
        }
    }
}
