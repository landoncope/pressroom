//
//  PressroomError.swift
//  App
//
//  Created by Dan Browne on 08/11/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

/// An error generated by Pressroom, which will be displayed to the end-user Client via an HTTP `Response`.
struct PressroomError: AbortError, Codable {
    // `AbortError` properties
    private(set) var status: HTTPResponseStatus

    // `Debuggable` properties
    private(set) var typeIdentifier: String
    private(set) var identifier: String
    private(set) var reason: String
    private(set) var sourceLocation: SourceLocation?
    private(set) var stackTrace: [String]?
    private(set) var possibleCauses: [String] = []
    private(set) var suggestedFixes: [String] = []
    private(set) var documentationLinks: [String] = []
    private(set) var stackOverflowQuestions: [String] = []
    private(set) var gitHubIssues: [String] = []

    /// Optional underlying error which could provide more context for debugging purposes.
    let underlyingError: Error?

    // Ignores the `status` property, since it will already be attached to the `Response` corresponding to this error
    enum CodingKeys: String, CodingKey {
        case typeIdentifier = "category"
        case identifier
        case reason
        case sourceLocation
        case stackTrace
        case possibleCauses
        case suggestedFixes
        case documentationLinks
        case stackOverflowQuestions
        case gitHubIssues
        case underlyingError
    }

    init(_ status: HTTPResponseStatus,
         typeIdentifier: String,
         identifier: String,
         reason: String,
         sourceLocation: SourceLocation,
         possibleCauses: [String] = [],
         suggestedFixes: [String] = [],
         documentationLinks: [String] = [],
         stackOverflowQuestions: [String] = [],
         gitHubIssues: [String] = [],
         underlyingError: Error? = nil) {
        self.status = status
        self.typeIdentifier = typeIdentifier
        self.identifier = identifier
        self.reason = reason
        self.underlyingError = underlyingError

        // Only initialize the following properties if the current environment is NOT release!
        // (Only want to expose this info in `Response`s from non-prod deployments)
        guard let currentEnvironment = try? Environment.detect() else {
            return
        }
        if !currentEnvironment.isRelease {
            self.sourceLocation = sourceLocation
            self.stackTrace = PressroomError.makeStackTrace()

            // These properties have default parameter values of `[]` because their `Debuggable` implementations
            // which are being overriden here are non-optional but we want the option
            // of them to not be specified in a call to `init()`.
            self.possibleCauses = possibleCauses
            self.suggestedFixes = suggestedFixes
            self.documentationLinks = documentationLinks
            self.stackOverflowQuestions = stackOverflowQuestions
            self.gitHubIssues = gitHubIssues
        }
    }

    // MARK: - Encodable and Decodable conformance

    // Decodable conformance for unit testing
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.status = .internalServerError
        self.typeIdentifier = try container.decode(String.self, forKey: .typeIdentifier)
        self.identifier = try container.decode(String.self, forKey: .identifier)
        self.reason = try container.decode(String.self, forKey: .reason)
        self.sourceLocation = nil
        self.stackTrace = try container.decode(Array<String>.self, forKey: .stackTrace)

        // Optional properties
        if let possibleCauses = try? container.decode(Array<String>.self, forKey: .possibleCauses) {
            self.possibleCauses = possibleCauses
        }
        if let suggestedFixes = try? container.decode(Array<String>.self, forKey: .suggestedFixes) {
            self.suggestedFixes = suggestedFixes
        }
        if let documentationLinks = try? container.decode(Array<String>.self, forKey: .documentationLinks) {
            self.documentationLinks = documentationLinks
        }
        if let stackOverflowQuestions = try? container.decode(Array<String>.self, forKey: .stackOverflowQuestions) {
            self.stackOverflowQuestions = stackOverflowQuestions
        }
        if let gitHubIssues = try? container.decode(Array<String>.self, forKey: .gitHubIssues) {
            self.gitHubIssues =  gitHubIssues
        }
        self.underlyingError = nil
    }

    // Synthesized `Encodable` conformance not possible because `Error` does not conform to it,
    // so the `localizedDescription` of the `Error` is encoded instead.
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(typeIdentifier, forKey: .typeIdentifier)
        try container.encode(identifier, forKey: .identifier)
        try container.encode(reason, forKey: .reason)

        // Only encode these properties if they are non-nil or non-empty arrays
        if sourceLocation != nil {
            try container.encode(sourceLocation, forKey: .sourceLocation)
        }
        if stackTrace != nil {
            try container.encode(stackTrace, forKey: .stackTrace)
        }
        if possibleCauses.count > 0 {
            try container.encode(possibleCauses, forKey: .possibleCauses)
        }
        if suggestedFixes.count > 0 {
            try container.encode(suggestedFixes, forKey: .suggestedFixes)
        }
        if documentationLinks.count > 0 {
            try container.encode(documentationLinks, forKey: .documentationLinks)
        }
        if stackOverflowQuestions.count > 0 {
            try container.encode(stackOverflowQuestions, forKey: .stackOverflowQuestions)
        }
        if gitHubIssues.count > 0 {
            try container.encode(gitHubIssues, forKey: .gitHubIssues)
        }
        if underlyingError != nil {
            var errorString = String(describing: underlyingError!)
            let localizedDescription = underlyingError!.localizedDescription
            if errorString != localizedDescription {
                errorString += " -- \(localizedDescription)"
            }
            try container.encode(errorString, forKey: .underlyingError)
        }
    }

    /// Returns a `PressroomError` based on some debugging information.
    ///
    /// - Parameters:
    ///   - debuggingInfo: An `DebuggingInfo` type containing in-depth debugging information
    ///                    (implicitly a `RawRepresentable` type with a `RawValue` of `String`).
    ///   - sourceLocation: A `SourceLocation` giving information on where the error occurred within the source code
    ///                     (use `.capture()` here for call-site information).
    ///   - documentationLinks: An optional array of string `URL`s linking to documentation pertaining to the error.
    ///   - stackOverflowQuestions: An optional array of string `URL`s linking to related Stack Overflow questions.
    ///   - gitHubIssues: An optional array of string `URL`s linking to related issues on Vapor's GitHub repo.
    ///   - underlyingError: An optional underlying `Error` giving more context if possible.
    /// - Returns: An initialized `PressroomError` object.
    static func error<T>(for debuggingInfo: T,
                         sourceLocation: SourceLocation,
                         documentationLinks: [String]? = nil,
                         questions: [String]? = nil,
                         gitHubIssues: [String]? = nil,
                         underlyingError: Error? = nil) -> PressroomError where T: DebuggingInfo, T.RawValue == String {
        return PressroomError(debuggingInfo.status,
                              typeIdentifier: "\(debuggingInfo.occurrenceClass)Error",
                              identifier: debuggingInfo.rawValue,
                              reason: debuggingInfo.reason,
                              sourceLocation: sourceLocation,
                              possibleCauses: debuggingInfo.possibleCauses,
                              suggestedFixes: debuggingInfo.suggestedFixes,
                              documentationLinks: documentationLinks != nil ? documentationLinks! : [String](),
                              stackOverflowQuestions: questions != nil ? questions! : [String](),
                              gitHubIssues: gitHubIssues != nil ? gitHubIssues! : [String](),
                              underlyingError: underlyingError)
    }
}

// MARK: - SourceLocation Encodable conformance

extension SourceLocation: Encodable {

    // Ignores the 'range' property, which is not `Encodable`
    enum CodingKeys: String, CodingKey {
        case file
        case function
        case line
        case column
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(file, forKey: .file)
        try container.encode(function, forKey: .function)
        try container.encode(line, forKey: .line)
        try container.encode(column, forKey: .column)
    }
}
