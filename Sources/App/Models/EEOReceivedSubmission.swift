//
//  EEOReceivedSubmission.swift
//  App
//
//  Created by Dan Browne on 08/04/2020.
//
//  ---------------------------------------------------------------------------
//
//  © 2020 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

// swiftlint:disable nesting

/// Represents a submission received by the EEO deposit service,
/// as reported back via a `POST` to the `v1/receive/submission` endpoint.
struct EEOReceivedSubmission: Content {

    struct Metadata: Content {

        struct Author: Content {

            struct Affiliation: Content {

                let institutionId: String?
                let institutionType: String?
                let institutionName: String
                let city: String
                let zipCode: String?
                let state: String?
                let country: String
                let department: String?
                let personTitle: String?
                let room: String?
                let phone: String?
                let fax: String?
                let address: [String]?
            }

            let salutation: String?
            let firstName: String
            let lastName: String
            let email: String?
            let isCorresponding: Bool
            let affiliations: [Affiliation]
        }

        let doi: String
        let manuscriptId: String?
        let manuscriptTitle: String?
        let journalTitle: String
        let journalAbbreviation: String?
        let digitalISSN: String
        let submissionStatus: String
        let keywords: [String]?
        let authors: [Author]
    }

    struct Attachment: Content, Hashable {

        let name: String
        let format: String
        let designation: String
        let url: String
        let urlValiditySeconds: String
    }

    let depositoryCode: String
    let importEntryId: Int
    let metadata: Metadata
    let attachments: [Attachment]
}
