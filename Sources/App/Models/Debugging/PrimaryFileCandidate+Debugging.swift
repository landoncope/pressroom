//
//  PrimaryFileCandidate+Debugging.swift
//  App
//
//  Created by Dan Browne on 12/07/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

extension PrimaryFileCandidate {

    /// The debugging information for a given primary file candidate determination error
    enum PrimaryFileCandidateDebuggingInfo: String, DebuggingInfo {
        case fileURLNilDueToUnderlyingError
        case invalidPrimaryFile
        case emptyDocumentContainerAttached
        case multiplePrimaryFileCandidatesFound

        var status: HTTPResponseStatus {
            switch self {
            case .fileURLNilDueToUnderlyingError,
                 .invalidPrimaryFile,
                 .emptyDocumentContainerAttached,
                 .multiplePrimaryFileCandidatesFound:
                return .badRequest
            }
        }

        var occurrenceClass: AnyClass {
            return PrimaryFileCandidate.self
        }

        var reason: String {
            switch self {
            case .fileURLNilDueToUnderlyingError:
                return """
                Reading the file URL failed because of an underlying error condition when \
                determining the primary file candidate!
                """
            case .invalidPrimaryFile:
                return """
                The document couldn't be compiled because the primary file specified in the \
                '\(Request.pressroomHeaders.primaryFile.stringValue)' header could not be found in \
                the document container attached to the Request!
                """
            case .emptyDocumentContainerAttached:
                return """
                The document couldn't be compiled because an empty document container \
                was attached to the Request!
                """
            case .multiplePrimaryFileCandidatesFound:
                return """
                The document couldn't be compiled because there were multiple primary file candidates found \
                in the document container attached to the Request!
                """
            }
        }

        var possibleCauses: [String] {
            switch self {
            case .fileURLNilDueToUnderlyingError:
                return ["""
                        An underlying error occured due to some misconfiguration of the incoming Request or format \
                        of its attached document container.
                        """]
            case .invalidPrimaryFile:
                return ["""
                    The primary file specified in the '\(Request.pressroomHeaders.primaryFile.stringValue)' header \
                    contains a typo, or the wrong file name has been used.
                    """]
            case .emptyDocumentContainerAttached:
                return ["The document container attached to the Request appears to be empty."]
            case .multiplePrimaryFileCandidatesFound:
                return ["""
                        The internal heuristics used in determining a candidate primary file for the document \
                        have found multiple candidates.
                        """,
                        """
                        Candidates with different file formats may be present in the container (e.g. '.md' and '.tex').
                        """,
                        """
                        Multiple 'index' file candidates may be present in the container\n \
                        (e.g. files named: 'index.md|tex', 'article.md|tex' or 'manuscript.md|tex').
                        """,
                        """
                        Multiple '.md' file candidates may be present in the container.
                        """]
            }
        }

        var suggestedFixes: [String] {
            switch self {
            case .fileURLNilDueToUnderlyingError:
                return ["""
                        Check the underlying error condition that caused this file URL read error for more information.
                        """]
            case .invalidPrimaryFile:
                return ["""
                    Check that the file name in the '\(Request.pressroomHeaders.primaryFile.stringValue)' header \
                    is valid and exists in the document container attached to the Request.
                    """]
            case .emptyDocumentContainerAttached:
                return ["""
                        Check that the document is being compressed correctly and the resulting container is not empty.
                        """]
            case .multiplePrimaryFileCandidatesFound:
                return ["""
                        Check the document contents before compressing it into a container to attach to the Request.
                        """,
                        """
                    If the container contents appear to be valid, explictly specifying a primary file for the \
                    document with the '\(Request.pressroomHeaders.primaryFile.stringValue)' header should \
                    solve the error.
                    """]
            }
        }
    }
}
