//
//  PrimaryFileCandidate.swift
//  App
//
//  Created by Dan Browne on 20/09/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Vapor

// swiftlint:disable file_length

/// Represents the candidate for the "primary" file for a given document container, based on its decompressed contents.
final class PrimaryFileCandidate: ContentIteratable {

    /// The result of determining a "primary" file candidate.
    /// Consists of the file `URL` of the "primary" file, and its `SelectionCriterion` heuristic.
    typealias CandidateDeterminationResult = (fileURL: URL, criterion: SelectionCriterion)

    // MARK: Enum definitions

    enum SelectionCriterion {
        case primaryFileHeaderProvided(fileName: String)
        case singleFileContainer(fileName: String)
        case indexFile(fileName: String)
        case texCandidate(fileName: String)
        case markdownCandidate(fileName: String)
        case largestFileFallback(fileName: String)
        case fileNotInsideContainer(fileName: String)

        func headerValue() -> String {
            switch self {
            case .primaryFileHeaderProvided(fileName: let name):
                return "Primary-File-Header-Provided \(name)"
            case .singleFileContainer(fileName: let name):
                return "Single-File-Container \(name)"
            case .indexFile(fileName: let name):
                return "Index-File \(name)"
            case .texCandidate(fileName: let name):
                return "TeX-Candidate \(name)"
            case .markdownCandidate(fileName: let name):
                return "Markdown-Candidate \(name)"
            case .largestFileFallback(fileName: let name):
                return "Largest-File-Fallback \(name)"
            case .fileNotInsideContainer(fileName: let name):
                return "File-Not-Inside-Container \(name)"
            }
        }
    }

    // MARK: Public variables

    /// `HTTPHeaders` containing information on the selection criterion and any compilation errors
    /// that were generated whilst transforming the document associated with the receiver.
    /// These headers should be appended to a `Response` before returning to the Client.
    public var responseHeaders: HTTPHeaders = [:]

    /// A `SelectionCriterion` that has been heuristically determined.
    private(set) var selectionCriterion: SelectionCriterion? {
        // Set the 'Pressroom-Primary-File-Selection-Criterion' `Response` header,
        // if the criterion itself is set to a non-nil value
        didSet {
            if let selectionCriterion = selectionCriterion {
                responseHeaders.add(name: Response.pressroomHeaders.primaryFileSelectionCriterion,
                                    value: selectionCriterion.headerValue())
            }
        }
    }

    /// An optional `Error` that was thrown as a result of a transformation
    /// of the document associated with the receiver.
    public var compilationError: Error? {
        // Set the 'Pressroom-Compiled-With-Errors' `Response` header,
        // if the compilation error is set to a non-nil value
        didSet {
            if let compilationError = compilationError {
                responseHeaders.add(name: Response.pressroomHeaders.compiledWithErrors,
                                    value: String(describing: compilationError))
            }
        }
    }

    // MARK: Private variables

    private var fileURL: URL?
    private var documentContainerContents = [URL]()
    private let primaryFileName: String?

    /// The specific type of "primary" file candidate being represented.
    ///
    /// - texImportStatements: Candidate containing TeX `\import{`, `\include{` or `\input{` statements.
    /// - texFile: Candidate is a TeX file.
    /// - markdownFile: Candidate is a Markdown file.
    /// - otherFile: Candidate is in some other file format.
    private enum CandidateType {
        case texImportStatements
        case texFile
        case markdownFile
        case otherFile

        /// The `CandidateType` which corresponding to a given path extension.
        ///
        /// - Parameter pathExtension: The path extension
        ///   (e.g. 'tex' in the case of a file path '/some/directory/manuscript.tex')
        /// - Returns: A specific `CandidateType` for TeX or Markdown files, or `.otherFile` for all other cases.
        static func type(for pathExtension: String) -> CandidateType {
            switch pathExtension {
            case "tex", "latex":
                return .texFile
            case "md", "markdown":
                return .markdownFile
            default:
                return .otherFile
            }
        }
    }

    private var candidateIndexFileURLs = [URL]()
    private var fileURLsByCandidateType = [CandidateType: [URL]]()
    private let candidateIndexFileNames = ["index.tex",
                                           "index.md",
                                           "article.tex",
                                           "article.md",
                                           "manuscript.tex",
                                           "manuscript.md"]

    // MARK: Initialization

    /// Initialize a `PrimaryFileCandidate` based on a given document container's contents.
    ///
    /// - Parameters:
    ///   - decompressedDocumentContainerFileURL: The file URL for the decompressed document container on disk,
    ///     inside a temporary ingest working directory.
    ///   - primaryFileName: The value of the optional `Pressroom-Primary-File` `Request` header,
    ///     which explicitly declares the 'primary' file to use for importing.
    /// - Throws: An `Error` if the decompressed document container contents cannot be determined,
    ///         based on the provided `decompressedDocumentContainerFileURL`.
    init(for decompressedDocumentContainerFileURL: URL, primaryFileName: String?) throws {
        self.primaryFileName = primaryFileName
        self.documentContainerContents = decompressedContainerContents(for: decompressedDocumentContainerFileURL)

        // Iterate over every file URL in the document container
        for fileURL in documentContainerContents {
            // Check if the `fileURL.lastPathComponent` is one of the `candidateIndexFileNames`
            if candidateIndexFileNames.contains(fileURL.lastPathComponent) {
                candidateIndexFileURLs.append(fileURL)
                // Skip-over the remainder of loop iteration in this case
                continue
            }

            // TeX: Check for files found containing `\include{`, `\import{` or `\input{`
            // (these are good fallback candidates for the TeX use-case)
            let contents = try? String.init(contentsOf: fileURL)
            if contents?.contains("\\include{") == true
                || contents?.contains("\\import{") == true
                || contents?.contains("\\input{") == true {

                if fileURLsByCandidateType[.texImportStatements] == nil {
                    fileURLsByCandidateType[.texImportStatements] = []
                }
                fileURLsByCandidateType[.texImportStatements]?.append(fileURL)
            } else {
                // If the file doesn't contain TeX `\include{`, `\import{` or `\input{` statements,
                // store it against its file extension
                if fileURLsByCandidateType[.type(for: fileURL.pathExtension)] == nil {
                    fileURLsByCandidateType[.type(for: fileURL.pathExtension)] = []
                }
                fileURLsByCandidateType[.type(for: fileURL.pathExtension)]?.append(fileURL)
            }
        }

        let candidateDeterminationResult = try determineCandidate()

        // `defer` setting properties, so that any associated willSet/didSet behavior is triggered by `init(…)`
        // (Workaround for default behavior where willSet/didSet is not called on object initialization)
        // swiftlint:disable:next inert_defer
        defer {
            self.fileURL = candidateDeterminationResult.fileURL
            self.selectionCriterion = candidateDeterminationResult.criterion
        }
    }

    /// Initialize a `PrimaryFileCandidate` explicitly with a given file `URL`.
    ///
    /// - Parameter fileURL: The file `URL` to explicitly set.
    init(fileURL: URL) {
        self.fileURL = fileURL
        self.selectionCriterion = nil
        self.primaryFileName = nil
    }

    // MARK: Public methods

    /// Safely returns the file `URL` that is stored when the receiver is initialized successfully.
    ///
    /// - Returns: The file `URL` that is stored when the receiver is initialized successfully.
    /// - Throws: A `PressroomError` if the private instance of the file URL is `nil`
    ///         (i.e. an underlying error condition prevented it from being set during initialization of the receiver).
    public func ensuredFileURL() throws -> URL {
        guard let fileURL = fileURL else {
            throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.fileURLNilDueToUnderlyingError,
                                       sourceLocation: .capture())
        }

        return fileURL
    }

    // MARK: Candidate file URL determination

    /// Determines the most appropriate candidate "primary" file, based on specific inference criteria and heuristics.
    ///
    /// - Returns: A `CandidateDeterminationResult` if the attempt was successful.
    /// - Throws: An `Abort` error if an appropriate candidate cannot be determined.
    // swiftlint:disable:next function_body_length cyclomatic_complexity
    private func determineCandidate() throws -> CandidateDeterminationResult {

        // If a `Pressroom-Primary-File` header was attached to the `Request`, find the corresponding file URL
        if let primaryFileName = primaryFileName {
            for fileURL in documentContainerContents where fileURL.lastPathComponent == primaryFileName {
                let selectionCriterion = SelectionCriterion.primaryFileHeaderProvided(fileName: primaryFileName)
                return (fileURL: fileURL, criterion: selectionCriterion)
            }

            // (Throw an `Error` if the file was not found in the document container after iterating its contents)
            throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.invalidPrimaryFile,
                                       sourceLocation: .capture())
        }

        // Return early for the special cases of '0' and '1' file URL in `documentContainerContents`
        if documentContainerContents.count == 0 {
            // Throw an `Error` if the decompressed container is empty
            throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.emptyDocumentContainerAttached,
                                       sourceLocation: .capture())
        } else if documentContainerContents.count == 1 {
            // Return the only file URL if there is only a single file in the decompressed container
            let candidate = documentContainerContents.first!
            let selectionCriterion = SelectionCriterion.singleFileContainer(fileName: candidate.lastPathComponent)

            return (fileURL: candidate, criterion: selectionCriterion)
        }

        // If there are multiple candidates of different file extensions, throw an `Abort` error
        if (fileURLsByCandidateType[.markdownFile]?.count ?? 0) > 0
            && (fileURLsByCandidateType[.texFile]?.count ?? 0) > 0 {
            throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound,
                                       sourceLocation: .capture())
        }

        // If a single `index.tex` or `index.md` file URL exists, return it
        // Otherwise, if there are multiple candidates, throw an `Abort` error
        // (zero candidates isn't necessarily an error)
        if candidateIndexFileURLs.count == 1 {
            let candidate = candidateIndexFileURLs.first!
            let selectionCriterion = SelectionCriterion.indexFile(fileName: candidate.lastPathComponent)

            return (fileURL: candidate, criterion: selectionCriterion)
        } else if candidateIndexFileURLs.count > 1 {
            throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound,
                                       sourceLocation: .capture())
        }

        // If a single TeX candidate (based on import statements) exists, return it
        // Otherwise, if there are multiple candidates (based on import statements),
        // return the one with the most `\include{`, `\import{` and `\input{` statements
        if let texImportStatementsCandidates = fileURLsByCandidateType[.texImportStatements] {
            if texImportStatementsCandidates.count == 1 {
                let candidate = texImportStatementsCandidates.first!
                let selectionCriterion = SelectionCriterion.texCandidate(fileName: candidate.lastPathComponent)

                return (fileURL: candidate, criterion: selectionCriterion)
            } else if texImportStatementsCandidates.count > 1 {
                // Calculate the number of `\include{`, `\import{` and `\input{` statements for each file URL
                var occurencesByFileURL = [URL: Int]()
                for fileURL in texImportStatementsCandidates {
                    let contents = try? String.init(contentsOf: fileURL)
                    let includeCount = (contents?.count(of: "\\include{") ?? 0)
                    let importCount = (contents?.count(of: "\\import{") ?? 0)
                    let inputCount = (contents?.count(of: "\\input{") ?? 0)
                    occurencesByFileURL[fileURL] = includeCount + importCount + inputCount
                }

                // Return the file URL with the most `\include{` and `\import{` statements
                let candidate = occurencesByFileURL.sorted(by: { $0.value > $1.value }).first!.key
                let selectionCriterion = SelectionCriterion.texCandidate(fileName: candidate.lastPathComponent)

                return (fileURL: candidate, criterion: selectionCriterion)
            }
        }

        // If a single fallback TeX candidate exists, return it
        // Otherwise, if there are multiple candidates, throw an `Abort` error
        // (zero candidates isn't necessarily an error)
        if let texCandidates = fileURLsByCandidateType[.texFile] {
            if texCandidates.count == 1 {
                let candidate = texCandidates.first!
                let selectionCriterion = SelectionCriterion.texCandidate(fileName: candidate.lastPathComponent)

                return (fileURL: candidate, criterion: selectionCriterion)
            } else if texCandidates.count > 1 {
                throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound,
                                           sourceLocation: .capture())
            }
        }

        // If a single fallback Markdown candidate exists, return it
        // Otherwise, if there are multiple candidates, throw an `Abort` error
        // (zero candidates isn't necessarily an error)
        if let markdownCandidates = fileURLsByCandidateType[.markdownFile] {
            if markdownCandidates.count == 1 {
                let candidate = markdownCandidates.first!
                let selectionCriterion = SelectionCriterion.markdownCandidate(fileName: candidate.lastPathComponent)

                return (fileURL: candidate, criterion: selectionCriterion)
            } else if markdownCandidates.count > 1 {
                throw PressroomError.error(for: PrimaryFileCandidateDebuggingInfo.multiplePrimaryFileCandidatesFound,
                                           sourceLocation: .capture())
            }
        }

        // If no valid candidate has already been returned, or any errors already thrown,
        // a Hail Mary fallback here of returning the largest file
        let sortedBySize = try documentContainerContents.sorted(by: {
            let size1 = try $0.resourceValues(forKeys: [.totalFileAllocatedSizeKey]).totalFileAllocatedSize ?? 0
            let size2 = try $1.resourceValues(forKeys: [.totalFileAllocatedSizeKey]).totalFileAllocatedSize ?? 0
            return size1 > size2
        })

        let candidate = sortedBySize.first!
        let selectionCriterion = SelectionCriterion.largestFileFallback(fileName: candidate.lastPathComponent)

        return (fileURL: candidate, criterion: selectionCriterion)
    }
}

// MARK: - `PDFTransformable` checks

extension PrimaryFileCandidate {

    /// An enum describing the result of checking that a given `PrimaryFileCandidate` is suitable for compiling
    /// to a PDF using a `PDFTransformable` service.
    ///
    /// - alreadyPDF: The `PrimaryFileCandidate` represents a document that already appears to be a PDF.
    /// - invalidCandidate: The `PrimaryFileCandidate` represents a document that cannot be compiled to a PDF
    ///                     using a `PDFTransformable` service.
    /// - validOfficeCandidate: The `PrimaryFileCandidate` represents an Office style document
    ///                         (i.e. '.docx', '.doc' or '.rtf') that can be compiled to a PDF using
    ///                         a `PDFTransformable` service.
    /// - validTeXCandidate: The `PrimaryFileCandidate` represents an Office style document that can be
    ///                      compiled to a PDF using a `PDFTransformable` service.
    enum PDFTransformSuitability {
        case alreadyPDF
        case invalidCandidate
        case validOfficeCandidate
        case validTeXCandidate
    }

    /// Determines if the receiver is suitable for a document transformation using a `PDFTransformable` service.
    ///
    /// - Parameter request: An optional associated `Request` to test in combination with the receiver.
    /// - Returns: `true` if there is suitability, and `false` otherwise.
    func pdfTransformSuitability(of request: Request? = nil) -> PDFTransformSuitability {
        guard let candidateFileURL = try? ensuredFileURL() else {
                return .invalidCandidate
        }
        let targetFileExtension = request?.http.headers.firstValue(name: Request.pressroomHeaders.targetFileExtension)

        // If `request` is not `nil`, also check `targetFileExtension` is "pdf".
        // Otherwise, the check is restricted to the candidate's `pathExtension`.
        if (candidateFileURL.pathExtension == "docx"
            || candidateFileURL.pathExtension == "doc"
            || candidateFileURL.pathExtension == "rtf")
            && (request != nil ? targetFileExtension == "pdf" : true) {
            return .validOfficeCandidate
        } else if (candidateFileURL.pathExtension == "tex")
            && (request != nil ? targetFileExtension == "pdf" : true) {
            return .validTeXCandidate
        } else if candidateFileURL.pathExtension == "pdf" {
            return .alreadyPDF
        } else {
            return .invalidCandidate
        }
    }
}

// MARK: - `SelectionCriterion` Equatable conformance

// (Used in PrimaryFileCandidateTests.swift)
extension PrimaryFileCandidate.SelectionCriterion: Equatable {
    static func == (lhs: PrimaryFileCandidate.SelectionCriterion,
                    rhs: PrimaryFileCandidate.SelectionCriterion) -> Bool {
        switch (lhs, rhs) {
        case (.primaryFileHeaderProvided(fileName: let name1), .primaryFileHeaderProvided(fileName: let name2)):
            return name1 == name2
        case (.singleFileContainer(fileName: let name1), .singleFileContainer(fileName: let name2)):
            return name1 == name2
        case (.indexFile(fileName: let name1), .indexFile(fileName: let name2)):
            return name1 == name2
        case (.texCandidate(fileName: let name1), .texCandidate(fileName: let name2)):
            return name1 == name2
        case (.markdownCandidate(fileName: let name1), .markdownCandidate(fileName: let name2)):
            return name1 == name2
        case (.largestFileFallback(fileName: let name1), .largestFileFallback(fileName: let name2)):
            return name1 == name2
        case (.fileNotInsideContainer(fileName: let name1), .fileNotInsideContainer(fileName: let name2)):
            return name1 == name2
        default:
            return false
        }
    }
}
