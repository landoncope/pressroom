//
//  ArbitrarilyFormattedDocument.swift
//  App
//
//  Created by Dan Browne on 04/06/2018.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Vapor

/// Represents document content (of any given format representable directly in UTF8-encoding,
/// or in a compressed container)
/// that is received in a `Request`, where it has been attached as multi-part form data.
struct ArbitrarilyFormattedDocument: Content {
    /// The document content, parsed into a `File` object containing the underlying `Data`,
    /// along with other file metadata.
    var file: File
}
