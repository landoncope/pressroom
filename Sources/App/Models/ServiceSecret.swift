//
//  ServiceSecret.swift
//  App
//
//  Created by Dan Browne on 06/11/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation
import Core

/// A base 64-encoded 'secret' `String`, which when decoded consists of one or more parts that are separated by a designated character.
struct ServiceSecret {

    // MARK: - Enum definitions

    /// An enum describing how many colon-separated substrings the decoded secret should contain.
    enum SecretType: Int {
        case onePart = 1
        case twoParts
        case threeParts
    }

    /// An enum describing the substring of the decoded secret when split on its separators.
    enum Credential: Int {
        case username = 0
        case password
        case key
    }

    /// An enum describing the possible error conditions when decoding the secret.
    enum DecodingError: LocalizedError {
        case notBase64Encoded
        case invalidFormat
        case notStringRepresentable
        case credentialNotPresent

        var errorDescription: String? {
            switch self {
            case .notBase64Encoded:
                return "The provided secret string is not base 64 encoded."
            case .invalidFormat:
                return """
                The decoded secret appears to be in an invalid format. \
                It should contain the specified number of substrings, separated by ':' characters \
                (or the custom separator specified).
                """
            case .notStringRepresentable:
                return """
                The decoded secret credential cannot be represented as a UTF-8 encoded string.
                """
            case .credentialNotPresent:
                return """
                The specified credential was not present within the original secret string.
                """
            }
        }
    }

    // MARK: - Private variables

    private let decodedComponents: [String]
    private let secretType: SecretType
    private let separator: UInt8

    // MARK: - Initialization

    /// Default initializer.
    /// - Parameters:
    ///   - encoded: The base 64 encoded secret `String`.
    ///   - secretType: The expected `SecretType` of the String.
    ///   - separator: The expected separator character (defaults to `.colon` if not explicitly set).
    init(encoded: String, secretType: SecretType, separator: UInt8 = .colon) throws {
        guard let decodedSecretData = Data(base64Encoded: encoded) else {
            throw DecodingError.notBase64Encoded
        }

        let decodedParts = try decodedSecretData.split(separator: separator).map { secretPart -> String in
            guard let stringRepresentation = String(data: secretPart, encoding: .utf8) else {
                throw DecodingError.notStringRepresentable
            }
            return stringRepresentation
        }

        guard decodedParts.count == secretType.rawValue else {
            throw DecodingError.invalidFormat
        }

        self.decodedComponents = decodedParts
        self.secretType = secretType
        self.separator = separator
    }

    // MARK: - Public methods

    /// Decode a `Credential` from the receiver, and return it as a `String`.
    /// - Parameter credential: The `Credential` to decode from the secret string.
    /// - Returns: The decoded `String` corresponding to the requested credential.
    func decode(_ credential: Credential) throws -> String {
        guard credential.rawValue < decodedComponents.count else {
            throw DecodingError.credentialNotPresent
        }

        return decodedComponents[credential.rawValue]
    }
}
