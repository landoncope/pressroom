//
//  DocumentProcessingLevel.swift
//  App
//
//  Created by Dan Browne on 30/10/2019.
//
//  ---------------------------------------------------------------------------
//
//  © 2019 Atypon Systems LLC
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Foundation

/// An enum describing the possible document processing levels for a given transformation operation.
///
/// - fullText: The full text of the document will be present in the transformed document.
/// - frontMatterOnly: Only front-matter content will be present in the transformed document.
enum DocumentProcessingLevel: String {
    case fullText = "full_text"
    case frontMatterOnly = "front_matter_only"
}
