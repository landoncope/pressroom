// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "PressroomServer",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.3.0"),

        // JWT
        .package(url: "https://github.com/vapor/jwt.git", from: "3.0.0"),

        // OpenAPI 3.0
        .package(url: "https://github.com/mczachurski/Swiftgger.git", from: "1.3.1"),

        // SwiftyBeaver
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver.git", from: "1.7.0"),
        .package(url: "https://github.com/SwiftyBeaver/SwiftyBeaver-Vapor.git", from: "1.1.0")
    ],
    targets: [
        .target(name: "App", dependencies: ["Vapor", "JWT", "Swiftgger", "SwiftyBeaver", "SwiftyBeaverVapor"]),
        .target(name: "PressroomServer", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)
