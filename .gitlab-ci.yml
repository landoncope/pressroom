stages:
  - fetch
  - build
  - test
  - archive
  - deploy

cache:
  key: ${CI_BUILD_REF_SLUG}
  paths:
    - Carthage/Build/

default:
  before_script:
    - . Scripts/export_env_vars.sh

fetch_pressroom:
  stage: fetch
  script:
    - set -e
    - ssh-keyscan github.com | sort -u - ~/.ssh/known_hosts -o ~/.ssh/known_hosts
    - git fetch --tags
    - git submodule sync --recursive
    - git submodule foreach git clean -f -d
    - git submodule foreach git checkout .
    - git checkout .
    - git submodule update --init --recursive
    - mkdir -p Carthage/Build/Mac # in preparation for the subsequent steps
    # Carthage build (inc. workaround via xcconfig for Xcode 11 AFNetworking builds)
    - XCODE_XCCONFIG_FILE=$(pwd)/xcconfigs/AFNetworking_Xcode_11_workaround.xcconfig carthage build --cache-builds --platform mac
    # Workaround for Carthage not inserting 'Build' directory symlinks (i.e. to `pressroom/Carthage/Build`)
    # into `pressroom/Carthage/Checkouts/Manuscripts/Carthage/Checkouts/*/Carthage/`
    - ./Scripts/carthage_build_directory_symlinks.sh
  artifacts:
    paths:
      - Carthage/Build/
    expire_in: 1 week

  tags:
    - xcode-11
    - macos-10.14
    - vapor

build_pressroom:
  stage: build
  script:
    - set -e
    # Ensure 'swiftlint' linting utility is installed via Homebrew
    - brew reinstall swiftlint
    # Workaround for an xcpretty bug that sporadically fails builds (https://github.com/supermarin/xcpretty/issues/190)
    - export LC_ALL=en_US.UTF-8
    # Workaround 'xcodebuild clean' failing with Xcode 10.x+ New Build System (same approach as Mac app)
    - rm -rf ~/.PressroomServerBuilds/DerivedData
    - xcodebuild -project PressroomServer.xcodeproj -scheme PressroomServer -derivedDataPath ~/.PressroomServerBuilds/DerivedData build | xcpretty
  tags:
    - xcode-11
    - macos-10.14
    - vapor

test_pressroom:
  stage: test
  script:
    - set -e
    # Workaround for an xcpretty bug that sporadically fails builds (https://github.com/supermarin/xcpretty/issues/190)
    - export LC_ALL=en_US.UTF-8
    # Workaround 'xcodebuild clean' failing with Xcode 10.x+ New Build System (same approach as Mac app)
    - rm -rf ~/.PressroomServerBuilds/DerivedData
    - xcodebuild -project PressroomServer.xcodeproj -scheme PressroomServer-Tests -derivedDataPath ~/.PressroomServerBuilds/DerivedData test | xcpretty
  tags:
    - xcode-11
    - macos-10.14
    - vapor

archive_pressroom_staging:
  stage: archive
  script:
    - set -e
    # Workaround for an xcpretty bug that sporadically fails builds (https://github.com/supermarin/xcpretty/issues/190)
    - export LC_ALL=en_US.UTF-8
    # Workaround 'xcodebuild clean' failing with Xcode 10.x+ New Build System (same approach as Mac app)
    - rm -rf ~/.PressroomServerBuilds/DerivedData
    # Ensure that the hidden archives directory exists
    - mkdir -p ~/.PressroomServerArchives
    # We need to bootstrap the Manuscripts Carthage dependency first before archiving
    - ./Carthage/Checkouts/Manuscripts/bootstrap
    # Ensure Homebrew dependencies are installed
    - ./Scripts/install_homebrew_dependencies.sh
    # Ensure Node dependencies are installed
    - ./Scripts/install_node_dependencies.sh
    # Archive PressroomServer...
    - xcodebuild -project PressroomServer.xcodeproj -scheme PressroomServer -derivedDataPath ~/.PressroomServerBuilds/DerivedData archive -archivePath ~/.PressroomServerArchives/PressroomServer-$CI_COMMIT_TAG_OR_SHORT_SHA.xcarchive | xcpretty
  only:
    - schedules
  tags:
    - xcode-11
    - macos-10.14
    - vapor
    - pressroom-staging

archive_pressroom_production:
  extends: archive_pressroom_staging
  stage: archive
  only:
    # git refs which are prefixed with a proper semantic version string, such as 1.0.0 or 1.0.0-rc.1+build.1
    # (Using recommended regex from https://github.com/semver/semver)
    # (Ideally want to do this, plus '- master' and '- tags' to be stricter, but 'only:' logic is OR'd not AND'd...)
    - /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/
  tags:
    - xcode-11
    - macos-10.14
    - vapor
    - pressroom-production


deploy_pressroom_staging:
  stage: deploy
  script:
    - set -e
    # Generates a launch daemon plist for booting PressroomServer (and restarting it if it falls over), and then loads it
    - MPPressroomServerJWTSecret=$APP_JWT_SECRET MPPressroomServerAPIKey=$APP_API_KEY MPPressroomServerSwiftyBeaverAppID=$SB_LOGGING_APP_ID MPPressroomServerSwiftyBeaverAppSecret=$SB_LOGGING_APP_SECRET MPPressroomServerSwiftyBeaverEncryptionKey=$SB_LOGGING_ENCRYPTION_KEY MPPressroomServerIAMTokenIntrospectionSecret=$APP_IAM_TOKEN_INTROSPECTION_SECRET MPPressroomServerEdifixSecret=$APP_EDIFIX_SECRET MPPressroomServerExtylesArcSecret=$APP_EXTYLES_ARC_SECRET MPPressroomServerLiteratumSFTPUsername=$APP_LITERATUM_SFTP_USERNAME MPPressroomServerLiteratumSFTPHostName=$APP_LITERATUM_SFTP_HOST_NAME MPPressroomServerLiteratumSFTPIdentityFilePath=$APP_LITERATUM_SFTP_IDENTITY_FILE_PATH MPPressroomServerLiteratumSFTPRemotePathPrefix=$APP_LITERATUM_SFTP_REMOTE_PATH_PREFIX MPPressroomServerLiteratumFTPSUsername=$APP_LITERATUM_FTPS_USERNAME MPPressroomServerLiteratumFTPSPassword=$APP_LITERATUM_FTPS_PASSWORD MPPressroomServerLiteratumFTPSHostName=$APP_LITERATUM_FTPS_HOST_NAME MPPressroomServerLiteratumFTPSRemotePathPrefix=$APP_LITERATUM_FTPS_REMOTE_PATH_PREFIX MPPressroomServerEEODepositServiceSecret=$APP_EEO_DEPOSIT_SERVICE_SECRET ./Scripts/deploy_pressroom.sh
  only:
    - schedules
  tags:
    - xcode-11
    - macos-10.14
    - vapor
    - pressroom-staging

deploy_pressroom_production:
  extends: deploy_pressroom_staging
  stage: deploy
  only:
    # git refs which are prefixed with a proper semantic version string, such as 1.0.0 or 1.0.0-rc.1+build.1
    # (Using recommended regex from https://github.com/semver/semver)
    # (Ideally want to do this, plus '- master' and '- tags' to be stricter, but 'only:' logic is OR'd not AND'd...)
    - /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/
  tags:
    - xcode-11
    - macos-10.14
    - vapor
    - pressroom-production
